import readSTL
import pyvista as pv
import numpy as np
from pathlib import Path
import pickle

if __name__ == "__main__":

  FABResFileCheck = Path("/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/no_frac.out")
  print("FABResFileCheck.is_file()=",FABResFileCheck.is_file())
  if FABResFileCheck.is_file():
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_XYZ.out', 'rb') as file:
      frac_XYZ = pickle.load(file)        
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/FracNXYZd.out', 'rb') as file:
      FracNXYZd = pickle.load(file)        
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_sege.out', 'rb') as file:
      frac_sege = pickle.load(file)        
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_segeID.out', 'rb') as file:
      frac_segeID = pickle.load(file)        
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/fracID.out', 'rb') as file:
      fracID = pickle.load(file)        
    with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_ele_ind.out', 'rb') as file:
      frac_ele_ind = pickle.load(file) 

  points=np.array(frac_XYZ,dtype=np.float64)
  cells=[]
  for i in range(0,len(frac_ele_ind)):
    cells.append(np.int32([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]]))
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  frac = pv.PolyData(points,cells)
  frac['RegionId']=fracID
  noConnfrac=len(np.unique(frac['RegionId']))
  frac['z']=frac.points[:,2:3]
  frac.save('dfnFAB.vtk')
  # print("frac",frac)
  # quit()

  case=1
  if (case==1):
    mtFile=r'/data/INER_DATA/SNFD2021Reposiroty/MT_o.stl'
    mtSTL=pv.read(mtFile)
    mt = mtSTL.connectivity(largest=False)
    noConnDH=len(np.unique(mt['RegionId']))
    mt['z']=mt.points[:,2:3]
    mt.save('mt.vtk')        
    print("mt",mt)
    print("noConnDH=",noConnDH)
  quit()
    
  case=1
  if (case==1):
    '''
    # Q1
    '''
    dhFile=r'/data/INER_DATA/SNFD2021Reposiroty/DH_o.stl'
    dhSTL=pv.read(dhFile)
    dh = dhSTL.connectivity(largest=False)
    strname1=r'dh'
    strname2=r'frac'
    noConnDH=len(np.unique(dh['RegionId']))
    dh['z']=dh.points[:,2:3]
    dh.save('dh.vtk')
    readSTL.comm(strname1,strname2,dh,frac)     
    dhfrac = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q1vtk/dh_frac.bin', dtype=np.int32).reshape(noConnDH, noConnfrac)


  dh=pv.read('dh.vtk')
  noConnDH=len(np.unique(dh['RegionId']))

  case=0
  if (case==1):
    '''
    # Q2
    '''
    edzFile=r'/data/INER_DATA/SNFD2021Reposiroty/EDZ_vol_o.stl'
    edzSTL=pv.read(edzFile)
    edz = edzSTL.connectivity(largest=False)

    
    noConnedz=len(np.unique(edz['RegionId']))
    print("noConnDH,noConnedz=",noConnDH,noConnedz)
    print(noConnDH*noConnedz)
    # holeInd=np.zeros((noConnDH,noConnFRAC),dtype=np.int32)

    strname1=r'dh'
    strname2=r'edz'
    # readSTL.comm(strname1,strname2,dh,edz)
    dhedz = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q2vtk/dh_edz.bin', dtype=np.int32).reshape(noConnDH, noConnedz)
    edz['z']=edz.points[:,2:3]
    edz.save('edz.vtk')


  '''
  # Q3
  '''
  dtFile=r'/data/INER_DATA/SNFD2021Reposiroty/DT_o.stl'
  dtSTL=pv.read(dtFile)
  dt = dtSTL.connectivity(largest=False)
  noConnDt=len(np.unique(dt['RegionId']))    
  strname1=r'dt'
  strname2=r'frac'
  readSTL.comm(strname1,strname2,dt,frac) 
  dt['z']=dt.points[:,2:3]
  dt.save('dt.vtk')
  # dtfrac = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q3vtk/dt_frac.bin', dtype=np.int32).reshape(noConnDt, noConnfrac)
  # print(dtfrac)
  # print(dtfrac.shape)



  print("=====")
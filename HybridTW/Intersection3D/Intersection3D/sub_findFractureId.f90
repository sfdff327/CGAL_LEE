module FindFracture
implicit none    
contains    

    
subroutine reMeshGenerator(&
    & EPSILON,SQEPS,&
    & NB_i,NB_j,UnionFile,IntersectionFile,&
    & no_node,no_ele,XYZ,ele_ind,fracID,FracNXYZd,intesection_nodeNB,&
    & no_A_node,no_A_ele,no_B_node,no_B_ele,&
    & Node_NB_A_trans,ele_NB_A_trans,Node_NB_B_trans,ele_NB_B_trans)

implicit none 
real*8 EPSILON,SQEPS
integer*4 i,j,k,ID,ii,jj,kk,QQ,WW,EE
integer*4 NB_i,NB_j
character(256) UnionFile,IntersectionFile
integer*4 no_node,no_ele
integer*4, allocatable:: intesection_nodeNB(:)
integer*4, allocatable:: ele_ind(:,:),fracID(:)
real*8, allocatable:: XYZ(:,:),FracNXYZd(:,:)


integer*4 new_no_node,new_no_ele
integer*4, allocatable:: new_intesection_nodeNB(:)
integer*4, allocatable:: new_ele_ind(:,:),new_fracID(:)
real*8, allocatable:: new_XYZ(:,:)

integer*4 no_A_node,no_A_ele,no_B_node,no_B_ele
integer*4, allocatable:: Node_NB_A_trans(:),Node_NB_B_trans(:)
integer*4, allocatable:: ele_NB_A_trans(:),ele_NB_B_trans(:)
integer*4 cal_A_ele,cal_B_ele

integer*4 no_union_node,no_union_ele
real*8, allocatable:: union_xyz(:,:)
integer*4, allocatable:: ele_union_ind(:,:)
integer*4, allocatable:: node_inPlaneAB(:),node_inAB_ind(:,:)
integer*4, allocatable:: ele_inPlaneAB(:),ele_checkFace(:)

integer*4 no_Intersection_node,no_Intersection_ele
real*8, allocatable:: Intersection_xyz(:,:)
integer*4, allocatable:: ele_Intersection_ind(:,:),same_node(:)
integer*4, allocatable:: node_Intersection_inPlaneAB(:),node_Intersection_inAB_ind(:,:)
integer*4, allocatable:: ele_Intersection_inPlaneAB(:),ele_Intersection_checkFace(:)

integer*4 old_union_node,old_union_ele

integer*4 inInd,addNode,addEle,addEle_I
real*8 Tdis

open(30,file=UnionFile)
read(30,*)
read(30,*) no_union_node,no_union_ele
close(30)
open(30,file=IntersectionFile)
read(30,*)
read(30,*) no_Intersection_node,no_Intersection_ele
close(30)

no_union_node=no_union_node+no_Intersection_node
no_union_ele=no_union_ele+no_Intersection_ele

if (allocated(union_xyz)) deallocate(union_xyz)
if (allocated(ele_union_ind)) deallocate(ele_union_ind)
if (allocated(node_inPlaneAB)) deallocate(node_inPlaneAB)
if (allocated(ele_inPlaneAB)) deallocate(ele_inPlaneAB)
if (allocated(ele_checkFace)) deallocate(ele_checkFace)
if (allocated(node_inAB_ind)) deallocate(node_inAB_ind)
allocate(union_xyz(3,no_union_node),ele_union_ind(3,no_union_ele))
allocate(node_inPlaneAB(no_union_node),node_inAB_ind(2,no_union_node))
allocate(ele_inPlaneAB(no_union_ele))
allocate(ele_checkFace(no_union_ele))

node_inPlaneAB=0
node_inAB_ind=0
ele_inPlaneAB=0
ele_checkFace=0

open(30,file=UnionFile)
read(30,*)
read(30,*) no_union_node,no_union_ele
old_union_node=no_union_node
old_union_ele=no_union_ele
do i=1,no_union_node
    read(30,*) (union_xyz(j,i),j=1,3)
end do
do i=1,no_union_ele
    read(30,*) k,(ele_union_ind(j,i),j=1,3)
    do j=1,3
        ele_union_ind(j,i)=ele_union_ind(j,i)+1 
    end do
end do
close(30,STATUS='DELETE')

open(30,file=IntersectionFile)
read(30,*)
read(30,*) no_Intersection_node,no_Intersection_ele
if (allocated(Intersection_xyz)) deallocate(Intersection_xyz)
if (allocated(ele_Intersection_ind)) deallocate(ele_Intersection_ind)
if (allocated(same_node)) deallocate(node_inAB_ind)
allocate(Intersection_xyz(3,no_Intersection_node))
allocate(ele_Intersection_ind(3,no_Intersection_ele))
allocate(same_node(no_Intersection_node))
same_node=0
do i=1,no_Intersection_node
    read(30,*) (Intersection_xyz(j,i),j=1,3)
    do j=1,no_union_node
        call dis3d(&
            & union_xyz(1,j),union_xyz(2,j),union_xyz(3,j),&
            & Intersection_xyz(1,i),Intersection_xyz(2,i),Intersection_xyz(3,i),&
            & Tdis)
        if (Tdis<=SQEPS) then
            same_node(i)=j
        end if
    end do
end do
k=0
do i=1,no_Intersection_node
    if (same_node(i)==0) then
        k=k+1
        same_node(i)=no_union_node+k
        union_xyz(:,no_union_node+k)=Intersection_xyz(:,i)
    end if
end do
no_union_node=no_union_node+k
do i=1,no_Intersection_ele
    read(30,*) k,(ele_Intersection_ind(j,i),j=1,3)
    do j=1,3
        ele_Intersection_ind(j,i)=ele_Intersection_ind(j,i)+1 
        !ele_Intersection_ind(j,i)=same_node(ele_Intersection_ind(j,i))
        ele_union_ind(j,no_union_ele+i)=same_node(ele_Intersection_ind(j,i))
    end do
end do
no_union_ele=no_union_ele+no_Intersection_ele

do i=1,no_union_node
    !read(30,*) (union_xyz(j,i),j=1,3)
    do ii=1,no_A_node
        call dis3d(&
            & union_xyz(1,i),union_xyz(2,i),union_xyz(3,i),&
            & XYZ(1,Node_NB_A_trans(ii)),XYZ(2,Node_NB_A_trans(ii)),XYZ(3,Node_NB_A_trans(ii)),&
            & Tdis)
        if (Tdis<=SQEPS) then
            node_inPlaneAB(i)=Node_NB_A_trans(ii)
            node_inAB_ind(1,i)=1
        end if
    end do 
    do ii=1,no_B_node
        call dis3d(&
            & union_xyz(1,i),union_xyz(2,i),union_xyz(3,i),&
            & XYZ(1,Node_NB_B_trans(ii)),XYZ(2,Node_NB_B_trans(ii)),XYZ(3,Node_NB_B_trans(ii)),&
            & Tdis)
        if (Tdis<=SQEPS) then
            node_inPlaneAB(i)=Node_NB_B_trans(ii)
            node_inAB_ind(2,i)=1
        end if
    end do
110 ii=0
end do

addNode=0
do i=1,no_union_node
    if (node_inPlaneAB(i)==0) then
        call checkPointInPlane(&
            & union_xyz(1,i),union_xyz(2,i),union_xyz(3,i),FracNXYZd,NB_i,EPSILON,SQEPS,node_inAB_ind(1,i))
        call checkPointInPlane(&
            & union_xyz(1,i),union_xyz(2,i),union_xyz(3,i),FracNXYZd,NB_j,EPSILON,SQEPS,node_inAB_ind(2,i))
        if (node_inAB_ind(1,i)/=0 .or. node_inAB_ind(2,i)/=0) then
            addNode=addNode+1
            node_inPlaneAB(i)=no_node+addNode
        end if
    end if
end do

addEle=0
do i=1,no_union_ele
    !read(30,*) k,(ele_union_ind(j,i),j=1,3)
    QQ=0
    WW=0
    EE=0
    do j=1,3
        !ele_union_ind(j,i)=ele_union_ind(j,i)+1        
        if (node_inPlaneAB(ele_union_ind(j,i))/=0) QQ=QQ+1
        WW=WW+node_inAB_ind(1,ele_union_ind(j,i))
        EE=EE+node_inAB_ind(2,ele_union_ind(j,i))
    end do
    if (WW==3 .or. EE==3) then
        addEle=addEle+1
        if (WW==3) then
            ele_checkFace(addEle)=1
        else if (EE==3) then
            ele_checkFace(addEle)=2
        end if
        ele_inPlaneAB(addEle)=i
    end if
    
end do
close(30,STATUS='DELETE')

new_no_node=no_node+addNode
new_no_ele=no_ele+addEle

if (allocated(new_intesection_nodeNB)) deallocate(new_intesection_nodeNB)
if (allocated(new_ele_ind)) deallocate(new_ele_ind)
if (allocated(new_fracID)) deallocate(new_fracID)
if (allocated(new_XYZ)) deallocate(new_XYZ)
allocate(new_intesection_nodeNB(new_no_node))
allocate(new_ele_ind(3,new_no_ele))
allocate(new_fracID(new_no_ele))
allocate(new_XYZ(3,new_no_node))

new_intesection_nodeNB(1:no_node)=intesection_nodeNB(1:no_node)
new_XYZ(:,1:no_node)=XYZ(:,1:no_node)
new_ele_ind(:,1:no_ele)=ele_ind(:,1:no_ele)
new_fracID(1:no_ele)=fracID(1:no_ele)

do i=1,no_union_node
    if (node_inPlaneAB(i)>no_node) then
        new_XYZ(:,node_inPlaneAB(i))=union_xyz(:,i)
    end if
    if (node_inAB_ind(1,i)==1 .and. node_inAB_ind(2,i)==1) then
        new_intesection_nodeNB(node_inPlaneAB(i))=1
    end if
end do

cal_A_ele=0
cal_B_ele=0
new_no_ele=no_ele
do i=1,addEle
    if (ele_inPlaneAB(i)/=0) then
        if (ele_checkFace(i)==1) then
            cal_A_ele=cal_A_ele+1
            if (cal_A_ele<=no_A_ele) then
                do j=1,3
                    new_ele_ind(j,ele_NB_A_trans(cal_A_ele))=&
                        & node_inPlaneAB(ele_union_ind(j,ele_inPlaneAB(i)))
                end do
                j=0
            else 
                new_no_ele=new_no_ele+1
                do j=1,3
                    new_ele_ind(j,new_no_ele)=&
                        & node_inPlaneAB(ele_union_ind(j,ele_inPlaneAB(i)))
                end do
                if (ele_checkFace(i)==1) then
                    new_fracID(new_no_ele)=NB_i
                else if (ele_checkFace(i)==2) then
                    new_fracID(new_no_ele)=NB_j
                end if
                j=0
            end if
        else if (ele_checkFace(i)==2) then
            cal_B_ele=cal_B_ele+1
            if (cal_B_ele<=no_B_ele) then
                do j=1,3
                    new_ele_ind(j,ele_NB_B_trans(cal_B_ele))=&
                        & node_inPlaneAB(ele_union_ind(j,ele_inPlaneAB(i)))
                end do
                j=0
            else 
                new_no_ele=new_no_ele+1
                do j=1,3
                    new_ele_ind(j,new_no_ele)=&
                        & node_inPlaneAB(ele_union_ind(j,ele_inPlaneAB(i)))
                end do
                if (ele_checkFace(i)==1) then
                    new_fracID(new_no_ele)=NB_i
                else if (ele_checkFace(i)==2) then
                    new_fracID(new_no_ele)=NB_j
                end if
                
                j=0
            end if
        end if
    end if
end do

OPEN(30,file='ID_U.dat')
write(30,"(A17)") 'Title="XY2D_plot"'
WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
WRITE(30,*) "ZONE N=",new_no_node,",E=",new_no_ele,", F=FEpoint, ET=triangle"
do ii=1,new_no_node
    write(30,"(3(G0,1x))") (new_XYZ(jj,ii),jj=1,3)
end do
do ii=1,new_no_ele
    write(30,"(3(I,1x))") (new_ele_ind(jj,ii),jj=1,3)
end do
!close(30)




!OPEN(30,file='ID_U.dat')
write(30,"(A17)") 'Title="XY2D_plot"'
WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
WRITE(30,*) "ZONE N=",old_union_node,",E=",old_union_ele,", F=FEpoint, ET=triangle"
do ii=1,old_union_node
    write(30,"(3(G0,1x))") (union_xyz(jj,ii),jj=1,3)
end do
do ii=1,old_union_ele
    write(30,"(3(I,1x))") (ele_union_ind(jj,ii),jj=1,3)
end do
write(30,"(A17)") 'Title="XY2D_plot"'
WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
WRITE(30,*) "ZONE N=",no_Intersection_node,",E=",no_Intersection_ele,", F=FEpoint, ET=triangle"
do ii=1,no_Intersection_node
    write(30,"(3(G0,1x))") (Intersection_xyz(jj,ii),jj=1,3)
end do
do ii=1,no_Intersection_ele
    write(30,"(3(I,1x))") (ele_Intersection_ind(jj,ii),jj=1,3)
end do
close(30)

if (allocated(intesection_nodeNB)) deallocate(intesection_nodeNB)
if (allocated(ele_ind)) deallocate(ele_ind)
if (allocated(fracID)) deallocate(fracID)
if (allocated(XYZ)) deallocate(XYZ)
allocate(intesection_nodeNB(new_no_node))
allocate(ele_ind(3,new_no_ele))
allocate(fracID(new_no_ele))
allocate(XYZ(3,new_no_node))

intesection_nodeNB(1:new_no_node)=new_intesection_nodeNB(1:new_no_node)
XYZ(:,1:new_no_node)=new_XYZ(:,1:new_no_node)
ele_ind(:,1:new_no_ele)=new_ele_ind(:,1:new_no_ele)
fracID(1:new_no_ele)=new_fracID(1:new_no_ele)

no_node=new_no_node
no_ele=new_no_ele

i=0

return
end subroutine reMeshGenerator

    
    
subroutine checkPointInPlane(&
    & x,y,z,FracNXYZd,NB_i,EPSILON,SQEPS,inInd)
implicit none
integer*4 NB_i,inInd
real*8, allocatable:: FracNXYZd(:,:)
real*8 x,y,z,EPSILON,SQEPS
real*8 sum

sum=FracNXYZd(4,NB_i)-FracNXYZd(1,NB_i)*x-FracNXYZd(2,NB_i)*y-FracNXYZd(3,NB_i)*z
if (dabs(sum)<SQEPS) then
    inInd=1
    if (dabs(FracNXYZd(3,NB_i))>EPSILON) then
        z=&
            & FracNXYZd(4,NB_i)- &
            & FracNXYZd(1,NB_i)*x - &
            & FracNXYZd(2,NB_i)*y
        z=z/FracNXYZd(3,NB_i)
    else if (dabs(FracNXYZd(2,NB_i))>EPSILON) then
        y=&
            & FracNXYZd(4,NB_i)- &
            & FracNXYZd(1,NB_i)*x - &
            & FracNXYZd(3,NB_i)*z
        y=y/FracNXYZd(2,NB_i)
    else if (dabs(FracNXYZd(1,NB_i))>EPSILON) then
        x=&
            & FracNXYZd(4,NB_i)- &
            & FracNXYZd(2,NB_i)*y - &
            & FracNXYZd(3,NB_i)*z
        x=x/FracNXYZd(1,NB_i) 
    end if
else
    inInd=0
end if


return
end subroutine checkPointInPlane 
    
subroutine findIDfractureNodeEle(&
        & ID,&
        & no_node,no_ele,XYZ,ele_ind,fracID,&
        & union_no_node,union_no_ele,union_XYZ,union_ele_ind,NB_trans,ele_trans)
implicit none
integer*4 i,j,k,ID
integer*4 no_node,no_ele
integer*4, allocatable:: ele_ind(:,:),fracID(:)
real*8, allocatable:: XYZ(:,:)
integer*4 union_no_node,union_no_ele
real*8, allocatable:: union_XYZ(:,:)
integer*4, allocatable:: union_ele_ind(:,:),NB_trans(:),ele_trans(:)
integer*4, allocatable:: nodeNB(:)


if (allocated(union_XYZ)) deallocate(union_XYZ)
if (allocated(union_ele_ind)) deallocate(union_ele_ind)
if (allocated(NB_trans)) deallocate(NB_trans)
if (allocated(ele_trans)) deallocate(ele_trans)
allocate(union_XYZ(3,no_node),union_ele_ind(3,no_ele))
allocate(nodeNB(no_node),NB_trans(no_node),ele_trans(no_ele))
nodeNB=0
NB_trans=0
ele_trans=0

union_no_ele=0
do i=1,no_ele
    if (fracID(i)==ID) then
        union_no_ele=union_no_ele+1 
        ele_trans(union_no_ele)=i
        do j=1,3
            union_ele_ind(j,union_no_ele)=ele_ind(j,i)
            nodeNB(ele_ind(j,i))=1
        end do
    end if
end do

union_no_node=0
do i=1,no_node
    if (nodeNB(i)==1) then
        union_no_node=union_no_node+1
        NB_trans(union_no_node)=i
        nodeNB(i)=union_no_node
        do j=1,3
            union_XYZ(j,union_no_node)=XYZ(j,i)
        end do
    end if
end do

do i=1,union_no_ele
    do j=1,3
        union_ele_ind(j,i)=nodeNB(union_ele_ind(j,i))
    end do
end do



return
end subroutine findIDfractureNodeEle
    

subroutine readOFF(no_A_node,no_A_ele,no_B_node,no_B_ele,XYZa,XYZb,ele_A_ind,ele_B_ind)
implicit none
integer*4 i,j,k
integer*4 no_A_node,no_A_ele
integer*4 no_B_node,no_B_ele
real*8, allocatable:: XYZa(:,:),XYZb(:,:)
integer*4, allocatable::ele_A_ind(:,:),ele_B_ind(:,:)   

if (allocated(XYZa)) deallocate(XYZa)
if (allocated(XYZb)) deallocate(XYZb)
if (allocated(ele_A_ind)) deallocate(ele_A_ind)
if (allocated(ele_B_ind)) deallocate(ele_B_ind)

open(30,file='./data/blobby.off')
read(30,*)
read(30,*) no_A_node,no_A_ele
allocate(XYZa(3,no_A_node),ele_A_ind(3,no_A_ele))
do i=1,no_A_node
    read(30,*) (XYZa(j,i),j=1,3)
end do
do i=1,no_A_ele
    read(30,*) k,(ele_A_ind(j,i),j=1,3)
end do
close(30)
open(30,file='./data/eight.off')
read(30,*)
read(30,*) no_B_node,no_B_ele
allocate(XYZb(3,no_B_node),ele_B_ind(3,no_B_ele))
do i=1,no_B_node
    read(30,*) (XYZb(j,i),j=1,3)
end do
do i=1,no_B_ele
    read(30,*) k,(ele_B_ind(j,i),j=1,3)
end do
close(30)    
return
end subroutine readOFF


subroutine temp_teta(no_node,no_ele,XYZ,ele_ind,File,order,FracNXYZd,NB_frac)
implicit none
integer*4 i,j,k
integer*4 no_node,no_ele,NB_frac
real*8, allocatable:: XYZ(:,:),FracNXYZd(:,:)
integer*4, allocatable:: ele_ind(:,:)
character(256) File
real*8 Nx,Ny,Nz,Nd
real*8 cXYZ(3)
integer*4 NB
integer*4 nei_ele_ind(3,no_ele)
integer*4 order(3,2)

real*8, allocatable:: newXYZ(:,:)
integer*4, allocatable:: newele_ind(:,:)

if (allocated(newXYZ)) deallocate(newXYZ)
if (allocated(newele_ind)) deallocate(newele_ind)
allocate(newXYZ(3,no_node+no_node),newele_ind(3,no_ele*20))

k=no_ele
call Find_nei_ELE_index_new(ele_ind,nei_ele_ind,k,no_ele)

i=1
!call plane_EQ(&
!    & XYZ(1,ele_ind(1,i)+1),XYZ(2,ele_ind(1,i)+1),XYZ(3,ele_ind(1,i)+1),&
!    & XYZ(1,ele_ind(2,i)+1),XYZ(2,ele_ind(2,i)+1),XYZ(3,ele_ind(2,i)+1),&
!    & XYZ(1,ele_ind(3,i)+1),XYZ(2,ele_ind(3,i)+1),XYZ(3,ele_ind(3,i)+1),&
!    & Nx,Ny,Nz,Nd)

Nx=FracNXYZd(1,NB_frac)
Ny=FracNXYZd(2,NB_frac)
Nz=FracNXYZd(3,NB_frac)
Nd=FracNXYZd(4,NB_frac)


do i=1,no_node
    newXYZ(1,i+no_node)=XYZ(1,i)-Nx*10.0
    newXYZ(2,i+no_node)=XYZ(2,i)-Ny*10.0
    newXYZ(3,i+no_node)=XYZ(3,i)-Nz*10.0
end do

do i=1,no_ele
    newele_ind(1,i+no_ele)=ele_ind(1,i)+no_node
    newele_ind(2,i+no_ele)=ele_ind(3,i)+no_node
    newele_ind(3,i+no_ele)=ele_ind(2,i)+no_node
end do

NB=0
do i=1,no_ele
    do j=1,3
        if (nei_ele_ind(j,i)==-1) then
            NB=NB+1
            newele_ind(1,NB+no_ele*2)=ele_ind(order(j,1),i)
            newele_ind(2,NB+no_ele*2)=ele_ind(order(j,2),i)+no_node
            newele_ind(3,NB+no_ele*2)=ele_ind(order(j,2),i)
            NB=NB+1
            newele_ind(1,NB+no_ele*2)=ele_ind(order(j,1),i)
            newele_ind(2,NB+no_ele*2)=ele_ind(order(j,1),i)+no_node
            newele_ind(3,NB+no_ele*2)=ele_ind(order(j,2),i)+no_node            
        end if
    end do
end do



open(500,file='cxyzPlot.dat')

write(500,"(A17)") 'Title="XY2D_plot"'
WRITE(500,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
WRITE(500,*) "ZONE N=",no_node*2,",E=",no_ele*2+NB,", F=FEPOINT, ET=triangle"

open(30,file=File)
write(30,"(A)") 'OFF'
write(30,"(3(I0,1x))") no_node*2,no_ele*2+NB,0
do i=1,no_node
    write(30,"(3(G0,1x))") (XYZ(j,i),j=1,3)
    write(500,"(3(G0,1x))") (XYZ(j,i),j=1,3)
end do
do i=no_node+1,no_node*2
    write(30,"(3(G0,1x))") (newXYZ(j,i),j=1,3)
    write(500,"(3(G0,1x))") (newXYZ(j,i),j=1,3)
end do
do i=1,no_ele
    write(30,"(4(I0,1x))") 3,(ele_ind(j,i),j=1,3)
    write(500,"(3(I0,1x))") (ele_ind(j,i)+1,j=1,3)
end do
do i=no_ele+1,no_ele*2+NB
    write(30,"(4(G0,1x))") 3,(newele_ind(j,i),j=1,3)
    write(500,"(3(G0,1x))") (newele_ind(j,i)+1,j=1,3)
end do

close(30)
close(500)
end subroutine temp_teta

    
end module FindFracture
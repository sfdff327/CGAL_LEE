program FracManFabRead
use FindFracture
implicit none

integer*4 i,j,k,Reason,NB,ii,jj
integer*4 NB_i,NB_j
integer*4 no_node,no_ele,no_frac
integer*4, allocatable:: node_ckeck(:),face_ckeck(:)
integer*4, allocatable:: ele_ind(:,:),fracID(:)
integer*4, allocatable:: intesection_nodeNB(:)
real*8, allocatable:: FracNXYZd(:,:)
real*8, allocatable:: XYZ(:,:)
character(256) word,fabFile,coo_transFile,triMeshFile,UnionFile,IntersectionFile
integer*4 oldNB
real*8 cooTXY(2)
logical :: file_exists

integer*4 union_no_node,union_no_ele
real*8, allocatable:: union_XYZ(:,:)
integer*4, allocatable:: union_ele_ind(:,:),NB_trans(:),ele_trans(:)

integer*4 no_A_node,no_A_ele
integer*4 no_B_node,no_B_ele
real*8, allocatable:: XYZa(:,:),XYZb(:,:)
integer*4, allocatable::ele_A_ind(:,:),ele_B_ind(:,:)
integer*4, allocatable:: Node_NB_A_trans(:),Node_NB_B_trans(:)
integer*4, allocatable:: ele_NB_A_trans(:),ele_NB_B_trans(:)
real*8 minXYZ(3)
integer*4 order(3,2)
real*8 EPSILON,SQEPS,TOL1


call triOrder(order)
call EPSILON_SQEPS(EPSILON,SQEPS)

open(999,file='mark_Frac_interserction.dat')

open(10,file='fabRead_sp.dat')
read(10,"(A256)") fabFile
read(10,"(A256)") coo_transFile
close(10)

if (coo_transFile(1:10)/='          ') then
    open(10,file=coo_transFile)
    do i=1,2
        read(10,"(A256)") word
        read(word(3:256),*) cooTXY(i)
    end do
    close(10)
else
    cooTXY=0.0d0    
end if


open(10,file=fabFile)
do while(.true.)
    read(10,"(A256)",IOSTAT=Reason) word
    if (word(1:12)=='BEGIN FORMAT') then
        do while(.true.)
            read(10,"(A256)",IOSTAT=Reason) word
            if (word(1:18)=='    No_Fractures =') then
                read(word(19:256),*) no_frac
                no_node=no_ele*4
        
                goto 110
            end if
        end do
    else
        if (word(1:14)=='BEGIN FRACTURE') then
            no_node=0
            no_ele=0
            do i=1,no_frac
                read(10,*) k,NB
                no_node=no_node+NB
                no_ele=no_ele+NB-2
                do j=1,NB+1
                    read(10,*)
                end do
            end do
            allocate(ele_ind(3,no_ele),fracID(no_ele))
            allocate(FracNXYZd(4,no_frac))
            allocate(XYZ(3,no_node))
            close(10)
            
            open(10,file=fabFile)
            do while(.true.)
                read(10,"(A256)",IOSTAT=Reason) word
                if (word(1:14)=='BEGIN FRACTURE') then
                    no_node=0
                    no_ele=0
                    oldNB=0
                    do i=1,no_frac                                              
                        read(10,*) k,NB          
                        do j=1,NB
                            no_node=no_node+1
                            read(10,*) k,(XYZ(ii,no_node),ii=1,3)
                            do ii=1,2
                                XYZ(ii,no_node)=XYZ(ii,no_node)+cooTXY(ii)
                            end do
                        end do
                        do j=1,NB-2
                            no_ele=no_ele+1
                            fracID(no_ele)=i
                            ele_ind(1,no_ele)=oldNB+1
                            ele_ind(2,no_ele)=oldNB+1+j
                            ele_ind(3,no_ele)=oldNB+1+j+1
                        end do                       
                        oldNB=no_node
                        
                        read(10,*)
                        
                    end do
                    goto 111
                end if
                j=0
                
            end do 
        end if                
    end if    
110 k=0
end do
111 k=0
close(10)

if (allocated(node_ckeck)) deallocate(node_ckeck)
if (allocated(intesection_nodeNB)) deallocate(intesection_nodeNB)
if (allocated(face_ckeck)) deallocate(face_ckeck)
allocate(node_ckeck(no_node))
allocate(intesection_nodeNB(no_node))
allocate(face_ckeck(no_frac))
node_ckeck=0
intesection_nodeNB=0
face_ckeck=0
do i=1,no_ele
    if (face_ckeck(fracID(i))==0) then
        call plane_EQ(&
            & XYZ(1,ele_ind(1,i)),XYZ(2,ele_ind(1,i)),XYZ(3,ele_ind(1,i)),&
            & XYZ(1,ele_ind(2,i)),XYZ(2,ele_ind(2,i)),XYZ(3,ele_ind(2,i)),&
            & XYZ(1,ele_ind(3,i)),XYZ(2,ele_ind(3,i)),XYZ(3,ele_ind(3,i)),&
            & FracNXYZd(1,fracID(i)),FracNXYZd(2,fracID(i)),FracNXYZd(3,fracID(i)),FracNXYZd(4,fracID(i)))
        face_ckeck(fracID(i))=1
        node_ckeck(ele_ind(1,i))=1
        node_ckeck(ele_ind(2,i))=1
        node_ckeck(ele_ind(3,i))=1
    end if
    do j=1,3
        if (node_ckeck(ele_ind(j,i))==0) then
            if (dabs(FracNXYZd(3,fracID(i)))>EPSILON) then
                XYZ(3,ele_ind(j,i))=&
                    & FracNXYZd(4,fracID(i))- &
                    & FracNXYZd(1,fracID(i))*XYZ(1,ele_ind(j,i)) - &
                    & FracNXYZd(2,fracID(i))*XYZ(2,ele_ind(j,i))
                XYZ(3,ele_ind(j,i))=XYZ(3,ele_ind(j,i))/FracNXYZd(3,fracID(i))
            else if (dabs(FracNXYZd(2,fracID(i)))>EPSILON) then
                XYZ(2,ele_ind(j,i))=&
                    & FracNXYZd(4,fracID(i))- &
                    & FracNXYZd(1,fracID(i))*XYZ(1,ele_ind(j,i)) - &
                    & FracNXYZd(3,fracID(i))*XYZ(3,ele_ind(j,i))
                XYZ(2,ele_ind(j,i))=XYZ(2,ele_ind(j,i))/FracNXYZd(2,fracID(i))
            else if (dabs(FracNXYZd(1,fracID(i)))>EPSILON) then
                XYZ(1,ele_ind(j,i))=&
                    & FracNXYZd(4,fracID(i))- &
                    & FracNXYZd(2,fracID(i))*XYZ(2,ele_ind(j,i)) - &
                    & FracNXYZd(3,fracID(i))*XYZ(3,ele_ind(j,i))
                XYZ(1,ele_ind(j,i))=XYZ(1,ele_ind(j,i))/FracNXYZd(1,fracID(i))                
            end if
            node_ckeck(ele_ind(j,i))=1
        end if
    end do
end do


OPEN(20,file='FabPlot.dat')
write(20,"(A17)") 'Title="XY2D_plot"'
WRITE(20,"(A35)") 'VARIABLES="x(m)","y(m)","z(m)","ID"'
WRITE(20,*) "ZONE N=",no_node,",E=",no_ele,", F=FEBLOCK, ET=triangle"
WRITE(20,"(A44)") 'varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)'
write(20,"(5(G0,1x))") (XYZ(1,i),i=1,no_node)
write(20,"(5(G0,1x))") (XYZ(2,i),i=1,no_node)
write(20,"(5(G0,1x))") (XYZ(3,i),i=1,no_node)
write(20,"(5(I0,1x))") (fracID(i),i=1,no_ele)
do i=1,no_ele
    write(20,"(3(I,1x))") (ele_ind(j,i),j=1,3)
end do
close(20)

NB_i=0

do while(NB_i<no_frac-1)
    
    NB_i=NB_i+1
    !NB_i=NB_i+i
    !if (NB_i>no_frac-1) goto 2001
    !NB_i=62
    
    
    write(triMeshFile,"(A)") './data/blobby.off'
    open(30,file=triMeshFile)
    call findIDfractureNodeEle(&
        & NB_i,&
        & no_node,no_ele,XYZ,ele_ind,fracID,&
        & union_no_node,union_no_ele,union_XYZ,union_ele_ind,NB_trans,ele_trans)
    if(allocated(Node_NB_A_trans)) deallocate(Node_NB_A_trans)
    if(allocated(ele_NB_A_trans)) deallocate(ele_NB_A_trans)
    allocate(Node_NB_A_trans(size(NB_trans)))
    allocate(ele_NB_A_trans(size(ele_trans)))
    Node_NB_A_trans(1:union_no_node)=NB_trans(1:union_no_node)
    ele_NB_A_trans(1:union_no_ele)=ele_trans(1:union_no_ele)
    
    open(30,file=triMeshFile)
    write(30,"(A)") 'OFF'
    write(30,"(3(I0,1x))") union_no_node,union_no_ele,0
    do ii=1,union_no_node
        write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
    end do
    do ii=1,union_no_ele
        write(30,"(4(I0,1x))") 3,(union_ele_ind(jj,ii)-1,jj=1,3)
    end do    
    close(30)
    
    OPEN(30,file='ID_I.dat')
    write(30,"(A17)") 'Title="XY2D_plot"'
    WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
    WRITE(30,*) "ZONE N=",union_no_node,",E=",union_no_ele,", F=FEpoint, ET=triangle"
    do ii=1,union_no_node
        write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
    end do
    do ii=1,union_no_ele
        write(30,"(4(I,1x))") (union_ele_ind(jj,ii),jj=1,3)
    end do
    close(30)
    

    NB_j=NB_i
    do while(NB_j<no_frac)
    NB_j=NB_j+1

        !NB_j=NB_j+j
        !if (NB_j>no_frac) goto 2002
        !NB_j=7202
        
        if ((NB_i==1 .and. NB_j==4412) .or. (NB_i==2 .and. NB_j==1544)) then
            k=0
        end if
        
        
        
        call findIDfractureNodeEle(&
            & NB_j,&
            & no_node,no_ele,XYZ,ele_ind,fracID,&
            & union_no_node,union_no_ele,union_XYZ,union_ele_ind,NB_trans,ele_trans)
        if(allocated(Node_NB_B_trans)) deallocate(Node_NB_B_trans)
        if(allocated(ele_NB_B_trans)) deallocate(ele_NB_B_trans)
        allocate(Node_NB_B_trans(size(NB_trans)))
        allocate(ele_NB_B_trans(size(ele_trans)))
        Node_NB_B_trans(1:union_no_node)=NB_trans(1:union_no_node)
        ele_NB_B_trans(1:union_no_ele)=ele_trans(1:union_no_ele)
    
        write(triMeshFile,"(A)") './data/eight.off'
        open(30,file=triMeshFile)
        write(30,"(A)") 'OFF'
        write(30,"(3(I0,1x))") union_no_node,union_no_ele,0
        do ii=1,union_no_node
            write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
        end do
        do ii=1,union_no_ele
            write(30,"(3(I0,1x))") 3,(union_ele_ind(jj,ii)-1,jj=1,3)
        end do    
        close(30)
        
        OPEN(30,file='ID_J.dat')
        write(30,"(A17)") 'Title="XY2D_plot"'
        WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
        WRITE(30,*) "ZONE N=",union_no_node,",E=",union_no_ele,", F=FEpoint, ET=triangle"
        do ii=1,union_no_node
            write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
        end do
        do ii=1,union_no_ele
            write(30,"(3(I,1x))") (union_ele_ind(jj,ii),jj=1,3)
        end do
        close(30)
        
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        call execute_command_line ('./corefinement_mesh_union >UUU', wait=.true.)
        INQUIRE(FILE='union.off', EXIST=file_exists)
        if (file_exists) then
            open(30,file='union.off')
            close(30,STATUS='DELETE')
            open(30,file='./data/eight.off')
            close(30,STATUS='DELETE')
            write(*,"(A,I0,A,I0)") 'found union.off i=',NB_i,' j=',NB_j
            !!pause
        else
            write(999,"(A,I0,A,I0)") 'non found union.off i=',NB_i,' j=',NB_j
            write(*,"(A,I0,A,I0)") 'non found union.off i=',NB_i,' j=',NB_j
            call readOFF(no_A_node,no_A_ele,no_B_node,no_B_ele,XYZa,XYZb,ele_A_ind,ele_B_ind)
            write(triMeshFile,"(A)") './tetA.off'
            call temp_teta(no_A_node,no_A_ele,XYZa,ele_A_ind,triMeshFile,order,FracNXYZd,NB_i)
            write(triMeshFile,"(A)") './tetB.off'
            call temp_teta(no_B_node,no_B_ele,XYZb,ele_B_ind,triMeshFile,order,FracNXYZd,NB_j)
            call execute_command_line ('./corefinement_mesh_union_and_intersection tetA.off tetB.off >ZZZ', wait=.true.)
            INQUIRE(FILE='union.off', EXIST=file_exists)
            UnionFile='union.off'
            IntersectionFile='intersection.off'
            if (file_exists) then
                write(*,"(A)") 'found'
                call reMeshGenerator(&
                    & EPSILON,SQEPS,&
                    & NB_i,NB_j,UnionFile,IntersectionFile,&
                    & no_node,no_ele,XYZ,ele_ind,fracID,FracNXYZd,intesection_nodeNB,&
                    & no_A_node,no_A_ele,no_B_node,no_B_ele,&
                    & Node_NB_A_trans,ele_NB_A_trans,Node_NB_B_trans,ele_NB_B_trans)
                
                write(triMeshFile,"(A)") './data/blobby.off'
                open(30,file=triMeshFile)
                call findIDfractureNodeEle(&
                    & NB_i,&
                    & no_node,no_ele,XYZ,ele_ind,fracID,&
                    & union_no_node,union_no_ele,union_XYZ,union_ele_ind,NB_trans,ele_trans)
                if(allocated(Node_NB_A_trans)) deallocate(Node_NB_A_trans)
                if(allocated(ele_NB_A_trans)) deallocate(ele_NB_A_trans)
                allocate(Node_NB_A_trans(size(NB_trans)))
                allocate(ele_NB_A_trans(size(ele_trans)))
                Node_NB_A_trans(1:union_no_node)=NB_trans(1:union_no_node)
                ele_NB_A_trans(1:union_no_ele)=ele_trans(1:union_no_ele)
                
                open(30,file=triMeshFile)
                write(30,"(A)") 'OFF'
                write(30,"(3(I0,1x))") union_no_node,union_no_ele,0
                do ii=1,union_no_node
                    write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
                end do
                do ii=1,union_no_ele
                    write(30,"(4(I0,1x))") 3,(union_ele_ind(jj,ii)-1,jj=1,3)
                end do    
                close(30)
                OPEN(30,file='ID_I.dat')
                write(30,"(A17)") 'Title="XY2D_plot"'
                WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
                WRITE(30,*) "ZONE N=",union_no_node,",E=",union_no_ele,", F=FEpoint, ET=triangle"
                do ii=1,union_no_node
                    write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
                end do
                do ii=1,union_no_ele
                    write(30,"(4(I,1x))") (union_ele_ind(jj,ii),jj=1,3)
                end do
                close(30)
                
                write(triMeshFile,"(A)") './data/eight.off'
                open(30,file=triMeshFile)
                call findIDfractureNodeEle(&
                    & NB_j,&
                    & no_node,no_ele,XYZ,ele_ind,fracID,&
                    & union_no_node,union_no_ele,union_XYZ,union_ele_ind,NB_trans,ele_trans)
                if(allocated(Node_NB_B_trans)) deallocate(Node_NB_B_trans)
                if(allocated(ele_NB_B_trans)) deallocate(ele_NB_B_trans)
                allocate(Node_NB_B_trans(size(NB_trans)))
                allocate(ele_NB_B_trans(size(ele_trans)))
                Node_NB_B_trans(1:union_no_node)=NB_trans(1:union_no_node)
                ele_NB_B_trans(1:union_no_ele)=ele_trans(1:union_no_ele)
                write(30,"(A)") 'OFF'
                write(30,"(3(I0,1x))") union_no_node,union_no_ele,0
                do ii=1,union_no_node
                    write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
                end do
                do ii=1,union_no_ele
                    write(30,"(3(I0,1x))") 3,(union_ele_ind(jj,ii)-1,jj=1,3)
                end do    
                close(30)
                
                OPEN(30,file='ID_J.dat')
                write(30,"(A17)") 'Title="XY2D_plot"'
                WRITE(30,"(A31)") 'VARIABLES="x(m)","y(m)","z(m)"'
                WRITE(30,*) "ZONE N=",union_no_node,",E=",union_no_ele,", F=FEpoint, ET=triangle"
                do ii=1,union_no_node
                    write(30,"(3(G0,1x))") (union_XYZ(jj,ii),jj=1,3)
                end do
                do ii=1,union_no_ele
                    write(30,"(3(I,1x))") (union_ele_ind(jj,ii),jj=1,3)
                end do
                close(30)
                
                open(70,file='PLOT_FabIntesect.dat')
                write(70,"(A17)") 'Title="XY2D_plot"'
                WRITE(70,"(A39)") 'VARIABLES="x(m)","y(m)","z(m)","fracID"'
                WRITE(70,*) 'ZONE N=',no_node,',E=',no_ele,', F=FEBLOCK, ET=TRIANGLE'
                WRITE(70,"(A44)") 'varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)'
                write(70,"(10(G0,1x))") ((XYZ(1,i)),i=1,no_node)
                write(70,"(10(G0,1x))") ((XYZ(2,i)),i=1,no_node)
                write(70,"(10(G0,1x))") ((XYZ(3,i)),i=1,no_node)
                write(70,"(10(I0,1x))") ((fracID(i)),i=1,no_ele)
                do i=1,no_ele
                    write(70,"(3(I,1x))") ((ele_ind(j,i)),j=1,3)
                end do
                close(70)
                
            else
                write(*,"(A)") 'non found'
            end if
            !pause
            
        end if    
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
    end do
2002 j=0
     open(20,file='temp_FAB', form='unformatted')
     write(20) NB_i
     write(20) no_node,no_ele
     write(20) ((XYZ(j,i),j=1,3),i=1,no_node)
     write(20) ((ele_ind(j,i),j=1,3),i=1,no_ele)
     write(20) (fracID(i),i=1,no_ele)
     close(20)
end do

2001 i=0
     
open(20,file='finial_FAB', form='unformatted')
write(20) NB_i
write(20) no_node,no_ele
write(20) ((XYZ(j,i),j=1,3),i=1,no_node)
write(20) ((ele_ind(j,i),j=1,3),i=1,no_ele)
write(20) (fracID(i),i=1,no_ele)
close(20)

i=0
    
end
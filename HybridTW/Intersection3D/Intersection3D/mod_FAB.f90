module FAB
implicit none
contains
   
subroutine FAB_reader(cooTXY,no_frac,frac_no_node,frac_no_ele,FracNXYZd,frac_XYZ,frac_ele_ind,fracID)
implicit none
integer*4 i,j,k,NB,oldNB,ii
real*8 cooTXY(2)
character(256) fabFile,coo_transFile,word
integer Reason
integer no_frac,frac_no_node,frac_no_ele
integer*4, allocatable:: frac_ele_ind(:,:),fracID(:)
real*8, allocatable:: FracNXYZd(:,:),frac_XYZ(:,:)

integer*4 frac_no_sege
integer*4, allocatable:: frac_sege(:,:)

integer*4, allocatable:: calPlaneEqCheck(:),nodeCheckPlane(:)
integer*4 e(3)

open(10,file='fabRead_sp.dat')
read(10,"(A256)") fabFile
read(10,"(A256)") coo_transFile
close(10)

if (coo_transFile(1:10)/='          ') then
    open(10,file=coo_transFile)
    do i=1,2
        read(10,"(A256)") word
        read(word(3:256),*) cooTXY(i)
    end do
else
    cooTXY=0.0d0    
end if
close(10)

open(10,file=fabFile)
do while(.true.)
    read(10,"(A256)",IOSTAT=Reason) word
    if (Reason==-1) exit
    if (word(1:14)=='BEGIN FRACTURE') then
        frac_no_node = 0
        frac_no_ele = 0
        frac_no_sege=0  !L
        do i=1,no_frac
            read(10,*) k,NB
            frac_no_node = frac_no_node + NB
            frac_no_ele = frac_no_ele + NB -2
            do j=1,NB+1
                read(10,*)
            end do
        end do
        frac_no_sege=frac_no_node
        allocate(frac_sege(2,frac_no_sege))
        allocate(frac_ele_ind(3,frac_no_ele),fracID(frac_no_ele))
        allocate(FracNXYZd(4,no_frac),frac_XYZ(3,frac_no_node),calPlaneEqCheck(no_frac),nodeCheckPlane(frac_no_node))
        FracNXYZd=0.0d0
    end if    
    if (word(1:12)=='BEGIN FORMAT') then
        do while(.true.)
            read(10,"(A256)",IOSTAT=Reason) word
            if (word(1:18)=='    No_Fractures =') then
                read(word(19:256),*) no_frac
                frac_no_node=frac_no_ele*4
            end if
            if (word(1:10)=='END FORMAT') then
                goto 111
            end if
        end do      
    end if
111 i=0
end do
close(10)

open(10,file=fabFile)
do while(.true.)
    read(10,"(A256)",IOSTAT=Reason) word
    if (Reason==-1) exit
    if (word(1:14)=='BEGIN FRACTURE') then
        frac_no_node=0
        frac_no_ele=0
        frac_no_sege=0
        oldNB=0
        do i=1,no_frac
            read(10,*) k,NB
            do j=1,NB
                frac_no_node=frac_no_node+1
                read(10,*) k,(frac_XYZ(ii,frac_no_node),ii=1,3)
                do ii=1,2
                    frac_XYZ(ii,frac_no_node)=frac_XYZ(ii,frac_no_node)+cooTXY(ii)
                end do
            end do
            do j=1,NB-2
                frac_no_ele=frac_no_ele+1
                fracID(frac_no_ele)=i
                frac_ele_ind(1,frac_no_ele)=oldNB+1
                frac_ele_ind(2,frac_no_ele)=oldNB+1+j
                frac_ele_ind(3,frac_no_ele)=oldNB+1+j+1
            end do   
            do j=1,NB-1
                frac_no_sege=frac_no_sege+1
                frac_sege(1,frac_no_sege)=frac_no_sege
                frac_sege(2,frac_no_sege)=frac_no_sege+1
            end do
            frac_no_sege=frac_no_sege+1
            frac_sege(1,frac_no_sege)=frac_no_sege
            frac_sege(2,frac_no_sege)=oldNB+1
            oldNB=frac_no_node
            read(10,*)
        end do
        goto 112
    end if
    j=0
end do
112 close(10)  
    
! open(4000,file='TestFabReader')
calPlaneEqCheck=0
nodeCheckPlane=0
do i = 1, frac_no_ele
    e(1) = frac_ele_ind(1,i)!+1
    e(2) = frac_ele_ind(2,i)!+1
    e(3) = frac_ele_ind(3,i)!+1
    
    if (calPlaneEqCheck(fracID(i))==0) then
        !write(4000,"(A,I0)") 'i = ',i
        !write(4000,"(3(G0,1x))") frac_XYZ(1,e(1)),frac_XYZ(2,e(1)),frac_XYZ(3,e(1))
        !write(4000,"(3(G0,1x))") frac_XYZ(1,e(2)),frac_XYZ(2,e(2)),frac_XYZ(3,e(2))
        !write(4000,"(3(G0,1x))") frac_XYZ(1,e(3)),frac_XYZ(2,e(3)),frac_XYZ(3,e(3))        
        !write(4000,"(3(I0,1x))") (frac_ele_ind(j,i),j=1,3)
        call plane_EQ(&
        & frac_XYZ(1,e(1)),frac_XYZ(2,e(1)),frac_XYZ(3,e(1)),&
        & frac_XYZ(1,e(2)),frac_XYZ(2,e(2)),frac_XYZ(3,e(2)),&
        & frac_XYZ(1,e(3)),frac_XYZ(2,e(3)),frac_XYZ(3,e(3)),&
        & FracNXYZd(1,fracID(i)),FracNXYZd(2,fracID(i)),FracNXYZd(3,fracID(i)),FracNXYZd(4,fracID(i)))
        do j=1,3
            nodeCheckPlane(e(j))=1
        end do
        calPlaneEqCheck(fracID(i))=1
    else
        do j=1,3
            if (nodeCheckPlane(e(j))==0)then
                call CheckNodeXYZinPlane(FracNXYZd(1,fracID(i)),FracNXYZd(2,fracID(i)),FracNXYZd(3,fracID(i)),FracNXYZd(4,fracID(i)),frac_XYZ(1,e(j)),frac_XYZ(2,e(j)),frac_XYZ(3,e(j)))
                nodeCheckPlane(e(j))=1
            end if
        end do        
    end if    
end do


OPEN(20,file='FabPlot.dat')
write(20,"(A17)") 'Title="XY2D_plot"'
WRITE(20,"(A35)") 'VARIABLES="x(m)","y(m)","z(m)","ID"'
WRITE(20,*) "ZONE N=",frac_no_node,",E=",frac_no_ele,", F=FEBLOCK, ET=triangle"
WRITE(20,"(A44)") 'varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)'
write(20,"(5(G0,1x))") (frac_XYZ(1,i),i=1,frac_no_node)
write(20,"(5(G0,1x))") (frac_XYZ(2,i),i=1,frac_no_node)
write(20,"(5(G0,1x))") (frac_XYZ(3,i),i=1,frac_no_node)
write(20,"(5(I0,1x))") (fracID(i),i=1,frac_no_ele)
do i=1,frac_no_ele
    write(20,"(3(I,1x))") (frac_ele_ind(j,i),j=1,3)
end do
close(20)

!do i=1,no_frac
!    write(4000,*) (FracNXYZd(j,i),j=1,4)
!end do
!close(4000)
i=0

return
end subroutine FAB_reader

end module FAB


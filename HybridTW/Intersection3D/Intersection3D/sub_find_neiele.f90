subroutine Find_nei_ELE_index_new(ELE_index,nei_ELE_index,max_ele,no_ELE)
implicit none
integer*4 ii,jj,iii,no_ELE,max_ele
integer*4 ELE_index(3,max_ele),nei_ELE_index(3,max_ele)

nei_ELE_index=-1
do ii=1,no_ELE
   do jj=ii+1,no_ELE
      if (nei_ELE_index(3,ii)==-1) then
         if (ELE_index(1,ii)==ELE_index(2,jj) .and. ELE_index(2,ii)==ELE_index(1,jj)) then
		    nei_ELE_index(3,ii)=jj
            nei_ELE_index(3,jj)=ii
			goto 121
         end if
         if (ELE_index(1,ii)==ELE_index(3,jj) .and. ELE_index(2,ii)==ELE_index(2,jj)) then
		    nei_ELE_index(3,ii)=jj
            nei_ELE_index(1,jj)=ii
			goto 121
         end if
         if(ELE_index(1,ii)==ELE_index(1,jj) .and. ELE_index(2,ii)==ELE_index(3,jj)) then
		    nei_ELE_index(3,ii)=jj
            nei_ELE_index(2,jj)=ii
			goto 121
         end if
      end if
      if (nei_ELE_index(1,ii)==-1) then
         if(ELE_index(2,ii)==ELE_index(2,jj) .and. ELE_index(3,ii)==ELE_index(1,jj)) then
		    nei_ELE_index(1,ii)=jj
            nei_ELE_index(3,jj)=ii
			goto 121
         end if
         if(ELE_index(2,ii)==ELE_index(3,jj) .and. ELE_index(3,ii)==ELE_index(2,jj)) then
		    nei_ELE_index(1,ii)=jj
            nei_ELE_index(1,jj)=ii
			goto 121
         end if
         if(ELE_index(2,ii)==ELE_index(1,jj) .and. ELE_index(3,ii)==ELE_index(3,jj)) then
		    nei_ELE_index(1,ii)=jj
            nei_ELE_index(2,jj)=ii
			goto 121
         end if
      end if
      if (nei_ELE_index(2,ii)==-1) then
         if(ELE_index(3,ii)==ELE_index(2,jj) .and. ELE_index(1,ii)==ELE_index(1,jj)) then
		    nei_ELE_index(2,ii)=jj
            nei_ELE_index(3,jj)=ii
			goto 121
         end if
         if(ELE_index(3,ii)==ELE_index(3,jj) .and. ELE_index(1,ii)==ELE_index(2,jj)) then
		    nei_ELE_index(2,ii)=jj
            nei_ELE_index(1,jj)=ii
			goto 121
         end if
         if(ELE_index(3,ii)==ELE_index(1,jj) .and. ELE_index(1,ii)==ELE_index(3,jj)) then
		    nei_ELE_index(2,ii)=jj
            nei_ELE_index(2,jj)=ii
			goto 121
         end if
      end if
  121 iii=0
   end do
end do
return
end

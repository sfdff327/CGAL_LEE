subroutine plane_EQ(x1,y1,z1,x2,y2,z2,x3,y3,z3,Nx,Ny,Nz,Nd)
implicit none
!f(x,y,z)= ax+by+cz=d
real*8 x1,y1,z1,x2,y2,z2,x3,y3,z3,Nx,Ny,Nz,Nd
real*8 vx,vy,vz,kx,ky,kz,tn

vx=x2-x1
vy=y2-y1
vz=z2-z1

kx=x3-x1
ky=y3-y1
kz=z3-z1

Nx=+(vy*kz-ky*vz)
Ny=-(vx*kz-kx*vz)
Nz=+(vx*ky-kx*vy)


if (dabs(Nx)>1.0d0) then
   tn=Nx
   Nx=Nx/dabs(tn)
   Ny=Ny/dabs(tn)
   Nz=Nz/dabs(tn)
end if

if (dabs(Ny)>1.0d0) then
   tn=Ny
   Nx=Nx/dabs(tn)
   Ny=Ny/dabs(tn)
   Nz=Nz/dabs(tn)
end if

if (dabs(Nz)>1.0d0) then
   tn=Nz
   Nx=Nx/dabs(tn)
   Ny=Ny/dabs(tn)
   Nz=Nz/dabs(tn)
end if

Nd=Nx*x1+Ny*y1+Nz*z1
return
end subroutine plane_EQ

subroutine CheckNodeXYZinPlane(Nx,Ny,Nz,Nd,ox,oy,oz)
real*8 Nx,Ny,Nz,Nd,x,y,z
integer*4 i
real*8 ox,oy,oz
x = ox
y = oy
z = oz
if (dabs(Nx)>0.9d0) then
   x = (Nd - (Ny*y + Nz*z))/Nx
   if (dabs(x-ox)<1.0d-6) then
      ox = x
      goto 99
   else
      if (dabs(Ny)>0.9d0) then
         y = (Nd - (Nx*x + Nz*z))/Ny
         if (dabs(y-oy)<1.0d-6) then
            oy = y
            goto 99
         else
            if (dabs(Nz)>0.9d0) then
               oz = (Nd - (Nx*x + Ny*y))/Nz
               goto 99
            end if
         end if
      end if
   end if
else
   if (dabs(Ny)>0.9d0) then
      y = (Nd - (Nx*x + Nz*z))/Ny
      if (dabs(y-oy)<1.0d-6) then
         oy = y
         goto 99
      else
         if (dabs(Nz)>0.9d0) then
            oz = (Nd - (Nx*x + Ny*y))/Nz
            goto 99
         end if
      end if
   else
      if (dabs(Nz)>0.9d0) then
         oz = (Nd - (Nx*x + Ny*y))/Nz
         goto 99
      end if   
   end if    
end if

99 i=0

return
end subroutine CheckNodeXYZinPlane
    
subroutine triOrder(order)
implicit none
integer*4 order(3,2)
order(1,1)=2
order(2,1)=3
order(3,1)=1
order(1,2)=3
order(2,2)=1
order(3,2)=2
return
end subroutine triOrder    

subroutine dis3d(x1,y1,z1,x2,y2,z2,dis)
real*8 x1,y1,z1,x2,y2,z2,dis
dis=((x1-x2)**2.0d0+(y1-y2)**2.0d0+(z1-z2)**2.0d0)**0.5d0
return
end subroutine dis3d
    
subroutine dis2d(x1,y1,x2,y2,dis)
real*8 x1,y1,x2,y2,dis
dis=((x1-x2)**2.0d0+(y1-y2)**2.0d0)**0.5d0
return
end subroutine dis2d      
    
    
    
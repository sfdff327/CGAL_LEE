program Intersection3D
use FAB

implicit none
integer*4 i,j,k,NB
integer Reason
real*8 minXYZ(3)
integer*4 order(3,2)
real*8 EPSILON,SQEPS,TOL1

character(256) word,coo_transFile
real*8 cooTXY(2)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
character(256) fabFile
integer no_frac,frac_no_node,frac_no_ele
integer*4, allocatable:: frac_ele_ind(:,:),fracID(:)
real*8, allocatable:: FracNXYZd(:,:),frac_XYZ(:,:)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


call triOrder(order)
call EPSILON_SQEPS(EPSILON,SQEPS)


open(10,file='fabRead_sp.dat')
read(10,"(A256)") fabFile
read(10,"(A256)") coo_transFile
close(10)

if (coo_transFile(1:10)/='          ') then
    open(10,file=coo_transFile)
    do i=1,2
        read(10,"(A256)") word
        read(word(3:256),*) cooTXY(i)
    end do
    close(10)
else
    cooTXY=0.0d0    
end if


call FAB_reader(cooTXY,no_frac,frac_no_node,frac_no_ele,FracNXYZd,frac_XYZ,frac_ele_ind,fracID)


i=0
end program Intersection3D
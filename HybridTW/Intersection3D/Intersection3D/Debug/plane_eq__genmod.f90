        !COMPILER-GENERATED INTERFACE MODULE: Mon Jun  7 18:52:14 2021
        ! This source file is for reference only and may not completely
        ! represent the generated interface used by the compiler.
        MODULE PLANE_EQ__genmod
          INTERFACE 
            SUBROUTINE PLANE_EQ(X1,Y1,Z1,X2,Y2,Z2,X3,Y3,Z3,NX,NY,NZ,ND)
              REAL(KIND=8) :: X1
              REAL(KIND=8) :: Y1
              REAL(KIND=8) :: Z1
              REAL(KIND=8) :: X2
              REAL(KIND=8) :: Y2
              REAL(KIND=8) :: Z2
              REAL(KIND=8) :: X3
              REAL(KIND=8) :: Y3
              REAL(KIND=8) :: Z3
              REAL(KIND=8) :: NX
              REAL(KIND=8) :: NY
              REAL(KIND=8) :: NZ
              REAL(KIND=8) :: ND
            END SUBROUTINE PLANE_EQ
          END INTERFACE 
        END MODULE PLANE_EQ__genmod

import numpy as np
import json
import geojson
import pandas as pd
from pathlib import Path
import pickle
import copy
import pyvista as pv
from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt

import meshpy.triangle as triangle
import math

import dem
import readTetgen
import pyToTecplot
import meshTool
import hybridFlow
import plotPyvista
import pyReadFab
import os

import readGeoJSON
from scipy.io import FortranFile
from pathlib import Path

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

if __name__ == "__main__":


    EpsgIn=3825
    EpsgOut=4326

    '''
    # 表土層深度
    '''
    depthSurfaceSoil=-70.0

    OriginPointsF=r'/data/INER_DATA/Domain2D/OriginPoints.txt'
    orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
    orPointCsv = orPointCsv.fillna('')
    orPointCsv.columns=["x", "y"]
    delta_x=np.float64(orPointCsv['x'].values[0])
    delta_y=np.float64(orPointCsv['y'].values[0])
    # print(orPointCsv)
    print("delta_x,delta_y=",delta_x,delta_y)
    # quit()
       

    case=1
    if (case==1):
        '''
        # 讀取500m DEM
        '''
        CsvFile=r'/data/INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
        xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
        xyzCsv = xyzCsv.fillna('')
        xyzCsv.columns=["x", "y", "z"]   
        # print(xyzCsv) 
        '''
        # 讀取模擬範圍，並且依據dem計算表面高程
        '''
        nx=201
        ny=139
        domainFile=r'/data/INER_DATA/Domain2D/domainboundary_large.xy'
        xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
        xyzDomain = xyzDomain.fillna('')
        xyzDomain.columns=["x", "y"]
        domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
        domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
    # quit()
    domain2d=pv.read('domain2d.vtk')
    demClip2d=pv.read('demClip2d.vtk')
    largeDem=pv.read('largeDem.vtk')
    # print(largeDem)
    # quit()

    case=0
    if (case==1):
        path=r'/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/testWriteMeshALL.poly'
        with open('polyF.dat','w',encoding = 'utf-8') as f:
            f.write(path)
        os.system('./ReadPoly')
        defxyzF=r'dfnXYZ.txt'
        defxyzCsv = pd.read_csv(defxyzF,sep='\\s+',header=None)
        defxyzCsv = defxyzCsv.fillna('')
        defxyzCsv.columns=["x", "y","z"]  
        points=np.zeros((len(defxyzCsv),3),dtype=np.float64)      
        for i in range(0,len(defxyzCsv)):
            points[i]=[defxyzCsv['x'][i],defxyzCsv['y'][i],defxyzCsv['z'][i]]
        dfnELEF=r'dfnELE.txt'
        dfnELECsv = pd.read_csv(dfnELEF,sep='\\s+',header=None)
        dfnELECsv = dfnELECsv.fillna('')
        dfnELECsv.columns=["e0", "e1","e2"]  
        faces=[]  
        for i in range(0,len(dfnELECsv)):
            faces.extend([3,dfnELECsv['e0'][i],dfnELECsv['e1'][i],dfnELECsv['e2'][i]])
        dfnPoly=pv.PolyData(points,faces)
        dfnPoly.save('dfnPoly.vtk')
        # quit()  

    '''# noTestFrac=10 代表局部裂隙,測試3D流場'''    
    noTestFrac=8000
    case=1
    if (case==1):
        dfnPoly=pyReadFab.fabRead(noTestFrac)       
    else:
        dfnPoly=pv.read('dfnPoly.vtk')

    # testPLOTfilename=r'dfnPoly.dat'
    # z=np.zeros(len(dfnPoly.points))
    # pyToTecplot.tecplotPyvistaUSG_z(testPLOTfilename,dfnPoly.points,dfnPoly.faces.reshape(-1,4),z)

    case=1
    if (case==1):   
        '''
        # 讀取F1, F2 STL FIles，並利用top，建立網格
        '''
        StlFile=r'/data/INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
        fault_1=pv.read(StlFile)
        StlFile=r'/data/INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
        fault_2=pv.read(StlFile)
        demClip3dsurf,fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,EpsgIn,EpsgOut,fault_1,fault_2)
    else:
        demClip3dsurf=pv.read('demClip3dsurf.vtk')
        fault12=pv.read('fault12.vtk')
    # print("fault12")
    # print(fault12)
    # plotter = pv.Plotter(off_screen=True)
    # plotter.add_mesh(fault12, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)
    # plotter.export_gltf('./fault12.gltf', rotate_scene=False,save_normals=True)
    # gltf = GLTF.load('./fault12.gltf', load_file_resources=True)
    # gltf.export('fault12.glb') 
    # os.system("rm fault12.gltf")  
    # print(np.mean(fault12.points,axis=0))      
    # quit()


    '''水文參數設定'''
    Kfracture=5.0e-5
    poro_fracture=0.4
    aperture=5.0e-6
    alpha=0.008
    sa=3.2
    density=1.0*(1.0+alpha*sa)
    Krock=1.0e-10
    poro_rock=5.4e-3 
    matrix_alpha_w=4.8
    Ksurface=1.0e-5
    poro_surface=5.4e-3 
    surface_alpha_w=4.8
    F1F2K=5.0e-6
    poro_F1F2K=0.4
    F1F2K_alpha_w=1.0    

    meger3dVtkF='meger3dTH.vtk'
    meger3dNonDensityVtkF='meger3dTH_nonDenstiy.vtk'

    meshKey=1
    if (meshKey==1):
        '''
        # case=1 #會重新製作poly files
        '''
        case=2
        if (case==1 or case==2):
            '''
            # 設定表土層與底部表面，耦合f1f2
            '''
            meger3d,megerFace=plotPyvista.surToTetgen(case,depthSurfaceSoil,demClip3dsurf,fault12,dfnPoly)
            # quit()
        
        topPoly3d=pv.read('topPoly3d.vtk')
        profilePoly3d=pv.read('profilePoly3d.vtk')
        meger3d=pv.read('megerTetra3d.vtk')
        megerFace=pv.read('megerTri2d.vtk')

        case=1
        if (case==1):
            '''
            # 搜尋上邊界node編號
            '''
            meger3dClipTop=plotPyvista.findMegerTopBc(meger3d,topPoly3d)
        meger3dClipTop=pv.read('meger3dClipTop.vtk')
        # print("meger3dClipTop.array_names")
        # print(meger3dClipTop.array_names)
        # quit()
    

        case=1
        if (case==1):
            '''
            # 搜尋側邊界node編號
            '''
            radius=500.0
            profile2dBCPoly=plotPyvista.findMegerProfileBc(megerFace,largeDem,radius)
        bcProfileFace=pv.read('bcProfileFace.vtk')
        # print("bcProfileFace.array_names")
        # print(bcProfileFace.array_names)   

        case=1
        if (case==1):
            '''
            # 產生複合域水流輸入檔
            '''
            hybridFlow.writeInputDensity(density,meger3d,megerFace,meger3dClipTop,bcProfileFace,Kfracture,poro_fracture,Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K) 
        
        case=1
        if (case==1):
            hybridFlow.runDualDomain_flowFortanProgram('DualDomain_flow')


        # meger3dVtkF='meger3dTH.vtk'
        case=1
        if (case==1):
            meger3dTH=hybridFlow.readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meger3d,meger3dVtkF)
            # print(meger3d.array_names)
        
        # meger3dNonDensityVtkF='meger3dTH_nonDenstiy.vtk'
        case=1
        if (case==1):
            os.system('cp FEM_AGMG_BC1_inf_nonDensity.dat FEM_AGMG_BC1_inf.dat')
            hybridFlow.runDualDomain_flowFortanProgram('DualDomain_flow')   
            meger3dT_nonDenstiy=hybridFlow.readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meger3d,meger3dNonDensityVtkF)       

        
    meger3dTH=pv.read(meger3dVtkF)
    meger3dTH_nonDensity=pv.read(meger3dNonDensityVtkF)
    case=1
    if (case==1):
        radius=500.0        
        meger3dTH=hybridFlow.Density2Con(meger3dVtkF,meger3dTH,meger3dTH_nonDensity,alpha,largeDem,radius)
    else:
        meger3dTH=pv.read(meger3dVtkF)
    print("con OK")
    quit()

    case=1
    if (case==1):
        q123Folder=r'/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
        hybridFlow.particleTracking(noTestFrac,delta_x,delta_y,EpsgIn,EpsgOut,q123Folder,meger3dTH,dfnPoly,Kfracture,poro_fracture,aperture,Krock,poro_rock,matrix_alpha_w,Ksurface,poro_surface,surface_alpha_w,F1F2K,poro_F1F2K,F1F2K_alpha_w)




    print("Program END")

    '''
    # 測試PT 必定需要修改的程式
    '''
    case=0
    if (case==1):
        q1Stream=pv.read('Q1_stream.vtk')
        # print(q1Stream)
        # print(np.mean(q1Stream.points,axis=0))
        # plotter = pv.Plotter(off_screen=True)
        # plotter.add_mesh(q1Stream, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)
        # plotter.export_gltf('./q1Stream.gltf', rotate_scene=False,save_normals=True)
        # gltf = GLTF.load('./q1Stream.gltf', load_file_resources=True)
        # gltf.export('q1Stream.glb') 
        # os.system("rm q1Stream.gltf")
        # quit()

        meger3dTH=pv.read('meger3dTH.vtk')
        length=100.0

        
        nb=0
        q1EndPoint=q1Stream.points[nb]
        q1EndPoint=[554.97534,690.52313,-502.18243]
        cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
        q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
        q1EndCub.save('q1EndCub.vtk')
        quit()

        tryTime=0
        print("tryTime,q1EndPoint=",tryTime,q1EndPoint)
        while True:               
            if (tryTime>100):
                tryTime=tryTime+1         
                stream, src = meger3dTH.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                nb=len(stream.points)-1
                q1EndPoint=stream.points[nb]
                print("tryTime,q1EndPoint=",tryTime,q1EndPoint)
                if (tryTime==1):
                    total_stream=stream
                else:
                    total_stream=total_stream+stream
                    total_stream.save('total_stream.vtk')                
                if(q1EndPoint[2]>100.0):
                    break
                else:
                    oldtryTime=tryTime
                    while (tryTime-oldtryTime)<20:
                        tryTime=tryTime+1
                        cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
                        q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
                        stream, src = q1EndCub.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                        nb=len(stream.points)-1
                        q1EndPoint=stream.points[nb]
                        print("tryTime,Local q1EndPoint=",tryTime,q1EndPoint)
                        if (tryTime==1):
                            total_stream=stream
                        else:
                            total_stream=total_stream+stream
                            total_stream.save('total_stream.vtk')
                        if(q1EndPoint[2]>100.0):
                            break
            else:
                tryTime=tryTime+1
                cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
                q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
                stream, src = q1EndCub.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                nb=len(stream.points)-1
                q1EndPoint=stream.points[nb]
                print("tryTime,Local q1EndPoint=",tryTime,q1EndPoint)
                if (tryTime==1):
                    total_stream=stream
                else:
                    total_stream=total_stream+stream
                    total_stream.save('total_stream.vtk')
                if(q1EndPoint[2]>100.0):
                    break
        # plotter = pv.Plotter(off_screen=True)
        # plotter.add_mesh(stream, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
        # plotter.show(screenshot='stream.png')
        
        print("stream.points")
        print(stream.points)
        quit()

        
    

    







        


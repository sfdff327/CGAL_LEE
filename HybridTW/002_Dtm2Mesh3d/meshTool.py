from posix import RTLD_NODELETE
import numpy as np
import pyvista as pv

def profileBcNodeInterpolate(bcProfileFace,Grid,key,radius):
    points=np.zeros((len(bcProfileFace.points),3),dtype=np.float64)
    for i in range(0,len(bcProfileFace.points)):
        points[i]=[bcProfileFace.points[i][0],bcProfileFace.points[i][1],0.0]
    # face=bcProfileFace.faces
    
    poly=pv.PolyData(points)
    # polyInterpolated = poly.interpolate(Grid)
    polyInterpolated = poly.interpolate(Grid, radius=radius)
    # print(polyInterpolated[key])

    return polyInterpolated[key]
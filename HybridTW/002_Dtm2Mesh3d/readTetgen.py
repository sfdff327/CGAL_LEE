import pandas as pd
import numpy as np
import pyvista as pv
import vtk
import copy
import pyToTecplot


def determinant_3x3(m):
    return (m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
            m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]) +
            m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1]))


def subtract(a, b):
    return (a[0] - b[0],
            a[1] - b[1],
            a[2] - b[2])

# def tetrahedron_calc_volume(a, b, c, d):
    # return ((determinant_3x3((subtract(a, b),
    #                              subtract(b, c),
    #                              subtract(c, d),
    #                              ))) / 6.0)
def tetrahedron_calc_volume(a, b, c, d):
    x21=b[0]-a[0] # x2-x1
    x32=c[0]-b[0] #x3-x2
    x43=d[0]-c[0] #x4-x3

    y12=a[1]-b[1] #y1-y2
    y23=b[1]-c[1] #y2-y3
    y34=c[1]-d[1] #y3-y4

    z12=a[2]-b[2] #z1-z2
    z23=b[2]-c[2] #z2-z3
    z34=c[2]-d[2] #z3-z4
    vol=x21*(y23*z34-y34*z23)+x32*(y34*z12-y12*z34)+x43*(y12*z23-y23*z12)
    return vol

def checkTetrahedronOrientation(xyz,old_ele_ind):
    ele_ind=copy.deepcopy(old_ele_ind)
    for i in range(0,len(ele_ind)):
        # print('test #',i)
        p0=xyz[ele_ind[i][1]]
        p1=xyz[ele_ind[i][2]]
        p2=xyz[ele_ind[i][3]]
        p3=xyz[ele_ind[i][4]]
        determinant=tetrahedron_calc_volume(p0, p1, p2, p3)
        # print("NB=",i,"determinant=",determinant)
        if (determinant<0.0):
            temp=ele_ind[i][2]
            ele_ind[i][2]=ele_ind[i][3]
            ele_ind[i][3]=temp
            # print("change element orientation #",i,"determinant=",determinant)
            
            # p0=xyz[ele_ind[i][0]]
            # p1=xyz[ele_ind[i][1]]
            # p2=xyz[ele_ind[i][2]]
            # p3=xyz[ele_ind[i][3]]
            # v1 = p1 - p0
            # v2 = p2 - p0
            # v3 = p3 - p0
            # norm12 = np.cross(v1, v2)
            # determinant = np.dot(norm12, v3)       
            # print("#",i," new determinant=",determinant)     
        # if (i==2): quit()
    return ele_ind

def readNodeEle(file):

    '''
    # read NODE
    '''
    nb=-2
    Path = file+r'.1.node'
    with open(Path) as f:
        for line in f.readlines():
            s = line.split(' ')
            nb=nb+1
            if (nb==-1):
                no_node=int(s[0])
                print("no_node=",no_node)
                xyz=np.zeros((no_node,3),dtype=np.float64)
                break
        f.close()    

    tetgenNodeCsv=pd.read_csv(Path,sep='\\s+',header=None,skiprows=1,nrows=no_node)
    tetgenNodeCsv.columns=["no", "x","y","z","m","b"]
    # print("tetgenNodeCsv")
    # print(tetgenNodeCsv)
    # print(tetgenNodeCsv.columns)
    print("len(tetgenNodeCsv)=",len(tetgenNodeCsv))
    for i in range(0,len(tetgenNodeCsv)):
        ii=tetgenNodeCsv['no'][i]
        xyz[ii][0]=tetgenNodeCsv['x'][i]
        xyz[ii][1]=tetgenNodeCsv['y'][i]
        xyz[ii][2]=tetgenNodeCsv['z'][i]

    '''
    # read ELEMENT
    '''
    nb=-2
    Path = file+r'.1.ele'
    with open(Path) as f:
        for line in f.readlines():
            s = line.split(' ')
            nb=nb+1
            if (nb==-1):
                no_ele=int(s[0])
                print("no_ele=",no_ele)
                ele_ind=np.zeros((no_ele,5),dtype=np.int32)
                ele_Id=np.zeros(no_ele,dtype=np.int32)
                break
        f.close()    
    tetgenEleCsv=pd.read_csv(Path,sep='\\s+',header=None,skiprows=1,nrows=no_ele)
    tetgenEleCsv.columns=["no", "e0","e1","e2","e3","id"]    
    # print(tetgenEleCsv.columns)
    print("len(tetgenEleCsv)=",len(tetgenEleCsv))  
    for i in range(0,len(tetgenEleCsv)):
        ii=tetgenEleCsv['no'][i]
        ele_ind[ii]=(4,tetgenEleCsv["e0"][i],tetgenEleCsv["e1"][i],tetgenEleCsv["e2"][i],tetgenEleCsv["e3"][i])
        ele_Id[ii]=tetgenEleCsv["id"][i]
    ele_ind=np.array(ele_ind).ravel()
    celltypes = np.empty(len(tetgenEleCsv), dtype=np.uint32)
    celltypes[:] = vtk.VTK_TETRA
    mesh = pv.UnstructuredGrid(ele_ind, celltypes, xyz) 
    mesh['region']=ele_Id

    '''
    # read faces
    '''
    nb=-2
    Path = file+r'.1.face'
    with open(Path) as f:
        for line in f.readlines():
            s = line.split(' ')
            nb=nb+1
            if (nb==-1):
                no_face=int(s[0])
                print("no_face=",no_face)
                face_ind=np.zeros((no_face,4),dtype=np.int32)
                face_Id=np.zeros(no_face,dtype=np.int32)
                break
        f.close()  
    tetgenFaceCsv=pd.read_csv(Path,sep='\\s+',header=None,skiprows=1,nrows=no_face)
    tetgenFaceCsv.columns=["no", "e0","e1","e2","id"]   
     # print(tetgenFaceCsv.columns)
    print("len(tetgenFaceCsv)=",len(tetgenFaceCsv))  
    for i in range(0,len(tetgenFaceCsv)):
        face_ind[i]=(3,tetgenFaceCsv["e0"][i],tetgenFaceCsv["e1"][i],tetgenFaceCsv["e2"][i])
        face_Id[i]=tetgenFaceCsv["id"][i]
    face_ind=np.array(face_ind).ravel()
    celltypes = np.empty(len(tetgenFaceCsv), dtype=np.uint32)
    celltypes[:] = vtk.VTK_TRIANGLE
    face = pv.UnstructuredGrid(face_ind, celltypes, xyz) 
    face['region']=face_Id

    testPLOTfilename='merge3d.dat'
    pyToTecplot.TetrahedronUSG_zCenter(testPLOTfilename,mesh.points,mesh.cells.reshape(-1,5),mesh['region'])
    testPLOTfilename='mergeface2d.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,face.points,face.cells.reshape(-1,4),face['region'])

    return mesh,face

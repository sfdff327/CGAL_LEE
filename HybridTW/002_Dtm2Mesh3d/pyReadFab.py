import numpy as np
import pyvista as pv
import os
from pathlib import Path
import pickle
from scipy.io import FortranFile

import pyToTecplot

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def fabRead(noTestFrac):
    folder='/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes'
    FABResFileCheck = Path(folder+r'/no_frac.out')
    if FABResFileCheck.is_file():
        with open(folder+'/frac_XYZ.out', 'rb') as file:
            frac_XYZ = pickle.load(file)        
            file.close() 
        with open(folder+'/fracID.out', 'rb') as file:
            fracID = pickle.load(file)   
            file.close()     
        with open(folder+'/frac_ele_ind.out', 'rb') as file:
            frac_ele_ind = pickle.load(file) 
            file.close()           
        with open(folder+'/no_frac.out', 'rb') as file:
            no_frac = pickle.load(file)
            file.close()
        file=folder+'/frac_K_ap.bin'
        flowResFileCheck = Path(file)
        if (flowResFileCheck.is_file):
            f = FortranFile(file, 'r')
            frac_K = f.read_reals(dtype=np.float64)
            frac_ap = f.read_reals(dtype=np.float64)
            f.close()
    # print(np.max(fracID))
    # quit()
    
    no_frac=noTestFrac
    cells=[]
    frac_ele_k=[]
    frac_ele_ap=[]
    nodeCheck=np.zeros(len(frac_XYZ),dtype=np.int32)
    for i in range(0,len(fracID)):
        if (fracID[i]<no_frac):
            cells.append([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]])
            for j in range(0,3):
                nodeCheck[frac_ele_ind[i][j]]=1
            frac_ele_k.append(np.float64(frac_K[fracID[i]]))
            frac_ele_ap.append(np.float64(frac_ap[fracID[i]]))
    no_points=np.sum(nodeCheck)
    points=np.zeros((no_points,3),dtype=np.float64)
    nb=-1
    for i in range(0,len(frac_XYZ)):
        if (nodeCheck[i]==1):
            nb=nb+1
            points[nb]=frac_XYZ[i]
    no_cells=len(cells)
    cells=np.array(cells).ravel()             
    dfnOrUsg = pv.PolyData(points,cells) 
    dfnOrUsg=dfnOrUsg.compute_normals()
    frac_ele_k=np.array(frac_ele_k)
    frac_ele_ap=np.array(frac_ele_ap)
    dfnOrUsg['frac_ele_k']=frac_ele_k
    dfnOrUsg['frac_ele_ap']=frac_ele_ap
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(dfnOrUsg, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=True,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
    plotter.show(screenshot='dfnOrUsg.png')
    dfnOrUsg.save('dfnOrUsg.stl',binary=False)
    # print("dfnOrUsg")
    # print(dfnOrUsg)
    outputFileCheck = Path("output.obj")
    if outputFileCheck.is_file():
        os.system('rm output.obj') 
    os.system('./mesh_arrangement dfnOrUsg.stl')
    
    dfnPoly=pv.read("output.obj")
    # print(dfnPoly)
    xyz=dfnPoly.points
    ele_ind=dfnPoly.faces.reshape(-1,4)
    labels=np.ones(len(ele_ind),dtype=np.int32)
    dfnPoly['labels']=labels
    testPLOTfilename=r'dfnPoly.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,dfnPoly['labels'])  
    # print("fracID=",fracID)
    # print("len(fracID)=",len(fracID)) #len(fracID)= 18392
    # print("frac_ele_ind=",frac_ele_ind)
    # print("len(frac_ele_ind)=",len(frac_ele_ind)) #len(frac_ele_ind)= 18392
    dfnPoly.save('dfnPoly.vtk')
    return dfnPoly
    
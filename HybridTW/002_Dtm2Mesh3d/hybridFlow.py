import io
import numpy as np
import os
import pyvista as pv
import readGeoJSON
from pathlib import Path
import pickle
from scipy.io import FortranFile
import pyToTecplot
import vector
import math

def Circumcenter_new(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    print(x1,y1,z1)
    print(x2,y2,z2)
    print(x3,y3,z3)
    x12=x1-x2
    x21=x2-x1
    x13=x1-x3
    x31=x3-x1
    x23=x2-x3
    x32=x3-x2
    y12=y1-y2
    y21=y2-y1
    y13=y1-y3
    y31=y3-y1
    y23=y2-y3
    y32=y3-y2
    z12=z1-z2
    z21=z2-z1
    z13=z1-z3
    z31=z3-z1
    z23=z2-z3
    z32=z3-z2
    a=np.float64(0.0)
    b=np.float64(0.0)
    c=np.float64(0.0)

    crx,cry,crz=cross_V_new(x12,y12,z12,x23,y23,z23)  
    dis=(crx*crx+cry*cry+crz*crz)**0.5
    print(dis)
    dis=np.float64(2.0)*dis*dis
    print(dis)
    a=(x23*x23+y23*y23+z23*z23)*(x12*x13+y12*y13+z12*z13)/dis
    b=(x13*x13+y13*y13+z13*z13)*(x21*x23+y21*y23+z21*z23)/dis
    c=(x12*x12+y12*y12+z12*z12)*(x31*x32+y31*y32+z31*z32)/dis

    cx=a*x1+b*x2+c*x3
    cy=a*y1+b*y2+c*y3
    cz=a*z1+b*z2+c*z3

    return cx,cy,cz

def cross_V_new(x1,y1,z1,x2,y2,z2):
    x3=y1*z2-z1*y2
    y3=-x1*z2+z1*x2
    z3=x1*y2-y1*x2
    return x3,y3,z3



def pnt2line(A, B, C):
    d = (C - B) / np.sqrt(np.sum(np.power((C - B), 2.0)))
    v = A - B
    t = np.dot(v,d)
    P = B + t * d
    return np.sqrt(np.sum(np.power((P - A), 2.0))),P
    # line_vec = vector.vector(start, end)
    # pnt_vec = vector.vector(start, pnt)
    # line_len = vector.length(line_vec)
    # line_unitvec = vector.unit(line_vec)
    # pnt_vec_scaled = vector.scale(pnt_vec, 1.0/line_len)
    # t = vector.dot(line_unitvec, pnt_vec_scaled)    
    # if t < 0.0:
    #     t = 0.0
    # elif t > 1.0:
    #     t = 1.0
    # nearest = vector.scale(line_vec, t)
    # dist = vector.distance(nearest, pnt_vec)
    # nearest = vector.add(nearest, start)
    # return (dist, nearest)
def threeCoors2TwoD(p,c, oxyz,a, b,xDir,yDir):
    # p=inPartEdge.points[i]
    # print("inPartEdge.points[i]=",inPartEdge.points[i])
    '''new x'''
    xdist,nearest=pnt2line(p, c, oxyz)
    # print("i,xdist",i,xdist)
    # print("nearest=",nearest)
    # print("pnx=",)
    if(np.dot(xDir,nearest-p)<0.0):
        xdist=-xdist
    '''new y'''
    ydist,nearest=pnt2line(p, a, b)
    # print("i,ydist",i,ydist)
    # print("nearest=",nearest)
    # print("pny=",nearest-p)    
    if(np.dot(yDir,nearest-p)<0.0):
        ydist=-ydist
    xy=np.array([xdist,ydist,0.0])
    return xy


def DfnEqFlux(outfile_name,inPart,aperture,poro_fracture):

    center=inPart.center
    # print(center)
    size=inPart.compute_cell_sizes() 
    # print(size.array_names)
    # print(size['Area'])
    maxArea=np.max(size['Area'])
    maxAreaNB=np.where(size['Area']==maxArea)[0][0]


    inPart_xyz=inPart.points
    inPart_ele_ind=inPart.cells.reshape(-1,4)
    inPolyData=pv.PolyData(inPart_xyz,inPart_ele_ind)
    meanNormal=(np.mean(inPolyData.cell_normals,axis=0))

    '''
    起始參考點與直線方程
    '''
    a=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][1]])
    b=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][2]])
    c=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][3]])
    # print(a)
    # print(b)
    # print(c)
    dist,oxyz=pnt2line(c, a, b)
    # print("dist=",dist)
    # print("nearest=",oxyz)
    xDir=b-a
    yDir=np.array(oxyz-c,dtype=np.float64)

    '''
    # xDir與yDir 分別為3D完全截切體最大三角形的局部xy軸單位向量
    '''
    xDir=xDir/(np.sqrt(np.sum(np.power(xDir, 2.0))))
    yDir=yDir/(np.sqrt(np.sum(np.power(yDir, 2.0))))
    # print("ab=",xDir)
    # print("co=",yDir)


    center=inPart.center
    inPartEdge=inPart.extract_feature_edges(feature_angle=30, boundary_edges=True, non_manifold_edges=True, feature_edges=True, manifold_edges=True)

    '''    
    cxy=threeCoors2TwoD(inPartEdge.center,c, oxyz,a, b,xDir,yDir)
    print(inPartEdge.center)
    xy=np.zeros((len(inPartEdge.points),3),dtype=np.float64)
    for i in range(0,len(inPartEdge.points)):
        xy[i]=threeCoors2TwoD(inPartEdge.points[i],c, oxyz,a, b,xDir,yDir)
    print(xy)
    '''

    xyz=inPartEdge.points
    ele_ind=inPartEdge.lines.reshape(-1,3)
    vXYZ=inPartEdge['vectors']
    vol=inPartEdge['velocityLen']
    Tdis=inPartEdge.compute_cell_sizes() #計算每個線段長度
    # print(Tdis['Length'])

    # aperture=0.1
    Ur=np.zeros(len(ele_ind),dtype=np.float64)
    UrVector=np.zeros((len(ele_ind),3),dtype=np.float64)
    EqFlux=np.float64(0.0)
    for i in range(0,len(ele_ind)):
        # print("i=",i)
        e1=ele_ind[i][1]
        e2=ele_ind[i][2]
        a=xyz[e1]
        b=xyz[e2]
        p2Ldist,nearest=pnt2line(center, a, b)
        dir=nearest-center
        dir=dir/(np.sqrt(np.sum(np.power(dir, 2.0))))
        # print("dir Len",np.sqrt(np.sum(np.power(dir, 2.0))))
        # quit()

        # temp_maxV=np.float64(vol[e1]+vol[e2])/np.float64(2.0)
        temp_maxV_XYZ=[
            np.float64(vXYZ[e1][0]+vXYZ[e2][0])/np.float64(2.0),
            np.float64(vXYZ[e1][1]+vXYZ[e2][1])/np.float64(2.0),
            np.float64(vXYZ[e1][2]+vXYZ[e2][2])/np.float64(2.0)
        ]
        # tempVelLen=np.sum(np.power(temp_maxV_XYZ, 2.0))
        Ur[i]=np.dot(dir,temp_maxV_XYZ)/poro_fracture
        UrVector[i]=dir*Ur[i]
        # print("i,Ur=",i,Ur[i])
        EqFlux=EqFlux+abs(Ur[i]*aperture*Tdis['Length'][i])
        # print("Tdis=",Tdis['Length'][i])
        # print("i,EqFlux",i,Ur[i]*aperture*Tdis['Length'][i])

    # print(np.sum(Tdis['Length']))
    # print("aperture=",aperture)
    # print("EqFlux=",EqFlux)
    # inPartEdge=inPartEdge.cell_centers()
    inPartEdge['Ur']=Ur
    inPartEdge['UrVector']=UrVector
    inPartEdge.set_active_scalars('UrVector',preference='cell')
    xyz=inPartEdge.points
    ele_ind=inPartEdge.lines.reshape(-1,3)
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","vx(m/s)","vy(m/s)","vz(m/s)"'+'\n')
        f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=LINESEG\n') 
        f.write('varlocation=([1,2,3]=nodal,[4,5,6]=CELLCENTERED)\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0, len(UrVector)):
            f.write(str(UrVector[i][0])+'\n')
        for i in range(0, len(UrVector)):
            f.write(str(UrVector[i][1])+'\n')
        for i in range(0, len(UrVector)):
            f.write(str(UrVector[i][2])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')
        f.close()

    return inPartEdge,EqFlux

def particleTracking(noTestFrac,delta_x,delta_y,EpsgIn,EpsgOut,q123Folder,meger3dTH,dfnPoly,Kfracture,poro_fracture,aperture,Krock,poro_rock,matrix_alpha_w,Ksurface,poro_surface,surface_alpha_w,F1F2K,poro_F1F2K,F1F2K_alpha_w):

    '''讀取原始FAB與STL轉換後的vtk'''
    dfnFAB=pv.read(q123Folder+r'/dfnFAB.vtk')
    dh=pv.read(q123Folder+r'/dh.vtk')
    edz=pv.read(q123Folder+r'/edz.vtk')
    dt=pv.read(q123Folder+r'/dt.vtk')
    mt=pv.read(q123Folder+r'/mt.vtk')

    '''#計算每個vtk的中心點經緯度'''
    case=0
    if (case==1):
        min=np.min(dfnFAB.points,axis=0)
        max=np.max(dfnFAB.points,axis=0)
        x=(min[0]+max[0])/2.0-delta_x
        y=(min[1]+max[1])/2.0-delta_y
        x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        print('dfnFAB cxcy=', x84,y84)
        min=np.min(dh.points,axis=0)
        max=np.max(dh.points,axis=0)
        x=(min[0]+max[0])/2.0-delta_x
        y=(min[1]+max[1])/2.0-delta_y
        x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        print('dh cxcy=', x84,y84)
        min=np.min(edz.points,axis=0)
        max=np.max(edz.points,axis=0)
        x=(min[0]+max[0])/2.0-delta_x
        y=(min[1]+max[1])/2.0-delta_y
        x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        print('edz cxcy=', x84,y84)
        min=np.min(dt.points,axis=0)
        max=np.max(dt.points,axis=0)
        x=(min[0]+max[0])/2.0-delta_x
        y=(min[1]+max[1])/2.0-delta_y
        x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        print('dt cxcy=', x84,y84)

    '''#計算每個vtk物件，具有多少組不連接的元件'''
    noConnfrac=len(np.unique(dfnFAB['RegionId']))
    noConnDH=len(np.unique(dh['RegionId']))
    noConnedz=len(np.unique(edz['RegionId']))
    noConnDt=len(np.unique(dt['RegionId']))
    noConnMt=len(np.unique(mt['RegionId']))
    print("noConnDH=",noConnDH)
    print("noConnfrac=",noConnfrac)
    print("noConnedz=",noConnedz)
    print("noConnMt=",noConnMt)

    dt.extract_cells(dt["RegionId"] == 80).save('Q3DT.vtk')
    # quit()

    '''
    testPLOTfilename=r'dfnFAB.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,dfnFAB.points,dfnFAB.faces.reshape(-1,4),dfnFAB['RegionId'])
    testPLOTfilename=r'dh.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,dh.points,dh.faces.reshape(-1,4),dh['RegionId'])    
    testPLOTfilename=r'edz.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,edz.points,edz.faces.reshape(-1,4),edz['RegionId'])    
    testPLOTfilename=r'dt.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,dt.points,dt.faces.reshape(-1,4),dt['RegionId'])    
    quit()
    '''

    '''
    # 讀取Q123，每個物件交互關係
    holeInd==-1, 完全沒有交結
    # holeInd==0 代表部份貫穿
    # holeInd==1 代表完全貫穿
    '''
    dhfrac = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q1vtk/dh_frac.bin', dtype=np.int32).reshape(noConnDH, noConnfrac)
    dhedz = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q2vtk/dh_edz.bin', dtype=np.int32).reshape(noConnDH, noConnedz)
    dtfrac = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q3vtk/dt_frac.bin', dtype=np.int32).reshape(noConnDt, noConnfrac)

    terminal_speed=np.min(meger3dTH['velocityLen'])
    case=1
    if (case==1):
        '''#讀取Q1部份貫穿'''
        titleA=r'DH'
        titleB=r'Fracture'  
        print("titleA,titleB=",titleA,titleB)              
        partConnNBforDhFrac = np.where((dhfrac== 0))
        partConnNBforDhFrac=np.array(partConnNBforDhFrac).swapaxes(0,1)
        filename=r'Q1Part.csv'
        ToWriteConnNB(titleA,titleB,filename,partConnNBforDhFrac)
        '''#讀取Q1完全貫穿'''
        fullConnNBforDhFrac = np.where((dhfrac== 1))
        fullConnNBforDhFrac=np.array(fullConnNBforDhFrac).swapaxes(0,1)
        filename=r'Q1Full.csv'
        ToWriteConnNB(titleA,titleB,filename,fullConnNBforDhFrac)

        '''#測試單dh完全截切PT'''
        for i in range(0,len(fullConnNBforDhFrac)):
            if(fullConnNBforDhFrac[i][1]==16):
                testNB=i
                # if(i>27):break
                break
            
        sigleDhFracVtkF=q123Folder+r'/Q1vtk/'+'dh'+str(fullConnNBforDhFrac[testNB][0])+'frac'+str(fullConnNBforDhFrac[testNB][1])+'.vtk'
        print("sigleDhFracVtkF")
        print(sigleDhFracVtkF)
        inPart=pv.read(sigleDhFracVtkF)
        inPart=inPart.interpolate(meger3dTH, sharpness=2, radius=5.0, strategy='null_value', null_value=0.0, n_points=50, pass_cell_arrays=True, pass_point_data=True, progress_bar=False)
        inPart.save('inPart.vtk')

    
        dh.extract_cells(dh["RegionId"] == fullConnNBforDhFrac[testNB][0]).save('Q1DH.vtk')
        dfnFAB.extract_cells(dfnFAB["RegionId"] == fullConnNBforDhFrac[testNB][1]).save('Q1Frac.vtk')


        '''
        # inPart 邊界網格計算等效流率
        '''
        outfile_name=r'Q1UrVector.dat'
        inPartEdge,EqFlux=DfnEqFlux(outfile_name,inPart,aperture,poro_fracture)
        inPartEdge.save('Q1Ur.vtk')
        # quit()


        print("len(inPart['velocityLen'])=",len(inPart['velocityLen']))
        maxVelL=np.max(inPart['velocityLen'])
        print("maxVelL=",maxVelL)
        nb=np.max(np.where(inPart['velocityLen']==maxVelL))
        # print('Q1 Vel=')
        # print(inPart['velocityLen'])        
        print('Q1 maxVelL=',maxVelL)
        print('Q1 EqFlux=',EqFlux)        
        # print(inPart['velocityLen'])
        # print(nb)        
        print("inPart.points[nb]")
        print(inPart.points[nb])
        # quit()
        point=[inPart.points[nb][0],inPart.points[nb][1],inPart.points[nb][2]]
        pointsrc=pv.PolyData(point)
        # stream, src = meger3dTH.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(inPart.points[nb]),integration_direction ='forward',interpolator_type='point') 
        stream = meger3dTH.streamlines_from_source(pointsrc, vectors='vectors', integrator_type=45, integration_direction='forward', surface_streamlines=False, initial_step_length=0.5, step_unit='cl', min_step_length=1.0e-10, max_step_length=100.0, max_steps=2000, terminal_speed=terminal_speed, max_error=1e-06, max_time=None, compute_vorticity=True, rotation_scale=1.0, interpolator_type='point', progress_bar=False)
        stream.save('Q1_stream.vtk')
        distance=stream.compute_implicit_distance(dfnPoly, inplace=True)
        distance.save('Q1_distance.vtk')
        plotter = pv.Plotter(off_screen=True)
        plotter.add_mesh(stream, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
        # plotter.add_mesh(meger3dTH.extract_feature_edges(), style='wireframe', smooth_shading=True,show_scalar_bar=False,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
        plotter.show(screenshot='Q1stream.png')        

        onebuilding=dh.extract_cells(dh["RegionId"] == fullConnNBforDhFrac[testNB][0])
        oneClip=dfnFAB.extract_cells(dfnFAB["RegionId"] == fullConnNBforDhFrac[testNB][1])
        testPLOTfilename=r'Q1Building.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,onebuilding.points,onebuilding.cells.reshape(-1,4))
        testPLOTfilename=r'Q1Clip.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,oneClip.points,oneClip.cells.reshape(-1,4))
        testPLOTfilename=r'Q1Stream.dat'
        pyToTecplot.tecplotPyvistaStreamFromSrc(testPLOTfilename,stream)
        nb=len(stream.points)-1
        print('End point=',stream.points[nb])

        '''
        # min_implicit_distance 設定為與dfn最短距離
        '''
        min_implicit_distance=0.5

        Fr=0.0
        TimeR=0.0
        pathLen=0.0
        for i in range(0,len(stream.points)-1):
            dis=distance['implicit_distance'][i]   
            if (abs(dis)<=min_implicit_distance):
                loc_poro=poro_fracture
                alpha_w=1.0e0
                # print('abs(dis)<=10.0')
            else:
                region=stream['region'][i]
                if (region==1):
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
                elif (region==2):
                    loc_poro=poro_surface
                    alpha_w=surface_alpha_w
                elif (region==3 or region==4 or region==5 or region==6):
                    loc_poro=poro_F1F2K
                    alpha_w=F1F2K_alpha_w
                else:
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
            # PtTime=stream['IntegrationTime'][i+1]-stream['IntegrationTime'][i]
            diffxyz=np.array([stream.points[i][0]-stream.points[i+1][0],stream.points[i][1]-stream.points[i+1][1],stream.points[i][2]-stream.points[i+1][2]])
            vLen=np.linalg.norm(stream['vectors'][i])
            TLen=np.linalg.norm(diffxyz)
            PtTime=TLen/vLen
            Fr=Fr+alpha_w*PtTime  #(vLen*loc_poro)
            TimeR=TimeR+PtTime
            pathLen=pathLen+TLen

            # if (region==1):
            #     f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')
            # elif (region==2):
            #     f.write(str(Ksurface)+' '+str(Ksurface)+' '+str(Ksurface)+' '+str(poro_surface)+'\n')
            # elif (region==3 or region==4 or region==5 or region==6):
            #     f.write(str(F1F2K)+' '+str(F1F2K)+' '+str(F1F2K)+' '+str(poro_F1F2K)+'\n')
            # else:
            #     f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')

        print("Q1 Fr=",Fr)
        print("Q1 TimeR=",TimeR)
        print("Q1 pathLen=",pathLen)


        # quit()



        # '''#展示單一個Q1成果'''
        # sigleDhFracVtkF=q123Folder+r'/Q1vtk/'+r'OBJ_dh'+str(connNBforDhFrac[0][0])+'frac'+str(connNBforDhFrac[1][0])+'.vtk'
        # print(sigleDhFracVtkF)
    

    case=1
    if (case==1):
        '''#讀取Q2部份貫穿'''
        titleA=r'DH'
        titleB=r'Edz'  
        print("titleA,titleB=",titleA,titleB)              
        partConnNBforDhEdz = np.where((dhedz== 0))
        partConnNBforDhEdz=np.array(partConnNBforDhEdz).swapaxes(0,1)
        filename=r'Q2Part.csv'
        ToWriteConnNB(titleA,titleB,filename,partConnNBforDhEdz)
        '''#讀取Q2完全貫穿'''
        fullConnNBforDhEdz = np.where((dhedz== 1))
        fullConnNBforDhEdz=np.array(fullConnNBforDhEdz).swapaxes(0,1)
        filename=r'Q2Full.csv'
        ToWriteConnNB(titleA,titleB,filename,fullConnNBforDhEdz)

        '''#測試單dh完全截切PT'''
        for i in range(0,len(fullConnNBforDhEdz)):
            if(fullConnNBforDhEdz[i][0]==508):
                testNB=i
                break
            
        sigleDhFracVtkF=q123Folder+r'/Q2vtk/'+'dh'+str(fullConnNBforDhEdz[testNB][0])+'edz'+str(fullConnNBforDhEdz[testNB][1])+'.vtk'
        print("sigleDhFracVtkF")
        print(sigleDhFracVtkF)
        inPart=pv.read(sigleDhFracVtkF)
        inPart=inPart.interpolate(meger3dTH, sharpness=2, radius=5.0, strategy='null_value', null_value=0.0, n_points=50, pass_cell_arrays=True, pass_point_data=True, progress_bar=False)
        # inPart.save('inPart.vtk')

        dh.extract_cells(dh["RegionId"] == fullConnNBforDhEdz[testNB][0]).save('Q2DH.vtk')
        edz.extract_cells(edz["RegionId"] == fullConnNBforDhEdz[testNB][1]).save('Q2EDZ.vtk')        

        '''
        # inPart 邊界網格計算等效流率
        '''
        outfile_name=r'Q2UrVector.dat'
        profileLen=0.3
        inPartEdge,EqFlux=DfnEqFlux(outfile_name,inPart,profileLen,poro_fracture)
        inPartEdge.save('Q2Ur.vtk')

        print("len(inPart['velocityLen'])=",len(inPart['velocityLen']))
        maxVelL=np.max(inPart['velocityLen'])
        nb=np.max(np.where(inPart['velocityLen']==maxVelL))
        # print('Q2 Vel=')
        # print(inPart['velocityLen'])
        print('Q2 maxVelL=',maxVelL)
        print('Q2 EqFlux=',EqFlux)         
        # print(inPart['velocityLen'])
        # print(nb)        
        print("inPart.points[nb]")
        print(inPart.points[nb])
        # quit()
        point=inPart.points[nb]
        pointsrc=pv.PolyData(point)
        stream, src = meger3dTH.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0,source_center=(inPart.points[nb]),integration_direction ='forward',interpolator_type='cell') 
        stream.save('Q2_stream.vtk')
        distance=stream.compute_implicit_distance(dfnPoly, inplace=True)
        distance.save('Q2_distance.vtk')

        onebuilding=dh.extract_cells(dh["RegionId"] == fullConnNBforDhEdz[testNB][0])
        oneClip=edz.extract_cells(edz["RegionId"] == fullConnNBforDhEdz[testNB][1])
        testPLOTfilename=r'Q2Building.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,onebuilding.points,onebuilding.cells.reshape(-1,4))
        testPLOTfilename=r'Q2Clip.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,oneClip.points,oneClip.cells.reshape(-1,4))
        testPLOTfilename=r'Q2Stream.dat'
        pyToTecplot.tecplotPyvistaStream(testPLOTfilename,stream, src)

        Fr=0.0
        TimeR=0.0
        pathLen=0.0
        for i in range(0,len(stream.points)-1):
            dis=distance['implicit_distance'][i]   
            if (abs(dis)<=min_implicit_distance):
                loc_poro=poro_fracture
                alpha_w=1.0e0
                # print('abs(dis)<=10.0')
            else:
                region=stream['region'][i]
                if (region==1):
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
                elif (region==2):
                    loc_poro=poro_surface
                    alpha_w=surface_alpha_w
                elif (region==3 or region==4 or region==5 or region==6):
                    loc_poro=poro_F1F2K
                    alpha_w=F1F2K_alpha_w
                else:
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
            # PtTime=stream['IntegrationTime'][i+1]-stream['IntegrationTime'][i]
            diffxyz=np.array([stream.points[i][0]-stream.points[i+1][0],stream.points[i][1]-stream.points[i+1][1],stream.points[i][2]-stream.points[i+1][2]])
            vLen=np.linalg.norm(stream['vectors'][i])
            TLen=np.linalg.norm(diffxyz)
            PtTime=TLen/vLen
            Fr=Fr+alpha_w*PtTime  #(vLen*loc_poro)
            TimeR=TimeR+PtTime
            pathLen=pathLen+TLen
            # print("i,alpha_w,Fr,TimeR=",i,alpha_w)

        print("Q2 Fr=",Fr)
        print("Q2 TimeR=",TimeR)
        print("Q2 pathLen=",pathLen)


    case=1
    if (case==1):
        '''#讀取Q3部份貫穿'''
        titleA=r'DT'
        titleB=r'Fracture'  
        print("titleA,titleB=",titleA,titleB)              
        partConnNBforDtFrac = np.where((dtfrac== 0))
        partConnNBforDtFrac=np.array(partConnNBforDtFrac).swapaxes(0,1)
        filename=r'Q3Part.csv'
        ToWriteConnNB(titleA,titleB,filename,partConnNBforDtFrac)
        '''#讀取Q3完全貫穿'''
        fullConnNBforDtFrac = np.where((dtfrac== 1))
        fullConnNBforDtFrac=np.array(fullConnNBforDtFrac).swapaxes(0,1)
        filename=r'Q3Full.csv'
        ToWriteConnNB(titleA,titleB,filename,fullConnNBforDtFrac)


        '''#測試單dh完全截切PT'''
        for i in range(0,len(fullConnNBforDtFrac)):
            if(fullConnNBforDtFrac[i][0]==80 and fullConnNBforDtFrac[i][1]==16):
                testNB=i
                break
            
        sigleDhFracVtkF=q123Folder+r'/Q3vtk/'+'dt'+str(fullConnNBforDtFrac[testNB][0])+'frac'+str(fullConnNBforDtFrac[testNB][1])+'.vtk'
        print("sigleDhFracVtkF")
        print(sigleDhFracVtkF)
        inPart=pv.read(sigleDhFracVtkF)
        inPart=inPart.interpolate(meger3dTH, sharpness=2, radius=5.0, strategy='null_value', null_value=0.0, n_points=50, pass_cell_arrays=True, pass_point_data=True, progress_bar=False)
        # inPart.save('inPart.vtk')



        dfnFAB.extract_cells(dfnFAB["RegionId"] == fullConnNBforDtFrac[testNB][1]).save('Q3Frac.vtk')

        '''
        # inPart 邊界網格計算等效流率
        '''
        outfile_name=r'Q3UrVector.dat'
        inPartEdge,EqFlux=DfnEqFlux(outfile_name,inPart,aperture,poro_fracture)
        inPartEdge.save('Q3Ur.vtk')

        print("len(inPart['velocityLen'])=",len(inPart['velocityLen']))
        maxVelL=np.max(inPart['velocityLen'])
        nb=np.max(np.where(inPart['velocityLen']==maxVelL))
        print('Q3 maxVelL=',maxVelL)
        print('Q3 EqFlux=',EqFlux)         
        # print(inPart['velocityLen'])
        # print(nb)        
        print("inPart.points[nb]")
        print(inPart.points[nb])
        # quit()
        point=inPart.points[nb]
        pointsrc=pv.PolyData(point)
        stream, src = meger3dTH.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0,source_center=(inPart.points[nb]),integration_direction ='forward',interpolator_type='cell') 
        stream.save('Q3_stream.vtk')
        distance=stream.compute_implicit_distance(dfnPoly, inplace=True)
        distance.save('Q3_distance.vtk')

        onebuilding=dt.extract_cells(dt["RegionId"] == fullConnNBforDtFrac[testNB][0])
        oneClip=dfnFAB.extract_cells(dfnFAB["RegionId"] == fullConnNBforDtFrac[testNB][1])
        testPLOTfilename=r'Q3Building.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,onebuilding.points,onebuilding.cells.reshape(-1,4))
        testPLOTfilename=r'Q3Clip.dat'
        pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,oneClip.points,oneClip.cells.reshape(-1,4))
        testPLOTfilename=r'Q3Stream.dat'
        pyToTecplot.tecplotPyvistaStream(testPLOTfilename,stream, src)

        Fr=0.0
        TimeR=0.0
        pathLen=0.0
        for i in range(0,len(stream.points)-1):
            dis=distance['implicit_distance'][i]   
            if (abs(dis)<=min_implicit_distance):
                loc_poro=poro_fracture
                alpha_w=1.0e0
                # print('abs(dis)<=10.0')
            else:
                region=stream['region'][i]
                if (region==1):
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
                elif (region==2):
                    loc_poro=poro_surface
                    alpha_w=surface_alpha_w
                elif (region==3 or region==4 or region==5 or region==6):
                    loc_poro=poro_F1F2K
                    alpha_w=F1F2K_alpha_w
                else:
                    loc_poro=poro_rock
                    alpha_w=matrix_alpha_w
            # PtTime=stream['IntegrationTime'][i+1]-stream['IntegrationTime'][i]
            diffxyz=np.array([stream.points[i][0]-stream.points[i+1][0],stream.points[i][1]-stream.points[i+1][1],stream.points[i][2]-stream.points[i+1][2]])
            vLen=np.linalg.norm(stream['vectors'][i])
            TLen=np.linalg.norm(diffxyz)
            PtTime=TLen/vLen
            Fr=Fr+alpha_w*PtTime  #(vLen*loc_poro)
            TimeR=TimeR+PtTime
            pathLen=pathLen+TLen

        print("Q3 Fr=",Fr)
        print("Q3 TimeR=",TimeR)
        print("Q3 pathLen=",pathLen)

        '''#展示單一個Q3成果'''
        # sigleDtFracVtkF=q123Folder+r'/Q3vtk/'+r'OBJ_dt'+str(connNBforDtFrac[0][0])+'frac'+str(connNBforDtFrac[1][0])+'.vtk'
        # single_DT=dt.extract_cells(dt["RegionId"] == connNBforDtFrac[0][0])
        # single_Frac=dfnFAB.extract_cells(dfnFAB["RegionId"] == connNBforDtFrac[1][0])
        # sigleDtFrac=pv.read(sigleDtFracVtkF)
        # single_DT.save('single_DT.vtk')
        # single_Frac.save('single_Frac.vtk')
        # sigleDtFrac.save('sigleDtFrac.vtk')


    return 0

def Density2Con(meger3dVtkF,meger3dTH,meger3dTH_nonDensity,alpha,largeDem,radius):

    # profileBcNodeInterpolate(bcProfileFace,largeDem,'z',radius)
    points=np.zeros((len(meger3dTH.points),3),dtype=np.float64)
    points[:,0:2]=meger3dTH.points[:,0:2]
    poly=pv.PolyData(points)

    Topz= poly.interpolate(largeDem, radius=radius)
    Botz=meger3dTH.points[:,2:3]
    # print("Topz['z']")
    # print(Topz['z'][:])
    # print(Topz['z'].shape)
    # print("Botz")
    # print(Botz[:])
    # print(Botz.shape)
    
    detaz=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    detaTh=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    sa=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    nb=0
    for i in range(0,len(meger3dTH['totalHead'])):
        detaz[i]=Topz['z'][i]-Botz[i]        
        detaTh[i]=meger3dTH['totalHead'][i]-meger3dTH_nonDensity['totalHead'][i]-Topz['z'][i]+meger3dTH.points[i][2]
        if (detaTh[i]>0.0):
            nb=nb+1
            sa[i]=detaTh[i]/detaz[i]/alpha
            # print("i,dz,dTh,TH,nTH,Tz,sa[i]=",i,' ',detaz[i],' ',detaTh[i],meger3dTH['totalHead'][i],' ',meger3dTH_nonDensity['totalHead'][i],' ',Topz['z'][i],' ',sa[i])

    meger3dTH['detaz']=detaz
    meger3dTH['detaTh']=detaTh
    meger3dTH['sa']= sa
    print("meger3dTH['sa'] OK")
    meger3dTH.save('meger3dCon.vtk')

    xyz=meger3dTH.points
    ele_ind=meger3dTH.cells.reshape(-1,5)
    testPLOTfilename=r'meger3dCon.dat'
    no_node = len(xyz)
    no_ele = len(ele_ind)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Th(m)","vx","vy","vz","dTh(m)","dz(m)","sa"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEPOINT,ET=TETRAHEDRON\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(meger3dTH['totalHead'][i])+' '+str(meger3dTH['vectors'][i][0])+' '+str(meger3dTH['vectors'][i][1])+' '+str(meger3dTH['vectors'][i][2])+' '+str(meger3dTH['detaTh'][i])+' '+str(meger3dTH['detaz'][i])+' '+str(meger3dTH['sa'][i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()

    return 0

def ToWriteConnNB(titleA,titleB,filename,partConnNBforDhFrac):
    import pandas as pd
    # partConnNBforDhFrac=np.array(partConnNBforDhFrac).swapaxes(0,1)
    # print(type(partConnNBforDhFrac))
    # print(partConnNBforDhFrac.shape)
    # print(partConnNBforDhFrac[0])
    df = pd.DataFrame(partConnNBforDhFrac, columns=[titleA,titleB])
    df.to_csv(filename, columns=[titleA,titleB], index = False)
    return 0

def readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meger3d,meger3dVtkF):
    # print(meger3d.array_names)
    # quit()
    file=r'flowTH.bin'
    flowTHPath = Path(file)
    if flowTHPath.is_file():            
        f = FortranFile(file, 'r')
        TH = f.read_reals(dtype=np.float64)
        vx = f.read_reals(dtype=np.float64)
        vy = f.read_reals(dtype=np.float64)
        vz = f.read_reals(dtype=np.float64)
        f.close()
        vectors=np.column_stack((vx,vy,vz))
        meger3d['vectors']=vectors
        meger3d['velocityLen']=np.linalg.norm(vectors,axis=1)
        meger3d['totalHead']=TH
        # print(meger3d.array_names)
        meger3d=meger3d.cell_data_to_point_data(pass_cell_data = True)
        plotter = pv.Plotter(off_screen=True)
        plotter.add_mesh(meger3d, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
        plotter.show(screenshot='meger3dTH.png') 
        meger3d.save(meger3dVtkF)
        min=np.min(meger3d.points,axis=0)
        max=np.max(meger3d.points,axis=0)
        x=(min[0]+max[0])/2.0-delta_x
        y=(min[1]+max[1])/2.0-delta_y
        x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        print('meger3d cxcy=', x84,y84)


    return meger3d

def runDualDomain_flowFortanProgram(programName):
    os.system(r'./'+programName)

def writeInputDensity(density,meger3d,megerFace,meger3dClipTop,bcProfileFace,Kfracture,poro_fracture,Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K):

    # print("array_names")
    # print(meger3d.array_names)
    # quit()
    xyz=meger3d.points
    ele_ind=meger3d.cells.reshape(-1,5)
    no_node=len(xyz)
    no_ele=len(ele_ind)
    print('start FEM_AGMG_HK')
    with open('FEM_AGMG_HK.dat','w',encoding = 'utf-8') as f:
        for i in range(0,no_ele):
            region=meger3d['region'][i]
            if (region==1):
                f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')
            elif (region==2):
                f.write(str(Ksurface)+' '+str(Ksurface)+' '+str(Ksurface)+' '+str(poro_surface)+'\n')
            elif (region==3 or region==4 or region==5 or region==6):
                f.write(str(F1F2K)+' '+str(F1F2K)+' '+str(F1F2K)+' '+str(poro_F1F2K)+'\n')
            else:
                f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')
        f.close()
    # quit()        

    '''
    # print MESH
    '''
    print('start FEM_AGMG_MESH')
    with open('FEM_AGMG_MESH.dat','w',encoding = 'utf-8') as f:
        f.write(str(no_node)+' '+str(no_ele)+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()




    print('start FEM_fracture_ele_HK')
    profileRegion=np.max(megerFace['region'])
    nodeNB=np.linspace(0,len(megerFace.points),num=len(megerFace.points),endpoint=False,dtype=np.int32)
    megerFace['nodeNB']=nodeNB

    def extract_node(mesh,key,node):
        idx = mesh[key] == node
        return idx
    
    idx=extract_node(megerFace,'region',profileRegion)
    dfnFace=megerFace.extract_cells(idx)    
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(dfnFace, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
    plotter.show(screenshot='dfnFace.png')  
    # quit()   

    dfn_ele_ind=dfnFace.cells.reshape(-1,4)
    print("dfn_ele_ind[0]")
    print(dfn_ele_ind[0])
    no_fracture=len(dfn_ele_ind)
    no_fracture_ele=no_fracture
    no_face_type=6
    with open('FEM_fracture_ele_HK.dat','w',encoding = 'utf-8') as f:
        f.write(str(no_fracture)+' '+str(no_face_type)+'\n')
        for i in range(0,no_fracture):
            f.write(str(i+1)+'\n')
        f.write(str(no_fracture_ele)+'\n')
        for i in range(0,no_fracture_ele):
            e0=dfnFace['nodeNB'][dfn_ele_ind[i][1]]+1
            e1=dfnFace['nodeNB'][dfn_ele_ind[i][2]]+1
            e2=dfnFace['nodeNB'][dfn_ele_ind[i][3]]+1
            f.write(str(e0)+' '+str(e1)+' '+str(e2)+' '+str(Kfracture)+' '+str(i+1)+' '+str(poro_fracture)+'\n')
        f.close()
    # quit()

    print('start FEM_AGMG_BC1_inf')
    print("density=",density)
    ni=0
    with open('FEM_AGMG_BC1_inf.dat','w',encoding = 'utf-8') as f:
        for i in range(0,len(meger3dClipTop['nodeNB'])):
            f.write(str(meger3dClipTop['nodeNB'][i]+1)+' '+str(meger3dClipTop.points[i][2])+'\n')
        for i in range(0,len(bcProfileFace['nodeNB'])):
            if (abs(bcProfileFace['z'][i]-bcProfileFace.points[i][2])>1e-5):
                Th=density*(bcProfileFace['z'][i]-bcProfileFace.points[i][2])+bcProfileFace.points[i][2]
                f.write(str(bcProfileFace['nodeNB'][i]+1)+' '+str(Th)+'\n')            
        f.close()
    with open('FEM_AGMG_BC1_inf_nonDensity.dat','w',encoding = 'utf-8') as f:
        for i in range(0,len(meger3dClipTop['nodeNB'])):
            f.write(str(meger3dClipTop['nodeNB'][i]+1)+' '+str(meger3dClipTop.points[i][2])+'\n')
        for i in range(0,len(bcProfileFace['nodeNB'])):
            if (abs(bcProfileFace['z'][i]-bcProfileFace.points[i][2])>1e-5):
                Th=1.0*(bcProfileFace['z'][i]-bcProfileFace.points[i][2])+bcProfileFace.points[i][2]
                f.write(str(bcProfileFace['nodeNB'][i]+1)+' '+str(Th)+'\n')
                # ni=ni+1
                # if (ni>5): quit()            
        f.close()        
    # quit()

    return 0


import os
from pathlib import Path
import pyvista as pv
import meshpy.triangle as triangle
import numpy as np
import pandas as pd

import readGeoJSON
import pyToTecplot
import readTetgen

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def profileBcNodeInterpolate(bcProfileFace,Grid,key,radius):
    points=np.zeros((len(bcProfileFace.points),3),dtype=np.float64)
    # for i in range(0,len(bcProfileFace.points)):
    #     points[i]=[bcProfileFace.points[i][0],bcProfileFace.points[i][1],0.0]
    # face=bcProfileFace.faces
    points[:,0:2]=bcProfileFace.points[:,0:2]
    
    poly=pv.PolyData(points)
    # polyInterpolated = poly.interpolate(Grid)
    polyInterpolated = poly.interpolate(Grid, radius=radius)
    # print(polyInterpolated[key])

    return polyInterpolated[key]

def findMegerProfileBc(megerFace,largeDem,radius):
    profileRegion=np.max(megerFace['region'])-1
    # print(profileRegion)

    nodeNB=np.linspace(0,len(megerFace.points),num=len(megerFace.points),endpoint=False,dtype=np.int32)
    megerFace['nodeNB']=nodeNB

    def extract_node(mesh,key,node):
        idx = mesh[key] == node
        return idx
    
    idx=extract_node(megerFace,'region',profileRegion)
    bcProfileFace=megerFace.extract_cells(idx)
    # quit()
    # radius=smallMtr*1.0
    valueInterpolated=profileBcNodeInterpolate(bcProfileFace,largeDem,'z',radius)
    bcProfileFace['z']=valueInterpolated
    print('bcProfileFace.array_names=',bcProfileFace.array_names)
    bcProfileFace.save('bcProfileFace.vtk')
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(bcProfileFace, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
    plotter.show(screenshot='bcProfileFace.png')  

    return bcProfileFace


def findMegerTopBc(meger3d,topPoly3d):
    nodeNB=np.linspace(0,len(meger3d.points),num=len(meger3d.points),endpoint=False,dtype=np.int32)
    meger3d['nodeNB']=nodeNB
    meger3dClipTop=meger3d.clip_surface(topPoly3d,invert=True,compute_distance = True)
    meger3dClipTop=meger3dClipTop.extract_points(abs(meger3dClipTop['implicit_distance'][:]) < 1.0)
    '''
    # 須注意，meger3dClipTop是包含每個完整網格，因此會有些節點>1.0
    '''
    points=[]
    nodeNB=[]
    nb=-1
    for i in range(0,len(meger3dClipTop.points)):
        if (abs(meger3dClipTop['implicit_distance'][i])<1.0):
            points.append([meger3dClipTop.points[i][0],meger3dClipTop.points[i][1],meger3dClipTop.points[i][2]])
            nodeNB.append(meger3dClipTop['nodeNB'][i])
    points=np.array(points)
    nodeNB=np.array(nodeNB)
    meger3dClipTop=pv.PolyData(points)   
    meger3dClipTop['nodeNB']=nodeNB
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(meger3dClipTop, style='points',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
    plotter.show(screenshot='meger3dClipTop.png')  
    meger3dClipTop.save('meger3dClipTop.vtk')  

    return meger3dClipTop

def findBCele(surfEdge):
    points2d=[]
    point_attributes=[]
    for i in range(0,len(surfEdge.points)):
        points2d.append((surfEdge.points[i][0],surfEdge.points[i][1]))
        point_attributes.append(i)

    lines2D=surfEdge.lines.reshape((-1, 3))
    # print(len(lines2D))
    facets=[]
    for i in range(0,len(lines2D)):
        # print(lines2D[i])
        facets.append((lines2D[i][1],lines2D[i][2]))
    # print(facets)
    info = triangle.MeshInfo()
    info.set_points(points2d,point_markers=point_attributes)
    # info.set_points(points2d)
    info.set_facets(facets)

    mesh = triangle.build(info,attributes=True, quality_meshing=False)   
    # print(len(mesh.points))    
    # print(len(mesh.elements))
    # print(len(mesh.neighbors))
    return mesh.points,mesh.elements,mesh.neighbors    

def surToTetgen(case,depthSurfaceSoil,demClip3dsurf,fault12,dfnPoly):
    STLminZ=np.min(fault12.points,axis=0)[2]
    print("STLminZ=",STLminZ)
    surfEdge=demClip3dsurf.extract_all_edges()

    meshxyz,meshele_ind,meshneighbors=findBCele(surfEdge)


    '''
    # 設定mesh no_node
    '''
    layer=2
    points=np.zeros((len(meshxyz)*(layer+1),3),dtype=np.float64)
    '''Top'''
    for i in range(0,len(meshxyz)):
        points[i]=[meshxyz[i][0],meshxyz[i][1],surfEdge['z'][i]]
    '''depthSurfaceSoil'''
    no_node2d=len(meshxyz)
    no_ele2d=len(meshele_ind)
    j=0
    for i in range(0,len(meshxyz)):
        nb=no_node2d+i+j*len(meshxyz)
        points[nb]=[meshxyz[i][0],meshxyz[i][1],depthSurfaceSoil]
    '''Bot'''
    j=1
    for i in range(0,len(meshxyz)):
        nb=no_node2d+i+j*len(meshxyz)
        points[nb]=[meshxyz[i][0],meshxyz[i][1],STLminZ]
    print("points OK")


    order2D=creatOrder2D()
    faces=[]
    for i in range(0,no_ele2d):
        faces.extend(np.int32((3,meshele_ind[i][0],meshele_ind[i][1],meshele_ind[i][2])))
    for j in range(0,layer):
        for i in range(0,no_ele2d):
            e0=np.int32(meshele_ind[i][2]+(no_node2d*(j+1)))
            e1=np.int32(meshele_ind[i][1]+(no_node2d*(j+1)))
            e2=np.int32(meshele_ind[i][0]+(no_node2d*(j+1)))
            # faces.append(np.int32((3,e0,e1,e2)))
            faces.extend(np.int32((3,e0,e1,e2)))
            # ii=len(faces)-1
            # print("TEST faces[",ii,"]=",ii,faces[ii])
        # for i in range(0,len(meshneighbors)): #測試沒有剖面網格
        #     if -1 in meshneighbors[i]:
        #         for k in range(0,3):
        #             if (meshneighbors[i][k]==-1):
        #                 n0=order2D[k][0]
        #                 n1=order2D[k][1]
        #                 e0=meshele_ind[i][n0]+(no_node2d*(j+0))
        #                 e1=meshele_ind[i][n1]+(no_node2d*(j+0))
        #                 e2=meshele_ind[i][n1]+(no_node2d*(j+1))
        #                 e3=meshele_ind[i][n0]+(no_node2d*(j+1))
        #                 # faces.append(np.int32((4,e0,e1,e2,e3)))
        #                 faces.extend(np.int32((4,e0,e1,e2,e3)))
    print("faces OK")
    
    poly3d = pv.PolyData(points, faces)
    poly3d=poly3d.triangulate()
    poly3d.save('poly3d.vtk')
    poly3d.save('poly3d.stl')

    # xyz=poly3d.points
    # ele_ind=poly3d.faces.reshape((-1, 4))
    # with open('solidMesh.dat','w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
    #     for i in range(0,len(xyz)):
    #         f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
    #     for i in range(0,len(ele_ind)):
    #         f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
    #     f.close()
    # quit()

    '''
    建立top表面網格
    '''
    points=np.zeros((len(meshxyz),3),dtype=np.float64)
    for i in range(0,len(meshxyz)):
        points[i]=[meshxyz[i][0],meshxyz[i][1],surfEdge['z'][i]+1.0]
    faces=[]
    for i in range(0,no_ele2d):
        faces.extend(np.int32((3,meshele_ind[i][0],meshele_ind[i][1],meshele_ind[i][2])))    
    topPoly3d = pv.PolyData(points, faces)
    topPoly3d=topPoly3d.triangulate()
    topPoly3d['z']=topPoly3d.points[:,2:3]
    topPoly3d.set_active_scalars('z')
    topPoly3d.save('topPoly3d.vtk')
    topPoly3dxyz=topPoly3d.points
    topPoly3dele_ind=topPoly3d.faces.reshape((-1, 4))
    # with open('topMesh.dat','w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(topPoly3dxyz))+',E='+str(len(topPoly3dele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
    #     for i in range(0,len(topPoly3dxyz)):
    #         f.write(str(topPoly3dxyz[i][0])+' '+str(topPoly3dxyz[i][1])+' '+str(topPoly3dxyz[i][2])+'\n')
    #     for i in range(0,len(topPoly3dele_ind)):
    #         f.write(str(topPoly3dele_ind[i][1]+1)+' '+str(topPoly3dele_ind[i][2]+1)+' '+str(topPoly3dele_ind[i][3]+1)+'\n')
    #     f.close()    

    '''
    # 建立側邊網格
    '''
    points=np.zeros((len(meshxyz)*(layer+1),3),dtype=np.float64)
    '''Top'''
    for i in range(0,len(meshxyz)):
        points[i]=[meshxyz[i][0],meshxyz[i][1],surfEdge['z'][i]]
    '''depthSurfaceSoil'''
    no_node2d=len(meshxyz)
    no_ele2d=len(meshele_ind)
    j=0
    for i in range(0,len(meshxyz)):
        nb=no_node2d+i+j*len(meshxyz)
        points[nb]=[meshxyz[i][0],meshxyz[i][1],depthSurfaceSoil]
    '''Bot'''
    j=1
    for i in range(0,len(meshxyz)):
        nb=no_node2d+i+j*len(meshxyz)
        points[nb]=[meshxyz[i][0],meshxyz[i][1],STLminZ]
    # print("points OK") 

    faces=[]
    # for i in range(0,no_ele2d):
        # faces.extend(np.int32((3,ele_ind[i][0],ele_ind[i][1],ele_ind[i][2])))
    for j in range(0,layer):
        # for i in range(0,no_ele2d):
        #     e0=np.int32(ele_ind[i][2]+(no_node2d*(j+1)))
        #     e1=np.int32(ele_ind[i][1]+(no_node2d*(j+1)))
        #     e2=np.int32(ele_ind[i][0]+(no_node2d*(j+1)))
            # faces.append(np.int32((3,e0,e1,e2)))
            # faces.extend(np.int32((3,e0,e1,e2)))
            # ii=len(faces)-1
            # print("TEST faces[",ii,"]=",ii,faces[ii])
        for i in range(0,len(meshneighbors)):
            if -1 in meshneighbors[i]:
                for k in range(0,3):
                    if (meshneighbors[i][k]==-1):
                        n0=order2D[k][0]
                        n1=order2D[k][1]
                        e0=meshele_ind[i][n0]+(no_node2d*(j+0))
                        e1=meshele_ind[i][n1]+(no_node2d*(j+0))
                        e2=meshele_ind[i][n1]+(no_node2d*(j+1))
                        e3=meshele_ind[i][n0]+(no_node2d*(j+1))
                        # faces.append(np.int32((4,e0,e1,e2,e3)))
                        faces.extend(np.int32((4,e0,e1,e2,e3)))

    # print("faces OK")
    
    profilePoly3d = pv.PolyData(points, faces)
    profilePoly3d=profilePoly3d.triangulate()
    profilePoly3d['z']=profilePoly3d.points[:,2:3]
    profilePoly3d.save('profilePoly3d.vtk')
    profilePoly3d.save('profilePoly3d.stl')
    # xyz=profilePoly3d.points
    # ele_ind=profilePoly3d.faces.reshape((-1, 4))
    # with open('profileMesh.dat','w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
    #     for i in range(0,len(xyz)):
    #         f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
    #     for i in range(0,len(ele_ind)):
    #         f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
    #     f.close() 
    # quit()  

    fault12.save('fault12.stl',binary=False)
    outputFileCheck = Path("output.obj")
    if outputFileCheck.is_file():
        os.system('rm output.obj')
    outputFileCheck = Path("out_labels.txt")
    if outputFileCheck.is_file():
        os.system('rm out_labels.txt')    
    os.system('./mesh_arrangement poly3d.stl fault12.stl profilePoly3d.stl')
    # quit()
    # os.system('./mesh_arrangement poly3d.stl')
    
    solidObj=pv.read("output.obj")
    out_labelsF=r'out_labels.txt'
    objLabels = pd.read_csv(out_labelsF,sep='\\s+',header=None)
    objLabels = objLabels.fillna('')
    objLabels.columns=["labels"] 
    solidObj["labels"]=objLabels["labels"]
    xyz=solidObj.points
    ele_ind=solidObj.faces.reshape(-1,4)
    testPLOTfilename=r'solidObj.dat'
    pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,solidObj['labels'])  
    # quit()  

    cellNB=np.linspace(0,len(solidObj.faces.reshape(-1,4)),num=len(solidObj.faces.reshape(-1,4)),endpoint=False,dtype=np.int32)
    solidObj['cellNB']=cellNB
    solidObjCenters=solidObj.cell_centers()
    solidObjCenters=solidObjCenters.clip_surface(topPoly3d,invert=True,compute_distance = True)
    xyz=solidObjCenters.points
    with open('solidObjCenters.dat','w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","data"\n')
        f.write('Zone i='+str(len(xyz))+',F=point\n') 
        for i in range(0,len(xyz)):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(solidObjCenters['cellNB'][i])+'\n')
        f.close()
    solidObjSubset = solidObj.extract_cells(solidObjCenters['cellNB'])

    print("len(solidObjSubset['labels'])=",len(solidObjSubset['labels']))
    # solidObjTop=solidObj.clip_surface(topPoly3d,invert=True,compute_distance = True)
    xyz=solidObjSubset.points
    ele_ind=solidObjSubset.cells.reshape(-1,4)
    testPLOTfilename=r'solidObjSubset.dat'
    # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,solidObjSubset['labels'])

    dfnpoints=dfnPoly.points
    dfnfaces=dfnPoly.faces.reshape(-1,4)
    maxLabels=np.max(solidObjSubset['labels'])
    dfnLabels=maxLabels+1

    fracMtr=25.0 #25.0
    FaultMtr=50.0
    poly_no_node=len(dfnPoly.points)
    mtrSet=np.linspace(50.0,50.0,num=len(xyz)+poly_no_node,dtype=np.float64)
    mtrSet[0:poly_no_node]=fracMtr
    for i in range(0,len(ele_ind)):
        if (solidObj["labels"][i]>1):
            mtrSet[ele_ind[i][0]+poly_no_node]=FaultMtr
            mtrSet[ele_ind[i][1]+poly_no_node]=FaultMtr
            mtrSet[ele_ind[i][2]+poly_no_node]=FaultMtr
    print("set FaultMtr OK")

    with open('meger.mtr','w',encoding = 'utf-8') as f:
        f.write(str(len(xyz)+poly_no_node)+' 1\n')
        for i in range(0,len(xyz)+poly_no_node):
            f.write(str(mtrSet[i])+'\n')
        f.close()
    print("mtr File OK")

    if (case!=2):
        '''
        # print tetgen poly File
        '''
        # '''
        with open('meger.poly','w',encoding = 'utf-8') as f:
            f.write('# part 1: node section\n')
            f.write(str(len(xyz)+poly_no_node)+' 3 1 1\n')
            for i in range(0,poly_no_node):
                f.write(str(i)+' '+str(dfnPoly.points[i][0])+' '+str(dfnPoly.points[i][1])+' '+str(dfnPoly.points[i][2])+' '+' 1 1\n')
            for i in range(0,len(xyz)):
                f.write(str(i+poly_no_node)+' '+str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+' 1 1\n')
            f.write('\n')
            f.write('# part 2: facet section\n')
            f.write(str(len(ele_ind)+len(dfnfaces))+' 1\n')
            for i in range(0,len(dfnfaces)):
                f.write('1   0  '+str(dfnLabels)+'   #'+str(i+1)+'\n')
                f.write(str(dfnfaces[i][0])+' '+str(dfnfaces[i][1])+' '+str(dfnfaces[i][2])+' '+str(dfnfaces[i][3])+'\n')   
            for i in range(0,len(ele_ind)):
                f.write('1   0  '+str(solidObj["labels"][i])+'   #'+str(len(dfnfaces)+i+1)+'\n')
                f.write(str(ele_ind[i][0])+' '+str(ele_ind[i][1]+poly_no_node)+' '+str(ele_ind[i][2]+poly_no_node)+' '+str(ele_ind[i][3]+poly_no_node)+'\n')
            f.write('\n')
            f.write('# part 3: hole section\n')
            f.write('0\n')
            f.write('\n')
            f.write('# part 4: region section\n')
            f.write('0\n')
            f.write('\n')
            f.close()
            print("poly File OK")
            # quit()
        # os.system('tetgen -pqMmA meger.poly')
        outputFileCheck = Path("meger.1.node")
        if outputFileCheck.is_file():
            os.system('rm meger.1.*')
            os.system('rm tetgenOut')
        os.system('tetgen -d meger.poly >tetgenOut')
        print("===========")
        print("===========")
        print("需要先檢查網格是否有交錯")
        print("再執行tetgen -pqmMnA meger.poly")
        runTetgenCheck=0
        f = open('tetgenOut')
        for line in f.readlines():
            # print(line)
            if 'No faces are intersecting.' in line :
                runTetgenCheck=1
        f.close    
        if (runTetgenCheck==1):
            # 需要先檢查網格是否有交錯
            os.system('tetgen -pqmMnA meger.poly')
        else:
            print("需要刪除tetgenOut 內有交錯的faces")
            quit()
        # '''
    else:
        os.system('tetgen -pqnmMA meger.poly')

    outputFileCheck = Path("meger.1.node")
    if outputFileCheck.is_file():    
        print("start read tetgen results")
        meger3d,megerFace=readTetgen.readNodeEle('meger')
        meger3d.save('megerTetra3d.vtk')
        megerFace.save('megerTri2d.vtk')    
    # quit()
    return meger3d,megerFace

def faultStl(demClip2d,delta_x,delta_y,EpsgIn,EpsgOut,fault1,fault2):
    fault1.translate([delta_x, delta_y, 0])
    fault2.translate([delta_x, delta_y, 0])

    fault1['z']=fault1.points[:,2:3]
    fault2['z']=fault2.points[:,2:3]
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(fault1, style='surface',cmap='jet',scalars='z', smooth_shading=True,show_scalar_bar=True,show_edges=True,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
    plotter.add_mesh(fault2, style='surface',cmap='jet',scalars='z', smooth_shading=True,show_scalar_bar=True,show_edges=True,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
    plotter.show(screenshot='faults12_orig.png')
    # quit()

    minx=np.min(demClip2d['z'])
    maxx=np.max(demClip2d['z'])
    STLmaxZ=maxx+abs(maxx-minx)/10.0
    print("STLmaxZ=",STLmaxZ)

    '''
    # re-generate F1 and F2 mesh
    '''
    minx=np.min(fault1.points,axis=0)
    STLminZ=minx[2]


    points=np.zeros((16,3),dtype=np.float64)
    points[0]=[fault1.points[562][0],fault1.points[562][1],STLminZ]
    points[1]=[fault1.points[563][0],fault1.points[563][1],STLminZ]
    points[2]=[fault1.points[2][0],fault1.points[2][1],STLminZ]
    points[3]=[fault1.points[0][0],fault1.points[0][1],STLminZ]
    points[4]=[fault1.points[560][0],fault1.points[560][1],STLmaxZ]
    points[5]=[fault1.points[561][0],fault1.points[561][1],STLmaxZ]
    points[6]=[fault1.points[1][0],fault1.points[1][1],STLmaxZ]
    points[7]=[fault1.points[6][0],fault1.points[6][1],STLmaxZ]

    points[8]=[fault2.points[7][0],fault2.points[7][1],STLminZ]
    points[9]=[fault2.points[5][0],fault2.points[5][1],STLminZ]
    points[10]=[fault2.points[149][0],fault2.points[149][1],STLminZ]
    points[11]=[fault2.points[150][0],fault2.points[150][1],STLminZ]
    points[12]=[fault2.points[9][0],fault2.points[9][1],STLmaxZ]
    points[13]=[fault2.points[10][0],fault2.points[10][1],STLmaxZ]
    points[14]=[fault2.points[198][0],fault2.points[198][1],STLmaxZ]
    points[15]=[fault2.points[202][0],fault2.points[202][1],STLmaxZ]
    faces = np.hstack([
            [6, 3, 2, 10, 11, 1, 0],
            [6, 4, 5, 15, 14, 6, 7],
            [4, 0, 1, 5, 4],
            [4, 1, 11, 15, 5],
            [4, 11, 10, 14, 15],
            [4, 10, 2, 6, 14],
            [4, 2, 3, 7, 6],
            [4, 3, 0, 4, 7],
            [4, 11, 10, 9, 8],
            [4, 12, 13, 14, 15],
            [4, 8, 9, 13, 12],
            [4, 9, 10, 14, 13],
            [4, 11, 8, 12, 15]])
    fault12=pv.PolyData(points, faces)
    fault12['z']=fault12.points[:,2:3]

    
    minx=np.min(fault12.points,axis=0)
    maxx=np.max(fault12.points,axis=0)
    x=(minx[0]+maxx[0])/2.0-delta_x
    y=(minx[1]+maxx[1])/2.0-delta_y
    x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
    print('fault12 cxcy=', x84,y84) 
    fault12.save('fault12.vtk')
    SUByvistaPlot(fault12,'fault12','z')
    # quit()
    
    '''
    # creat demClip3d surf
    '''    
    points=np.zeros((len(demClip2d.points),3),dtype=np.float64)
    for i in range(0,len(demClip2d.points)):
        points[i]=[demClip2d.points[i][0],demClip2d.points[i][1],demClip2d['z'][i]]       
    faces=demClip2d.cells
    demClip3dsurf=pv.PolyData(points,faces)
    demClip3dsurf['z']=demClip2d['z']
    demClip3dsurf=demClip3dsurf.triangulate()
    print(demClip3dsurf)
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(demClip3dsurf, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=True,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
    plotter.show(screenshot='demClip3dsurf.png')
    demClip3dsurf.save('demClip3dsurf.vtk')


    return demClip3dsurf,fault12

def creatOrder2D():
    order2D=[]
    order2D.append((1,2))
    order2D.append((2,0))
    order2D.append((0,1))
    return order2D

def face2dToPoly3D(surfInterpolated,depth):
    order2D=creatOrder2D()    

def SUByvistaPlot(mesh,Filename,scarar):
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(mesh, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=True,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
    plotter.show(screenshot=Filename+'.png')
    mesh=mesh.triangulate()    
    xyz=mesh.points
    ele_ind=mesh.faces.reshape(-1,4)
    testPLOTfilename=Filename+'.dat'
    pyToTecplot.tecplotPyvistaUSG_z(testPLOTfilename,xyz,ele_ind,mesh[scarar])  

def dem2dClip(delta_x, delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem):
    domain2d.translate([delta_x, delta_y, 0])
    demClip2d.translate([delta_x, delta_y, 0])
    largeDem.translate([delta_x, delta_y, 0])

    # plotter = pv.Plotter(off_screen=True)
    # plotter.add_mesh(largeDem, style='surface',scalars='z',cmap='jet', smooth_shading=True,show_scalar_bar=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)  
    # # plotter.add_scalar_bar('z(m)',color='#000000', interactive=True, vertical=True,
    #                     #    title_font_size=20,
    #                     #    label_font_size=20,
    #                     #    outline=False,width=0.2)    
    # # plotter.show_grid()
    # # plotter.show_bounds(color='#000000', xlabel='x(m)', ylabel='y(m)', zlabel='z(m)',grid=True,all_edges=True)
    # # plotter.camera_position = 'xy'
    # plotter.export_gltf('./largeDem.gltf', rotate_scene=False,save_normals=True)
    # gltf = GLTF.load('./largeDem.gltf', load_file_resources=True)
    # gltf.export('largeDem.glb')    
    # plotter.show(screenshot='dem.png')

    min=np.min(largeDem.points,axis=0)
    max=np.max(largeDem.points,axis=0)
    x=(min[0]+max[0])/2.0-delta_x
    y=(min[1]+max[1])/2.0-delta_y
    x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
    print('largeDem cxcy=', x84,y84)
    largeDem.set_active_scalars('z')
    largeDem.save('largeDem.vtk')

    # Lvtk=pv.read('largeDem.vtk')
    # scalarsName=Lvtk.array_names # 查詢有哪些參數可以當scalars
    # print(scalarsName)

    # plotter = pv.Plotter(off_screen=True)
    # # plotter.add_axes(x_color='#000000')
    # # plotter.add_mesh(poly1, cmap='terrain', clim=[-100, 400], style='surface')
    # # plotter.add_mesh(surf, color='b', style='points',render_points_as_spheres=True,point_size=10.0)
    # plotter.add_mesh(domain2d, color='r', style="wireframe",smooth_shading=True,show_scalar_bar=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)
    # plotter.add_mesh(demClip2d, style='surface',scalars='z',cmap='jet', smooth_shading=True,show_scalar_bar=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)      
    # plotter.export_gltf('./demClip2d.gltf', rotate_scene=False,save_normals=True)
    # gltf = GLTF.load('./demClip2d.gltf', load_file_resources=True)
    # gltf.export('demClip2d.glb')
    # plotter.show(screenshot='demClip2d.png') 
    min=np.min(domain2d.points,axis=0)
    max=np.max(domain2d.points,axis=0)
    x=(min[0]+max[0])/2.0-delta_x
    y=(min[1]+max[1])/2.0-delta_y
    x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
    print('domain2d cxcy=', x84,y84)
    domain2d.set_active_scalars('z')
    domain2d.save('domain2d.vtk')

    min=np.min(demClip2d.points,axis=0)
    max=np.max(demClip2d.points,axis=0)
    x=(min[0]+max[0])/2.0-delta_x
    y=(min[1]+max[1])/2.0-delta_y   
    x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)    
    print('demClip2d cxcy=', x84,y84)
    demClip2d.set_active_scalars('z')
    demClip2d.save('demClip2d.vtk')

    return domain2d,demClip2d,largeDem
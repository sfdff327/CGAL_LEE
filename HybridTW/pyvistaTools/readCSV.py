import pandas as pd

def readCSV(CsvFile):
    df_t = pd.read_csv(CsvFile,sep='\\s+',header=None)
    df_t = df_t.fillna('')
    # print(df_t)

    return df_t
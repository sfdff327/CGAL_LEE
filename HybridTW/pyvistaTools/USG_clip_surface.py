import pyvista as pv
import numpy as np
import vtk
import matplotlib.pyplot as plt
from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def USG_clip_surface(grid,surf):
    clipGrid = grid.clip_surface(surf,compute_distance = True)
    return clipGrid

# '''
# # polydata
# '''
# vertices= []
# vertices.append([-1.0, -1.0, 1.0])
# vertices.append([3.0, -1.0, 1.0])
# vertices.append([3.0, 4.0, 1.0])
# vertices.append([-1.0, 4.0, 1.0])
# vertices.append([-1.0, -1.0, 5.0])
# vertices.append([3.0, -1.0, 5.0])
# vertices.append([3.0, 4.0, 5.0])
# vertices.append([-1.0, 4.0, 5.0])
# vertices=np.array(vertices)

# cells=[]
# cells.append([4,0,3,2,1])
# cells.append([4,0,1,5,4])
# cells.append([4,1,2,6,5])
# cells.append([4,2,3,7,6])
# cells.append([4,3,0,4,7])
# cells.append([4,4,5,6,7])

# faces = np.hstack(cells)    # triangle
# print("faces")
# print(faces)

# surf = pv.PolyData(vertices, faces)
# surf.save('surf.stl',binary=False)
# # surf=surf.compute_normals()
# print("surf")
# print(surf)

# surfxyz=surf.points
# surfele_ind=np.reshape(surf.faces, (-1, 5))

# '''
# # unstructured Grid
# '''
# nx=10
# ny=7
# nz=6

# dx=2.1
# dy=2.3
# dz=2.5

# ox=-2.0
# oy=-2.0
# oz=-0.0

# xyz=[]
# for k in range(0,nz):
#     for j in range(0,ny):
#         for i in range(0,nx):
#             xyz.append([ox+i*dx,oy+j*dy,oz+k*dz])
# # print(xyz)
# xyz=np.array(xyz)

# def calculateHexahedralCell(k,j,i,nlay,nrow,ncol,cells):
#     c0=(k  )*nrow*ncol+(j  )*ncol+i
#     c1=(k  )*nrow*ncol+(j  )*ncol+i+1
#     c2=(k  )*nrow*ncol+(j+1)*ncol+i+1
#     c3=(k  )*nrow*ncol+(j+1)*ncol+i
#     c4=(k+1)*nrow*ncol+(j  )*ncol+i
#     c5=(k+1)*nrow*ncol+(j  )*ncol+i+1
#     c6=(k+1)*nrow*ncol+(j+1)*ncol+i+1
#     c7=(k+1)*nrow*ncol+(j+1)*ncol+i
#     cells.append([8,c0,c1,c2,c3,c4,c5,c6,c7])
#     print(c0,c1,c2,c3,c4,c5,c6,c7)
#     return cells

# cells=[]
# cellID=[]
# NB=-1
# for k in range(0,nz-1):
#     for j in range(0,ny-1):
#         for i in range(0,nx-1):
#             calculateHexahedralCell(k,j,i,nz,ny,nx,cells)
#             NB=NB+1
#             cellID.append(NB)

# no_cells=len(cells)
# cells=np.array(cells).ravel()
# celltypes = np.empty(no_cells, dtype=np.uint32)
# celltypes[:] = vtk.VTK_HEXAHEDRON
# grid = pv.UnstructuredGrid(cells, celltypes, xyz)            
# print("grid")
# print(grid)
# cellID=np.array(cellID)
# grid['cellID']=cellID
# grid.save('grid.vtk')


# cmap = plt.cm.get_cmap("jet", 12)

# plotter = pv.Plotter(off_screen=True)
# plotter.add_mesh(surf, color='r', style='wireframe', line_width=3)
# plotter.add_mesh(grid, scalars='cellID',cmap=cmap, style='wireframe', line_width=3)
# # plotter.camera_position = 'xz'
# plotter.show(screenshot='Mesh.png')


# # for i in range(0,len(surf.faces)):
#     # print(i,surf.faces[i])
# result = grid.clip_surface(surf,compute_distance = True)
# # result = surf.clip_surface(grid,compute_distance = True)
# print(result)
# print("len(result['cellID'])=",len(result['cellID']))
# print("result['cellID']=")
# print(result['cellID'])


# ele_ind=np.reshape(grid.cells, (-1, 9))
# no_node = len(grid.points)
# no_ele = len(ele_ind)
# # print("no_node,no_ele=",no_node,no_ele)
# with open('Grid.dat','w',encoding = 'utf-8') as f:
#     f.write('Title="XY2D_plot"\n')
#     f.write('Variables="x(m)","y(m)","z(m)"\n')
#     f.write('Zone N='+str(len(surfxyz))+',E='+str(len(surfele_ind))+',F=FEpoint,ET=QUADRILATERAL\n') 
#     for i in range(0,len(surfxyz)):
#         f.write(str(surfxyz[i][0])+' '+str(surfxyz[i][1])+' '+str(surfxyz[i][2])+'\n')
#     for i in range(0,len(surfele_ind)):
#         f.write(str(surfele_ind[i][1]+1)+' '+str(surfele_ind[i][2]+1)+' '+str(surfele_ind[i][3]+1)+' '+str(surfele_ind[i][4]+1)+'\n')
#     f.write('Title="XY2D_plot"\n')
#     f.write('Variables="x(m)","y(m)","z(m)"\n')
#     f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=BRICK\n') 
#     for i in range(0,no_node):
#         f.write(str(grid.points[i][0])+' '+str(grid.points[i][1])+' '+str(grid.points[i][2])+'\n')
#     for i in range(0,no_ele):
#         f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+' '+str(ele_ind[i][5]+1)+' '+str(ele_ind[i][6]+1)+' '+str(ele_ind[i][7]+1)+' '+str(ele_ind[i][8]+1)+'\n')
#     f.write('Title="XY2D_plot"\n')
#     f.write('Variables="x(m)","y(m)","z(m)"\n')
#     f.write('Zone N='+str(no_node)+',E='+str(len(result['cellID']))+',F=FEpoint,ET=BRICK\n') 
#     for i in range(0,no_node):
#         f.write(str(grid.points[i][0])+' '+str(grid.points[i][1])+' '+str(grid.points[i][2])+'\n')
#     for k in range(0,len(result['cellID'])):
#         i=result['cellID'][k]
#         # print(result['cellID'][k])
#         f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+' '+str(ele_ind[i][5]+1)+' '+str(ele_ind[i][6]+1)+' '+str(ele_ind[i][7]+1)+' '+str(ele_ind[i][8]+1)+'\n')






# result.set_active_scalars('cellID',preference='point')
# result=result.cell_data_to_point_data()
# plotter = pv.Plotter(off_screen=True)
# # plotter.add_mesh(surf, color='r', style='wireframe', line_width=3)
# plotter.add_mesh(grid, color='r', style='surface', line_width=3)
# # plotter.add_mesh(result, color='tan')
# plotter.add_mesh(result, style='surface',scalars='cellID',cmap=cmap, smooth_shading=True,lighting=True,show_edges=True,reset_camera=True,metallic=1.0)
# plotter.show_grid()
# gltfFile=r'Difference.gltf'
# plotter.export_gltf(gltfFile, rotate_scene=False,save_normals=False)
# gltf = GLTF.load(gltfFile, load_file_resources=True)
# glbFile=r'Difference.glb'
# gltf.export(glbFile)
# plotter.show(screenshot='Difference.png')




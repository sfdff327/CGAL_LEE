import pyvista as pv
import numpy as np
import vtk
import matplotlib.pyplot as plt
from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def readSTL(StlFile):
    stl=pv.read(StlFile)
    plotter = pv.Plotter(off_screen=True)
    # plotter.add_mesh(stl, color='r', style='surface', show_edges=True)
    # plotter.show_grid()
    # plotter.camera_position = 'xy'
    # plotter.show(screenshot='stl.png')
    return stl


import trimesh
import io
import numpy as np
import tempfile
import sys
import matplotlib.pyplot as plt
import pyToTecplot
import pyvista as pv
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from xvfbwrapper import Xvfb

display = Xvfb(width=1920, height=1080)
display.start()

'''
# Fast and Robust Mesh Arrangements using Floating-point Arithmetic
# git clone --recursive https://github.com/gcherchi/FastAndRobustMeshArrangements.git
# This is the code we used for the experiments in the paper "Fast and Robust Mesh Arrangements using Floating-point Arithmetic" by G. Cherchi, M. Livesu, R. Scateni and M. Attene (SIGGRAPH Asia 2020).
# https://github.com/gcherchi/FastAndRobustMeshArrangements
'''
'''
# tetgen with pyvista
# https://tetgen.pyvista.org/api.html
'''
def test_mesh_arrangementOBJtoTetgen():
    from meshpy.tet import MeshInfo, build,Options
    import meshpy.tet as tet
    from pathlib import Path

    '''
    points=[]
    # points.append(np.array([TTIxyz[i][0],TTIxyz[i][1]], np.float64))
    points.append(np.array([-2, 0, 0], np.float64))
    points.append(np.array([3, 0, 0], np.float64))
    points.append(np.array([3, 2, 0], np.float64))
    points.append(np.array([-2, 2, 0], np.float64))
    points.append(np.array([-2, 0, 12], np.float64))
    points.append(np.array([3, 0, 12], np.float64))
    points.append(np.array([3, 2, 12], np.float64))
    points.append(np.array([-2, 2, 12], np.float64))


    facets=[]
    facets.append(np.array([0, 1, 2, 3], np.int32))
    facets.append(np.array([4, 5, 6, 7], np.int32))
    facets.append(np.array([0, 4, 5, 1], np.int32))
    facets.append(np.array([1, 5, 6, 2], np.int32))
    facets.append(np.array([2, 6, 7, 3], np.int32))
    facets.append(np.array([3, 7, 4, 0], np.int32))

    
    info = tet.MeshInfo()

    
    info.set_points(points)
    info.set_facets(facets)
    mesh = build(info)
    meshPoints=np.array(mesh.points,dtype=np.float64)
    meshElements=np.array(mesh.elements,dtype=np.int32)
    # print(meshPoints)
    # print(meshElements)

    xyzmin=np.min(meshPoints,axis=0)
    xyzmax=np.max(meshPoints,axis=0)
    print("xyzmin=",xyzmin)
    print("xyzmax=",xyzmax)
    for i in range(0,len(xyzmin)):
        delt=(xyzmax[i]-xyzmin[i])/10.0e0
        xyzmax[i]=xyzmax[i]+delt
        xyzmin[i]=xyzmin[i]-delt
    '''

    fracUSGobj=pv.read('./stlFile/9121output.obj')
    ORpoints,ORfacets=readPolydataPointsFacesNoneZ(fracUSGobj)
    testPLOTfilename=r'testORfracture.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,ORpoints,ORfacets)
    fracUSGobj.save('testWriteMeshALL.ply',binary=False)
    # quit()


    # print(ORfacets[114979-1])
    # print(ORfacets[26590-1])
    # print(ORfacets[26592-1])
    # print(ORfacets[26593-1])
    Check_ORfacets=np.zeros(len(ORfacets),dtype=int)
    FABResFileCheck = Path("out")
    print(FABResFileCheck)
    if FABResFileCheck.is_file():
        print("start read tetgen out")
        file_object1 = open(FABResFileCheck,'r')
        Lines = file_object1.readlines()
        # count = 0
        for line in Lines:
            # count += 1
            # print("Line{}: {}".format(count, line.strip()))
            # print("Line{}: {}".format(count, line[0:10].strip()))
            # print(len(line.strip()))
            # print(len(line))
            if ':' in line:
                NB00 = line.find('#')+1
                NB01 = line.find(' i')
                NB02 = line.rfind('#')+1
                NB03 = line.rfind(' at')
                if (NB00>0 and NB01>0):
                    selfIntersectNb=int(line[NB00:NB01])-1
                    Check_ORfacets[selfIntersectNb]=1
                    selfIntersectNb=int(line[NB02:NB03])-1
                    Check_ORfacets[selfIntersectNb]=1
                    # print("line[NB00:NB01]=",line[NB00:NB01])
                    # print("line[NB02:NB03]=",line[NB02:NB03])
                    # print(selfIntersectNb)
                # quit()
            # if (count==10):quit()
        no_node=len(ORpoints)
        noele=len(ORfacets)
        with open("testTetErr.dat",'w',encoding = 'utf-8') as f:
            count=0
            for j in range(0,len(ORfacets)):
                if (Check_ORfacets[j]==1):
                    count=count+1
            f.write('Title="XY2D_plot"\n')
            f.write('Variables="x(m)","y(m)","z(m)","ID"\n')
            f.write('Zone N='+str(no_node)+',E='+str(count)+',F=FEBLOCK,ET=TRIANGLE\n') 
            f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
            for i in range(0,no_node):
                f.write(str(ORpoints[i][0])+'\n')
            for i in range(0,no_node):
                f.write(str(ORpoints[i][1])+'\n')
            for i in range(0,no_node):
                f.write(str(ORpoints[i][2])+'\n')
            for j in range(0,len(ORfacets)):
                if (Check_ORfacets[j]==1):
                    f.write(str(j+1)+'\n')
            for j in range(0,len(ORfacets)):
                if (Check_ORfacets[j]==1):
                    f.write(str(ORfacets[j][1]+1)+' '+str(ORfacets[j][2]+1)+' '+str(ORfacets[j][3]+1)+'\n')            
    else:
        import os
        os.system('tetgen -d testWriteMeshALL.ply > out')
    # quit()

    delEleId=[8804,8805,8809,8811,17290,17596,17618]
    # maxID=21500 #8806
    maxID=21900 #8806
    maxID=len(ORfacets) #8806
    print("maxID=",maxID)

    # with open("testTetErr.dat",'w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)","ID"\n')
    #     f.write('Zone N='+str(no_node)+',E='+str(len(ORfacets))+',F=FEBLOCK,ET=TRIANGLE\n') 
    #     f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][0])+'\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][1])+'\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][2])+'\n')
    #     for j in range(0,len(ORfacets)):
    #         f.write(str(j+1)+'\n')    
    #     for j in range(0,len(ORfacets)):
    #         f.write(str(ORfacets[j][1]+1)+' '+str(ORfacets[j][2]+1)+' '+str(ORfacets[j][3]+1)+'\n')                 

    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)","ID"\n')
    #     f.write('Zone N='+str(no_node)+',E='+str(1)+',F=FEBLOCK,ET=TRIANGLE\n') 
    #     f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][0])+'\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][1])+'\n')
    #     for i in range(0,no_node):
    #         f.write(str(ORpoints[i][2])+'\n')
    #     f.write(str(maxID)+'\n')
    #     for j in range(0,1):
    #         f.write(str(ORfacets[maxID-1][1]+1)+' '+str(ORfacets[maxID-1][2]+1)+' '+str(ORfacets[maxID-1][3]+1)+'\n')  

    startTestNb= maxID
    while(maxID<=len(ORfacets)):
        print("maxID=",maxID,maxID-1)
        checkEleID=np.zeros(len(ORpoints),dtype=np.int)
        count=-1
        for i in range(0,maxID):
            # print("i=",i)
            QQ=0
            for k in range(0,len(delEleId)):
                if (i==delEleId[k]):
                    # print("i,delEleId[k]=",i,delEleId[k])
                    QQ=1
            if (QQ==0):
                for j in range(1,len(ORfacets[i])):
                    checkEleID[ORfacets[i][j]]=1
                    # print("i,j=",i,j)
        # print('checkEleID OK')   

        Nb=-1
        for i in range(0,len(ORpoints)):
            if (checkEleID[i]==1):
                Nb=Nb+1
                checkEleID[i]=Nb
            elif (checkEleID[i]==0):
                checkEleID[i]=-1
        # print('checkEleID to 1 or -1 OK')

        points=[]
        for i in range(0,len(ORpoints)):
            if (checkEleID[i]>=0):
        #         print(i,checkEleID[i])
                points.append(np.array(ORpoints[i],dtype=np.float64))
        facets=[]            
        for i in range(0,maxID):
            facets.append(np.array([checkEleID[ORfacets[i][1]],checkEleID[ORfacets[i][2]],checkEleID[ORfacets[i][3]]], np.int32))
        # print(facets)
        # print('points facets append OK')

        xyzmin=np.min(points,axis=0)
        xyzmax=np.max(points,axis=0)
        # print("xyzmin=",xyzmin)
        # print("xyzmax=",xyzmax)
        for i in range(0,len(xyzmin)):
            delt=(xyzmax[i]-xyzmin[i])/5.0e0
            xyzmax[i]=xyzmax[i]+delt
            xyzmin[i]=xyzmin[i]-delt
        no_point=len(points)
        boxPoints,boxFacets=xyzmaxmin(xyzmax,xyzmin,no_point)
        # print('boxPoints,boxFacets OK')
        
        xyz=[]
        ele_ind=[]
        for i in range(0,len(points)):
            xyz.append(np.array(points[i], np.float64))
        for i in range(0,len(boxPoints)):
            xyz.append(np.array(boxPoints[i], np.float64))

        for i in range(0,len(facets)):
            ele_ind.append(np.array(facets[i][0:], np.int32))
            # print(ele_ind)
            # if (i==5):quit()
        for i in range(0,len(boxFacets)):
            ele_ind.append(np.array(boxFacets[i], np.int32))
        # print('xyz,ele_ind OK')
            
        # with open("testTetInfo.dat",'w',encoding = 'utf-8') as f:          
        #     f.write('Title="XY2D_plot"\n')
        #     f.write('Variables="x(m)","y(m)","z(m)"\n')
        #     f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEPOINT,ET=TRIANGLE\n') 
        #     for i in range(0,len(xyz)):
        #         f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
        #     for j in range(0,len(ele_ind)):
        #         f.write(str(ele_ind[j][0]+1)+' '+str(ele_ind[j][1]+1)+' '+str(ele_ind[j][2]+1)+'\n')
        # print('testTetInfo.dat OK')

        # vertices=xyz
        # cells=[]
        # for i in range(0,len(ele_ind)):
        #     cells.append(np.int32([3,ele_ind[i][0],ele_ind[i][1],ele_ind[i][2]]))
        # points=np.array(vertices,dtype=np.float64)
        # no_cells=len(cells)
        # cells=np.array(cells).ravel()                
        # celltypes = np.empty(no_cells, dtype=np.uint32)
        # celltypes[:] = vtk.VTK_TRIANGLE
        # fractureInBox = pv.PolyData(points,cells)
        # fractureInBox.compute_normals(inplace=True)
        # fractureInBox.save('testWriteMeshMaxID.ply',binary=False)
        # print('testWriteMeshMaxID.ply OK')
        with open("testWriteMeshALL.poly",'w',encoding = 'utf-8') as f:
            f.write('# part 1 - node list\n')
            f.write(str(len(xyz))+' 3 0 0\n')
            f.write('\n')
            for i in range(0,len(xyz)):
                f.write(str(i)+' '+str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
            f.write('\n')
            f.write('# part 2 - facet list\n')
            f.write(str(len(ele_ind))+' 0\n')
            f.write('\n')
            for i in range(0,len(ele_ind)):
                f.write('1\n')
                f.write('3 '+str(ele_ind[i][0])+' '+str(ele_ind[i][1])+' '+str(ele_ind[i][2])+'\n')
            f.write('\n')
            f.write('# part 3 - hole list\n')
            f.write('0\n')
            f.write('# part 4 - region list\n')
            f.write('0\n')
        # quit()

        info3d = tet.MeshInfo()
        # print("info3d = tet.MeshInfo()")
        info3d.set_points(xyz)
        # print("info3d.set_points(xyz)")
        info3d.set_facets(ele_ind)
        print("info3d.set_facets(ele_ind)")
        mesh3d = build(info3d,options=Options(switches='pqM')) #M可以讓太近的點merge
        # print(dir(mesh3d))
        # print("mesh3d = build(info3d)")
        mesh3dPoints=np.array(mesh3d.points,dtype=np.float64)
        mesh3dElements=np.array(mesh3d.elements,dtype=np.int32)
        print(mesh3dPoints)
        print(mesh3dElements)
        print(len(mesh3dPoints))
        print(len(mesh3dElements))
        if (len(mesh3dPoints)==0 and len(mesh3dElements)==0):
            delEleId.append(maxID-1)
            print(delEleId)
        maxID=maxID+1
    with open("domainMesh.dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone N='+str(len(mesh3dPoints))+',E='+str(len(mesh3dElements))+',F=FEPOINT,ET=TETRAHEDRON\n')
        for i in range(0,len(mesh3dPoints)):
            f.write(str(mesh3dPoints[i][0])+' '+str(mesh3dPoints[i][1])+' '+str(mesh3dPoints[i][2])+'\n')
        for j in range(0,len(mesh3dElements)):
            f.write(str(mesh3dElements[j][0]+1)+' '+str(mesh3dElements[j][1]+1)+' '+str(mesh3dElements[j][2]+1)+' '+str(mesh3dElements[j][3]+1)+'\n')

    # quit()

    points=[]
    facets=[]
    xyzmin=np.min(points,axis=0)
    xyzmax=np.max(points,axis=0)
    print("xyzmin=",xyzmin)
    print("xyzmax=",xyzmax)
    for i in range(0,len(xyzmin)):
        delt=(xyzmax[i]-xyzmin[i])/5.0e0
        xyzmax[i]=xyzmax[i]+delt
        xyzmin[i]=xyzmin[i]-delt

    no_point=len(points)
    boxPoints,boxFacets=xyzmaxmin(xyzmax,xyzmin,no_point)
    
    xyz=[]
    ele_ind=[]
    for i in range(0,len(points)):
        xyz.append(np.array(points[i], np.float64))
    for i in range(0,len(boxPoints)):
        xyz.append(np.array(boxPoints[i], np.float64))

    for i in range(0,len(facets)):
        ele_ind.append(np.array(facets[i][1:], np.int32))
    # print(ele_ind)
    # quit()
    for i in range(0,len(boxFacets)):
        ele_ind.append(np.array(boxFacets[i], np.int32))

    info3d = tet.MeshInfo()
    print("info3d = tet.MeshInfo()")
    info3d.set_points(xyz)
    print("info3d.set_points(xyz)")
    info3d.set_facets(ele_ind)
    print("info3d.set_facets(ele_ind)")
    mesh3d = build(info3d)
    print("mesh3d = build(info3d)")
    mesh3dPoints=np.array(mesh3d.points,dtype=np.float64)
    mesh3dElements=np.array(mesh3d.elements,dtype=np.int32)
    print(mesh3dPoints)
    print(mesh3dElements)

    print("OK")
    quit()

def xyzmaxmin(xyzmax,xyzmin,no_point):

    boxPoints=[]
    boxFacets=[]
    boxPoints.append(np.array([xyzmin[0], xyzmin[1], xyzmin[2]], np.float64))
    boxPoints.append(np.array([xyzmax[0], xyzmin[1], xyzmin[2]], np.float64))
    boxPoints.append(np.array([xyzmax[0], xyzmax[1], xyzmin[2]], np.float64))
    boxPoints.append(np.array([xyzmin[0], xyzmax[1], xyzmin[2]], np.float64))
    boxPoints.append(np.array([xyzmin[0], xyzmin[1], xyzmax[2]], np.float64))
    boxPoints.append(np.array([xyzmax[0], xyzmin[1], xyzmax[2]], np.float64))
    boxPoints.append(np.array([xyzmax[0], xyzmax[1], xyzmax[2]], np.float64))
    boxPoints.append(np.array([xyzmin[0], xyzmax[1], xyzmax[2]], np.float64))

    boxFacets.append(np.array([0+no_point,3+no_point,2+no_point,1+no_point], np.int32))
    boxFacets.append(np.array([0+no_point,1+no_point,5+no_point,4+no_point], np.int32))
    boxFacets.append(np.array([1+no_point,2+no_point,6+no_point,5+no_point], np.int32))
    boxFacets.append(np.array([2+no_point,3+no_point,7+no_point,6+no_point], np.int32))
    boxFacets.append(np.array([0+no_point,4+no_point,7+no_point,3+no_point], np.int32))
    boxFacets.append(np.array([4+no_point,5+no_point,6+no_point,7+no_point], np.int32))

    # for i in range(0,len(boxPoints)):
    #     print(boxPoints[i])
    # for i in range(0,len(boxFacets)):
    #     print(boxFacets[i])


    return boxPoints,boxFacets


def test_trimeshIntersections(frac_XYZ,frac_ele_ind):

    
    '''
    # 測試pv read 3d file
    
    # stl=trimesh.exchange.stl.load_stl_binary('./stlFile/40509.stl')
    # stl=pv.read('./stlFile/40509.stl')
    stl=pv.read('./stlFile/three_cubes.stl')
    print(stl)
    print(stl.points)
    print(stl.faces)
    xyz,ele_ind=readPolydataPointsFacesNoneZ(stl)
    print(len(xyz))
    print(len(ele_ind))
    # print(len(z))
    testPLOTfilename=r'testStl.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)

    obj=pv.read('./stlFile/output.obj')
    print(obj)
    print(obj.points)
    print(obj.faces) 
    xyz,ele_ind=readPolydataPointsFacesNoneZ(obj)
    print(len(xyz))
    print(len(ele_ind))
    # print(len(z))
    testPLOTfilename=r'testObj.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)       
    quit()
    '''
    # quit()
    no_frac_XYZ=len(frac_XYZ)
    no_frac_ele_ind=len(frac_ele_ind)
    vertices=frac_XYZ
    cells=[]
    for i in range(0,len(frac_ele_ind)):
        cells.append(np.int32([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]]))
    # quit()
    points=np.array(vertices,dtype=np.float64)
    no_cells=len(cells)
    cells=np.array(cells).ravel()                
    celltypes = np.empty(no_cells, dtype=np.uint32)
    celltypes[:] = vtk.VTK_TRIANGLE
    fracUSG = pv.PolyData(points,cells)
    fracUSG.compute_normals(inplace=True)
    # print(fracUSG.points)
    # print(fracUSG.faces)
    xyz=np.float64(fracUSG.points)
    ele_ind=np.reshape(fracUSG.faces,(-1, 4))
    testPLOTfilename=r'testPyFracUSG.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)  

    fracUSG.save('Fracture.stl',binary=False)
    fracUSGobj=pv.read('./stlFile/9121output.obj')
    xyz,ele_ind=readPolydataPointsFacesNoneZ(fracUSGobj)
    testPLOTfilename=r'testfracUSGObj.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)       
    # quit()

    # quit()

    vertices=[
            [ 5.00000003,3.82934597,-4.07075391],
            [ 1.57137754,2.83575603,-1.21536646],
            [ 2.92728689,-4.07954512,-1.99356971],
            [ 5.00000003,-3.47888813,-3.71974359]]
    faces=[[0,1,2],[0,2,3]]
    points=np.array(vertices,dtype=np.float64)
    cells=[]
    for i in range(0,len(faces)):
        cells.append([3,faces[i][0],faces[i][1],faces[i][2]])
    no_cells=len(cells)
    cells=np.array(cells).ravel()                
    celltypes = np.empty(no_cells, dtype=np.uint32)
    celltypes[:] = vtk.VTK_TRIANGLE
    # tri00 = pv.UnstructuredGrid(cells, celltypes, points)  
    tri00 = pv.PolyData(points,cells)  
    print("tri00")
    print(tri00)
    tri00.save('tri00.stl',binary=False)

    vertices=[
            [ 3.83972838,-0.11664066,-4.99999986],
            [ 4.75958636, 0.36365428,-0.64845643],
            [ 5.00000003,-1.67775187,-0.47395954],
            [ 5.00000003,-5.00000009,-1.95011339],
            [ 4.32204678,-5.00000009,-4.99999986]
            ]
    faces=[[0,1,2],[0,2,3],[0,3,4],]            
    points=np.array(vertices,dtype=np.float64)
    cells=[]
    for i in range(0,len(faces)):
        cells.append([3,faces[i][0],faces[i][1],faces[i][2]])
    no_cells=len(cells)
    cells=np.array(cells).ravel()                
    celltypes = np.empty(no_cells, dtype=np.uint32)
    celltypes[:] = vtk.VTK_TRIANGLE
    # tri01 = pv.UnstructuredGrid(cells, celltypes, points)  
    tri01 = pv.PolyData(points,cells)  
    print("tri01")  
    print(tri01)  
    tri01.save('tri01.stl',binary=False)

    add = tri00.boolean_add(tri01)
    print("add")
    print(add)
    print(add.points)
    print(add.faces)
    xyz=np.float64(add.points)
    ele_ind=np.reshape(add.faces,(-1, 4))
    testPLOTfilename=r'testPyAdd.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)

    add.save('add.stl')

    obj=pv.read('./stlFile/output.obj')
    print(obj)
    print(obj.points)
    print(obj.faces) 
    xyz,ele_ind=readPolydataPointsFacesNoneZ(obj)
    print(len(xyz))
    print(len(ele_ind))
    # print(len(z))
    testPLOTfilename=r'testObj.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)



    union = tri00.boolean_union(tri01)
    print("union")
    print(union)
    print(union.points)
    print(union.faces)
    xyz=np.float64(union.points)
    ele_ind=np.reshape(union.faces,(-1, 4))
    testPLOTfilename=r'testPyUnion.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)

    diff = tri00.boolean_difference(tri01)
    print("diff")
    print(diff)
    print(diff.points)
    print(diff.faces)
    xyz=np.float64(diff.points)
    ele_ind=np.reshape(diff.faces,(-1, 4))
    testPLOTfilename=r'testPyDiff.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)

    triangulate=tri00.triangulate(tri01)
    print("triangulate")
    print(triangulate)
    print(triangulate.points)
    print(triangulate.faces)
    xyz=np.float64(triangulate.points)
    ele_ind=np.reshape(triangulate.faces,(-1, 4))
    testPLOTfilename=r'testPyTriangulate.dat'
    pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)


    add = trimesh.Trimesh(
        vertices=[
            [ 5.00000003,3.82934597,-4.07075391],
            [ 1.57137754,2.83575603,-1.21536646],
            [ 2.92728689,-4.07954512,-1.99356971],
            [ 5.00000003,-3.47888813,-3.71974359],
            [ 3.83972838,-0.11664066,-4.99999986],
            [ 4.75958636, 0.36365428,-0.64845643],
            [ 5.00000003,-1.67775187,-0.47395954],
            [ 5.00000003,-5.00000009,-1.95011339],
            [ 4.32204678,-5.00000009,-4.99999986]],
        faces=[
            [0,1,2],[0,2,3],[4,5,6],[4,6,7],[4,7,8]],process=False
    )
    new_vertices ,new_faces = trimesh.remesh.subdivide(add.vertices,add.faces)
    readd=trimesh.Trimesh(vertices=new_vertices,faces=new_faces,process=False)
    outfile=r'testReadd.dat'
    pyToTecplot.TecplotTrimesh(outfile,readd)



    mesh01 = trimesh.Trimesh(
        vertices=[
            [ 5.00000003,3.82934597,-4.07075391],
            [ 1.57137754,2.83575603,-1.21536646],
            [ 2.92728689,-4.07954512,-1.99356971],
            [ 5.00000003,-3.47888813,-3.71974359]],
        faces=[
            [0,1,2],[0,2,3]],process=False
    )
    mesh02 = trimesh.Trimesh(
        vertices=[
            [ 3.83972838,-0.11664066,-4.99999986],
            [ 4.75958636, 0.36365428,-0.64845643],
            [ 5.00000003,-1.67775187,-0.47395954],
            [ 5.00000003,-5.00000009,-1.95011339],
            [ 4.32204678,-5.00000009,-4.99999986]
            ],
        faces=[
            [0,1,2],[0,2,3],[0,3,4],],process=False
    )    
    outfile=r'testMesh01.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh01)
    outfile=r'testMesh02.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh02)

    meshes = [0 for i in range(2)]
    meshes[0] = mesh01
    meshes[1] = mesh02

    mesh01.normals, _ = trimesh.triangles.normals(mesh01.triangles)
    mesh02.normals, _ = trimesh.triangles.normals(mesh02.triangles)

    mesh03 = trimesh.intersections.slice_mesh_plane(
        mesh02,
        mesh01.normals[0],
        mesh01.vertices[0])
    outfile=r'testMesh03.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh03)

    # mesh04=mesh01.intersection(mesh02, engine='scad')
    # mesh04=trimesh.boolean.intersection(meshes, engine='blender')
    mesh04=mesh01.union(mesh02, engine='blender')
    outfile=r'testMesh04.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh04)
    mesh05=mesh01.intersection(mesh02, engine='blender')    
    outfile=r'testMesh05.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh05)
    # print(mesh05.vertices)
    # print(mesh05.faces)    
    mesh06=mesh01.difference(mesh02, engine='blender')    
    outfile=r'testMesh06.dat'
    pyToTecplot.TecplotTrimesh(outfile,mesh06)
    # print(mesh06.vertices)
    # print(mesh06.faces)    
    # mesh05=mesh02.intersection(mesh04, engine='blender')

    # outfile=r'testMesh05.dat'
    # pyToTecplot.TecplotTrimesh(outfile,mesh05)

    # mesh04=trimesh.boolean.union([mesh01,mesh02],engine='scad')
    # mesh04.export(file_obj='testMesh.glb',file_type='glb')
    # print(trimesh.interfaces.blender.exists)
    # print(trimesh.interfaces.scad.exists)
    # print(trimesh.interfaces.blender._search_path)
    # print(trimesh.interfaces.blender._blender_executable)
            

    quit()

    for i,n in enumerate(normals):
        current_mesh = trimesh.intersections.slice_mesh_plane(
            mesh02, 
            normals[i], 
            mesh01.vertices[mesh01.faces[i][0]])
    # mesh03=mesh01.intersection(other=mesh02)
    outfile=r'testMesh03.dat'
    pyToTecplot.TecplotTrimesh(outfile,current_mesh)

    # current_mesh = mesh02


    # u=trimesh.boolean.boolean_automatic(meshes,operation='union')
    u=trimesh.boolean.boolean_automatic(mesh02,operation='union')




    mesh04=trimesh.boolean.union([mesh01,mesh02])
    quit()
    # scene01 = mesh01.scene()
    # scene02 = mesh02.scene()
    # scene01.camera.resolution = (4000, 2000)  
    # scene01.camera_transform = scene01.camera.look_at(
    #     points=mesh01.vertices,
    #     rotation=trimesh.transformations.euler_matrix(np.pi / 3, 0, np.pi / 5)) 
    # scene02.camera.resolution = (4000, 2000)  
    # scene02.camera_transform = scene02.camera.look_at(
    #     points=mesh02.vertices,
    #     rotation=trimesh.transformations.euler_matrix(np.pi / 3, 0, np.pi / 5)) 
    # with open('testMesh.png', 'wb') as f:
    #     f.write(scene01.save_image(visible=True)))                                     
    #     # f.write(scene02.save_image(visible=True)                                     


    return 0

def readPolydataPointsFaces(mesh):
    xyz=np.float64(mesh.points)
    z=np.float64(mesh.active_scalars)
    ele_ind=np.reshape(mesh.faces, (-1, 4))
    return xyz,ele_ind,z

def readPolydataPointsFacesNoneZ(mesh):
    xyz=np.float64(mesh.points)
    ele_ind=np.reshape(mesh.faces, (-1, 4))
    return xyz,ele_ind


if __name__== "__main__":
    result = main(data)   


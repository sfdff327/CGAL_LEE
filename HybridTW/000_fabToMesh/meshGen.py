from numpy.core.defchararray import add


def Triangle2D(xyzInter,E1Nb,NbEleInter,eachNbEleInter,frac_segeID,frac_sege,frac_XYZ):
    import sys
    import numpy as np
    import copy
    import tool
    import pyToTecplot
    import meshpy.triangle as triangle
    from pathlib import Path
    import pickle

    EPSILON= sys.float_info.epsilon
    SQEPS=np.sqrt(EPSILON)  

    # x1=4.63837439280146 
    # y1=-3.9576308123951423
    # x2=4.735920139237492 
    # y2=-1.1699798414767597
    # x3=4.745745838805441 
    # y3=-0.8891821446421467
    # x4=4.77686018453081 
    # y4=3.8275421669233425e-10

    # Sx,Sy,ind = SLineF(x1,y1,x2,y2,x3,y3,x4,y4,SQEPS)
    # print("Sx,Sy,ind=",Sx,Sy,ind)
    # quit()

    no_frac = len(E1Nb)
    frac_no_sege = len(frac_segeID)
    print("no_frac=",no_frac)
    print("frac_no_sege=",frac_no_sege)
    # for i in range(0,no_frac):
    #     print(i,len(E1Nb[i]))
    # quit()

    fracXYZCheck = Path("TempMG/fracXYZCheck.out")
    if fracXYZCheck.is_file():
        with open(fracXYZCheck, 'rb') as file:
            frac_sege = pickle.load(file)
        print("read temp frac_sege")
    else:
        # outfile_name=r'FracSegeBF.dat'
        # pyToTecplot.TecplotLINESEG3D(outfile_name,frac_XYZ,frac_sege)
        for i in range(0, int(len(frac_XYZ)-1)):
            for j in range(i+1 , len(frac_XYZ)):
                Tdis = tool.dis3d(frac_XYZ[i][0],frac_XYZ[i][1],frac_XYZ[i][2],frac_XYZ[j][0],frac_XYZ[j][1],frac_XYZ[j][2])
                # print(i,j,Tdis)
                if (Tdis<SQEPS):
                    # print("theSamePoint",i,j,Tdis)
                    # print("裂隙頂點重複，需要更新frac_sege")
                    # quit()
                    for k in range(0, len(frac_sege)):
                        for ki in range(0,2):
                            if (frac_sege[k][ki]==j):
                                # print("=========")
                                # print("BF frac_sege[k]=",frac_sege[k])
                                # print("change frac_sege j,i",j,i)
                                frac_sege[k][ki]=i
                                # print("AF frac_sege[k]=",frac_sege[k])
                                # print("=========")
        with open(fracXYZCheck,'wb') as file:
            pickle.dump(frac_sege,file)
        print("write temp frac_sege")
    # quit()

    # outfile_name=r'FracSegeAF.dat'
    # pyToTecplot.TecplotLINESEG3D(outfile_name,frac_XYZ,frac_sege)    
    # quit()
    # print("BF E1Nb")
    # for i in range(0,len(E1Nb)):
    #     # print("E1Nb[i]=",E1Nb[i])

    locTransNbRes = Path("TempMG/locTransNbRes.out")
    if locTransNbRes.is_file():
        with open(locTransNbRes, 'rb') as file:
            locTransNb = pickle.load(file)
        print("read temp locTransNb")
    else:
        locTransNb=np.zeros(len(xyzInter),dtype=np.int32)
        for i in range(0, int(len(xyzInter)-1)):
            if (locTransNb[i]==0):
                for j in range(i+1 , len(xyzInter)):
                    if (locTransNb[j]==0):
                        Tdis = tool.dis3d(xyzInter[i][0],xyzInter[i][1],xyzInter[i][2],xyzInter[j][0],xyzInter[j][1],xyzInter[j][2])                    
                        if (Tdis<SQEPS): 
                            # print(i,j,Tdis)
                            locTransNb[j]=i+1
        with open(locTransNbRes,'wb') as file:
            pickle.dump(locTransNb,file)
        print("write temp locTransNb")
    # quit()

    mesh_iniE1NbRes = Path("TempMG/mesh_iniE1NbRes.out")
    if mesh_iniE1NbRes.is_file():
        with open(mesh_iniE1NbRes, 'rb') as file:
            E1Nb = pickle.load(file)
        print("read temp ini E1Nb")
    else:
        # print("locTransNb")
        # print(locTransNb)
        # print("AF E1Nb")
        for i in range(0,len(E1Nb)):
            for j in range(0,len(E1Nb[i])):
                if (locTransNb[E1Nb[i][j]]!=0):
                    E1Nb[i][j]=locTransNb[E1Nb[i][j]]-1
            # print("E1Nb[i]=",E1Nb[i])
        with open(mesh_iniE1NbRes,'wb') as file:
            pickle.dump(E1Nb,file)
        print("write temp ini E1Nb")            
    # quit()


    globalAddSegeRes = Path("TempMG/globalAddSegeRes.out")
    if globalAddSegeRes.is_file():
        with open('TempMG/globalAddSegeRes.out', 'rb') as file:
            globalAddSege = pickle.load(file)
        print("read temp ini globalAddSege")
        with open('TempMG/globalBiggestNB.out', 'rb') as file:
            globalBiggestNB = pickle.load(file)
        print("read temp ini globalBiggestNB")
        with open('TempMG/localAddSege.out', 'rb') as file:
            localAddSege = pickle.load(file)
        print("read temp ini localAddSege")
        with open('TempMG/no_TTI_point.out', 'rb') as file:
            no_TTI_point = pickle.load(file)
        print("read temp ini no_TTI_point")
    else:
        localAddSege =[]
        globalBiggestNB =[]
        globalAddSege=[]
        no_TTI_point=[]

        for fracID in range(0, no_frac):
            localAddSege.append([])
            globalAddSege.append([])
            Nb= len(localAddSege)-1
            no_TTI_point.append(len(E1Nb[fracID]))
            for i in range(0, int(len(E1Nb[fracID])/2)):
                localAddSege[Nb].append([i*2+0,i*2+1])
                globalAddSege[Nb].append([E1Nb[fracID][i*2+0],E1Nb[fracID][i*2+1]])
            if (fracID>0):
                print("fracID=",fracID)
                print("len(E1Nb[fracID])=",len(E1Nb[fracID]))
                if (len(E1Nb[fracID])>0):
                    print("E1Nb[fracID][len(E1Nb[fracID])-1]=",E1Nb[fracID][len(E1Nb[fracID])-1])                
                    E1b=E1Nb[fracID][len(E1Nb[fracID])-1]
                    if (E1a>E1b):
                        appendE1= E1a
                    else:
                        appendE1= E1b
                        E1a = E1b
                    globalBiggestNB.append(appendE1)
                    # print("fracID,E1a,E1b=",fracID,E1a,E1b)
                else:
                    globalBiggestNB.append(None)
            else:                
                globalBiggestNB.append(E1Nb[fracID][len(E1Nb[fracID])-1])
                E1a=E1Nb[fracID][len(E1Nb[fracID])-1]  

        with open('TempMG/globalAddSegeRes.out','wb') as file:
            pickle.dump(globalAddSege,file)
        print("write temp ini globalAddSege")  
        with open('TempMG/globalBiggestNB.out','wb') as file:
            pickle.dump(globalBiggestNB,file)
        print("write temp ini globalBiggestNB")  
        with open('TempMG/localAddSege.out','wb') as file:
            pickle.dump(localAddSege,file)
        print("write temp ini localAddSege")  
        with open('TempMG/no_TTI_point.out','wb') as file:
            pickle.dump(no_TTI_point,file)
        print("write temp ini no_TTI_point")  

                    
        #     print("localAddSege=",localAddSege[fracID])      
        #     print("globalAddSege=",globalAddSege[fracID])      
        #     print("fracID=",fracID,"globalBiggestNB[len(globalBiggestNB)-1]=",globalBiggestNB[len(globalBiggestNB)-1])
        # print("no_TTI_point=",no_TTI_point)
        # for i in range(0, len(xyzInter)):
        #     print(i,xyzInter[i])
    # quit()

    xyz= []
    ele_ind = []
    cell_scalar=[]
    TransE1Nb = []
    ThreePlaneE1 =[]

    for i in range(0, int(len(frac_XYZ)+len(xyzInter))):
        TransE1Nb.append(-1) #準備每個裂隙頂點+TII的線段頂點，對應mesh的編號，預設-1
    # print(TransE1Nb)
    # quit()


    stopID =5000
    tagFracID=1401
    for fracID in range(0, no_frac):
        if (fracID==stopID):
            quit()
        print("fracID=",fracID)
        if (fracID==tagFracID):
            print("========")
            print("========")
            print("fracID=",fracID,"len(E1Nb[fracID])=",len(E1Nb[fracID]))

        '''
        TTIxyz2d: 每片裂隙面上之xyzInter頂點 3d
        '''
        TTIxyz2d=[]
        for j in range(0, len(E1Nb[fracID])):
            TTIxyz2d.append(np.array([xyzInter[E1Nb[fracID][j]][0], xyzInter[E1Nb[fracID][j]][1], xyzInter[E1Nb[fracID][j]][2]], np.float64))
        # print("len(TTIxyz2d)=",len(TTIxyz2d))
        # for i in range(0, len(TTIxyz2d)):
        #     print(TTIxyz2d[i][0],TTIxyz2d[i][1],TTIxyz2d[i][2])
        # quit()


        xyz2d=[]   #裂隙頂點 3d
        NodeNb=[]  #裂隙頂點於全域的編號 
        SegeNb=[]  #局部編號
        for j in range(0, frac_no_sege):            
            if (fracID == frac_segeID[j]):
                # print("fracID,j=",fracID,j,frac_sege[j])
                SegeNb.append(frac_sege[j])
                for k in range(0, 2):
                    if (len(NodeNb)>0):
                        QQ=0
                        for tnb in range(len(NodeNb)):
                            if (NodeNb[tnb]==frac_sege[j][k]):
                                QQ=1
                                break
                        if (QQ==0):
                            NodeNb.append(frac_sege[j][k])  
                    else:
                        NodeNb.append(frac_sege[j][k])
        # if (fracID==tagFracID):
        #     print("NodeNb")
        #     print(NodeNb)  
        #     print("SegeNb")
        #     print(SegeNb)
            # quit()

        for i in range(0, len(SegeNb)):
            for j in range(0, 2):
                for k in range(0,len(NodeNb)):
                    if (SegeNb[i][j]==NodeNb[k]):
                        SegeNb[i][j]=k
        # print("SegeNb=",SegeNb)
        # quit()

        for i in range(0, len(NodeNb)):
            xyz2d.append(np.array([frac_XYZ[NodeNb[i]][0], frac_XYZ[NodeNb[i]][1], frac_XYZ[NodeNb[i]][2]], np.float64))
        # quit()
        # for i in range(0, len(xyz2d)):
        #     print(xyz2d[i][0],xyz2d[i][1],xyz2d[i][2])
    

        # print("fracID=",fracID)
        # print("len(ThreePlaneE1[fracID]=",len(ThreePlaneE1[fracID]))
        # if (len(ThreePlaneE1[fracID])!=0):
        #     for i in range(len(ThreePlaneE1[fracID])):
        #         x1= xyzInter[ThreePlaneE1[fracID][i]][0]
        #         y1= xyzInter[ThreePlaneE1[fracID][i]][1]
        #         z1= xyzInter[ThreePlaneE1[fracID][i]][2]
        #     print("fracID=",fracID,"len",len(ThreePlaneE1[fracID]))
        #     print("len(xyzInter)",len(xyzInter))
        #     print("quit")
        #     quit()


        no_sege = []
        # for i in range(0,int(len(TTIxyz2d)/2)):
        #     addSege.append([i*2+0,i*2+1])
        addSege = copy.deepcopy(localAddSege[fracID])
        plotaddSege = copy.deepcopy(localAddSege[fracID])
        '''
        addSege：設定每片裂隙上TTI線段頂點編號
        '''
        # print("len(addSege)=",len(addSege))        
        # for i in range(0, len(addSege)):
        #     print("addSege=",addSege[i][0],addSege[i][1])
        # # quit()

        #判斷TTI頂點編號重複
        checkNode=[]        
        for i in range(0,len(TTIxyz2d)):
            # checkNode.append(np.array([0],dtype=np.int32))
            checkNode.append(0)
        # print("checkNode")
        # print(checkNode)
        # quit()

        #判斷每個TTI頂點是否有重複，若發生重複，則改變addSege編號
        #TTIxyz2d發生重複，頂點編號比較大的checkNode[j]=i+1
        # print("BF addSege=") 
        # for i in range(0, len(addSege)):
        #     print("addSege=", addSege[i][0],addSege[i][1])
       
        for i in range(0, len(TTIxyz2d)-1):
            if (checkNode[i]==0):
                for j in range(i+1,len(TTIxyz2d)):
                    if (checkNode[j]==0):
                        dis = tool.dis3d(TTIxyz2d[i][0],TTIxyz2d[i][1],TTIxyz2d[i][2],TTIxyz2d[j][0],TTIxyz2d[j][1],TTIxyz2d[j][2])
                        if (dis<=SQEPS):
                            # print("TTI i,j=",i,j)
                            # print("E1Nb[fracID][i]=",E1Nb[fracID][i])
                            # print("E1Nb[fracID][j]=",E1Nb[fracID][j])
                            E1Nb[fracID][j]=E1Nb[fracID][i]
                            if (checkNode[i]==0):
                                checkNode[j]=i+1
                            else:
                                checkNode[j]=checkNode[i]
                            for k in range(0,len(addSege)):
                                for kk in range(0,2):
                                    if (addSege[k][kk]==j):
                                        addSege[k][kk]=checkNode[j]-1
        # print("checkNode=",checkNode)
        # print("len(addSege)=",len(addSege))
        # print("AF addSege")
        # for i in range(0, len(addSege)):
        #     print("addSege=", addSege[i][0],addSege[i][1])
        # print(E1Nb[fracID])
        # quit()

        '''
        計算3d 裂隙 mapping至局部2D座標，平面方程式、單位向量、原點
        '''
        Eq2d,unit,org_x,org_y,org_z = threeMapTwoD(EPSILON,xyz2d[0][0],xyz2d[0][1],xyz2d[0][2],xyz2d[1][0],xyz2d[1][1],xyz2d[1][2],xyz2d[2][0],xyz2d[2][1],xyz2d[2][2])

        #裂隙頂點2d轉換
        xy=[]
        for i in range(0, len(xyz2d)):
            tempx=(Eq2d[0][0]*xyz2d[i][0]+Eq2d[0][1]*xyz2d[i][1]+Eq2d[0][2]*xyz2d[i][2]-Eq2d[0][3])/((Eq2d[0][0]*Eq2d[0][0]+Eq2d[0][1]*Eq2d[0][1]+Eq2d[0][2]*Eq2d[0][2])**0.5e0)
            tempy=(Eq2d[1][0]*xyz2d[i][0]+Eq2d[1][1]*xyz2d[i][1]+Eq2d[1][2]*xyz2d[i][2]-Eq2d[1][3])/((Eq2d[1][0]*Eq2d[1][0]+Eq2d[1][1]*Eq2d[1][1]+Eq2d[1][2]*Eq2d[1][2])**0.5e0)
            xy.append(np.array([tempx,tempy], np.float64))

        #TTI頂點2d轉換
        TTIxyz=[]
        for i in range(0, len(TTIxyz2d)):
            tempx=(Eq2d[0][0]*TTIxyz2d[i][0]+Eq2d[0][1]*TTIxyz2d[i][1]+Eq2d[0][2]*TTIxyz2d[i][2]-Eq2d[0][3])/((Eq2d[0][0]*Eq2d[0][0]+Eq2d[0][1]*Eq2d[0][1]+Eq2d[0][2]*Eq2d[0][2])**0.5e0)
            tempy=(Eq2d[1][0]*TTIxyz2d[i][0]+Eq2d[1][1]*TTIxyz2d[i][1]+Eq2d[1][2]*TTIxyz2d[i][2]-Eq2d[1][3])/((Eq2d[1][0]*Eq2d[1][0]+Eq2d[1][1]*Eq2d[1][1]+Eq2d[1][2]*Eq2d[1][2])**0.5e0)
            TTIxyz.append(np.array([tempx,tempy], np.float64))

        # pyToTecplot.PrintSege2DPoint(xy,SegeNb)
        # pyToTecplot.PrintSege2DPoint(TTIxyz,addSege)
        if (fracID==tagFracID):
            outfile_name=r'fracture2D.dat'
            pyToTecplot.TecplotLINESEG2D(outfile_name,xy,SegeNb)
            outfile_name=r'Intersect2D.dat'
            pyToTecplot.TecplotLINESEG2D(outfile_name,TTIxyz,addSege)                         
        # quit()

        '''
        測試TTI線段是否有交點，判斷三面共頂點
        '''
        # print("len(addSege)=",len(addSege)) 
        # print("須檢查addSege是否新增加線段也有測試")
        # print("BF E1Nb[fracID]",E1Nb[fracID])
        '''
        1. 判斷Sxy是否與SS, SE頂點座標相同，GSameP,SameP[0:3]=0 不同， =1相同 
        2. 必須與SSSE四點皆不同GSameP=0，不同則要新增頂點xyz，新增頂點編號
        3. 若與SSSE其中一點相同，則不新增SS或SE之線段
        '''   
        # print("========")
        # print("BF globalAddSege")
        # for i in range(0, len(globalAddSege)):    
        #     print(globalAddSege[i])
        # print("========")
        # print("========")
        # print("BF addSege")
        # print(addSege)
        # print("========")
        
        # print("len(addSege)=",len(addSege))
        i=-1
        while i<len(addSege)-1:
        # for i in range(0, len(addSege)-1):            
            i=i+1
            # if (fracID==tagFracID):
            #     print("len(addSege)=",len(addSege))
            #     print("i=",i)
            if (addSege[i][0]!=addSege[i][1]):
                SS0=addSege[i][0]
                SS1=addSege[i][1]

                j=i
                while j<len(addSege)-1:
                # for j in range(i+1, len(addSege)):
                    j=j+1
                    SE0=addSege[j][0]
                    SE1=addSege[j][1]  
                    if (addSege[j][0]!=addSege[j][1]):           
                        if (SS0==SE0 or SS0==SE1 or SS1==SE0 or SS1==SE1):
                            continue
                        else:
                            # if (fracID==tagFracID):
                                # print("i,i=",i,j)
                                # print("SS0,SS1=,",SS0,SS1,"; SE0,SE1=",SE0,SE1)
                                # print(TTIxyz[SS0][0],TTIxyz[SS0][1])
                                # print(TTIxyz[SS1][0],TTIxyz[SS1][1])
                                # print(TTIxyz[SE0][0],TTIxyz[SE0][1])
                                # print(TTIxyz[SE1][0],TTIxyz[SE1][1])
                                # outfile_name=r'Intersect2D.dat'
                                # pyToTecplot.TecplotLINESEG2D(outfile_name,TTIxyz,addSege) 
                                        
                            Sx,Sy,ind = SLineF(TTIxyz[SS0][0],TTIxyz[SS0][1],TTIxyz[SS1][0],TTIxyz[SS1][1],TTIxyz[SE0][0],TTIxyz[SE0][1],TTIxyz[SE1][0],TTIxyz[SE1][1],SQEPS)
                            if (ind==1):
                                # if (fracID==tagFracID):
                                #     print("i,i=",i,j)
                                #     print("ind,SQEPS=",ind,SQEPS)                                    
                                #     print("SS0,SS1=,",SS0,SS1,"; SE0,SE1=",SE0,SE1)
                                #     print(TTIxyz[SS0][0],TTIxyz[SS0][1])
                                #     print(TTIxyz[SS1][0],TTIxyz[SS1][1])
                                #     print(TTIxyz[SE0][0],TTIxyz[SE0][1])
                                #     print(TTIxyz[SE1][0],TTIxyz[SE1][1])
                                #     print("Sx,Sy=",Sx,Sy)
                                #     # outfile_name=r'Intersect2D.dat'
                                #     # pyToTecplot.TecplotLINESEG2D(outfile_name,TTIxyz,addSege)                                
                                # print("i,i=",i,j)
                                # print("SS0,SS1=,",SS0,SS1,"; SE0,SE1=",SE0,SE1)
                                IsSxyIn = tool.pointInTwoSege(Sx,Sy,TTIxyz[SS0][0],TTIxyz[SS0][1],TTIxyz[SS1][0],TTIxyz[SS1][1],TTIxyz[SE0][0],TTIxyz[SE0][1],TTIxyz[SE1][0],TTIxyz[SE1][1],SQEPS)
                                if (IsSxyIn>-1): #IsSxyIn=-1 不在SS SE線段內
                                    if (fracID==tagFracID):
                                        print("i,i=",i,j)
                                        print("ind,IsSxyIn,SQEPS=",ind,IsSxyIn,SQEPS)                                    
                                        print("SS0,SS1=,",SS0,SS1,"; SE0,SE1=",SE0,SE1)
                                        print(TTIxyz[SS0][0],TTIxyz[SS0][1])
                                        print(TTIxyz[SS1][0],TTIxyz[SS1][1])
                                        print(TTIxyz[SE0][0],TTIxyz[SE0][1])
                                        print(TTIxyz[SE1][0],TTIxyz[SE1][1])
                                        print("Sx,Sy=",Sx,Sy)
                                        # outfile_name=r'Intersect2D.dat'
                                        # pyToTecplot.TecplotLINESEG2D(outfile_name,TTIxyz,addSege)     
                                    # print("i,i=",i,j)
                                    # print("SS0,SS1=,",SS0,SS1,"; SE0,SE1=",SE0,SE1)
                                    GSameP,SameP = tool.ISPinSegeSame(Sx,Sy,TTIxyz[SS0][0],TTIxyz[SS0][1],TTIxyz[SS1][0],   TTIxyz [SS1][1],TTIxyz[SE0][0],TTIxyz[SE0][1],TTIxyz[SE1][0],TTIxyz[SE1][1],SQEPS)
                                    # if(fracID==tagFracID):
                                    #     print("NbEleInter")
                                    #     print(NbEleInter)
                                    addNbEleInter = getFracIDinNbEleInter(NbEleInter,E1Nb,fracID,SS0,SS1,SE0,SE1,tagFracID)
                                    # print("addNbEleInter=",addNbEleInter)
                                    globalSS=copy.deepcopy(globalAddSege[fracID][i])
                                    globalSE=copy.deepcopy(globalAddSege[fracID][j])

                                    # print("addNbEleInter=",addNbEleInter)
                                    # print("globalSS=",globalSS)
                                    # print("globalSE=",globalSE)

                                    if (GSameP==0):
                                        globalNbNew=len(xyzInter)
                                        for ii in range(0, len(addNbEleInter)):
                                            if (addNbEleInter[ii]==fracID):
                                                localNbNew=no_TTI_point[fracID]                                        
                                                # print(addSege[i],addSege[j])                                        
                                                TTIxyz.append(np.array([Sx, Sy], np.float64))
                                                # print("BF localAddSege=",localAddSege[addNbEleInter[ii]])
                                                addSege.append([localNbNew,SS1])
                                                addSege.append([localNbNew,SE1])
                                                addSege[i][1]=localNbNew
                                                addSege[j][1]=localNbNew
                                                checkNode.append(np.array([0],dtype=np.int32))

                                                TransE1Nb.append(-1) #準備每個裂隙頂點+TII的線段頂點，對應mesh的編號，預設-1

                                                no_TTI_point[addNbEleInter[ii]]=no_TTI_point[addNbEleInter[ii]]+1

                                                # print("fracID=",addNbEleInter[ii], "BF E1Nb",E1Nb[addNbEleInter[ii]])
                                                E1Nb[addNbEleInter[ii]].append(globalNbNew)
                                                NbEleInter.append(addNbEleInter)
                                                # Nb=len(NbEleInter)-1
                                                # print("NbEleInter[Nb]=",NbEleInter[Nb])
                                                # print("fracID=",addNbEleInter[ii],"AF E1Nb",E1Nb[addNbEleInter[ii]])

                                                SS1=addSege[i][1] # SS1需要更改
                                                # print(addSege[i],addSege[j])

                                                # print("AF localAddSege=",localAddSege[addNbEleInter[ii]])
                                                # print("=======")
                                                # print("BF globalAddSege=",globalAddSege[addNbEleInter[ii]])
                                                globalAddSege[addNbEleInter[ii]].append([globalNbNew,globalAddSege  [addNbEleInter    [ii]][i][1]])
                                                globalAddSege[addNbEleInter[ii]].append([globalNbNew,globalAddSege  [addNbEleInter    [ii]][j][1]])
                                                globalAddSege[addNbEleInter[ii]][i][1]=globalNbNew
                                                globalAddSege[addNbEleInter[ii]][j][1]=globalNbNew
                                                # print("AF globalAddSege=",globalAddSege[addNbEleInter[ii]])
                                                x3d = unit[0][0]*Sx+unit[1][0]*Sy+org_x
                                                y3d = unit[0][1]*Sx+unit[1][1]*Sy+org_y
                                                z3d = unit[0][2]*Sx+unit[1][2]*Sy+org_z
                                                xyzInter.append(np.array([x3d, y3d, z3d], np.float64))
                                                # print(len(xyzInter))

                                            elif (addNbEleInter[ii]>fracID):

                                                localNbNew=no_TTI_point[addNbEleInter[ii]]  
                                                # print("fracID=",addNbEleInter[ii],"localNbNew",localNbNew)           
                                                # print("fracID=",addNbEleInter[ii],"BF loc sege",localAddSege[addNbEleInter    [ii]    ])
                                                # print("fracID=",addNbEleInter[ii],"BF glo Sege=",globalAddSege    [addNbEleInter  [ii]])

                                                no_TTI_point[addNbEleInter[ii]]=no_TTI_point[addNbEleInter[ii]]+1

                                                # print("fracID=",addNbEleInter[ii],"BF E1Nb",E1Nb[addNbEleInter[ii]])
                                                E1Nb[addNbEleInter[ii]].append(globalNbNew)
                                                # print("fracID=",addNbEleInter[ii],"AF E1Nb",E1Nb[addNbEleInter[ii]])

                                                for jj in range(0, len(globalAddSege[addNbEleInter[ii]])):
                                                    if (globalAddSege[addNbEleInter[ii]][jj]==globalSS or globalAddSege[addNbEleInter[ii]][jj]==globalSE):
                                                        # print("fracID=",addNbEleInter[ii],"Sege NB=",jj)
                                                        globalAddSege[addNbEleInter[ii]].append([globalNbNew,globalAddSege[addNbEleInter[ii]][jj][1]])
                                                        globalAddSege[addNbEleInter[ii]][jj][1]=globalNbNew
                                                        localAddSege[addNbEleInter[ii]].append([localNbNew,localAddSege[addNbEleInter[ii]][jj][1]])
                                                        localAddSege[addNbEleInter[ii]][jj][1]=localNbNew
                                                # print("fracID=",addNbEleInter[ii],"AF loc sege",localAddSege[addNbEleInter    [ii]    ])
                                                # print("fracID=",addNbEleInter[ii],"AF glo Sege=",globalAddSege    [addNbEleInter  [ii]])
                                    else:
                                        # print("GSameP!=0",GSameP)
                                        intersectionPointNb=-1
                                        globalNb=-1
                                        for ii in range(0,len(SameP)):
                                            # print("ii=",ii)
                                            # print("SameP[",ii,"]",SameP[ii])
                                            if (SameP[ii]==1):
                                                if(ii==0):
                                                    intersectionPointNb=SS0
                                                    globalNb=globalSS[0]
                                                elif (ii==1):
                                                    intersectionPointNb=SS1
                                                    globalNb=globalSS[1]
                                                elif (ii==2):
                                                    intersectionPointNb=SE0
                                                    globalNb=globalSE[0]
                                                elif (ii==3):
                                                    intersectionPointNb=SE1
                                                    globalNb=globalSE[1]
                                                break
                                        # print("intersectionPointNb=",intersectionPointNb)
                                        # print("三面共點,SameP[ii]=1，不須增加新點")
                                        # quit()
                                        for ii in range(0, len(addNbEleInter)):
                                            if (addNbEleInter[ii]==fracID):
                                                if (SameP[0]==0 and SameP[1]==0):
                                                    addSege.append([intersectionPointNb,SS1])
                                                    addSege[i][1]=intersectionPointNb
                                                    globalAddSege[addNbEleInter[ii]].append([globalNb,globalAddSege[addNbEleInter[ii]][i][1]])
                                                    globalAddSege[addNbEleInter[ii]][i][1]=globalNb
                                                if (SameP[2]==0 and SameP[3]==0):
                                                    addSege.append([intersectionPointNb,SE1])
                                                    addSege[j][1]=intersectionPointNb
                                                    globalAddSege[addNbEleInter[ii]].append([globalNb,globalAddSege[addNbEleInter[ii]][j][1]])
                                                    globalAddSege[addNbEleInter[ii]][j][1]=globalNb
                                                # print("addNbEleInter[ii]=",addNbEleInter[ii])
                                                # print("localAddSege[fracID]")
                                                # print(localAddSege[fracID])
                                                # print("globalAddSege[fracID]")
                                                # print(globalAddSege[fracID])
                                            elif (addNbEleInter[ii]>fracID):
                                                # print("addNbEleInter[ii]=",addNbEleInter[ii])
                                                if ((SameP[0]==0 and SameP[1]==0) or (SameP[2]==0 and SameP[3]==0)):
                                                    # print("localAddSege[addNbEleInter[ii]]")
                                                    # print(localAddSege[addNbEleInter[ii]])
                                                    # print("globalAddSege[addNbEleInter[ii]]")
                                                    # print(globalAddSege[addNbEleInter[ii]])
                                                    # print("globalNb=",globalNb)
                                                    localNbNew=no_TTI_point[addNbEleInter[ii]]
                                                    # print("fracID=",addNbEleInter[ii],"localNbNew",localNbNew)
                                                    if (SameP[0]==0 and SameP[1]==0):
                                                        globalSSSE=globalSS
                                                    elif (SameP[2]==0 and SameP[3]==0):
                                                        globalSSSE=globalSE
                                                    for jj in range(0, len(globalAddSege[addNbEleInter[ii]])):
                                                        if (globalAddSege[addNbEleInter[ii]][jj]==globalSSSE):
                                                            # print("jj=",jj)
                                                            # print("globalAddSege[addNbEleInter[ii]]")
                                                            # print(globalAddSege[addNbEleInter[ii]])
                                                            # print("localAddSege[addNbEleInter[ii]]")
                                                            # print(localAddSege[addNbEleInter[ii]])
                                                            # print("E1Nb[addNbEleInter[ii]]=",E1Nb[addNbEleInter[ii]])
                                                            E1Nb[addNbEleInter[ii]].append(globalNb)
                                                            # print("E1Nb[addNbEleInter[ii]]=",E1Nb[addNbEleInter[ii]])
                                                            globalAddSege[addNbEleInter[ii]].append([globalNb,globalAddSege[addNbEleInter[ii]][jj][1]])
                                                            globalAddSege[addNbEleInter[ii]][jj][1]=globalNb
                                                            localAddSege[addNbEleInter[ii]].append([localNbNew,localAddSege[addNbEleInter[ii]][jj][1]])
                                                            localAddSege[addNbEleInter[ii]][jj][1]=localNbNew
                                                            # print("globalAddSege[addNbEleInter[ii]]")
                                                            # print(globalAddSege[addNbEleInter[ii]])
                                                            # print("localAddSege[addNbEleInter[ii]]")
                                                            # print(localAddSege[addNbEleInter[ii]])
                                                    # quit()
        # if (fracID==tagFracID):
        #     print("find sege intersection OK")
        
        # print("i,j,len(addSege)=",i,j,len(addSege))
        '''
        no_xyzInter  = len(xyzInter)                             
        NbEleInter[no_xyzInter] 相交線段頂點，與哪些裂隙編號相關 # NbEleInter 0 [0, 16] 
        xyzInter[no_xyzInter] 相交線段頂點的座標 # 0 [628.60691691 148.78549866 154.20836102]
        eachNbEleInter[no_frac]，依據每片裂隙上的交線頂點是與其他裂隙連結 # eachNbEleInter 0 [16, 16]
        E1Nb[no_frac] [依據裂隙編號]紀錄每個頂點編號 # fracID= 0 E1Nb [0, 1, 2, 3]
        '''
        # print("AF globalAddSege")
        # for i in range(0,len(globalAddSege)):
        #     print(globalAddSege[i])
        # print("=========")
        # print("AF addSege")
        # print(addSege)
        # print("=========")
        # quit()

        # TTIxyz=[(2.0,2.0),(0.0,0.0),(3.0,1.0),(4.0,0.0)]
        # addSege=np.array([(0,1),(2,3)],dtype=np.int32)
        # xy=[(0.0,5.0),(0.0,0.0),(5.0,0.0),(5.0,5.0)]
        # print("addSege")
        # print(addSege)       
        # IS TTIxyz at xy
        # print("checkNode=",checkNode)

        '''
        測試TTI是否與xy重複
        # globalAddSege
        #TTIxyz2d發生重複，頂點編號比較大的checkNode[j]=i+1，為了避免i=0，無法使用負號
        #發生重複，xyIsTTI[j]=i+1
        '''

        # print("BF checkNode")
        # print(checkNode)
        # print("BF addSege")
        # print(addSege)

        checkTTIxyz=np.zeros(len(TTIxyz),dtype=np.int32)
        xyIsTTI=np.zeros(len(xy),dtype=np.int32)
        for i in range(0, len(TTIxyz)): 
            if (checkNode[i]>0):
                checkTTIxyz[i]=-1
            elif (checkNode[i]==0):                                   
                for j in range(0, len(xy)):
                    Tdis = tool.dis2d(TTIxyz[i][0],TTIxyz[i][1],xy[j][0],xy[j][1])
                    # print(i,j,Tdis)
                    if (Tdis<=SQEPS):                    
                        checkTTIxyz[i]=-1
                        for k in range(0, len(addSege)):
                            for kk in range(0,2):
                                if (addSege[k][kk]==i):
                                    # print("i,j=",i,j)
                                    # print(k,kk,addSege[k][kk])
                                    addSege[k][kk]=-j-1
                                    if (xyIsTTI[j]==0): 
                                        xyIsTTI[j]=i+1
                        # print("TTI與xy頂點重複，需要補程式")
                        # print("如果重點，需要修改globalAddSege")
                        # print("TTIxyz i=",i,"xy j=",j)
                        # quit()
        # print("AF xyIsTTI")
        # print(xyIsTTI)
        # print("AF addSege")
        # print(addSege)      
        # print("AF checkTTIxyz")
        # print(checkTTIxyz)          
        # quit()
       

        no_xy=len(xy)
        # print("old no_xy",no_xy)
        for i in range(len(checkTTIxyz)):
            if (checkTTIxyz[i]==0):
                xy.append(np.array([TTIxyz[i][0],TTIxyz[i][1]], np.float64))
                checkTTIxyz[i]=no_xy
                no_xy=no_xy+1
        # print("fracID=",fracID)
        # print("no_xy=",no_xy)
        # for i in range(len(checkTTIxyz)):        
        #     print(i,checkNode[i],checkTTIxyz[i]) 
        # print("AAF checkTTIxyz")
        # print(checkTTIxyz)                   
        # print("=========")
        # quit()

        '''
        測試TTI是否落在xy邊，則要新增Sege
        先依據TTI是否是重複點(checkTTIxyz[i]!=-1)，再增加SegeNb
        '''
        # print("BF SegeNb=",SegeNb)
        for i in range(0, len(TTIxyz)):
            if (checkTTIxyz[i]!=-1):
                # print("checkTTIxyz[i]=",checkTTIxyz[i])
                # print("TTIxyz[",i,"]=",TTIxyz[i])
                for j in range(0,len(SegeNb)):
                    Tdis = tool.dis2d(xy[SegeNb[j][0]][0],xy[SegeNb[j][0]][1],xy[SegeNb[j][1]][0],xy[SegeNb[j][1]][1])
                    Rdis = tool.dis2d(xy[SegeNb[j][0]][0],xy[SegeNb[j][0]][1],TTIxyz[i][0],TTIxyz[i][1])
                    Ldis = tool.dis2d(TTIxyz[i][0],TTIxyz[i][1],xy[SegeNb[j][1]][0],xy[SegeNb[j][1]][1])
                    if (abs(Tdis-Rdis-Ldis)<=SQEPS):
                        # print("checkTTIxyz[i]=",checkTTIxyz[i])
                        # print("TTIxyz[",i,"]=",TTIxyz[i])
                        # print("j,(",xy[SegeNb[j][0]][0],xy[SegeNb[j][0]][1],",",xy[SegeNb[j][1]][0],xy[SegeNb[j][1]][1],")")
                        SegeNb.append(np.array([checkTTIxyz[i], SegeNb[j][1]], np.int32))
                        SegeNb[j][1]=checkTTIxyz[i]
        # print("AF SegeNb=",SegeNb)
        # quit()

        # print("addSege")
        # print(addSege) #[[0, 1], [1, 3]]
        # quit()
        
        tS=np.zeros(2,dtype=int)
        for i in range(0, len(addSege)):
            S0 =addSege[i][0]
            S1 =addSege[i][1]
            if (S0!=S1):                
                for j in range(0, 2):
                    if (addSege[i][j]>=0):
                        # addSege[i][j]=checkTTIxyz[addSege[i][j]]
                        tS[j]=checkTTIxyz[addSege[i][j]]
                    else:
                        # addSege[i][j]=-(addSege[i][j]+1)
                        tS[j]=-(addSege[i][j]+1)
                SegeNb.append(np.array([tS[0], tS[1]], np.int32))
        # print("0F SegeNb=",SegeNb)
        # quit()

        # print("addSege")
        # print(addSege) #[[4, 5], [5, 6]]
        # print("E1Nb[fracID]")
        # print(E1Nb[fracID]) #[0, 1, 2, 3]
        # print("SegeNb")
        # print(SegeNb)

        # print("--------")
        # print("len(xy)=",len(xy))
        # for i in range(0, len(xy)):
        #     print(xy[i][0],xy[i][1],0.0)
        # print("len(SegeNb)=",len(SegeNb))
        # for i in range(0, len(SegeNb)): 
        #     print(SegeNb[i][0]+1,SegeNb[i][1]+1) 
        # print("--------")
        # quit()


        info = triangle.MeshInfo()
        info.set_points(xy)
        info.set_facets(SegeNb)
        mesh = triangle.build(info,quality_meshing=False)
        mesh_points = np.array(mesh.points)
        mesh_tris = np.array(mesh.elements)
        # mesh_points[0][0] #<class 'numpy.float64'>
        # mesh_tris[0][0] #<class 'numpy.int64'>

        outfile_name='MeshRes/Mesh'+'_'+str('%02d' % fracID)+'.dat'
        pyToTecplot.TecplotMesh2D(outfile_name,mesh_points,mesh_tris,xy,SegeNb,TTIxyz,plotaddSege)
        # quit()
        # print("len(xy)=",len(xy))
        # print("len(xyz)=",len(xyz))
        # print("len(TransE1Nb)=",len(TransE1Nb))
        if (len(xyz)==0):
            # print("xyz fracID=",fracID)
            # print("checkTTIxyz=",checkTTIxyz)
            # print("checkNode=",checkNode)
            # print("xyIsTTI=",xyIsTTI)
            # print("addSege=",addSege)
            # print("globalAddSege=",globalAddSege[fracID])
            # print("E1Nb=",E1Nb[fracID])
            # print("NodeNb=",NodeNb)
            # quit()

            for i in range(0, len(mesh_points)):
                x3d = unit[0][0]*mesh_points[i][0]+unit[1][0]*mesh_points[i][1]+org_x
                y3d = unit[0][1]*mesh_points[i][0]+unit[1][1]*mesh_points[i][1]+org_y
                z3d = unit[0][2]*mesh_points[i][0]+unit[1][2]*mesh_points[i][1]+org_z
                xyz.append(np.array([x3d, y3d, z3d], np.float64))
            for i in range(0, len(mesh_tris)):
                ele_ind.append(np.array(mesh_tris[i]))
                cell_scalar.append(fracID)
            for i in range(0,len(NodeNb)):
                TransE1Nb[NodeNb[i]]=i
            no_frac_XYZ=len(frac_XYZ)
            for i in range(0, len(checkTTIxyz)):
                if(checkTTIxyz[i]!=-1):
                    Nb=i+no_frac_XYZ
                    TransE1Nb[Nb]=checkTTIxyz[i]

            # for i in range(0,no_frac_XYZ):
            #     print("F",i,TransE1Nb[i])
            # for i in range(0,len(xyzInter)):
            #     Nb=no_frac_XYZ+i
            #     print("IT",i,TransE1Nb[Nb])


            # for i in range(0,len(TransE1Nb)):
            #     print("i,TransE1Nb=",i,TransE1Nb[i])
            # quit()
            # j=len(TransE1Nb)-1
            # print("TransE1Nb[j] j=",j)
            # for i in range(0, len(checkTTIxyz)):
            #     TransE1Nb[j][i]=-TransE1Nb[j][i]
        else:

            # print("xyz fracID=",fracID)
            # print("checkTTIxyz=",checkTTIxyz)
            # print("checkNode=",checkNode)
            # print("xyIsTTI=",xyIsTTI)
            # print("addSege=",addSege)
            # print("globalAddSege=",globalAddSege[fracID])
            # print("E1Nb=",E1Nb[fracID])
            # print("NodeNb=",NodeNb)
            # quit()
            no_xyz=len(xyz)
            locTransNb=[]

            for i in range(0,len(NodeNb)):
                # print("NodeNb,TransE1Nb=",NodeNb[i],TransE1Nb[NodeNb[i]])
                if (TransE1Nb[NodeNb[i]]!=-1):
                    locTransNb.append(TransE1Nb[NodeNb[i]])
                else:
                    x3d =frac_XYZ[NodeNb[i]][0]
                    y3d =frac_XYZ[NodeNb[i]][1]
                    z3d =frac_XYZ[NodeNb[i]][2]
                    xyz.append(np.array([x3d, y3d, z3d], np.float64))
                    locTransNb.append(no_xyz)
                    TransE1Nb[NodeNb[i]]=no_xyz
                    no_xyz=no_xyz+1
            # for i in range(0,len(locTransNb)):
            #     print("i,locTransNb[i]=",i,locTransNb[i])
            # print("========")                    
            
            for i in range(0,len(E1Nb[fracID])):
                if (checkTTIxyz[i]!=-1):
                    Nb=E1Nb[fracID][i]+no_frac_XYZ
                    # print("E1Nb,TransE1Nb=",i,E1Nb[fracID][i],TransE1Nb[Nb])
                    if (TransE1Nb[Nb]!=-1):
                        locTransNb.append(TransE1Nb[Nb])
                    else:
                        x3d =xyzInter[E1Nb[fracID][i]][0]
                        y3d =xyzInter[E1Nb[fracID][i]][1]
                        z3d =xyzInter[E1Nb[fracID][i]][2]
                        xyz.append(np.array([x3d, y3d, z3d], np.float64))  
                        locTransNb.append(no_xyz)
                        TransE1Nb[Nb]=no_xyz
                        no_xyz=no_xyz+1                                 

            # for i in range(0,len(locTransNb)):
            #     print("i,locTransNb[i]=",i,locTransNb[i])

            for i in range(0, len(mesh_tris)):
                # print("BF mesh_tris[i]=",mesh_tris[i])
                for j in range(0,3):
                    mesh_tris[i][j]=locTransNb[mesh_tris[i][j]]
                # print("AF mesh_tris[i]=",mesh_tris[i])
                ele_ind.append(np.array(mesh_tris[i]))
                cell_scalar.append(fracID)
            # quit()
            # print("fracID,len(xyz)=",fracID,len(xyz))


            # # biggestNB = E1Nb[fracID-1][len(E1Nb[fracID-1])-1]
            # biggestNB = globalBiggestNB[fracID-1]
            # print("biggestNB=",biggestNB)
            # # print("old_no_node=",old_no_node)
            # # print("old no_xy=",len(xyz2d))
            # testArray=np.zeros(len(mesh_points), dtype=np.int32)
            # for i in range(0, len(xyz2d)):
            #     xyzAddInd=0
            #     if (xyIsTTI[i]==0):
            #         testArray[i]=old_no_node+i
            #         xyzAddInd=1
            #     else:
            #         if (E1Nb[fracID][xyIsTTI[i]]>biggestNB):
            #             testArray[i]=old_no_node+i
            #             xyzAddInd=1
            #         else:
            #             tFId0=NbEleInter[E1Nb[fracID][xyIsTTI[i]]][0]
            #             tFId1=NbEleInter[E1Nb[fracID][xyIsTTI[i]]][1]
            #             if (fracID==tFId1):
            #                 tf=tFId0
            #             else:
            #                 tf=tFId0
            #             for j in range(0,len(E1Nb[tf])):
            #                 if (E1Nb[tf][j]==E1Nb[fracID][xyIsTTI[i]]):
            #                     testArray[i]=TransE1Nb[tf][j]
            #     if (xyzAddInd==1):
            #         x3d = unit[0][0]*mesh_points[i][0]+unit[1][0]*mesh_points[i][1]+org_x
            #         y3d = unit[0][1]*mesh_points[i][0]+unit[1][1]*mesh_points[i][1]+org_y
            #         z3d = unit[0][2]*mesh_points[i][0]+unit[1][2]*mesh_points[i][1]+org_z
            #         xyz.append(np.array([x3d, y3d, z3d], np.float64))

            # tempTrans =[]
            # # print("fracID=",fracID)
            # # print("checkTTIxyz")
            # # print(checkTTIxyz)
            # for i in range(0,len(E1Nb[fracID])):
            #     # print("E1Nb[fracID][i]=",E1Nb[fracID][i])
            #     # print("checkTTIxyz[i]=",checkTTIxyz[i])
            #     if(checkTTIxyz[i]>0):
            #         if (E1Nb[fracID][i]>biggestNB):
            #             testArray[checkTTIxyz[i]]=checkTTIxyz[i]+old_no_node
            #             x3d = unit[0][0]*mesh_points[checkTTIxyz[i]][0]+unit[1][0]*mesh_points[checkTTIxyz[i]][1]+org_x
            #             y3d = unit[0][1]*mesh_points[checkTTIxyz[i]][0]+unit[1][1]*mesh_points[checkTTIxyz[i]][1]+org_y
            #             z3d = unit[0][2]*mesh_points[checkTTIxyz[i]][0]+unit[1][2]*mesh_points[checkTTIxyz[i]][1]+org_z
            #             xyz.append(np.array([x3d, y3d, z3d], np.float64))
            #             tempTrans.append(checkTTIxyz[i]+old_no_node)
            #         else:
            #             # print("=========")
            #             # print("E1Nb[fracID][i]=",E1Nb[fracID][i])
            #             # print("checkTTIxyz[i]=",checkTTIxyz[i])
            #             tFId0=NbEleInter[E1Nb[fracID][i]][0]
            #             tFId1=NbEleInter[E1Nb[fracID][i]][1]
            #             if (fracID==tFId1):
            #                 tf=tFId0
            #                 for j in range(0,len(E1Nb[tf])):
            #                     if (E1Nb[tf][j]==E1Nb[fracID][i]):
            #                         # print("fracID=",fracID)
            #                         # print("len(testArray)=",len(testArray))
            #                         # print("len(TransE1Nb)=",len(TransE1Nb))
            #                         # print("tf=",tf,"j=",j)
            #                         # print("len(TransE1Nb[tf])=",len(TransE1Nb[tf]))
            #                         testArray[checkTTIxyz[i]]=TransE1Nb[tf][j]
            #             else:
            #                 testArray[checkTTIxyz[i]]=checkTTIxyz[i]+old_no_node
            #                 x3d = unit[0][0]*mesh_points[checkTTIxyz[i]][0]+unit[1][0]*mesh_points[checkTTIxyz[i]][1]+org_x
            #                 y3d = unit[0][1]*mesh_points[checkTTIxyz[i]][0]+unit[1][1]*mesh_points[checkTTIxyz[i]][1]+org_y
            #                 z3d = unit[0][2]*mesh_points[checkTTIxyz[i]][0]+unit[1][2]*mesh_points[checkTTIxyz[i]][1]+org_z
            #                 xyz.append(np.array([x3d, y3d, z3d], np.float64))
            #                 tempTrans.append(checkTTIxyz[i]+old_no_node)                        
            #     else:
            #         # print("checkTTIxyz[i]=",checkTTIxyz[i])
            #         tempTrans.append(checkTTIxyz[i])

            # TransE1Nb.append(np.array(tempTrans,dtype=np.int32))
            # # print("len(testArray)=",len(testArray))
            # # print("testArray")
            # # print(testArray)
            # # print("TransE1Nb")
            # # print(TransE1Nb)
            # # print("======")
            # # print("======")

            # me2=np.zeros(3,dtype=np.int32)
            # for i in range(0, len(mesh_tris)):
            #     for j in range(0,3):
            #         me2[j] = testArray[mesh_tris[i][j]]
            #     ele_ind.append(np.array(me2))   


        outfile_name='MeshRes/out'+'_'+str('%02d' % fracID)+'.dat'
        with open(outfile_name,'w',encoding = 'utf-8') as f:
            # f.write('Title="XY2D_plot"\n')
            # f.write('Variables="x(m)","y(m)","z(m)"\n')
            # f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
            # for i in range(0, len(xyz)):
            #     f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
            # for i in range(0,len(ele_ind)):
            #     f.write(str(ele_ind[i][0]+1)+' '+str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+'\n')
            # f.close()
            f.write('Title="XY2D_plot"\n')
            f.write('Variables="x(m)","y(m)","z(m)","ID"\n')
            f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=TRIANGLE\n') 
            f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
            for i in range(0, len(xyz)):
                f.write(str(xyz[i][0])+'\n')
            for i in range(0, len(xyz)):
                f.write(str(xyz[i][1])+'\n')
            for i in range(0, len(xyz)):
                f.write(str(xyz[i][2])+'\n')
            for i in range(0, len(cell_scalar)):
                f.write(str(cell_scalar[i])+'\n')
            for i in range(0,len(ele_ind)):
                f.write(str(ele_ind[i][0]+1)+' '+str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+'\n')
            f.close()



        # print("len(xyz)=",len(xyz))
        # for i in range(0, len(xyz)):
        #     print(xyz[i][0],xyz[i][1],xyz[i][2])

        # print("len(ele_ind)=",len(ele_ind))
        # for i in range(0, len(ele_ind)):
        #     print(ele_ind[i][0]+1,ele_ind[i][1]+1,ele_ind[i][2]+1)


        # print("E1Nb[fracID]=",E1Nb[fracID])
        # print("checkTTIxyz=",checkTTIxyz)

        

        # TransE1Nb.append(checkTTIxyz)
        # j=len(TransE1Nb)-1
        # print("TransE1Nb[j] j=",j)
        # for i in range(0, len(checkTTIxyz)):
        #     TransE1Nb[j][i]=-TransE1Nb[j][i]
        # print("TransE1Nb=",TransE1Nb)

        old_no_node = len(xyz) 
        # print("old_no_node=",old_no_node)
        # quit()
    outfile_name='MeshRes/outFinish.dat'
    with open(outfile_name,'w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
    #     for i in range(0, len(xyz)):
    #         f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
    #     for i in range(0,len(ele_ind)):
    #         f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+' \n')
    #     f.close()
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m),"ID"\n')
        f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=TRIANGLE\n') 
        f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0, len(cell_scalar)):
            f.write(str(cell_scalar[i])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+' '+str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+'\n')
        f.close()    
    return 0


def threeMapTwoD(EPSILON,tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3):
    import numpy as np
    Eq2d = []
    unit = []

    org_x=tx1
    org_y=ty1
    org_z=tz1

    mx=(org_x+tx2)/2.0e0
    my=(org_y+ty2)/2.0e0
    mz=(org_z+tz2)/2.0e0

    cx,cy,cz = Circumcenter_new(org_x,org_y,org_z,tx2,ty2,tz2,tx3,ty3,tz3) 
    # print("org_x,org_y,org_z=",org_x,org_y,org_z)   
    # print("mx,my,mz=",mx,my,mz)   
    # print("cx,cy,cz=",cx,cy,cz)
    # quit() 

    a1=tx2-tx1
    b1=ty2-ty1
    c1=tz2-tz1
    d1=a1*tx1+b1*ty1+c1*tz1
    a2=mx-cx
    b2=my-cy
    c2=mz-cz
    d2=a2*tx1+b2*ty1+c2*tz1

    if ((a2*a2+b2*b2+c2*c2)<=EPSILON):          
        mx=(org_x+tx3)/2.0e0
        my=(org_y+ty3)/2.0e0
        mz=(org_z+tz3)/2.0e0           
        a1=tx3-tx1
        b1=ty3-ty1
        c1=tz3-tz1
        d1=a1*tx1+b1*ty1+c1*tz1
        a2=mx-cx
        b2=my-cy
        c2=mz-cz
        d2=a2*tx1+b2*ty1+c2*tz1


    len_unit_vector01=(a1*a1+b1*b1+c1*c1)**0.5e0
    unit_vector01_x = a1/len_unit_vector01
    unit_vector01_y = b1/len_unit_vector01
    unit_vector01_z = c1/len_unit_vector01
    # print(type(unit_vector01_x))
    unit.append(np.array([unit_vector01_x,unit_vector01_y,unit_vector01_z], dtype=np.float64))

    len_unit_vector02=(a2*a2+b2*b2+c2*c2)**0.5e0
    unit_vector02_x = a2/len_unit_vector02
    unit_vector02_y = b2/len_unit_vector02
    unit_vector02_z = c2/len_unit_vector02
    unit.append(np.array([unit_vector02_x,unit_vector02_y,unit_vector02_z], dtype=np.float64))


    Eq2d.append(np.array([a1, b1, c1, d1], np.float64))
    Eq2d.append(np.array([a2, b2, c2, d2], np.float64))

    # tempx=(Eq2d[0][0]*tx3+Eq2d[0][1]*ty3+Eq2d[0][2]*tz3-Eq2d[0][3])/((Eq2d[0][0]*Eq2d[0][0]+Eq2d[0][1]*Eq2d[0][1]+Eq2d[0][2]*Eq2d[0][2])**0.5e0)
    # tempy=(Eq2d[1][0]*tx3+Eq2d[1][1]*ty3+Eq2d[1][2]*tz3-Eq2d[1][3])/((Eq2d[1][0]*Eq2d[1][0]+Eq2d[1][1]*Eq2d[1][1]+Eq2d[1][2]*Eq2d[1][2])**0.5e0)
    # print("testEq2d")
    # print(tx3,ty3,tz3)
    # print(tempx,tempy,0.0)
    # x3d = unit_vector01_x*tempx+unit_vector02_x*tempy+org_x
    # y3d = unit_vector01_y*tempx+unit_vector02_y*tempy+org_y
    # z3d = unit_vector01_z*tempx+unit_vector02_z*tempy+org_z
    # print(x3d,y3d,z3d)
    # quit()
    
    return Eq2d,unit,org_x,org_y,org_z

def Circumcenter_new(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    import numpy as np
    x12=x1-x2
    x21=x2-x1
    x13=x1-x3
    x31=x3-x1
    x23=x2-x3
    x32=x3-x2

    y12=y1-y2
    y21=y2-y1
    y13=y1-y3
    y31=y3-y1
    y23=y2-y3
    y32=y3-y2

    z12=z1-z2
    z21=z2-z1
    z13=z1-z3
    z31=z3-z1
    z23=z2-z3
    z32=z3-z2

    a=np.float64(0.0)
    b=np.float64(0.0)
    c=np.float64(0.0)
    two=np.float64(2.0)

    crx,cry,crz = cross_V_new(x12,y12,z12,x23,y23,z23)
    dis = length_new(crx,cry,crz)
    dis=(two*dis*dis)
    a=(x23*x23+y23*y23+z23*z23)*(x12*x13+y12*y13+z12*z13)/dis
    b=(x13*x13+y13*y13+z13*z13)*(x21*x23+y21*y23+z21*z23)/dis
    c=(x12*x12+y12*y12+z12*z12)*(x31*x32+y31*y32+z31*z32)/dis

    cx=a*x1+b*x2+c*x3
    cy=a*y1+b*y2+c*y3
    cz=a*z1+b*z2+c*z3
    return cx,cy,cz

def cross_V_new(x1,y1,z1,x2,y2,z2):
    x3=y1*z2-z1*y2
    y3=-x1*z2+z1*x2
    z3=x1*y2-y1*x2
    return x3,y3,z3

def length_new(x,y,z):
    import numpy as np
    o5=np.float64(0.5)
    dis=(x*x+y*y+z*z)**o5
    return dis

def SLineF(x1,y1,x2,y2,x3,y3,x4,y4,SQEPS):
    import numpy as np
    ind=0
    if(abs(x1-x2)>SQEPS):
        a1=-(y1-y2)/(x1-x2)
        c1=(y1+a1*x1)
        b1=np.float64(1.0)
    else:
        a1=np.float64(1.0)
        b1=np.float64(0.0)
        c1=x1

    if(abs(x3-x4)>SQEPS):
        a2=-(y3-y4)/(x3-x4)
        c2=(y3+a2*x3)
        b2=np.float64(1.0)
    else:
        a2=np.float64(1.0)
        b2=np.float64(0.0)
        c2=x3
    # print("abs(a1-a2)=",abs(a1-a2))
    if (abs(a1-a2)>SQEPS):
        Sx=(c1*b2-b1*c2)/(a1*b2-b1*a2)
        Sy=(a1*c2-c1*a2)/(a1*b2-b1*a2)
        ind=1
    else:
        Sx=-9999.0
        Sy=-9999.0
        ind=0
    
    return Sx,Sy,ind

def getFracIDinNbEleInter(NbEleInter,E1Nb,fracID,SS0,SS1,SE0,SE1,tagFracID):
    addNbEleInter=[]
    # if (fracID==tagFracID):
    #     print("NbEleInter=",NbEleInter[fracID])
    #     print("E1Nb=",E1Nb[fracID])
    #     print("fracID=",fracID)
    #     print("SS0,SS1,SE0,SE1=",SS0,SS1,SE0,SE1)
    # addNbEleInter.append(NbEleInter[E1Nb[fracID][SS0]][0])
    # addNbEleInter.append(NbEleInter[E1Nb[fracID][SS0]][1])
    for ii in range(0,len(NbEleInter[E1Nb[fracID][SS0]])):
        addNbEleInter.append(NbEleInter[E1Nb[fracID][SS0]][ii])
    # if (fracID==tagFracID):
    #     print("len(addNbEleInter)=",len(addNbEleInter))
    #     print("addNbEleInter=",addNbEleInter)
    #     print("E1Nb[fracID][SE0]=",E1Nb[fracID][SE0])
    for ii in range(0, len(NbEleInter[E1Nb[fracID][SS1]])):
        QQ=0
        for jj in range(0, len(addNbEleInter)):
            if (NbEleInter[E1Nb[fracID][SS1]][ii]==addNbEleInter[jj]):
                QQ=1
        if (QQ==0):
            addNbEleInter.append(NbEleInter[E1Nb[fracID][SS1]][ii])
    for ii in range(0, len(NbEleInter[E1Nb[fracID][SE0]])):
        QQ=0
        for jj in range(0, len(addNbEleInter)):
            if (NbEleInter[E1Nb[fracID][SE0]][ii]==addNbEleInter[jj]):
                QQ=1
        if (QQ==0):
            addNbEleInter.append(NbEleInter[E1Nb[fracID][SE0]][ii]) 
    for ii in range(0, len(NbEleInter[E1Nb[fracID][SE1]])):
        QQ=0
        for jj in range(0, len(addNbEleInter)):
            if (NbEleInter[E1Nb[fracID][SE1]][ii]==addNbEleInter[jj]):
                QQ=1
        if (QQ==0):
            addNbEleInter.append(NbEleInter[E1Nb[fracID][SE1]][ii]) 
    return addNbEleInter

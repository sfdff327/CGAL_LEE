import os
import json
import calendar
from datetime import date
from numpy.core.defchararray import rpartition
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import numpy as np
import FabtoMesh
import meshpyLee
import sys

import ctypes  
import memToNparry
import tool
import pyTTI
from pathlib import Path
import pickle
import pyToTecplot
import trimeshAPI

# import pyvistaSlice

# pyvistaSlice.single_slice()

tri1 = np.empty((3, 3), dtype=np.float32)
tri1 = np.empty((3, 3), dtype=np.float32)
tri1 = [[1.0, 1.0, 5.0],[7.0, 1.0, 5.0],[7.0, 7.0, 5.0]]
tri2 = [[1.5, 6.0, 1.0],[6.5, 6.0, 1.0],[6.5, 6.0, 15.0]]
# print(type(tri1[0][1]))


EPSILON= sys.float_info.epsilon
SQEPS=np.sqrt(EPSILON)

# print("EPSILON=",EPSILON)
# print("SQEPS=",SQEPS)
# quit()

# xyzInter = np.array([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]], np.float64)
# xyz2d =[]
# coor = np.array([-1.0, -2.0, -3.0], np.float64)
# xyz2d.append(coor)
# xyz2d.append(np.array([-4.0, -5.0, -6.0], np.float64))
# print(xyz2d[0][0])
# quit()

FABResFileCheck = Path("FABRes/no_frac.out")
# print(FABResFileCheck.is_file())
if FABResFileCheck.is_file():
    with open('FABRes/frac_XYZ.out', 'rb') as file:
            frac_XYZ = pickle.load(file)        
    with open('FABRes/FracNXYZd.out', 'rb') as file:
            FracNXYZd = pickle.load(file)        
    with open('FABRes/frac_sege.out', 'rb') as file:
            frac_sege = pickle.load(file)        
    with open('FABRes/frac_segeID.out', 'rb') as file:
            frac_segeID = pickle.load(file)        
    with open('FABRes/fracID.out', 'rb') as file:
            fracID = pickle.load(file)        
    with open('FABRes/frac_ele_ind.out', 'rb') as file:
            frac_ele_ind = pickle.load(file)        
    with open('FABRes/cooY.out', 'rb') as file:
            cooY = pickle.load(file)        
    with open('FABRes/cooX.out', 'rb') as file:
            cooX = pickle.load(file)        
    with open('FABRes/no_frac.out', 'rb') as file:
            no_frac = pickle.load(file)
else:
    FabData = FabtoMesh.FabFileRead()
    print("Run FabtoMesh.FabFileRead() OK")
    no_frac = FabData.no_frac
    
    meshpyLee.test()
    print("Run meshpyLee.test() OK")

    pyTTI.findeachFracIsTheSame(FabData)
    print("Run pyTTI.findeachFracIsTheSame() OK")

    with open('FABRes/frac_XYZ.out', 'rb') as file:
            frac_XYZ = pickle.load(file)        
    with open('FABRes/FracNXYZd.out', 'rb') as file:
            FracNXYZd = pickle.load(file)        
    with open('FABRes/frac_sege.out', 'rb') as file:
            frac_sege = pickle.load(file)        
    with open('FABRes/frac_segeID.out', 'rb') as file:
            frac_segeID = pickle.load(file)        
    with open('FABRes/fracID.out', 'rb') as file:
            fracID = pickle.load(file)        
    with open('FABRes/frac_ele_ind.out', 'rb') as file:
            frac_ele_ind = pickle.load(file)        
    with open('FABRes/cooY.out', 'rb') as file:
            cooY = pickle.load(file)        
    with open('FABRes/cooX.out', 'rb') as file:
            cooX = pickle.load(file)        
    with open('FABRes/no_frac.out', 'rb') as file:
            no_frac = pickle.load(file)

trimeshAPI.test_trimeshIntersections(frac_XYZ,frac_ele_ind)

trimeshAPI.test_mesh_arrangementOBJtoTetgen()

# quit()


my_file = Path("ResTTI/no_frac.out")
if my_file.is_file():
    with open(my_file, 'rb') as file:
        test_no_frac = pickle.load(file)
    # print("test_no_frac=",test_no_frac)
    # print("no_frac=",no_frac)
    # quit()
    if (test_no_frac==no_frac):
        print("pass pyTTI.getTtiSpoints()")
        # xyzInter = np.loadtxt('ResTTI/xyzInter.out',dtype=np.float64, delimiter=',')
        # NbEleInter = np.loadtxt('ResTTI/NbEleInter.out',dtype=np.int32, delimiter=',')
        with open('ResTTI/xyzInter.out', 'rb') as file:
            xyzInter = pickle.load(file)
        xyzInter=np.array(xyzInter)
        # print("xyzInter")
        # print(xyzInter)
        # quit()
        with open('ResTTI/NbEleInter.out', 'rb') as file:
            NbEleInter = pickle.load(file)
        NbEleInter=np.array(NbEleInter)
        # print("NbEleInter")
        # print(NbEleInter)
        # quit()
        with open('ResTTI/no_xyzInter.out', 'rb') as file:
            no_xyzInter = pickle.load(file)
        # print("no_xyzInter")
        # print(no_xyzInter)
        # quit() 
        with open('ResTTI/eachNbEleInter.out', 'rb') as file:
            eachNbEleInter = pickle.load(file)
        eachNbEleInter=np.array(eachNbEleInter)
        # print("eachNbEleInter")
        # print(eachNbEleInter)
        # quit() 
        with open('ResTTI/E1Nb.out', 'rb') as file:
            E1Nb = pickle.load(file)
        E1Nb=np.array(E1Nb)
        # print("E1Nb")
        # print(E1Nb)
        # quit()

    else:
        no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb = pyTTI.getTtiSpoints(FabData)
else:
    no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb = pyTTI.getTtiSpoints(FabData)

pyToTecplot.TecplotTriangle3D(frac_XYZ ,frac_ele_ind ,fracID)
# pyTTI.creat2DPoly(no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb,FabData)
pyToTecplot.TecplotTriangle3D_By_eachNbEleInter(frac_XYZ ,frac_ele_ind ,fracID,no_frac,eachNbEleInter,frac_segeID,frac_sege)

pyTTI.creat2DPoly(no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb,no_frac,frac_XYZ,frac_ele_ind,frac_segeID,frac_sege)
 




# x11=0.0 
# y11=0.0
# z11=0.0
# x12=6.0
# y12=0.0
# z12=0.0
# x13=3.0
# y13=4.0
# z13=0.0
# x21=2.0
# y21=1.0
# z21=0.0
# x22=7.0 
# y22=-0.5 
# z22=0.0
# x23=4.0
# y23=2.0
# z23=0.0
# outdata = pyTTI.tritri(x11,y11,z11,x12,y12,z12,x13,y13,z13,x21,y21,z21,x22,y22,z22,x23,y23,z23)
# spoint = memToNparry.make_nd_array(outdata.spoint, [6,3], dtype=np.float64, order='C', own_data=True)
# checkTTI = outdata.checkTTI
# print("checkTTI=",checkTTI) #checkTTI= 6
# print("spoint=")
# print(spoint)
# # [[ 5.33333333e+00  0.00000000e+00  0.00000000e+00]
# #  [ 5.33333333e+00  8.88888889e-01  0.00000000e+00]
# #  [-9.99900000e+03 -9.99900000e+03 -9.99900000e+03]
# #  [-9.99900000e+03 -9.99900000e+03 -9.99900000e+03]
# #  [-9.99900000e+03 -9.99900000e+03 -9.99900000e+03]
# #  [-9.99900000e+03 -9.99900000e+03 -9.99900000e+03]]


print("OK")

# print("no_frac_intersection")
# print(no_frac_intersection)
# print("frac_intersection")
# print(frac_intersection)



# print('cooX=',cooX)
# print('cooY=',cooY)
# print('no_frac=',no_frac)
# print('frac_no_node=',frac_no_node)
# print('frac_no_ele=',frac_no_ele)
# print('frac_no_sege=',frac_no_sege)
# print('frac_ele_ind')
# print(frac_ele_ind)
# print('fracID')
# print(fracID)
# print('frac_sege')
# print(frac_sege)
# print('FracNXYZd')
# print(FracNXYZd)
# print('frac_XYZ')
# print(frac_XYZ)

# quit()


import os
import json
import calendar
from datetime import date
import re
from numpy.lib.function_base import append
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import ctypes
import numpy as np

sim = cdll.LoadLibrary(os.path.dirname(os.path.abspath(__file__)) + "/TTI.so")

class Triangle( Structure ):
    pass

class Outdata( Structure ):
    pass

def tritri(x11,y11,z11,x12,y12,z12,x13,y13,z13,x21,y21,z21,x22,y22,z22,x23,y23,z23):
    ndpointer = np.ctypeslib.ndpointer

    class Triangle( Structure ):
        _fields_ = [
        ( "x11", c_double ),
		( "y11", c_double ),
		( "z11", c_double ),
        ( "x12", c_double ),
		( "y12", c_double ),
		( "z12", c_double ),
        ( "x13", c_double ),
		( "y13", c_double ),
		( "z13", c_double ),
        ( "x21", c_double ),
		( "y21", c_double ),
		( "z21", c_double ),
        ( "x22", c_double ),
		( "y22", c_double ),
		( "z22", c_double ),
        ( "x23", c_double ),
		( "y23", c_double ),
		( "z23", c_double )
        ]

    class Outdata( Structure ):
        _fields_ = [
        ( "checkTTI", c_int32 ),        
		( "spoint", ndpointer(dtype=np.float64,ndim=2))
	]

    triangle = Triangle()
    triangle.memShapes = []
    outdata = Outdata()
    outdata.memShapes = []

    triangle.x11 = x11
    triangle.memShapes.append({"field_name": "x11", "dtype": np.float64, "shape": None })
    triangle.y11 = y11
    triangle.memShapes.append({"field_name": "y11", "dtype": np.float64, "shape": None })
    triangle.z11 = z11
    triangle.memShapes.append({"field_name": "z11", "dtype": np.float64, "shape": None })
    triangle.x12 = x12
    triangle.memShapes.append({"field_name": "x12", "dtype": np.float64, "shape": None })
    triangle.y12 = y12
    triangle.memShapes.append({"field_name": "y12", "dtype": np.float64, "shape": None })
    triangle.z12 = z12
    triangle.memShapes.append({"field_name": "z12", "dtype": np.float64, "shape": None })
    triangle.x13 = x13
    triangle.memShapes.append({"field_name": "x13", "dtype": np.float64, "shape": None })
    triangle.y13 = y13
    triangle.memShapes.append({"field_name": "y13", "dtype": np.float64, "shape": None })
    triangle.z13 = z13
    triangle.memShapes.append({"field_name": "z13", "dtype": np.float64, "shape": None })

    triangle.x21 = x21
    triangle.memShapes.append({"field_name": "x21", "dtype": np.float64, "shape": None })
    triangle.y21 = y21
    triangle.memShapes.append({"field_name": "y21", "dtype": np.float64, "shape": None })
    triangle.z21 = z21
    triangle.memShapes.append({"field_name": "z21", "dtype": np.float64, "shape": None })
    triangle.x22 = x22
    triangle.memShapes.append({"field_name": "x22", "dtype": np.float64, "shape": None })
    triangle.y22 = y22
    triangle.memShapes.append({"field_name": "y22", "dtype": np.float64, "shape": None })
    triangle.z22 = z22
    triangle.memShapes.append({"field_name": "z22", "dtype": np.float64, "shape": None })
    triangle.x23 = x23
    triangle.memShapes.append({"field_name": "x23", "dtype": np.float64, "shape": None })
    triangle.y23 = y23
    triangle.memShapes.append({"field_name": "y23", "dtype": np.float64, "shape": None })
    triangle.z23 = z23
    triangle.memShapes.append({"field_name": "z23", "dtype": np.float64, "shape": None })

    # null_fds, save = pipe.suppress()
    sim.TTI(byref(triangle),byref(outdata))
    # pipe.resume(null_fds, save)  

    return outdata

def findeachFracIsTheSame(FabData):
    import memToNparry
    import tool
    import sys

    EPSILON= sys.float_info.epsilon
    SQEPS=np.sqrt(EPSILON)
    
    frac_XYZ = memToNparry.make_nd_array(FabData.frac_XYZ, [FabData.frac_no_node,3], dtype=np.float64, order='C', own_data=True)
    FracNXYZd = memToNparry.make_nd_array(FabData.FracNXYZd, [FabData.no_frac,4], dtype=np.float64, order='C', own_data=True)
    frac_sege = memToNparry.make_nd_array(FabData.frac_sege, [FabData.frac_no_sege,2], dtype=np.int32, order='C', own_data=True)
    fracID = memToNparry.make_nd_array(FabData.fracID, [FabData.frac_no_ele], dtype=np.int32, order='C', own_data=True)
    frac_ele_ind = memToNparry.make_nd_array(FabData.frac_ele_ind, [FabData.frac_no_ele,3], dtype=np.int32, order='C', own_data=True)
    cooY = FabData.cooY
    cooX = FabData.cooX
    frac_no_sege = FabData.frac_no_sege
    frac_no_ele = FabData.frac_no_ele
    frac_no_node = FabData.frac_no_node
    no_frac = FabData.no_frac

    no_frac_intersection = np.zeros(no_frac,dtype=np.int32)
    frac_intersection = np.ones(no_frac,dtype=np.int32)
    NB_trans_frac_intersection = np.zeros(no_frac,dtype=np.int32)
    checkFracInter = np.ones(no_frac,dtype=np.int32)
    frac_intersection[:]=-1
    NB_trans_frac_intersection[:]=-1
    checkFracInter[:]=-1
    # print("frac_intersection=",frac_intersection)
    # quit()

    # no_frac_intersection = np.zeros(5,dtype=np.int32)
    # NB_trans_frac_intersection = np.zeros(5,dtype=np.int32)
    # frac_intersection = []
    # NB_trans_frac_intersection[:]=-1
    # frac_intersection.append([])
    # frac_intersection[0].append(5)
    # frac_intersection.append([])
    # frac_intersection[1].append(6)
    # frac_intersection[0].append(9)
    # print("len(frac_intersection)=",len(frac_intersection))
    # quit()

    CheckFracNxyzIsTheSame = 1 #20210707 需要修改加上判斷裂隙是否共平面
    if (CheckFracNxyzIsTheSame==0):
        NB=0
        for i in range(0, frac_no_ele-1):
            # print("i=",i)
            if (checkFracInter[fracID[i]]==-1):
                x = frac_XYZ[frac_ele_ind[i][0]][0]
                y = frac_XYZ[frac_ele_ind[i][0]][1]
                z = frac_XYZ[frac_ele_ind[i][0]][2]
                checkFracInter[fracID[i]]=1
                for j in range(i+1, frac_no_ele):
                    if (fracID[i]!=fracID[j]):
                        if (checkFracInter[fracID[j]]==-1):
                            print("fracID[i]=",fracID[i])
                            print("fracID[j]=",fracID[j])
                            nx=FracNXYZd[fracID[j]][0]
                            ny=FracNXYZd[fracID[j]][1]
                            nz=FracNXYZd[fracID[j]][2]
                            nd=FracNXYZd[fracID[j]][3]
                            # print(x,y,z,nx,ny,nz,nd)
                            ind = tool.checkPointInPlane(x,y,z,nx,ny,nz,nd,EPSILON)
                            if (ind==1):
                                print("i,j=",i,j)
                                quit()  
                                if (no_frac_intersection[i]==0):
                                    frac_intersection.append([])
                                    NB_trans_frac_intersection[i]=NB
                                    frac_intersection[NB_trans_frac_intersection[i]].append(j)
                                else:
                                    frac_intersection[NB_trans_frac_intersection[i]].append(j)

                                no_frac_intersection[i]=no_frac_intersection[i]+1

                                if (no_frac_intersection[j]==0):
                                    frac_intersection.append([])
                                    NB_trans_frac_intersection[j]=NB
                                    frac_intersection[NB_trans_frac_intersection[j]].append(i)
                                else:
                                    frac_intersection[NB_trans_frac_intersection[j]].append(i)                        
                                no_frac_intersection[j]=no_frac_intersection[j]+1

    return 0

def getTtiSpoints(FabData):
    import memToNparry
    import tool
    import sys
    import pyToTecplot
    from pysimgeo.ctypes import utils as ctypesUtils
    import pickle


    EPSILON= sys.float_info.epsilon
    SQEPS=np.sqrt(EPSILON)
    
    frac_XYZ = memToNparry.make_nd_array(FabData.frac_XYZ, [FabData.frac_no_node,3], dtype=np.float64, order='C', own_data=True)
    FracNXYZd = memToNparry.make_nd_array(FabData.FracNXYZd, [FabData.no_frac,4], dtype=np.float64, order='C', own_data=True)
    frac_sege = memToNparry.make_nd_array(FabData.frac_sege, [FabData.frac_no_sege,2], dtype=np.int32, order='C', own_data=True)
    fracID = memToNparry.make_nd_array(FabData.fracID, [FabData.frac_no_ele], dtype=np.int32, order='C', own_data=True)
    frac_ele_ind = memToNparry.make_nd_array(FabData.frac_ele_ind, [FabData.frac_no_ele,3], dtype=np.int32, order='C', own_data=True)
    cooY = FabData.cooY
    cooX = FabData.cooX
    frac_no_sege = FabData.frac_no_sege
    frac_no_ele = FabData.frac_no_ele
    frac_no_node = FabData.frac_no_node
    no_frac = FabData.no_frac



    xmin=np.min(frac_XYZ,axis=0)
    for i in range(0,len(xmin)):
        xmin[i]=0.0e0    
    for i in range(0, frac_no_node):
        frac_XYZ[i][0]=frac_XYZ[i][0]-xmin[0]
        frac_XYZ[i][1]=frac_XYZ[i][1]-xmin[1]
        frac_XYZ[i][2]=frac_XYZ[i][2]-xmin[2]

    # pyToTecplot.TecplotTriangle3D(frac_XYZ,frac_ele_ind,fracID)        

    # sampleFracID= [0,1,2,3,4,5,1490,1533,1540,1543,1593,1700,3420,3431,3451,3458,4411,4467,4469,4470,6297,6454,6530,6542,6543,6556]
    # print("no_frac=",no_frac)
    # print("frac_no_node=",frac_no_node)
    # print("frac_no_ele=",frac_no_ele)
    # print("frac_no_sege=",frac_no_sege)
    # print("frac_ele_ind")
    # print(frac_ele_ind)
    # quit()


    no_frac_intersection = np.zeros(frac_no_ele,dtype=np.int32)
    frac_intersection = np.ones(frac_no_ele,dtype=np.int32)
    NB_trans_frac_intersection = np.zeros(frac_no_ele,dtype=np.int32)
    checkFracInter = np.ones(frac_no_ele,dtype=np.int32)
    frac_intersection[:]=-1
    NB_trans_frac_intersection[:]=-1
    checkFracInter[:]=-1

    no_xyzInter = -1
    xyzInter = []
    NbEleInter =[]
    eachNbEleInter = []
    E1Nb = []
    E2Nb = []
    for i in range(0, no_frac):
    # for i in range(0, frac_no_ele):
        eachNbEleInter.append([])
        E1Nb.append([])
        # E2Nb.append([])

    # print(xyzInter)
    # print(eachNbEleInter)
    # print("=======")
    # quit()

    # for i in range(0, len(fracID)):
    #     print("fracID",i,fracID[i])
    # quit()

   
    NB=0
    for i in range(0, frac_no_ele-1):
        faceID1 = fracID[i]
        print("no_frac,faceID1=",no_frac,faceID1)
        for j in range(i+1, frac_no_ele):
            faceID2 = fracID[j]
            if (faceID1!=faceID2):
                outdata = tritri(
                    frac_XYZ[frac_ele_ind[i][0]][0],
                    frac_XYZ[frac_ele_ind[i][0]][1],
                    frac_XYZ[frac_ele_ind[i][0]][2],
                    frac_XYZ[frac_ele_ind[i][1]][0],
                    frac_XYZ[frac_ele_ind[i][1]][1],
                    frac_XYZ[frac_ele_ind[i][1]][2],
                    frac_XYZ[frac_ele_ind[i][2]][0],
                    frac_XYZ[frac_ele_ind[i][2]][1],
                    frac_XYZ[frac_ele_ind[i][2]][2],
                    frac_XYZ[frac_ele_ind[j][0]][0],
                    frac_XYZ[frac_ele_ind[j][0]][1],
                    frac_XYZ[frac_ele_ind[j][0]][2],
                    frac_XYZ[frac_ele_ind[j][1]][0],
                    frac_XYZ[frac_ele_ind[j][1]][1],
                    frac_XYZ[frac_ele_ind[j][1]][2],
                    frac_XYZ[frac_ele_ind[j][2]][0],
                    frac_XYZ[frac_ele_ind[j][2]][1],
                    frac_XYZ[frac_ele_ind[j][2]][2])
                if (outdata.checkTTI>0):
                    # print("faceID1,faceID2,i,j=",faceID1+1,',',faceID2+1,',',i+1,',',j+1)
                    spoint = memToNparry.make_nd_array(outdata.spoint, [6,3], dtype=np.float64, order='C', own_data=True)
                    checkTTI = outdata.checkTTI
                    # print("checkTTI=",checkTTI)
                    if (checkTTI==1):
                        spoint[1][0]=spoint[0][0]
                        spoint[1][1]=spoint[0][1]
                        spoint[1][2]=spoint[0][2]


                    eachNbEleInter[faceID1].append(faceID2)
                    eachNbEleInter[faceID1].append(faceID2)
                    eachNbEleInter[faceID2].append(faceID1)
                    eachNbEleInter[faceID2].append(faceID1)
                    E1=no_xyzInter+1
                    E2=no_xyzInter+2
                    # E1Nb[faceID1].append(no_xyzInter+1,no_xyzInter+2)
                    # E1Nb[faceID2].append(no_xyzInter+1,no_xyzInter+2)
                    E1Nb[faceID1].append(E1)
                    E1Nb[faceID1].append(E2)
                    E1Nb[faceID2].append(E1)
                    E1Nb[faceID2].append(E2)
                    xyzInter.append(np.array([spoint[0][0],spoint[0][1],spoint[0][2]]))
                    xyzInter.append(np.array([spoint[1][0],spoint[1][1],spoint[1][2]]))
                    NbEleInter.append([faceID1,faceID2])
                    NbEleInter.append([faceID1,faceID2])
                    no_xyzInter=no_xyzInter+2
                    # print("no_xyzInter=",len(xyzInter))
                    # print([spoint[0][0],spoint[0][1],spoint[0][2]])
                    # print([spoint[1][0],spoint[1][1],spoint[1][2]])
                    # if (i==0 and j==2):
                    #     quit()

                    # no_xyzInter=no_xyzInter+1
                    # xyzInter = np.append(xyzInter, [[spoint[0][0],spoint[0][1],spoint[0][2]]],axis=0)
                    # NbEleInter.append([i,j])
                    # eachNbEleInter[i].append(j)
                    # eachNbEleInter[j].append(i)
                    # E1Nb[i].append(no_xyzInter)
                    # E1Nb[j].append(no_xyzInter)
                    # # E2Nb[j].append(no_xyzInter)
                    # no_xyzInter=no_xyzInter+1
                    # xyzInter = np.append(xyzInter, [[spoint[1][0],spoint[1][1],spoint[1][2]]],axis=0)
                    # NbEleInter.append([i,j])
                    # eachNbEleInter[i].append(j)
                    # eachNbEleInter[j].append(i)
                    # E1Nb[i].append(no_xyzInter)
                    # E1Nb[j].append(no_xyzInter) 
                    # E2Nb[j].append(no_xyzInter) 

    # print("len(E1Nb)[0]=",len(E1Nb[0]))
    # quit()
    
    # np.savetxt('ResTTI/xyzInter.out', xyzInter, delimiter=',')
    # np.savetxt('ResTTI/NbEleInter.out', NbEleInter, delimiter=',',fmt = '%.i')
    with open('ResTTI/xyzInter.out','wb') as file:
        pickle.dump(xyzInter,file)
    with open('ResTTI/NbEleInter.out','wb') as file:
        pickle.dump(NbEleInter,file)
    with open('ResTTI/no_xyzInter.out','wb') as file:
        pickle.dump(no_xyzInter,file)
    with open('ResTTI/no_frac.out','wb') as file:
        pickle.dump(no_frac,file)
    with open('ResTTI/eachNbEleInter.out','wb') as file:
        pickle.dump(eachNbEleInter,file)
    with open('ResTTI/E1Nb.out','wb') as file:
        pickle.dump(E1Nb,file)
    # with open('uu.pk', 'rb') as file_1:
    #     b = pickle.load(file_1)

    
    # quit()
    return no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb #,E2Nb


# def creat2DPoly(no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb,FabData):
def creat2DPoly(no_xyzInter,xyzInter,NbEleInter,eachNbEleInter,E1Nb,no_frac,frac_XYZ,frac_ele_ind,frac_segeID,frac_sege):
    import memToNparry
    import tool
    import sys
    import pyToTecplot
    import meshGen

    EPSILON= sys.float_info.epsilon
    SQEPS=np.sqrt(EPSILON)
    
    # frac_XYZ = memToNparry.make_nd_array(FabData.frac_XYZ, [FabData.frac_no_node,3], dtype=np.float64, order='C', own_data=True)
    # FracNXYZd = memToNparry.make_nd_array(FabData.FracNXYZd, [FabData.no_frac,4], dtype=np.float64, order='C', own_data=True)
    # frac_sege = memToNparry.make_nd_array(FabData.frac_sege, [FabData.frac_no_sege,2], dtype=np.int32, order='C', own_data=True)
    # frac_segeID = memToNparry.make_nd_array(FabData.frac_segeID, [FabData.frac_no_sege], dtype=np.int32, order='C', own_data=True)
    # fracID = memToNparry.make_nd_array(FabData.fracID, [FabData.frac_no_ele], dtype=np.int32, order='C', own_data=True)
    # frac_ele_ind = memToNparry.make_nd_array(FabData.frac_ele_ind, [FabData.frac_no_ele,3], dtype=np.int32, order='C', own_data=True)
    # cooY = FabData.cooY
    # cooX = FabData.cooX
    # frac_no_sege = FabData.frac_no_sege
    # frac_no_ele = FabData.frac_no_ele
    # frac_no_node = FabData.frac_no_node
    # no_frac = FabData.no_frac
    frac_no_node = len(frac_XYZ)
    # pyToTecplot.TecplotTriangle3D(frac_XYZ,frac_ele_ind,fracID)

    xmin=np.min(frac_XYZ,axis=0)
    # print("xmin=",xmin)
    for i in range(0,len(xmin)):
        xmin[i]=0.0e0
    print("xmin=",xmin)
    # quit()
    
    for i in range(0, frac_no_node):
        frac_XYZ[i][0]=frac_XYZ[i][0]-xmin[0]
        frac_XYZ[i][1]=frac_XYZ[i][1]-xmin[1]
        frac_XYZ[i][2]=frac_XYZ[i][2]-xmin[2]
    # pyToTecplot.TecplotTriangle3D(frac_XYZ,frac_ele_ind)

    print("no_frac=",no_frac)
    print("len(xyzInter)=",len(xyzInter))
    # print("=========")   
    # print("len(NbEleInter)=",len(NbEleInter)) 
    # print("len(xyzInter)=",len(xyzInter)) 
    # print("len(eachNbEleInter)=",len(eachNbEleInter)) 
    # print("len(E1Nb)=",len(E1Nb)) 
    # for i in range(0, len(NbEleInter)):
    #     print("NbEleInter",i,NbEleInter[i])  
    #     # NbEleInter 相交線段頂點，與哪些裂隙編號相關
    # print("=========")
    # # quit()
    # print("xyzInter")
    # for i in range(0, len(xyzInter)):
    #     print(xyzInter[i][0],xyzInter[i][1],xyzInter[i][2])
    # #    # xyzInter 相交線段頂點的座標 #0 [628.60691691 148.78549866 154.20836102]
    # # quit()    
    # print("=========")
    # print("eachNbEleInter")
    # for i in range(0,no_frac):
    #     print("eachNbEleInter",i,eachNbEleInter[i])
    # #     # eachNbEleInter，依據每片裂隙上的交線頂點是與其他裂隙連結 # eachNbEleInter 0 [16, 16]
    # # quit()
    # print("=========")
    # print("E1Nb,E2Nb")
    # for i in range(0,no_frac):
    #     print("fracID=",i,"E1Nb",E1Nb[i])
    #     # E1Nb 依據裂隙編號紀錄每個頂點編號 # fracID= 0 E1Nb [0, 1, 2, 3]
    # print("=========")
    # quit()


    # print("frac_segeID")
    # print(frac_segeID)


    meshGen.Triangle2D(xyzInter,E1Nb,NbEleInter,eachNbEleInter,frac_segeID,frac_sege,frac_XYZ)
    
    
        # meshGen.Triangle2D(i,E1Nb,xyz2d,SegeNb,TTIxyz2d)

    # with open('testTTI.dat','w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(4)+',E='+str(2)+',F=FEpoint,ET=TRIANGLE\n')
    #     for i in range(0,4):
    #         f.write(str(frac_XYZ[i][0])+' '+str(frac_XYZ[i][1])+' '+str(frac_XYZ[i][2])+'\n')
    #     f.write(str(frac_ele_ind[0][0]+1)+' '+str(frac_ele_ind[0][1]+1)+' '+str(frac_ele_ind[0][2]+1)+'\n')
    #     f.write(str(frac_ele_ind[1][0]+1)+' '+str(frac_ele_ind[1][1]+1)+' '+str(frac_ele_ind[1][2]+1)+'\n')
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(E1Nb[0]))+',E='+str(int(len(E1Nb[0])/2))+',F=FEpoint,ET=LINESEG\n')
    #     for i in range(0,len(E1Nb[0])):
    #         f.write(str(xyzInter[E1Nb[0][i]][0])+' '+str(xyzInter[E1Nb[0][i]][1])+' '+str(xyzInter[E1Nb[0][i]][2])+'\n')
    #     for i in range(0,int(len(E1Nb[0])/2)):
    #         f.write(str(i*2+1)+' '+str(i*2+2)+'\n') 
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)"\n')
    #     f.write('Zone N='+str(len(E1Nb[1]))+',E='+str(int(len(E1Nb[1])/2))+',F=FEpoint,ET=LINESEG\n')
    #     for i in range(0,len(E1Nb[1])):
    #         f.write(str(xyzInter[E1Nb[1][i]][0])+' '+str(xyzInter[E1Nb[1][i]][1])+' '+str(xyzInter[E1Nb[1][i]][2])+'\n')
    #     for i in range(0,int(len(E1Nb[1])/2)):
    #         f.write(str(i*2+1)+' '+str(i*2+2)+'\n')                   
    #     f.close()

    return 0
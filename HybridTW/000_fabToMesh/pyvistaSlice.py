def single_slice():
    import pyvista as pv
    import matplotlib.pyplot as plt
    from pyvista import examples

    from xvfbwrapper import Xvfb
    display = Xvfb(width=1920, height=1080)
    display.start()

    mesh = examples.load_channels()
    cmap = plt.cm.get_cmap("jet", 51)

    single_slice = mesh.slice(normal=[1, 1, 0])
    plotter = pv.Plotter(off_screen=True)
    # plotter.add_mesh(single_slice,style='surface',show_edges=True,edge_color='000000',cmap=cmap)
    plotter.add_mesh(single_slice,style='surface',show_edges=False,cmap=cmap)
    plotter.show(screenshot='slices01.png')
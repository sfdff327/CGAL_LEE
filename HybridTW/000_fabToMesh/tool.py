import numpy as np

def ISPinSegeSame(Sx,Sy,x1,y1,x2,y2,x3,y3,x4,y4,SQEPS):
    SameP =np.zeros(4,dtype=int)
    GSameP=0
    dis = dis2d(x1,y1,Sx,Sy)
    if (dis<=SQEPS): SameP[0]=1
    dis = dis2d(x2,y2,Sx,Sy)
    if (dis<=SQEPS): SameP[1]=1
    dis = dis2d(x3,y3,Sx,Sy)
    if (dis<=SQEPS): SameP[2]=1
    dis = dis2d(x4,y4,Sx,Sy)
    if (dis<=SQEPS): SameP[3]=1
    GSameP=0
    for i in range(0,4):
        if (SameP[i]==1):
            GSameP=1
            break

    return GSameP,SameP

def pointInTwoSege(Sx,Sy,x1,y1,x2,y2,x3,y3,x4,y4,SQEPS):
    IsSxyIn=-1
    Tdis = dis2d(x1,y1,x2,y2)
    Rdis = dis2d(x1,y1,Sx,Sy)
    Ldis = dis2d(Sx,Sy,x2,y2)
    if ((abs(Tdis-Rdis-Ldis))<SQEPS):
        Tdis = dis2d(x3,y3,x4,y4)
        Rdis = dis2d(x3,y3,Sx,Sy)
        Ldis = dis2d(Sx,Sy,x4,y4)
        if ((abs(Tdis-Rdis-Ldis))<SQEPS): 
            IsSxyIn=1
    return IsSxyIn

def checkPointInPlane(x,y,z,nx,ny,nz,nd,EPSILON):
    difNd = np.abs(x *nx + y*ny + z*nz - nd)
    # print("difNd=",difNd)
    if (difNd<=EPSILON):
        ind =1
    else:
        ind = 0
    return ind

def dis3d(x1,y1,z1,x2,y2,z2):
    dis = ((x1-x2)**2.0+(y1-y2)**2.0+(z1-z2)**2.0)**0.5
    return dis

def dis2d(x1,y1,x2,y2):
    dis = ((x1-x2)**2.0+(y1-y2)**2.0)**0.5
    return dis
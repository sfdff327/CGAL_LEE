import os
import json
import calendar
from datetime import date
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import ctypes
import numpy as np
import memToNparry
import pickle

sim = cdll.LoadLibrary(os.path.dirname(os.path.abspath(__file__)) + "/lib.so")
# print(os.path.dirname(os.path.abspath(__file__)) + "/lib.so")
class Geometry( Structure ):
    pass

class FabReadFor( Structure ):
    pass

def FabFileRead():

    ndpointer = np.ctypeslib.ndpointer

    class FabReadFor( Structure ):
        _fields_ = [
		( "cooX", c_double ),        
		( "cooY", c_double ),
        ( "no_frac", c_int ),        
        ( "frac_no_node", c_int ),        
        ( "frac_no_ele", c_int ),        
        ( "frac_no_sege", c_int ),        
		( "frac_ele_ind", ndpointer(dtype=np.int32,ndim=2)),
		( "fracID", ndpointer(dtype=np.int32,ndim=1)),
        ( "frac_sege", ndpointer(dtype=np.int32,ndim=2)),
        ( "frac_segeID", ndpointer(dtype=np.int32,ndim=1)),
		( "FracNXYZd", ndpointer(dtype=np.float64,ndim=2)),
		( "frac_XYZ", ndpointer(dtype=np.float64,ndim=2))
	]

    FabData = FabReadFor()
    FabData.memShapes = []

    # print("test BF")
    null_fds, save = pipe.suppress()
    sim.FAB_reader(byref(FabData))
    pipe.resume(null_fds, save)    

    frac_XYZ = memToNparry.make_nd_array(FabData.frac_XYZ, [FabData.frac_no_node,3], dtype=np.float64, order='C',own_data=True)
    with open('FABRes/frac_XYZ.out','wb') as file:
        pickle.dump(frac_XYZ,file)

    FracNXYZd = memToNparry.make_nd_array(FabData.FracNXYZd, [FabData.no_frac,4], dtype=np.float64, order='C',own_data=True)
    with open('FABRes/FracNXYZd.out','wb') as file:
        pickle.dump(FracNXYZd,file)

    frac_sege = memToNparry.make_nd_array(FabData.frac_sege, [FabData.frac_no_sege,2], dtype=np.int32, order='C',own_data=True)
    with open('FABRes/frac_sege.out','wb') as file:
        pickle.dump(frac_sege,file)

    frac_segeID = memToNparry.make_nd_array(FabData.frac_segeID, [FabData.frac_no_sege], dtype=np.int32, order='C', own_data=True)
    with open('FABRes/frac_segeID.out','wb') as file:
        pickle.dump(frac_segeID,file)

    fracID = memToNparry.make_nd_array(FabData.fracID, [FabData.frac_no_ele], dtype=np.int32, order='C', own_data=True)
    with open('FABRes/fracID.out','wb') as file:
        pickle.dump(fracID,file)

    frac_ele_ind = memToNparry.make_nd_array(FabData.frac_ele_ind, [FabData.frac_no_ele,3], dtype=np.int32, order='C',own_data=True)
    with open('FABRes/frac_ele_ind.out','wb') as file:
        pickle.dump(frac_ele_ind,file)

    cooY = FabData.cooY
    with open('FABRes/cooY.out','wb') as file:
        pickle.dump(cooY,file)

    cooX = FabData.cooX
    with open('FABRes/cooX.out','wb') as file:
        pickle.dump(cooX,file)

    no_frac = FabData.no_frac
    with open('FABRes/no_frac.out','wb') as file:
        pickle.dump(no_frac,file)

    # print("test AF")
    # FabData.memShapes.append({"field_name": "no_frac", "dtype": np.int32, "shape": None })
    # FabData.memShapes.append({"field_name": "frac_no_node", "dtype": np.int32, "shape": None })
    # FabData.memShapes.append({"field_name": "frac_no_ele", "dtype": np.int32, "shape": None })
    # FabData.memShapes.append({"field_name": "frac_no_sege", "dtype": np.int32, "shape": None })
    # FabData.memShapes.append({"field_name": "cooX", "dtype": np.float64, "shape": None })
    # FabData.memShapes.append({"field_name": "cooY", "dtype": np.float64, "shape": None })
    
    # FabData.memShapes.append({"field_name": "frac_ele_ind", "dtype": np.int32, "shape": [FabData.frac_no_ele,3] })
    # FabData.memShapes.append({"field_name": "fracID", "dtype": np.int32, "shape": [FabData.frac_no_ele] })
    # FabData.memShapes.append({"field_name": "frac_sege", "dtype": np.int32, "shape": [FabData.frac_no_sege,2] })

    # FabData.memShapes.append({"field_name": "FracNXYZd", "dtype": np.float64, "shape": [FabData.no_frac,4] })
    # FabData.memShapes.append({"field_name": "frac_XYZ", "dtype": np.float64, "shape": [FabData.frac_no_node,3] })
    # FabDataDict = utils.getdict(FabData)


    

    return FabData   



if __name__ == "__main__":
    trimesh = FabFileRead()
from mayavi import mlab
import numpy as np
import algorithm
import json


# mlab.options.offscreen = True
# mlab.clf()
# mlab.close( all = True )
# mlab.figure(1)


def doPlot(plot_data, epsg_src=3857):
  q = plot_data["q"]
  me = plot_data["me"]
  u = plot_data["u"]
  fill_contour = plot_data["fill_contour"]
  # print("start doPlot")
  # print("q=",q)
  # print("me=",me)
  # print("u=",u)
  # print("fill_contour=",fill_contour)
  # quit()

  q = np.array(q)
  me = np.array(me)
  u = np.array(u)

  data = algorithm.plot(q, me, u, fill_contour, "jet", epsg_src)
  # print("data=",data)
  # print(epsg_src);
  return data

def main(plot_data, epsg_src=3857):
	data = doPlot(plot_data, epsg_src)
	return data

if __name__ == "__main__":
	redis_ip = sys.argv[1]
	plot_id = sys.argv[2]
	redis_client = redis.StrictRedis(host=redis_ip, port=6379, db=0)	
	plot_data = json.loads(redis_client.get(plot_id))
	result = main(plot_data)	
	print(json.dumps(result))





from mayavi import mlab
import numpy as np
from fc_meshtools import simplicial as mshsim
import fc_mayavi4mesh.simplicial as mlab4sim
from vtk import vtkPolyDataMapper
import json

import matplotlib
import matplotlib.cm
#from matplotlib import cm

import re
# from pysimgeo.mayavi.util.polydata import PolyDataHelper
from polydata import PolyDataHelper



def plot(q, me, u, fill_contour, cmap_name, epsg_src=3857):

  print("start algorithm.plot")
  dim = q.ndim
  # print("dim=",dim)
  # quit()
  cmap = matplotlib.cm.get_cmap(cmap_name)
  # print("cmap=",cmap)
  json_data = {}

  if dim == 1 or dim == 3:
    fill_contour = True

  print("q=")
  print(q)
  print("me=")
  print(me)
  print("u=")
  print(u)
  # quit()

  min_x, max_x = min(q[0]), max(q[0])
  min_y, max_y = min(q[1]), max(q[1])
  center_x = (max_x + min_x) / 2
  center_y = (max_y + min_y) / 2
  q[0] = q[0] - center_x
  q[1] = q[1] - center_y
  plot_data = mlab4sim.plot(q, me, u)
  plot_data.contour.filled_contours = fill_contour
  plot_data.enable_contours = True  

  contour_filter = plot_data.contour.contour_filter
  contour_filter_output = contour_filter.get_output()

  polyDataHelper = PolyDataHelper(contour_filter_output)
  if dim == 1 or dim == 3:
    polyDataHelper.extract_polylines_from_polydata()
    json_data = polyDataHelper.polylines_to_json(center_x, center_y, cmap, epsg_src)
  elif dim == 2 and fill_contour == True:
    polyDataHelper.extract_surfaces_from_polydata()
    json_data = polyDataHelper.surfaces_to_json(center_x, center_y, cmap, epsg_src)
  elif dim == 2 and fill_contour == False:
    polyDataHelper.extract_polylines_from_polydata()
    json_data = polyDataHelper.polylines_to_json(center_x, center_y, cmap, epsg_src)

  return json_data

def quiver(q, me, w, mode="2darrow", line_width=1, scale_mode="scalar", mask_points=300, cmap_name="jet", epsg_src=3857):
  dim = q.ndim
  cmap = matplotlib.cm.get_cmap(cmap_name)
  json_data = {}

  min_x, max_x = min(q[0]), max(q[0])
  min_y, max_y = min(q[1]), max(q[1])
  center_x = (max_x + min_x) / 2
  center_y = (max_y + min_y) / 2
  q[0] = q[0] - center_x
  q[1] = q[1] - center_y

  quiver_data = mlab4sim.quiver( q, me, w, line_width=line_width, scale_mode=scale_mode, mask_points=mask_points, mode=mode)
  output = quiver_data.glyph.glyph.get_output()
  polyDataHelper = PolyDataHelper(output)

  if mode == "arrow" or mode == "axes" or mode == "cone" or mode == "cube" or mode == "cylinder" or mode == "point" or mode == "sphere":
    polyDataHelper.extract_surfaces_from_polydata()
    json_data = polyDataHelper.surfaces_to_json(center_x, center_y, cmap, epsg_src)
  else:
    polyDataHelper.extract_polylines_from_polydata()
    json_data = polyDataHelper.polylines_to_json(center_x, center_y, cmap, epsg_src)

  return json_data


def slice(q, me, u, cmap_name, origin=(0,0,1), normal=(0,0,1), epsg_src=3857):
  dim = q.ndim
  cmap = matplotlib.cm.get_cmap(cmap_name)
  json_data = {}

  min_x, max_x = min(q[0]), max(q[0])
  min_y, max_y = min(q[1]), max(q[1])
  center_x = (max_x + min_x) / 2
  center_y = (max_y + min_y) / 2
  q[0] = q[0] - center_x
  q[1] = q[1] - center_y

  slice_data = mlab4sim.slice(q, me, u, origin=origin, normal=normal)  
  slice_data.contour.filled_contours = True
  slice_data.enable_contours = True  

  contour_filter = slice_data.contour.contour_filter
  contour_filter_output = contour_filter.get_output()

  polyDataHelper = PolyDataHelper(contour_filter_output)

  polyDataHelper.extract_surfaces_from_polydata()
  json_data = polyDataHelper.surfaces_to_json(center_x, center_y, cmap, epsg_src)
  return json_data


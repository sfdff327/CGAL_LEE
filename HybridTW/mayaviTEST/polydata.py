# from pysimgeo.projection import projection
import matplotlib

class Point():
  def __init__(self, x, y, z, scalar):
    self.x = x
    self.y = y
    self.z = -z
    self.scalar = scalar
    self.norm_scalar = scalar
    self.scalar_normalized = False

  def __str__(self):
    return "{}, {}, {}".format(self.x, self.y, self.z, self.scalar)

  def normalize_scalar(self, norm):
    self.norm_scalar = norm(self.scalar)
    self.scalar_normalized = True

  def to_json(self, cmap):
    r, g, b, a = cmap(self.norm_scalar)      
    return {
      "x": str(self.x),
      "y": str(self.y),
      "z": str(self.z),
      "scalar": str(self.scalar),
      # "norm_scalar": str(self.norm_scalar),
      # "scalar_normalized": self.scalar_normalized,
      "rgb": {
        "r": r,
        "g": g,
        "b": b
      }
    }

class Polyline():
  def __init__(self):
    self.points = []

  def to_json(self, cmap):
    coords = []        
    for point in self.points:
      coords.append(point.to_json(cmap))
    return coords   

class Triangle():
  def __init__(self, p1, p2, p3):
    self.p1 = p1
    self.p2 = p2
    self.p3 = p3

  def to_json(self, cmap):
    return [self.p1.to_json(cmap), self.p2.to_json(cmap), self.p3.to_json(cmap)] 


class Surface():
  def __init__(self):
    self.points = []
    
  def _to_triangles(self):
    num_tris = len(self.points) - 2
    tris = []
    for i in range(num_tris):
      tri = Triangle(self.points[0], self.points[i+2], self.points[i+1])
      tris.append(tri)
    return tris

  def to_json(self, cmap):
    tris = []
    for tri in self._to_triangles():
      tris.append(tri.to_json(cmap))
    return tris    


class PolyDataHelper():
  def __init__(self, data):
    self.data = data
    self.surfaces = []
    self.polylines = []


  def _extract_target_data(self, target_type):
    # num_points_n, point_x, ... point_n,
    target_data = []

    if target_type == "surface":
      target_data = self.data.polys.data.to_array()
      target = Surface()
    elif target_type == "polyline":
      target_data = self.data.lines.data.to_array()
      target = Polyline()

    scalars_data = self.data.point_data.scalars.to_array()
    points_data = self.data.points.to_array()
    norm = matplotlib.colors.Normalize(vmin=min(scalars_data),vmax=max(scalars_data))

    targets = []
    points_num = 0
    for ele in target_data:
      if points_num == 0:
        if len(target.points) > 0:
          targets.append(target)            
        points_num = ele

        if target_type == "surface":
          target = Surface()
        elif target_type == "polyline":
          target = Polyline()
      else:
        point = Point(points_data[ele][0], points_data[ele][1], points_data[ele][2], scalars_data[ele])
        point.normalize_scalar(norm)
        target.points.append(point)
        points_num = points_num - 1

    if target_type == "surface":
      self.surfaces = targets
    elif target_type == "polyline":
      self.polylines = targets

  def extract_surfaces_from_polydata(self):
    self._extract_target_data("surface")
  
  def extract_polylines_from_polydata(self):
    self._extract_target_data("polyline")

  def surfaces_to_json(self, center_x, center_y, cmap, epsg_src):
    tris = []
    for surface in self.surfaces:
      for tri in surface.to_json(cmap):
        tris.append(tri)
    # # center_x, center_y = projection.transform(epsg_src, 4326, center_x, center_y)            
    # center_x = 120.0
    # center_y = 23.0
            
    data = {
      "center": {
        "center_x": center_x,
        "center_y": center_y,
      },
      "zoom_level": 13,
      "tris":tris
    }
    return data

  def polylines_to_json(self, center_x, center_y, cmap, epsg_srcf):
    polylines = []
    for polyline in self.polylines:
      polylines.append(polyline.to_json())
    # center_x, center_y = projection.transform(epsg_src,4326, center_x, center_y)
    center_x = 120.0
    center_y = 23.0
    data = {
      "center": {
        "center_x": center_x,
        "center_y": center_y
      },
      "zoom_level": 13,
      "polylines":polylines
    }
    return data
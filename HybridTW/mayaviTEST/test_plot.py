from mayavi import mlab
import numpy as np
import json
import re
import os
import sys
# sys.path.append(os.path.abspath("../../"))
import plot

from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

input_type = "q2"
fill_contour = True
epsg_src =  3826 #3857: world meter, 3826: TWD97

mlab.options.offscreen = True
mlab.clf()
mlab.close( all = True )
mlab.figure (1)


if __name__ == "__main__":	
  if input_type == "q1":
    q1 = [[],[],[]]
    with open('q1.dat') as q1_file:
      for line in q1_file.readlines():
        line = line.strip("\r\n ")  
        x, y, z = re.split("\s+", line)     
        q1[0].append(float(x))
        q1[1].append(float(y))
        q1[2].append(float(z))  
    q = np.array(q1)

    me1 = [[],[]] 
    with open('me1.dat') as me1_file:
      for line in me1_file.readlines():
        line = line.strip("\r\n ")
        
        x, y = re.split("\s+", line)    
        
        me1[0].append(int(x))
        me1[1].append(int(y))
    me = np.array(me1)

    u1 = [] 
    with open('u1.dat') as u1_file:
      for line in u1_file.readlines():
        line = line.strip("\r\n ")                    
        u1.append(float(line))
    u = np.array(u1)

  elif input_type == "q2":
    # print("start write to q2")
    q2 = [[],[],[]]
    with open('q2.dat') as q2_file:
      for line in q2_file.readlines():
        line = line.strip("\r\n ")  
        x, y, z = re.split("\s+", line)     
        q2[0].append(float(x))
        q2[1].append(float(y))
        q2[2].append(float(z))  
    q = np.array(q2)
    # print("q=",q)

    me2 = [[],[],[]] 
    with open('me2.dat') as me2_file:
      for line in me2_file.readlines():
        line = line.strip("\r\n ")
        
        x, y, z = re.split("\s+", line)    
        
        me2[0].append(int(x))
        me2[1].append(int(y))
        me2[2].append(int(z))
    me = np.array(me2)
    # print("me=",me)

    u2 = [] 
    with open('u2.dat') as u2_file:
      for line in u2_file.readlines():
        line = line.strip("\r\n ")                    
        u2.append(float(line))
    u = np.array(u2)
    # print("u=",u)

  elif input_type == "q3":
    q3 = [[],[],[]]
    with open('q3.dat') as q3_file:
      for line in q3_file.readlines():
        line = line.strip("\r\n ")
        x, y, z = re.split("\s+", line)  
        
        q3[0].append(float(x))
        q3[1].append(float(y))
        q3[2].append(float(z))

    q = np.array(q3)


    me3 = [[],[],[],[]] 
    with open('me3.dat') as me3_file:
      for line in me3_file.readlines():
        line = line.strip("\r\n ")
        
        x, y, z, h = re.split("\s+", line) 
        
        me3[0].append(int(x))
        me3[1].append(int(y))
        me3[2].append(int(z))
        me3[3].append(int(h))
    me = np.array(me3)

    u3 = [] 
    with open('u3.dat') as u3_file:
      for line in u3_file.readlines():
        line = line.strip("\r\n ")                    
        u3.append(float(line))
    u = np.array(u3)
  
  plot_data = {
    "q": q,
    "me": me,
    "u": u,
    "fill_contour": fill_contour
  }
  # print("plot_data=",plot_data)
  # quit()

  plot_result = plot.main(plot_data, epsg_src)
  #print(json.dumps(plot_result))
  with open('data.json', 'w') as outfile:
    json.dump(plot_result, outfile)
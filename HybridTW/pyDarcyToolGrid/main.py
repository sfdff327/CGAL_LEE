import os
from scipy.io import FortranFile
import numpy as np

import pyvista as pv
import vtk

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

os.system('./FreadGrid')

f = FortranFile('xyzB', 'r')
nbcells, nbxfaces, nbyfaces, nbzfaces, nbcgtnodes, nbmark, nbcutcells, nbxcutfaces,nbycutfaces, nbzcutfaces,nbpolcells, nbxpolfaces, nbypolfaces, nbzpolfaces,nbtpolfaces, nbpolys, nbpts=f.read_ints(np.int32)
lvmax=f.read_ints(np.int32)
dxmin, dymin, dzmin =f.read_reals(np.float64)
xor, yor, zor=f.read_reals(np.float64)

ix=f.read_ints(np.int32)
iy=f.read_ints(np.int32)
iz=f.read_ints(np.int32)
lx=f.read_ints(np.int32)
ly=f.read_ints(np.int32)
lz=f.read_ints(np.int32)

xfacesH=f.read_ints(np.int32)
xfacesL=f.read_ints(np.int32)
yfacesH=f.read_ints(np.int32)
yfacesL=f.read_ints(np.int32)
zfacesH=f.read_ints(np.int32)
zfacesL=f.read_ints(np.int32)

cgtH=f.read_ints(np.int32)
cgtL=f.read_ints(np.int32)
cgtCut=f.read_ints(np.int32)

mk=f.read_ints(np.int32)

# print("len(xfacesH)=",len(xfacesH))

points=np.zeros((nbcells*8,3),dtype=np.float64)
xyzMin=np.zeros((nbcells,3),dtype=np.float64)
xyzMax=np.zeros((nbcells,3),dtype=np.float64)
scalars=np.zeros((nbcells),dtype=np.float64)
nb=-1
cells=[]
for i in range(0,nbcells):
    xyzMin[i][0]=xor+dxmin*ix[i]
    xyzMin[i][1]=yor+dymin*iy[i]
    xyzMin[i][2]=zor+dzmin*iz[i]
    xyzMax[i][0]=xor+dxmin*(ix[i]+2.0**lx[i])
    xyzMax[i][1]=yor+dymin*(iy[i]+2.0**ly[i])
    xyzMax[i][2]=zor+dzmin*(iz[i]+2.0**lz[i])
    nb=nb+1
    c0=nb
    points[nb][0]=xyzMin[i][0]
    points[nb][1]=xyzMin[i][1]
    points[nb][2]=xyzMin[i][2]

    nb=nb+1
    c1=nb
    points[nb][0]=xyzMax[i][0]
    points[nb][1]=xyzMin[i][1]
    points[nb][2]=xyzMin[i][2]

    nb=nb+1
    c2=nb
    points[nb][0]=xyzMax[i][0]
    points[nb][1]=xyzMax[i][1]
    points[nb][2]=xyzMin[i][2]
    nb=nb+1
    c3=nb
    points[nb][0]=xyzMin[i][0]
    points[nb][1]=xyzMax[i][1]
    points[nb][2]=xyzMin[i][2]

    nb=nb+1
    c4=nb
    points[nb][0]=xyzMin[i][0]
    points[nb][1]=xyzMin[i][1]
    points[nb][2]=xyzMax[i][2]
    nb=nb+1
    c5=nb
    points[nb][0]=xyzMax[i][0]
    points[nb][1]=xyzMin[i][1]
    points[nb][2]=xyzMax[i][2]
    nb=nb+1
    c6=nb
    points[nb][0]=xyzMax[i][0]
    points[nb][1]=xyzMax[i][1]
    points[nb][2]=xyzMax[i][2]
    nb=nb+1
    c7=nb
    points[nb][0]=xyzMin[i][0]
    points[nb][1]=xyzMax[i][1]
    points[nb][2]=xyzMax[i][2] 
    cells.append([8,c0,c1,c2,c3,c4,c5,c6,c7]) 

    scalars[i]=mk[i] 

no_cells=len(cells)
cells=np.array(cells).ravel()
celltypes = np.empty(no_cells, dtype=np.uint32)
celltypes[:] = vtk.VTK_HEXAHEDRON
grid = pv.UnstructuredGrid(cells, celltypes, points)

grid['mk']=scalars

grid.save('grid.vtk')

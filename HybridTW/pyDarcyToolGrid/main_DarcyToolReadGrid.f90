program readGrid
implicit none

integer*4 i,j,k
integer*4 iunit
integer*4 nbcells, nbxfaces, &
 & nbyfaces, nbzfaces, &
 & nbcgtnodes, nbmark, &
 & nbcutcells, nbxcutfaces,  &
 & nbycutfaces, nbzcutfaces, & 
 & nbpolcells, nbxpolfaces,  &
 & nbypolfaces, nbzpolfaces, & 
 & nbtpolfaces, nbpolys,  &
 & nbpts

integer*4 lvmax
real*8 dxmin, dymin, dzmin
real*8 xor, yor, zor

integer*4,allocatable:: ix(:),iy(:),iz(:),lx(:),ly(:),lz(:),mk(:)
integer*4,allocatable:: xfacesH(:),xfacesL(:),yfacesH(:),yfacesL(:),zfacesH(:),zfacesL(:)
integer*4, allocatable:: cgtH(:),cgtL(:),cgtCut(:)

character(8) word
character(32) name, varpos

integer nporo,nVFWS,nPERMX,nPERMY,nPERMZ,ni,nj
real*4, allocatable:: poro(:),VFWS(:),PERMX(:),PERMY(:),PERMZ(:)

iunit=1001
open(iunit,file='xyz')
read(iunit,*) !'#GD#V304'
read(iunit,*) nbcells, nbxfaces, &
 & nbyfaces, nbzfaces, &
 & nbcgtnodes, nbmark, &
 & nbcutcells, nbxcutfaces,  &
 & nbycutfaces, nbzcutfaces, & 
 & nbpolcells, nbxpolfaces,  &
 & nbypolfaces, nbzpolfaces, & 
 & nbtpolfaces, nbpolys,  &
 & nbpts
read(iunit,*) lvmax
read(iunit,*) dxmin, dymin, dzmin
read(iunit,*) xor, yor, zor
allocate(ix(nbcells+nbcutcells),iy(nbcells+nbcutcells),iz(nbcells+nbcutcells))
allocate(lx(nbcells+nbcutcells),ly(nbcells+nbcutcells),lz(nbcells+nbcutcells))
do i=1,nbcells+nbcutcells
    read(iunit,*) ix(i),iy(i),iz(i),lx(i),ly(i),lz(i)
end do
 
allocate(xfacesH(nbxfaces+nbxcutfaces),xfacesL(nbxfaces+nbxcutfaces))
do i=1,nbxfaces+nbxcutfaces
    read(iunit,*) xfacesH(i), xfacesL(i)
end do
 
allocate(yfacesH(nbyfaces+nbycutfaces),yfacesL(nbyfaces+nbycutfaces))
do i=1,nbyfaces+nbycutfaces
    read(iunit,*) yfacesH(i), yfacesL(i)
end do
 
allocate(zfacesH(nbzfaces+nbzcutfaces),zfacesL(nbzfaces+nbzcutfaces))
do i=1,nbzfaces+nbzcutfaces
    read(iunit,*) zfacesH(i), zfacesL(i)
end do

allocate(cgtH(nbcgtnodes),cgtL(nbcgtnodes),cgtCut(nbcgtnodes))
do i=1,nbcgtnodes
    read(iunit,*) cgtH(i), cgtL(i), cgtCut(i)
end do 

allocate(mk(nbmark))
do i=1,nbmark
    read(iunit,*) mk(i)
end do
close(iunit)
 
iunit=20
open(iunit,file='xyzB',form='unformatted')
! write(iunit) '#GD#V304'
write(iunit) nbcells, nbxfaces, &
    & nbyfaces, nbzfaces, &
    & nbcgtnodes, nbmark, &
    & nbcutcells, nbxcutfaces,  &
    & nbycutfaces, nbzcutfaces, & 
    & nbpolcells, nbxpolfaces,  &
    & nbypolfaces, nbzpolfaces, & 
    & nbtpolfaces, nbpolys,  &
    & nbpts
write(iunit) lvmax
write(iunit) dxmin, dymin, dzmin
write(iunit) xor, yor, zor
! do i=1,nbcells+nbcutcells
!     write(iunit) ix(i),iy(i),iz(i),lx(i),ly(i),lz(i)
! enddo
write(iunit) ix
write(iunit) iy
write(iunit) iz
write(iunit) lx
write(iunit) ly
write(iunit) lz

! do i=1,nbxfaces+nbxcutfaces
!     write(iunit) xfacesH(i), xfacesL(i)
! enddo
write(iunit) xfacesH
write(iunit) xfacesL

! do i=1,nbyfaces+nbycutfaces
!     write(iunit) yfacesH(i), yfacesL(i)
! enddo
write(iunit) yfacesH
write(iunit) yfacesL

! do i=1,nbzfaces+nbzcutfaces
!     write(iunit) zfacesH(i), zfacesL(i)
! enddo
write(iunit) zfacesH
write(iunit) zfacesL

! do i=1,nbcgtnodes
!     write(iunit) cgtH(i), cgtL(i), cgtCut(i)
! enddo 
write(iunit) cgtH
write(iunit) cgtL
write(iunit) cgtCut

! do i=1,nbmark
!     write(iunit) mk(i)
! enddo
write(iunit) mk

close(iunit)



 !name = 'permx'
 !varpos = 'xface'
 !write(61) UTIL_SIGN_HEADER('#V5#V300')

open(61,file='PORO',form='unformatted')
write(*,*) 'start read PORO'
read(61) word!UTIL_SIGN_HEADER('#V5#V300')
write(*,*) 'read word'
read(61) name, varpos
write(*,*) 'name, varpos'
read(61) nporo,ni,nj
write(*,*) 'nbx,ni,nj=',nporo,ni,nj
allocate(poro(nporo))
read(61) (poro(i),i=1,nporo)
write(*,*) '(poro(i),i=1,nporo)'
! write(*,*) poro
close(61)


open(61,file='VFWS',form='unformatted')
write(*,*) 'start read VFWS'
read(61) word!UTIL_SIGN_HEADER('#V5#V300')
write(*,*) 'read word'
read(61) name, varpos
write(*,*) 'name, varpos'
read(61) nVFWS,ni,nj
write(*,*) 'nVFWS,ni,nj=',nVFWS,ni,nj
allocate(VFWS(nVFWS))
read(61) (VFWS(i),i=1,nVFWS)
write(*,*) '(VFWS(i),i=1,nVFWS)'
! write(*,*) VFWS
close(61)

open(61,file='PERMX',form='unformatted')
write(*,*) 'start read PERMX'
read(61) word!UTIL_SIGN_HEADER('#V5#V300')
write(*,*) 'read word'
read(61) name, varpos
write(*,*) 'name, varpos'
read(61) nPERMX,ni,nj
write(*,*) 'nPERMX,ni,nj=',nPERMX,ni,nj
allocate(PERMX(nPERMX))
read(61) (PERMX(i),i=1,nPERMX)
write(*,*) '(PERMX(i),i=1,nPERMX)'
! write(*,*) PERMX
close(61)

open(61,file='PERMY',form='unformatted')
write(*,*) 'start read PERMY'
read(61) word!UTIL_SIGN_HEADER('#V5#V300')
write(*,*) 'read word'
read(61) name, varpos
write(*,*) 'name, varpos'
read(61) nPERMY,ni,nj
write(*,*) 'nPERMY,ni,nj=',nPERMY,ni,nj
allocate(PERMY(nPERMY))
read(61) (PERMY(i),i=1,nPERMY)
write(*,*) '(PERMY(i),i=1,nPERMY)'
! write(*,*) PERMY
close(61)


open(61,file='PERMZ',form='unformatted')
write(*,*) 'start read PERMZ'
read(61) word!UTIL_SIGN_HEADER('#V5#V300')
write(*,*) 'read word'
read(61) name, varpos
write(*,*) 'name, varpos'
read(61) nPERMZ,ni,nj
write(*,*) 'nPERMZ,ni,nj=',nPERMZ,ni,nj
allocate(PERMZ(nPERMZ))
read(61) (PERMZ(i),i=1,nPERMZ)
write(*,*) '(PERMZ(i),i=1,nPERMZ)'
! write(*,*) PERMZ
close(61)

j=0
end program readGrid
def color_map_color(value, cmap_name='jet', vmin=0, vmax=1):
    import matplotlib.cm as cm
    import matplotlib as matplotlib
    # norm = plt.Normalize(vmin, vmax)
    norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)
    cmap = cm.get_cmap(cmap_name) 
    rgb = cmap(norm(value))  # will return rgba, we take only first 3 so we get rgb
    # color = matplotlib.colors.rgb2hex(rgb)
    return rgb

# colors = numpy.random.uniform(size=(no_node,4))
# value = [1,2,3,4,5,6,7,8,9,10]
# color = []
# for i in range(0, len(value)):
#     # v =color_map_color(value[i],vmin=1, vmax=10)
#     color.append(color_map_color(value[i],vmin=1, vmax=10))
# print("color=",color)

def points2Gltf(triangles,points,z,zmax,zmin,outfile):
    import numpy as np
    import pygltflib

    points = np.array(
    [
        [-0.5, -0.5, 0.5],
        [0.5, -0.5, 0.5],
        [-0.5, 0.5, 0.5],
        [0.5, 0.5, 0.5],
        [0.5, -0.5, -0.5],
        [-0.5, -0.5, -0.5],
        [0.5, 0.5, -0.5],
        [-0.5, 0.5, -0.5],
    ],
    dtype=np.float32,
    )
    
    triangles = np.array(
    [
        [0],
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
    ],
    dtype=np.uint32,)
    # print("triangles=",triangles)

    z = np.array(
    [
        [10.0],
        [0.0],
        [10.0],
        [0.0],
        [0.0],
        [10.0],
        [0.0],
        [10.0],
    ],
    dtype=np.float32,
    )
    # zmax = np.max(z)
    # zmin = np.min(z)
    # print("zmax,zmin=",zmax,zmin)


    rgb=[]
    for i in range(0, len(z)):
        rgb.append(color_map_color(z[i], cmap_name='jet', vmin=zmin, vmax=zmax))
    rgb = np.array(rgb,dtype=np.float32)

    triangles_binary_blob = triangles.flatten().tobytes()
    points_binary_blob = points.tobytes()
    rgb_binary_blob = rgb.tobytes()


    gltf = pygltflib.GLTF2(
    scene=0,
    scenes=[pygltflib.Scene(nodes=[0])],
    nodes=[pygltflib.Node(mesh=0)],
    meshes=[
        pygltflib.Mesh(
            primitives=[
                pygltflib.Primitive(
                    attributes=pygltflib.Attributes(POSITION=1, COLOR_0=2), indices=0, mode=0
                )
            ]
        )
    ],
    accessors=[
        pygltflib.Accessor(
            bufferView=0,
            # componentType=pygltflib.UNSIGNED_BYTE,
            componentType=pygltflib.UNSIGNED_INT,
            count=triangles.size,
            type=pygltflib.SCALAR,
            max=[int(triangles.max())],
            min=[int(triangles.min())],
        ),
        pygltflib.Accessor(
            bufferView=1,
            componentType=pygltflib.FLOAT,
            count=len(points),
            type=pygltflib.VEC3,
            max=points.max(axis=0).tolist(),
            min=points.min(axis=0).tolist(),
        ),
        pygltflib.Accessor(
            bufferView=2,
            componentType=pygltflib.FLOAT,
            count=len(rgb),
            type=pygltflib.VEC4,
        ),        
    ],
    bufferViews=[
        pygltflib.BufferView(
            buffer=0,
            byteLength=len(triangles_binary_blob),
            target=pygltflib.ELEMENT_ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(triangles_binary_blob),
            byteLength=len(points_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(points_binary_blob)+len(triangles_binary_blob),
            byteLength=len(rgb_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),        
    ],
    buffers=[
        pygltflib.Buffer(
            byteLength=len(triangles_binary_blob) + len(points_binary_blob) + len(rgb_binary_blob)
        )
    ],
)
    gltf.set_binary_blob(triangles_binary_blob + points_binary_blob + rgb_binary_blob)
    # glb = b"".join(gltf.save_to_bytes())  # save_to_bytes returns an array of the components of a glb
    # gltf = pygltflib.GLTF2.load_from_bytes(glb)
    # filename2 = "testCreatGltf.glb"
    gltf.save(outfile)

def lines2Gltf(triangles,points,z,zmax,zmin,outfile):
    import numpy as np
    import pygltflib

    points = np.array(
    [
        [-0.5, -0.5, 0.5],
        [0.5, -0.5, 0.5],
        [-0.5, 0.5, 0.5],
        [0.5, 0.5, 0.5],
        [0.5, -0.5, -0.5],
        [-0.5, -0.5, -0.5],
        [0.5, 0.5, -0.5],
        [-0.5, 0.5, -0.5],
    ],
    dtype=np.float32,
    )
    
    triangles = np.array(
    [
        [0, 1],
        [0, 2],
        [3, 2],
        [3, 1],
        [1, 0],
        [1, 4],
        [5, 4],
        [5, 0],
        [3, 1],
        [3, 6],
        [4, 6],
        [4, 1],
        [2, 3],
        [2, 7],
        [6, 7],
        [6, 3],
        [0, 2],
        [0, 5],
        [7, 5],
        [7, 2],
        [5, 7],
        [5, 4],
        [6, 4],
        [6, 7],
    ],
    dtype=np.uint32,)
    # print("triangles=",triangles)

    z = np.array(
    [
        [10.0],
        [0.0],
        [10.0],
        [0.0],
        [0.0],
        [10.0],
        [0.0],
        [10.0],
    ],
    dtype=np.float32,
    )
    # zmax = np.max(z)
    # zmin = np.min(z)
    # print("zmax,zmin=",zmax,zmin)


    rgb=[]
    for i in range(0, len(z)):
        rgb.append(color_map_color(z[i], cmap_name='jet', vmin=zmin, vmax=zmax))
    rgb = np.array(rgb,dtype=np.float32)

    triangles_binary_blob = triangles.flatten().tobytes()
    points_binary_blob = points.tobytes()
    rgb_binary_blob = rgb.tobytes()


    gltf = pygltflib.GLTF2(
    scene=0,
    scenes=[pygltflib.Scene(nodes=[0])],
    nodes=[pygltflib.Node(mesh=0)],
    meshes=[
        pygltflib.Mesh(
            primitives=[
                pygltflib.Primitive(
                    attributes=pygltflib.Attributes(POSITION=1, COLOR_0=2), indices=0, mode=1
                )
            ]
        )
    ],
    accessors=[
        pygltflib.Accessor(
            bufferView=0,
            # componentType=pygltflib.UNSIGNED_BYTE,
            componentType=pygltflib.UNSIGNED_INT,
            count=triangles.size,
            type=pygltflib.SCALAR,
            max=[int(triangles.max())],
            min=[int(triangles.min())],
        ),
        pygltflib.Accessor(
            bufferView=1,
            componentType=pygltflib.FLOAT,
            count=len(points),
            type=pygltflib.VEC3,
            max=points.max(axis=0).tolist(),
            min=points.min(axis=0).tolist(),
        ),
        pygltflib.Accessor(
            bufferView=2,
            componentType=pygltflib.FLOAT,
            count=len(rgb),
            type=pygltflib.VEC4,
        ),        
    ],
    bufferViews=[
        pygltflib.BufferView(
            buffer=0,
            byteLength=len(triangles_binary_blob),
            target=pygltflib.ELEMENT_ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(triangles_binary_blob),
            byteLength=len(points_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(points_binary_blob)+len(triangles_binary_blob),
            byteLength=len(rgb_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),        
    ],
    buffers=[
        pygltflib.Buffer(
            byteLength=len(triangles_binary_blob) + len(points_binary_blob) + len(rgb_binary_blob)
        )
    ],
)
    gltf.set_binary_blob(triangles_binary_blob + points_binary_blob + rgb_binary_blob)
    # glb = b"".join(gltf.save_to_bytes())  # save_to_bytes returns an array of the components of a glb
    # gltf = pygltflib.GLTF2.load_from_bytes(glb)
    # filename2 = "testCreatGltf.glb"
    gltf.save(outfile)


def mesh2Gltf(triangles,points,z,zmax,zmin,outfile):
    import numpy as np
    import pygltflib

    # print("triangles=")
    # print(triangles)
    # print("points=")
    # print(points)
    # print("z=")
    # print(z)

    # points = np.array(
    # [
    #     [-0.5, -0.5, 0.5],
    #     [0.5, -0.5, 0.5],
    #     [-0.5, 0.5, 0.5],
    #     [0.5, 0.5, 0.5],
    #     [0.5, -0.5, -0.5],
    #     [-0.5, -0.5, -0.5],
    #     [0.5, 0.5, -0.5],
    #     [-0.5, 0.5, -0.5],
    # ],
    # dtype="float32",
    # )
    
    # triangles = np.array(
    # [
    #     [0, 1, 2],
    #     [3, 2, 1],
    #     [1, 0, 4],
    #     [5, 4, 0],
    #     [3, 1, 6],
    #     [4, 6, 1],
    #     [2, 3, 7],
    #     [6, 7, 3],
    #     [0, 2, 5],
    #     [7, 5, 2],
    #     [5, 7, 4],
    #     [6, 4, 7],
    # ],
    # dtype="uint8",)
    # print("triangles=",triangles)

    # z = np.array(
    # [
    #     [10.0],
    #     [0.0],
    #     [10.0],
    #     [0.0],
    #     [0.0],
    #     [10.0],
    #     [0.0],
    #     [10.0],
    # ],
    # dtype="float32",
    # )
    # zmax = np.max(z)
    # zmin = np.min(z)
    # print("zmax,zmin=",zmax,zmin)


    rgb=[]
    for i in range(0, len(z)):
        rgb.append(color_map_color(z[i], cmap_name='jet', vmin=zmin, vmax=zmax))
    rgb = np.array(rgb,dtype=np.float32)

    triangles_binary_blob = triangles.flatten().tobytes()
    points_binary_blob = points.tobytes()
    rgb_binary_blob = rgb.tobytes()


    gltf = pygltflib.GLTF2(
    scene=0,
    scenes=[pygltflib.Scene(nodes=[0])],
    nodes=[pygltflib.Node(mesh=0)],
    meshes=[
        pygltflib.Mesh(
            primitives=[
                pygltflib.Primitive(
                    attributes=pygltflib.Attributes(POSITION=1, COLOR_0=2), indices=0
                )
            ]
        )
    ],
    accessors=[
        pygltflib.Accessor(
            bufferView=0,
            # componentType=pygltflib.UNSIGNED_BYTE,
            componentType=pygltflib.UNSIGNED_INT,
            count=triangles.size,
            type=pygltflib.SCALAR,
            max=[int(triangles.max())],
            min=[int(triangles.min())],
        ),
        pygltflib.Accessor(
            bufferView=1,
            componentType=pygltflib.FLOAT,
            count=len(points),
            type=pygltflib.VEC3,
            max=points.max(axis=0).tolist(),
            min=points.min(axis=0).tolist(),
        ),
        pygltflib.Accessor(
            bufferView=2,
            componentType=pygltflib.FLOAT,
            count=len(rgb),
            type=pygltflib.VEC4,
        ),        
    ],
    bufferViews=[
        pygltflib.BufferView(
            buffer=0,
            byteLength=len(triangles_binary_blob),
            target=pygltflib.ELEMENT_ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(triangles_binary_blob),
            byteLength=len(points_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),
        pygltflib.BufferView(
            buffer=0,
            byteOffset=len(points_binary_blob)+len(triangles_binary_blob),
            byteLength=len(rgb_binary_blob),
            target=pygltflib.ARRAY_BUFFER,
        ),        
    ],
    buffers=[
        pygltflib.Buffer(
            byteLength=len(triangles_binary_blob) + len(points_binary_blob) + len(rgb_binary_blob)
        )
    ],
)
    gltf.set_binary_blob(triangles_binary_blob + points_binary_blob + rgb_binary_blob)
    # glb = b"".join(gltf.save_to_bytes())  # save_to_bytes returns an array of the components of a glb
    # gltf = pygltflib.GLTF2.load_from_bytes(glb)
    # filename2 = "testCreatGltf.glb"
    gltf.save(outfile)


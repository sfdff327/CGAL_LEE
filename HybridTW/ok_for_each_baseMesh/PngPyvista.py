import numpy as np
import pyvista as pv
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def PngMesh(MT_mesh,pngfilename):
    mtNode = []
    faces = []
    for i in range(0,MT_mesh['no_ele']):
        e0=MT_mesh["ele_ind"][i][0]
        e1=MT_mesh["ele_ind"][i][1]
        e2=MT_mesh["ele_ind"][i][2]    
        face = np.hstack([[3,e0,e1,e2]])
        faces.append(face)
        # print("e0,e1,e2,faces=",e0,e1,e2,faces)
    # print(faces)

    mtNode=MT_mesh['XYZ']
    mtZ = np.empty((MT_mesh['no_node']), dtype=np.float32)
    for i in range(0, MT_mesh['no_node']):
        mtZ[i] =MT_mesh['XYZ'][i][2]
    # print(mtNode)
    mesh = pv.PolyData(mtNode,faces)
    pl = pv.Plotter(off_screen=True)
    pl.add_mesh(mesh,scalars =mtZ, color='red', point_size=5, render_points_as_spheres=True)
    pl.show(screenshot=pngfilename)

    return mesh

import os
import json
import calendar
from datetime import date
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import product, shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import numpy as np
import pyvista as pv
import meshio



from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

import TecRead
import PngPyvista

import CreatGltf
import mayaviPlot

arr = (c_char * 64 * 1)()
arr[0].value = b"MT.dat"
nstring = pointer(c_int(1))
MT_mesh = TecRead.TecFileRead(nstring,arr)
pngfilename = 'MT.png'
mtMesh = PngPyvista.PngMesh(MT_mesh,pngfilename)

# print(dir(mtMesh))
ox = mtMesh.center[0]
oy = mtMesh.center[1]
oz = mtMesh.center[2]
for i in range(0,MT_mesh['no_node']):
    MT_mesh['XYZ'][i][0]=MT_mesh['XYZ'][i][0]-ox
    MT_mesh['XYZ'][i][1]=MT_mesh['XYZ'][i][1]-oy
    MT_mesh['XYZ'][i][2]=MT_mesh['XYZ'][i][2]-oz
pngfilename = 'MT2.png'
mtMesh = PngPyvista.PngMesh(MT_mesh,pngfilename)
mtMesh.save('mtBaseMesh.vtk', binary = True)

# pl = pv.Plotter(off_screen=True)
# pl.add_mesh(mtMesh, opacity=0.1)
# pl.add_mesh(pv.PolyData(mtMesh.center),  point_size=10, render_points_as_spheres=True)
# pl.show(screenshot='MT.png')

# quit()

arr[0].value = b"DT.dat"
DT_mesh = TecRead.TecFileRead(nstring,arr)
pngfilename = 'DT.png'
dtMesh = PngPyvista.PngMesh(DT_mesh,pngfilename)

ox = dtMesh.center[0]
oy = dtMesh.center[1]
oz = dtMesh.center[2]
for i in range(0,DT_mesh['no_node']):
    DT_mesh['XYZ'][i][0]=DT_mesh['XYZ'][i][0]-ox
    DT_mesh['XYZ'][i][1]=DT_mesh['XYZ'][i][1]-oy
    DT_mesh['XYZ'][i][2]=DT_mesh['XYZ'][i][2]-oz
pngfilename = 'DT2.png'
dtMesh = PngPyvista.PngMesh(DT_mesh,pngfilename)
dtMesh.save('dtBaseMesh.vtk', binary = True)


arr[0].value = b"DH.dat"
DH_mesh = TecRead.TecFileRead(nstring,arr)
pngfilename = 'DH.png'
dhMesh = PngPyvista.PngMesh(DH_mesh,pngfilename)

ox = dhMesh.center[0]
oy = dhMesh.center[1]
oz = dhMesh.center[2]
for i in range(0,DH_mesh['no_node']):
    DH_mesh['XYZ'][i][0]=DH_mesh['XYZ'][i][0]-ox
    DH_mesh['XYZ'][i][1]=DH_mesh['XYZ'][i][1]-oy
    DH_mesh['XYZ'][i][2]=DH_mesh['XYZ'][i][2]-oz
pngfilename = 'DH2.png'
dhMesh = PngPyvista.PngMesh(DH_mesh,pngfilename)
dhMesh.save('dhBaseMesh.vtk', binary = True)

ele_ind= DH_mesh["ele_ind"]
ele_ind = np.array(ele_ind,dtype=np.uint32)
xyz= np.array(DH_mesh['XYZ'],dtype="float32")

z = []
for i in range(0,DH_mesh['no_node']):
    # print("DH_mesh['XYZ'][i][2]=",DH_mesh['XYZ'][i][2])
    z.append(DH_mesh['XYZ'][i][1])
    # print("z=",z)
# print("z=",z)
z=np.array(z,dtype="float32")

with open('mayaOR.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","scalar"'+'\n')
    f.write('Zone T="mesh",N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n') 
    for i in range(0, len(xyz)):
        f.write(str(xyz[i][0])+','+str(xyz[i][1])+','+str(xyz[i][2])+','+str(z[i])+'\n')
    for i in range(0,len(ele_ind)):
        f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')


no_layer=11
me,q,u,zmax,zmin =mayaviPlot.refinementPLOT(ele_ind,xyz,z,no_layer)

outfile = 'dh.gltf'
# CreatGltf.mesh2Gltf(ele_ind,xyz,z,outfile)
CreatGltf.mesh2Gltf(me,q,u,zmax,zmin,outfile)
''' test 1d 2d glb
# outfile = 'dhPoints.gltf'
# CreatGltf.points2Gltf(me,q,u,zmax,zmin,outfile)    
# outfile = 'dhLines.gltf'
# CreatGltf.lines2Gltf(me,q,u,zmax,zmin,outfile)    
'''

pl = pv.Plotter(off_screen=True)
pl.add_mesh(mtMesh, render_points_as_spheres=True)
pl.add_mesh(dtMesh, render_points_as_spheres=True)
pl.add_mesh(dhMesh, render_points_as_spheres=True)
pl.show(screenshot='ZZ.png')




print("OK")

# GLTF meshs primitive.mode
# https://github.com/KhronosGroup/glTF/tree/master/specification/2.0/#reference-primitive
#
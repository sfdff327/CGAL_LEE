import os
import json
import calendar
from datetime import date
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import numpy as np

sim = cdll.LoadLibrary(os.path.dirname(os.path.abspath(__file__)) + "/tecplot.so")
# print(os.path.dirname(os.path.abspath(__file__)) + "/lib.so")
class Geometry( Structure ):
    pass

class TECReadFor( Structure ):
    pass

def TecFileRead(nstring,arr):

    ndpointer = np.ctypeslib.ndpointer

    class TECReadFor( Structure ):
        _fields_ = [       
        ( "no_node", c_int ),        
        ( "no_ele", c_int ),               
		( "ele_ind", ndpointer(dtype=np.int32,ndim=2)),
		( "XYZ", ndpointer(dtype=np.float64,ndim=2))
	]


    tecData = TECReadFor()
    tecData.memShapes = []

    # print("test BF")
    null_fds, save = pipe.suppress()
    sim.tecPointTriangle(nstring,arr,byref(tecData))
    pipe.resume(null_fds, save)    
    # print("test AF")
    tecData.memShapes.append({"field_name": "no_node", "dtype": np.int32, "shape": None })
    tecData.memShapes.append({"field_name": "no_ele", "dtype": np.int32, "shape": None })
    
    tecData.memShapes.append({"field_name": "ele_ind", "dtype": np.int32, "shape": [tecData.no_ele,3] })

    tecData.memShapes.append({"field_name": "XYZ", "dtype": np.float64, "shape": [tecData.no_node,3] })

    tecDataDict = utils.getdict(tecData)

    return tecDataDict    


if __name__ == "__main__":
    trimesh = TecFileRead()
from mayavi import mlab

from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

mlab.options.offscreen = True
mlab.clf()
mlab.close( all = True )
mlab.figure (1)

def refinementPLOT(ele_ind,xyz,z,no_layer, cmap_name='jet',fill_contour = True):
    import numpy as np
    import matplotlib
    import matplotlib.cm
    from fc_meshtools import simplicial as mshsim
    import fc_mayavi4mesh.simplicial as mlab4sim
    # from vtk import vtkPolyDataMapper
    # import re
    # from polydata import PolyDataHelper
    # import os
    # import sys   
    # import meshInfo 

    
    # dim = xyz.ndim
    # print("dim=",dim)
    

    cmap = matplotlib.cm.get_cmap(cmap_name)  

    q2 = [[],[],[]] 
    for i in range(0, len(xyz)):
        q2[0].append(xyz[i][0])
        q2[1].append(xyz[i][1])
        q2[2].append(xyz[i][2])
    q = np.array(q2)        

    me2 = [[],[],[]] 
    for i in range(0, len(ele_ind)):
        me2[0].append(ele_ind[i][0])
        me2[1].append(ele_ind[i][1])
        me2[2].append(ele_ind[i][2])
        # print(ele_ind[i][0],ele_ind[i][1],ele_ind[i][2])
    me = np.array(me2)

    zmax = max(z)
    zmin = min(z)
    
    lever = np.linspace(zmin, zmax, num=no_layer)
    lever = np.append(lever,zmax+abs(zmax-zmin)/10.0e0)
    print("lever=",lever)
    # print("xyz")
    # print(xyz)
    # print("ele_ind")
    # print(ele_ind)
    # print("z")
    # print(z)
    # plot_data = mlab4sim.plot(q, me, z)
    # plot_data = mlab4sim.plotiso(q, me, z, contours=25)
    plot_data = mlab4sim.plotiso(q, me, z, contours=lever)
    plot_data.contour.filled_contours = fill_contour
    # plot_data.enable_contours = True 
    # print("dir(plot_data)=",dir(plot_data))
    # print("========")
    # print("dir(plot_data.contour.contour_filter)=",dir(plot_data.contour.contour_filter))
    contour_filter = plot_data.contour.contour_filter
    contour_filter_output = contour_filter.get_output()
    scalars_data = contour_filter_output.point_data.scalars.to_array()
    points_data = contour_filter_output.points.to_array()
    polys_data = contour_filter_output.polys.to_array()

    q =[]
    u =[]
    for i in range(0, len(points_data)):
        q.append(points_data[i])
        u.append(scalars_data[i])

    me=[]
    NB=0
    while NB<len(polys_data):
        no_semi_point=polys_data[NB]
        # print(no_semi_point)
        SE=NB+1
        EE=NB+no_semi_point+1
        for i in range(0,EE-SE-2):
            E1 = polys_data[SE]
            E2 = polys_data[SE+i+1]
            E3 = polys_data[SE+i+2]
            me.append([E1,E2,E3])
        NB=EE
        if (NB<len(polys_data)):no_semi_point=polys_data[NB]  

    q=np.array(q,dtype=np.float32)
    u=np.array(u,dtype=np.float32)
    me = np.array(me,dtype=np.uint32) 

    with open('mayaRef.dat','w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","scalar"'+'\n')
        f.write('Zone T="mesh",N='+str(len(q))+',E='+str(len(me))+',F=FEpoint,ET=TRIANGLE\n') 
        for i in range(0, len(q)):
            f.write(str(q[i][0])+','+str(q[i][1])+','+str(q[i][2])+','+str(u[i])+'\n')
        for i in range(0,len(me)):
            f.write(str(me[i][0]+1)+','+str(me[i][1]+1)+','+str(me[i][2]+1)+'\n')
     
    return me,q,u,zmax,zmin


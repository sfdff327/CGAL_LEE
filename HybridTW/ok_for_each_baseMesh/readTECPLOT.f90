module TECPLOT
use, intrinsic :: ISO_C_BINDING
implicit none
contains


subroutine tecPointTriangle(nstring,c_string,TECinput_inf) bind(c, name="tecPointTriangle")
use, intrinsic :: ISO_C_BINDING
implicit none
integer*4 i,j,k,NB,oldNB,ii

type, BIND(C) :: TECinput
    integer (C_INT) no_node,no_ele
    type (C_PTR) ele_ind
    type (C_PTR) XYZ
end type TECinput

integer(c_int), intent(in) :: nstring
character, dimension(256, nstring), intent(in), target :: c_string 
character(len=256) :: fstrings(nstring)
integer :: lenstr

integer*4 no_node,no_ele
character(256) fabFile,coo_transFile,word
integer Reason

type (TECinput), intent(out) :: TECinput_inf

integer (C_INT), ALLOCATABLE, target, save :: ele_ind(:,:)
real (C_DOUBLE), ALLOCATABLE, target, save :: XYZ(:,:)

do i = 1, nstring
  lenstr = cstrlen(c_string(:, i))
  fstrings(i) = transfer(c_string(1:lenstr, i), fstrings(i))
  fstrings(i)(lenstr + 1:) = " "
  enddo


! open(10,file='MT.dat')
open(10,file=fstrings(1))
do i=1,3
read(10,"(A256)") word
end do
read(word(09:256),*) no_node
read(word(25:256),*) no_ele

if (ALLOCATED(ele_ind)) DEALLOCATE(ele_ind)
ALLOCATE(ele_ind(3,no_ele))
if (ALLOCATED(XYZ)) DEALLOCATE(XYZ)
ALLOCATE(XYZ(3,no_node))        

! XYZ=1.2
do i=1,no_node
  read(10,*) (XYZ(j,i),j=1,3)
end do
! ele_ind=2
do i=1,no_ele
  read(10,*) (ele_ind(j,i),j=1,3)
  do j=1,3
    ele_ind(j,i)=ele_ind(j,i)-1
  end do
end do
close(10)


TECinput_inf%ele_ind = C_LOC(ele_ind)
TECinput_inf%XYZ = C_LOC(XYZ)
TECinput_inf%no_node=no_node
TECinput_inf%no_ele=no_ele


return
end subroutine tecPointTriangle

function cstrlen(carray) result(res)
use, intrinsic :: ISO_C_BINDING
implicit none
    character(kind=c_char), intent(in) :: carray(:)
    integer :: res
    integer :: ii
    do ii = 1, size(carray)
      if (carray(ii) == c_null_char) then
        res = ii - 1
        return
      end if
    end do
    res = ii
end function cstrlen

end module TECPLOT


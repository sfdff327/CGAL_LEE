import readSTL
import pyvista as pv
import numpy as np
from pathlib import Path
import pickle
import os
import pandas as pd

if __name__ == "__main__":
    FABResFileCheck = Path("/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/no_frac.out")
    # print("FABResFileCheck.is_file()=",FABResFileCheck.is_file())
    if FABResFileCheck.is_file():
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_XYZ.out', 'rb') as file:
                frac_XYZ = pickle.load(file)        
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/FracNXYZd.out', 'rb') as file:
                FracNXYZd = pickle.load(file)        
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_sege.out', 'rb') as file:
                frac_sege = pickle.load(file)        
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_segeID.out', 'rb') as file:
                frac_segeID = pickle.load(file)        
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/fracID.out', 'rb') as file:
                fracID = pickle.load(file)        
        with open('/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes/frac_ele_ind.out', 'rb') as file:
                frac_ele_ind = pickle.load(file) 

    points=np.array(frac_XYZ,dtype=np.float64)
    cells=[]
    for i in range(0,len(frac_ele_ind)):
        cells.append(np.int32([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]]))
    no_cells=len(cells)
    cells=np.array(cells).ravel()                
    celltypes = np.empty(no_cells, dtype=np.uint32)
    frac = pv.PolyData(points,cells)
    frac['RegionId']=fracID
    noConnfrac=len(np.unique(frac['RegionId']))
    frac['z']=frac.points[:,2:3]
    frac.save('dfnFAB.vtk')

    command = r'./mesh_arrangement'
    nb=[1501,4447,7126]
    for i in range(0,len(nb)):
        single_fracF=r'frac_'+str(nb[i])+'.stl'
        single_frac=frac.extract_cells(frac["RegionId"] == nb[i])
        single_frac=single_frac.extract_surface()
        single_frac=single_frac.triangulate()
        single_frac.save(single_fracF)
        command = command+' '+single_fracF

    os.system(command)

    solidObj=pv.read("output.obj")
    out_labelsF=r'out_labels.txt'
    objLabels = pd.read_csv(out_labelsF,sep='\\s+',header=None)
    objLabels = objLabels.fillna('')
    objLabels.columns=["labels"] 
    solidObj["labels"]=objLabels["labels"]
    solidObj.save('solidObj.vtk')



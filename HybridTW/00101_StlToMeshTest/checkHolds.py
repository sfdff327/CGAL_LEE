import pyvista as pv
import numpy as np
import vtk
import os
import pandas as pd
from numpy.core.numeric import NaN
from pathlib import Path
from xvfbwrapper import Xvfb
display = Xvfb(width=1920, height=1080)
display.start()

def EulerFormula(strname1,strname2,dhNB,fracNB,single_DH,single_FRAC):
    col, n_contacts = single_FRAC.collision(single_DH)
    print("n_contacts=",n_contacts)
    holeInd=np.int32(-1)
    if (n_contacts>0):
        print("i,j=",dhNB,fracNB)
        file = Path("single_DH.stl")
        if file.is_file():
            os.system('rm single_DH.stl')
        file = Path("single_FRAC.stl")
        if file.is_file():
            os.system('rm single_FRAC.stl')
        file = Path("output.obj")
        if file.is_file():
            os.system('rm output.obj')
        file = Path("out_labels.txt")
        if file.is_file():
            os.system('rm out_labels.txt')

        single_DH.save('single_DH.stl')
        single_FRAC.save('single_FRAC.stl')
        command = './mesh_arrangement single_DH.stl single_FRAC.stl'
        os.system(command)
        obj=pv.read("output.obj")
                
        file1 = open('out_labels.txt', 'r')
        df_t = pd.read_csv(file1,header=None)
                # print(df_t)
        onjscalars = np.array(df_t.values.tolist())
        obj['celltype']=onjscalars
        objFrac=obj.extract_cells(obj["celltype"] == 2)
        objFrac=objFrac.extract_surface()
        # print("objFrac")
        # print(objFrac)
        # plotter = pv.Plotter(off_screen=True)
        # # plotter.add_mesh(single_DH, color='c', style="surface", show_edges=False)
        # plotter.add_mesh(objFrac, color='r', style="surface", show_edges=False, opacity=0.25)
        # plotter.show(screenshot='stl.png')
        center=objFrac.cell_centers()
        selected = center.select_enclosed_points(single_DH,check_surface=False)
        # print(selected['SelectedPoints'])
        outPts=objFrac.extract_cells(selected["SelectedPoints"] == 0)
        inPts=objFrac.extract_cells(selected["SelectedPoints"] == 1)
        # pts = center.extract_points(selected['SelectedPoints'].view(bool),adjacent_cells=False)
        '''
        plotter = pv.Plotter(off_screen=True)
        plotter.add_mesh(single_DH, color='c', style="surface", show_edges=False, opacity=0.25)
        plotter.add_mesh(inPts, color='w', style="surface", show_edges=True)
        plotter.add_mesh(outPts, cmap='jet',scalars=outPts.points[:,2], style="surface", show_edges=True,scalar_bar_args={'title': 'z(m)'})
        # plotter.add_mesh(objFrac, color='r', style='surface', show_edges=True)
        plotter.show(screenshot='stl.png')
        '''
        V=outPts.n_points
        F=outPts.n_cells
        E=outPts.extract_all_edges().n_cells
        # print("clipFrac.n_points=",V) 
        # print("clipFrac.n_faces=",F)
        # print("clipFrac.n_edges=",E)
        # print("V - E + F=",V - E + F)
        if ((V - E + F)==1):
            holeInd=np.int32(0)
        else:
            holeInd=np.int32(1)  
        '''
        # holeInd==0 代表部份貫穿
        # holeInd==1 代表完全貫穿
        未來計算最大速度差值，僅需要找 inPts.points
        '''
        # print("holeInd=",holeInd)
        # print("type=",type(holeInd))
        # '''
        binfile=strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
        inPts.save(binfile)
        binfile='out'+strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
        outPts.save(binfile)
        binfile='OBJ_'+strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
        obj.save(binfile)
        print("holeInd=",holeInd)
        quit()
        # '''
        # inPts.save(binfile,binary=False)
        # objFrac=obj.extract_cells(obj["celltype"] == 1)
        # objFrac=objFrac.extract_surface()
        # objFrac.save('obj01.stl')
        # objFrac=obj.extract_cells(obj["celltype"] == 2)
        # objFrac=objFrac.extract_surface()
        # objFrac.save('obj02.stl')
        # objFrac=obj.extract_cells(obj["celltype"] == 3)
        # objFrac=objFrac.extract_surface()
        # objFrac.save('obj03.stl')
        # inPts=inPts.extract_surface()
        # inPts.save('inPts.stl')
        # outPts=outPts.extract_surface()
        # outPts.save('outPts.stl')
    return holeInd

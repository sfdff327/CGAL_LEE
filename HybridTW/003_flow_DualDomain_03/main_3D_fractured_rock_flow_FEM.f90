program readTetGen
integer*4 i,j,k,m,NI,NJ,MI,MJ,NII,NJJ,NB
character(256) nodefile,elefile,facefile,edgefile,word
integer*4 no_node,no_ele,no_face,no_edge,no_face_type
real*8, allocatable:: x(:),y(:),z(:)
real*8, allocatable:: x3(:),y3(:),z3(:)
real*8, allocatable:: x2(:),y2(:),z2(:)
real*8 vx2(3),vy2(3),vz2(3)
integer*4 NODE,tri_NODE
integer*4, allocatable:: ele_ind(:,:),ele_2D_ind(:),global_ele_2D_ind(:)
integer*4, allocatable:: face_ind(:,:),face_ID(:),BC_node_ind(:)
integer*4, allocatable:: fracture_ele_ind(:,:),real_fracture_ele_ind(:,:),fracture_ID(:)
integer*4 no_fracture_ele,no_fracture_node
real*8 cx,cy,cz,varC
real*8, allocatable:: KrockX(:),KrockY(:),KrockZ(:),poro_rock(:)
real*8, allocatable:: Kfracture(:),poro_fracture(:)

integer*4, allocatable:: face_node_ind(:),check_fracture_ID(:),real_fracuteID(:),NO_ind(:),concentration_node_ind(:)
real*8, allocatable:: concentration3(:)
real*8, allocatable:: volume(:)
integer*4 CinfractureID
integer*4 no_2D_node,no_2D_ele
real*8 tx(3),ty(3),tz(3),mx,my,mz,ox,oy,oz
real*8 a(4),b(4),c(4),d(4),temp_x,temp_y,temp_z
integer*4 BC_type
real*8 BC_coordinate(2),BC_H(2)
real*8 EPSLON,TOL1,SQEPS
integer*4 no_BC1_node
integer*4, allocatable:: BC1_NB(:)
real*8, allocatable:: BC1_value(:)
integer*4 no_BC2_face
integer*4, allocatable:: BC2_NB1(:),BC2_NB2(:),BC2_NB3(:)
real*8, allocatable:: BC2_value(:)
real*8 value,value2,unit_value,unit_value2
real*8 aperture

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FEM stiffness matrix
real*8, allocatable:: SK_2D(:,:),F(:),Fx(:),Fy(:),Fz(:)
real*8, allocatable:: HP(:),RES_U(:)
real*8 WCSS,WCRR,THETSE
integer*4 IQ,JQ,NP
real*8 XE(4),YE(4),UE(4),HQ(4),HKG(4),PHIMRG(4)

real*8 xq(4),yq(4),zq(4)
real*8 LL1(4),LL2(4),LL3(4),LL4(4)
real*8 D1,D2,D3,D4
real*8 N(4),DNX(4),DNY(4),DNZ(4),DJAC
real*8 SATKX,SATKY,SATKZ,SATKXY,SATKXZ,SATKYZ
real*8 AKXQP,AKYQP,AKZQP,AKXYQP,AKXZQP,AKYZQP

real*8 L1KG,L2KG,L3KG,HNP
real*8 SNFE,CSFE,RQ(4),QA(4,4),QB(4,4),WG(4)
real*8 QRX(4),QRY(4),QRZ(4)
real*8 DN(3,2),AREA
integer*4 KG,NE

real*8, allocatable:: SK_frac_2D(:,:),F_frac(:)
real*8, allocatable:: HP_frac(:),RES_U_frac(:)
integer*4, allocatable:: test_face_node_ind(:)
integer*4, allocatable:: fracture_1st_ele(:)
integer*4 no_fracture
integer*4, allocatable:: NB_fracture_TETGEN(:)
integer*4, allocatable:: fracture_1st_ele_check(:)
real*8, allocatable:: EQa1(:),EQb1(:),EQc1(:),EQd1(:)
real*8, allocatable:: EQa2(:),EQb2(:),EQc2(:),EQd2(:)
real*8, allocatable:: org_x(:),org_y(:),org_z(:)      !each 2D fracture original xyz
real*8, allocatable:: Nx(:),Ny(:),Nz(:),Nd(:)
real*8 vpx,vpy,vpz
real*8, allocatable:: fracture_x(:),fracture_y(:),fracture_z(:),fracture_h(:)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Darcy's velocity
real*8, allocatable:: vx(:),vy(:),vz(:),vx2D(:),vy2D(:),vz2D(:),vx_2Dto3D(:),vy_2Dto3D(:),vz_2Dto3D(:)
real*8  DNX3(3),DNY3(3),DNZ3(3)
real*8, allocatable:: unit_vx1(:),unit_vy1(:),unit_vz1(:)
real*8, allocatable:: unit_vx2(:),unit_vy2(:),unit_vz2(:)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Darcy's velocity

!!!!!!!!!!!!!!!!!!!!!!!!!!!AGMG
!real (kind(0d0)), allocatable:: AGMG_A(:),TH(:),R(:)
real*8, allocatable:: AGMG_A(:),TH(:),R(:),cal_AGMG_A(:)
integer*4, allocatable:: AGMG_IA(:),AGMG_JA(:),cal_AGMG_JA(:),cal_AGMG_IA(:)
integer*4 cal_NB,no_AGMG_A,chech_ind
integer*4 iter,iprint,nhinv
real (kind(0d0)) :: tol
integer*4, allocatable:: NLRN(:),LRN(:,:),ture_LRN(:,:)
integer*4 NLRNN,IEMJ,IEM
integer*4, allocatable:: rank_NB(:),rank_ind(:)
integer*4 max_no_nei
!!!!!!!!!!!!!!!!!!!!!!!!!!!AGMG

integer*4, allocatable:: node_check(:),return_OR_nodeNB(:),new_2d_ele_ind(:,:)
real*8, allocatable:: xn3(:),yn3(:),zn3(:),xn2(:),yn2(:),vxn2(:),vyn2(:)

logical :: file_exists
integer*4 no_IC
integer*4, allocatable:: IC_NB(:)
real*8, allocatable:: IC_value(:)


aperture=1.0d-2

!call cal_3D_AREA(0.0d0,0.0d0,0.0d0,   1.0d0,0.0d0,0.0d0,   1.0d0,1.0d0,0.0d0,area)


tri_NODE=3

EPSLON=1.0D0
EPSLON=EPSLON/2.0D0
TOL1=1.0D0+EPSLON
do while(TOL1.GT.1.0D0)
   EPSLON=EPSLON/2.0D0
   TOL1=1.0D0+EPSLON
end do
SQEPS=DSQRT(EPSLON)

!open(10,file='sp.dat')
!read(10,"(a256)") nodefile
!read(10,"(a256)") elefile
!read(10,"(a256)") facefile
!read(10,"(a256)") edgefile
!read(10,*) cx,cy,cz
!read(10,*) varC
!read(10,*) CinfractureID
!read(10,*) Kfracture
!read(10,*) Krock
!read(10,*) BC_type
!if (BC_type==1 .or. BC_type==2 .or. BC_type==3) then
!   do i=1,2
!      read(10,*) BC_coordinate(i),BC_H(i)
!   end do
!end if
!close(10)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! input information
NODE=4
open(10,file='FEM_AGMG_MESH.dat')
! open(10,file='FEM_AGMG_MESH.dat',form="binary")
read(10,*) no_node,no_ele
allocate(x(no_node),y(no_node),z(no_node),face_node_ind(no_node),concentration_node_ind(no_node))
allocate(x3(no_node),y3(no_node),z3(no_node),concentration3(no_node))
allocate(test_face_node_ind(no_node))
! allocate(ele_ind(no_ele,NODE))
allocate(ele_ind(no_ele,NODE),KrockX(no_ele),KrockY(no_ele),KrockZ(no_ele),poro_rock(no_ele))

do i=1,no_node
   read(10,*) x(i),y(i),z(i)
end do
do i=1,no_ele
   read(10,*) (ele_ind(i,j),j=1,NODE)
end do
close(10)
write(*,*) 'read mesh'

open(10,file='FEM_fracture_ele_HK.dat')
! open(10,file='FEM_fracture_ele_HK.dat',form="binary")
read(10,*) no_fracture,no_face_type
no_face_type=no_fracture

allocate(NB_fracture_TETGEN(no_fracture))
read(10,*) (NB_fracture_TETGEN(i),i=1,no_fracture)
read(10,*) no_fracture_ele  !modify IHL 20190104
allocate(Kfracture(no_fracture_ele),poro_fracture(no_fracture_ele))
allocate(face_ind(no_fracture_ele,tri_NODE),face_ID(no_fracture_ele),fracture_ele_ind(no_fracture_ele,tri_NODE),fracture_ID(no_fracture_ele))
allocate(fracture_1st_ele(no_fracture))

NI=0
do i=1,no_fracture_ele
   NI=NI+1
   read(10,*) (face_ind(NI,j),j=1,tri_NODE),Kfracture(NI),face_ID(NI),poro_fracture(NI)
   ! write(*,"(3(I0,1x),G0,1x,I0,G0)") (face_ind(NI,j),j=1,tri_NODE),Kfracture(NI),face_ID(NI),poro_fracture(NI)
   ! if (NI==10) stop
   !Kfracture(NI)=Kfracture(NI)*aperture
   call cal_3D_AREA(x(face_ind(NI,1)),y(face_ind(NI,1)),z(face_ind(NI,1)),x(face_ind(NI,2)),y(face_ind(NI,2)),z(face_ind(NI,2)),x(face_ind(NI,3)),y(face_ind(NI,3)),z(face_ind(NI,3)),area)
   if (area<EPSLON) then
      NI=NI-1
   end if
end do
close(10)
no_fracture_ele=NI

fracture_ID=3
write(*,*) 'fracture ele'

open(10,file='FEM_AGMG_BC1_inf.dat')
no_BC1_node=0
do while(.not. eof(10))
    read(10,"(A256)") word
    if (word(1:20)/='                    ') no_BC1_node=no_BC1_node+1
end do
close(10)
allocate(BC1_NB(no_BC1_node),BC1_value(no_BC1_node))

open(10,file='FEM_AGMG_BC1_inf.dat')
do i=1,no_BC1_node
   read(10,*) BC1_NB(i),BC1_value(i)
end do
close(10)
write(*,*) 'BC1 node'

open(10,file='FEM_AGMG_BC2_inf.dat')
no_BC2_face=0
do while(.not. eof(10))
    read(10,"(A256)") word
    if (word(1:20)/='                    ') no_BC2_face=no_BC2_face+1
end do
close(10)
allocate(BC2_NB1(no_BC2_face),BC2_NB2(no_BC2_face),BC2_NB3(no_BC2_face),BC2_value(no_BC2_face))

open(10,file='FEM_AGMG_BC2_inf.dat')
do i=1,no_BC2_face
   read(10,*) BC2_NB1(i),BC2_NB2(i),BC2_NB3(i),BC2_value(i)
   call modify_ele_NB(no_ele,ele_ind,BC2_NB1(i),BC2_NB2(i),BC2_NB3(i))
end do
close(10)
write(*,*) 'BC2 node'

INQUIRE(FILE="FEM_AGMG_IC.dat", EXIST=file_exists)
 no_IC=0
 !if (file_exist) then
open(10,file='FEM_AGMG_IC.dat')
do while(.not. eof(10))
   read(10,"(A256)") word
   if (word(1:20)/='                    ') no_IC=no_IC+1
end do
close(10)
allocate(IC_NB(no_IC),IC_value(no_IC))
open(10,file='FEM_AGMG_IC.dat')
do i=1,no_IC
   read(10,*) IC_NB(i),IC_value(i)
end do
close(10)
 !end if
write(*,*) 'IC node'


open(10,file='FEM_AGMG_HK.dat')
! open(10,file='FEM_AGMG_HK.dat',form="binary")
do i=1,no_ele
   ! read(10,*) KrockX,KrockY,KrockZ,poro_rock
   read(10,*) KrockX(i),KrockY(i),KrockZ(i),poro_rock(i)
end do
close(10)
write(*,*) 'matrix HK'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! input information



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cal no_node_neigh
max_no_nei=630
allocate(NLRN(no_node),LRN(no_node,max_no_nei))
write(*,*) 'allocate NLRN and LRN'

NLRN=0
LRN=0

do M=1,no_ele
   ! write(*,"(A,I0,1x,I0)") 'no_ele,M=',no_ele,M
   do i=1,NODE
      IEM=ele_ind(M,i)
      !write(*,*) 'IEM=',IEM
      do j=1,NODE
         IEMJ=ele_ind(M,j)
         !write(*,*) 'IEMJ=',IEMJ
         if (NLRN(IEM)==0) then
            NLRN(IEM)=NLRN(IEM)+1
            !write(*,*) 'NLRN(IEM)=',NLRN(IEM)
            LRN(IEM,1)=IEMJ
            !write(*,*) 'LRN(IEM,1)=',LRN(IEM,1)
         else
            do k=1,NLRN(IEM)
               Na=LRN(IEM,k)
               !write(*,*) 'LRN(IEM,k)=',LRN(IEM,k)
               if (Na==IEMJ) goto 100
            end do
            NLRN(IEM)=NLRN(IEM)+1
            !write(*,*) 'type 2 NLRN(IEM)=',NLRN(IEM)
            NLRNN=NLRN(IEM)
            !write(*,*) 'NLRNN=',NLRNN
            LRN(IEM,NLRNN)=IEMJ
            !write(*,*) 'LRN(IEM,NLRNN)=',LRN(IEM,NLRNN)
         end if
  100 end do
  end do
end do
write(*,*) 'creat LRN'

max_no_nei=0
no_AGMG_A=0
do i=1,no_node
   if (max_no_nei<NLRN(i)) max_no_nei=NLRN(i)
   no_AGMG_A=no_AGMG_A+NLRN(i)
end do
allocate(ture_LRN(no_node,max_no_nei))
ture_LRN=0
write(*,"(A,I0)") 'max_no_nei=',max_no_nei

do i=1,no_node
   allocate(rank_NB(NLRN(i)),rank_ind(NLRN(i)))
   do j=1,NLRN(i)
      rank_ind(j)=j
      rank_NB(j)=LRN(i,j)
   end do
   call Quick_sort_integer(rank_NB,rank_ind,NLRN(i))
   do j=1,NLRN(i)
      ture_LRN(i,j)=rank_NB(j)
   end do
   deallocate(rank_NB,rank_ind)
end do
deallocate(LRN)

open(10,file='FEM_AGMG_SET_inf.dat',form="binary")
write(10) max_no_nei
do i=1,no_node
    write(10) NLRN(i)
    write(10) (ture_LRN(i,j),j=1,NLRN(i))
end do
close(10)
write(*,*) 'finish cal node neigh'


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
allocate(SK_2D(no_node,max_no_nei),F(no_node))
allocate(HP(no_node),RES_U(no_node))
write(*,*) 'allocate SK_2D'
SK_2D=0.0d0
SK_velocity_2D=0.0d0
F=0.0d0
RES_U=0.0d0
! HP=z
HP=0.0d0

do i=1,no_IC
   HP(IC_NB(i))=IC_value(i)-z(IC_NB(i))
end do


WCSS=0.25
WCRR=0.05
do i=1,no_BC1_node
   HP(BC1_NB(i))=BC1_value(i)-z(BC1_NB(i))
end do
write(*,"(A)") 'HP(BC1_NB(i))=BC1_value(i) OK'
RES_U=Hp

!open(20,file='FEM_2d_input.dat')
open(30,file='unit_vector.dat')
open(45,file='FEM_plane_EQ.dat')
allocate(ele_2D_ind(tri_NODE),global_ele_2D_ind(tri_NODE))
allocate(x2(3),y2(3),z2(3))
DATA LL1/1.0D0, 0.0D0, 0.0D0, 0.0d0/
DATA LL2/0.0D0, 1.0D0, 0.0D0, 0.0d0/
DATA LL3/0.0D0, 0.0D0, 1.0D0, 0.0d0/
DATA WG/0.333333333333333D0, 0.333333333333333D0,0.333333333333333D0, 0.0d0/
SNFE=0.0d0
CSFE=1.0d0


!open(3001,file='2D_coordinate_eleind.dat',form="binary")
open(3001,file='2D_coordinate_eleind.dat')
!open(3002,file='2D_planeEQ.dat',form="binary")

allocate(fracture_1st_ele_check(no_face_type))
allocate(EQa1(no_face_type),EQb1(no_face_type),EQc1(no_face_type),EQd1(no_face_type))
allocate(EQa2(no_face_type),EQb2(no_face_type),EQc2(no_face_type),EQd2(no_face_type))
allocate(org_x(no_face_type),org_y(no_face_type),org_z(no_face_type))
allocate(Nx(no_face_type),Ny(no_face_type),Nz(no_face_type),Nd(no_face_type))


fracture_1st_ele_check=0
EQa1=0.0d0
EQb1=0.0d0
EQc1=0.0d0
EQd1=0.0d0
EQa2=0.0d0
EQb2=0.0d0
EQc2=0.0d0
EQd2=0.0d0

open(5001,file='FEM_AGMG_area.dat',form="binary")
open(5002,file='FEM_AGMG_volume.dat',form="binary")

!NB_fracture_TETGEN
!face_ID

do NI=1,no_fracture_ele
   ! write(*,"(A,I0,1x,I0)") 'no_fracture_ele,Ni=',no_fracture_ele,Ni
   ! if (NI==10817) then
   !   write(*,*) 'Ni=',NI
   ! end if
   no_2D_ele=1
   do j=1,tri_NODE
      global_ele_2D_ind(j)=face_ind(Ni,j)
      ele_2D_ind(j)=j
   end do
   ! if (NI==10817) then
   !   write(*,*) 'global_ele_2D_ind'
   ! end if

   no_2D_node=3
   do j=1,tri_NODE
      x2(j)=x(face_ind(Ni,j))
      y2(j)=y(face_ind(Ni,j))
      z2(j)=z(face_ind(Ni,j))
   end do
   ! if (NI==10817) then
   !   write(*,*) 'x2(j)=x(face_ind(Ni,j))'
   !   write(*,"(A,I0)") 'face_ID(NI)= ',face_ID(NI)
   ! end if

   if (fracture_1st_ele_check(face_ID(NI))==0) then
       call plane_EQ(x2(1),y2(1),z2(1),x2(2),y2(2),z2(2),x2(3),y2(3),z2(3),Nx(face_ID(NI)),Ny(face_ID(NI)),Nz(face_ID(NI)),Nd(face_ID(NI)))
       write(45,"(I,1x,4(E,1x))") face_ID(NI),Nx(face_ID(NI)),Ny(face_ID(NI)),Nz(face_ID(NI)),Nd(face_ID(NI))
       fracture_1st_ele_check(face_ID(NI))=1
    !    do i=1,1
    !        do j=1,tri_NODE
    !            tx(j)=x2(j)
    !            ty(j)=y2(j)
    !            tz(j)=z2(j)            
    !        end do
    !    end do
    !    org_x(face_ID(NI))=tx(1)
    !    org_y(face_ID(NI))=ty(1)
    !    org_z(face_ID(NI))=tz(1)
       
    !    mx=(tx(1)+tx(2))/2.0d0
    !    my=(ty(1)+ty(2))/2.0d0
    !    mz=(tz(1)+tz(2))/2.0d0
    !    call Circumcenter_new(tx(1),ty(1),tz(1),tx(2),ty(2),tz(2),tx(3),ty(3),tz(3),ox,oy,oz)
       
    !    a(1)=tx(2)-tx(1)
    !    b(1)=ty(2)-ty(1)
    !    c(1)=tz(2)-tz(1)
    !    d(1)=a(1)*tx(1)+b(1)*ty(1)+c(1)*tz(1)
    !    a(2)=mx-ox
    !    b(2)=my-oy
    !    c(2)=mz-oz
    !    d(2)=a(2)*tx(1)+b(2)*ty(1)+c(2)*tz(1)
    !    if ((a(2)*a(2)+b(2)*b(2)+c(2)*c(2))<=EPSLON) then          
    !        mx=(tx(1)+tx(3))/2.0d0
    !        my=(ty(1)+ty(3))/2.0d0
    !        mz=(tz(1)+tz(3))/2.0d0
           
    !        a(1)=tx(3)-tx(1)
    !        b(1)=ty(3)-ty(1)
    !        c(1)=tz(3)-tz(1)
    !        d(1)=a(1)*tx(1)+b(1)*ty(1)+c(1)*tz(1)
           
    !        a(2)=mx-ox
    !        b(2)=my-oy
    !        c(2)=mz-oz
    !        d(2)=a(2)*tx(1)+b(2)*ty(1)+c(2)*tz(1)
    !    end if
       
       call triangle3dTo2DCoor(x2,y2,z2,a,b,c,d,org_x(face_ID(NI)),org_y(face_ID(NI)),org_z(face_ID(NI)))

       do i=1,2
        !    unit_vector=(a(i)*a(i)+b(i)*b(i)+c(i)*c(i))**0.5d0
        !    write(30,"(I,1x,4(E,1x))") face_ID(NI),a(i)/unit_vector,b(i)/unit_vector,c(i)/unit_vector,d(i)/unit_vector
           write(30,"(I,1x,4(E,1x))") face_ID(NI),a(i),b(i),c(i),d(i)
       end do
       
       
       EQa1(face_ID(NI))=a(1)
       EQb1(face_ID(NI))=b(1)
       EQc1(face_ID(NI))=c(1)
       EQd1(face_ID(NI))=d(1)
       EQa2(face_ID(NI))=a(2)
       EQb2(face_ID(NI))=b(2)
       EQc2(face_ID(NI))=c(2)
       EQd2(face_ID(NI))=d(2)       
   end if
   ! if (NI==10817) then
   !   write(*,*) 'EQa1(face_ID(NI))=a(1'
   ! end if   
   
   do i=1,no_2D_node
      temp_x=(EQa1(face_ID(NI))*x2(i)+EQb1(face_ID(NI))*y2(i)+EQc1(face_ID(NI))*z2(i)-EQd1(face_ID(NI)))/((EQa1(face_ID(NI))*EQa1(face_ID(NI))+EQb1(face_ID(NI))*EQb1(face_ID(NI))+EQc1(face_ID(NI))*EQc1(face_ID(NI)))**0.5d0)
      temp_y=(EQa2(face_ID(NI))*x2(i)+EQb2(face_ID(NI))*y2(i)+EQc2(face_ID(NI))*z2(i)-EQd2(face_ID(NI)))/((EQa2(face_ID(NI))*EQa2(face_ID(NI))+EQb2(face_ID(NI))*EQb2(face_ID(NI))+EQc2(face_ID(NI))*EQc2(face_ID(NI)))**0.5d0)
      x2(i)=temp_x
      y2(i)=temp_y
   end do
   ! if (NI==10817) then
   !   write(*,*) 'x2(i)=temp_x'
   ! end if     

   call determinant_value(x2(ele_2D_ind(1)),y2(ele_2D_ind(1)),&
                        & x2(ele_2D_ind(2)),y2(ele_2D_ind(2)),&
                        & x2(ele_2D_ind(3)),y2(ele_2D_ind(3)),unit_value)
   ! if (NI==10817) then
   !   write(*,*) 'unit_value'
   ! end if  

   if (unit_value<EPSLON) then
       k=ele_2D_ind(2)
       ele_2D_ind(2)=ele_2D_ind(3)
       ele_2D_ind(3)=k
       k=global_ele_2D_ind(2)
       face_ind(Ni,2)=face_ind(Ni,3)
       global_ele_2D_ind(2)=global_ele_2D_ind(3)
       face_ind(Ni,3)=k
       global_ele_2D_ind(3)=k       
       unit_value=-unit_value
   end if
   ! if (NI==10817) then
   !   write(*,*) 'if (unit_value<EPSLON) then'
   ! end if    

   !write(3001) no_2D_node,no_2D_ele         !Binary file (20170924,LEE)
   do i=1,no_2D_node
      !write(3001) x2(i),y2(i)
      write(3001,*) x2(i),y2(i)
   end do
   do i=1,no_2D_ele
      !write(3001) (ele_2D_ind(j),j=1,3)
      write(3001,*) (ele_2D_ind(j),j=1,3)
   end do
   do i=1,no_2D_ele
      !write(3001) (global_ele_2D_ind(j),j=1,3)
      write(3001,*) (global_ele_2D_ind(j),j=1,3)
      !write(3002) a(1),b(1),c(1),d(1)
      !write(3002) a(2),b(2),c(2),d(2)
   end do
   ! if (NI==10817) then
   !   write(*,*) 'write(3001,*) (global_ele_2D_ind(j),j=1,3)'
   ! end if    
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!write 2D information

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! if (NI==10817) then
   !   write(*,*) 'no_2D_ele=',no_2D_ele
   ! end if      
   do M=1,no_2D_ele
       SATKX =Kfracture(NI)
       SATKZ =Kfracture(NI)
       SATKXZ=0.0d0
       WCSS=0.25
       WCRR=0.05
       THETSE=poro_fracture(M) !WCSS-WCRR
       
       do IQ=1,3
           XE(IQ)=x2(ele_2D_ind(IQ))
           YE(IQ)=y2(ele_2D_ind(IQ))
           NP=global_ele_2D_ind(IQ)
           HQ(IQ)=HP(NP)
       end do
      !  if (NI==10817) then
      !      write(*,*) 'HQ(IQ)=HP(NP)'
      !  end if  
       
	   call GRADI3(XE,YE,DN,AREA)
      !  if (NI==10817) then
      !      write(*,*) 'call GRADI3(XE,YE,DN,AREA)'
      !  end if       
       !AREA=AREA*aperture
       write(5001) AREA
       
       RQ=0.0d0
       QA=0.0d0
       QB=0.0d0
       
       do kg=1,3
           L1KG=LL1(KG)
           L2KG=LL2(KG)
           L3KG=LL3(KG)
           N(1)=L1KG
           N(2)=L2KG
           N(3)=L3KG
           HKG(KG)=0.0  
           
           do IQ=1,3
               RQ(IQ)=RQ(IQ)+&
                   & (DN(IQ,1)*(SATKX *AREA*WG(KG) *SNFE+SATKXZ*AREA*WG(KG)*CSFE)+ &
                   &  DN(IQ,2)*(SATKXZ*AREA*WG(KG) *SNFE+SATKZ *AREA*WG(KG)*CSFE))
               do JQ=1,3
                   QA(IQ,JQ)=QA(IQ,JQ) 
                   QB(IQ,JQ)=QB(IQ,JQ) + DN(IQ,1)*(SATKX*AREA*WG(KG)*DN(JQ,1)+SATKXZ*AREA*WG(KG)*DN(JQ,2))+DN(IQ,2)*(SATKXZ*AREA*WG(KG)*DN(JQ,1)+SATKZ*AREA*WG(KG)*DN(JQ,2))
               end do
           end do  
       end do
      !  if (NI==10817) then
      !      write(*,"(A,I0,A)") 'Ni= ',Ni,'RQ(IQ)=RQ(IQ)+&'
      !  end if
       
       do kg=1,3
           MI=global_ele_2D_ind(kg)
           do JQ=1,3
               NJ=global_ele_2D_ind(JQ)
               do IEM=1,NLRN(MI)
                   if (NJ==ture_LRN(MI,IEM)) then
                       goto 566
                   end if
               end do
566            SK_2D(MI,IEM)=SK_2D(MI,IEM)+QB(kg,JQ)
           end do  
       end do
      !  if (NI==10817) then
      !      write(*,*) 'SK_2D(MI,IEM)=SK_2D(MI,IEM)+QB(kg,JQ)'
      !  end if      
   end do
   ! if (NI==10817) then
   !    write(*,"(A,I0)") 'Finifsh Ni= ',Ni
   ! end if    
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end do
close(3001)
!close(3002)
close(30)
write(*,*) 'finished to calucate fracture stiffness matrix'

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ROCK matrix
LL1(1)=1.0D0
LL1(2)=0.0D0
LL1(3)=0.0D0
LL1(4)=0.0D0
LL2(1)=0.0D0
LL2(2)=1.0D0
LL2(3)=0.0D0
LL2(4)=0.0D0
LL3(1)=0.0D0
LL3(2)=0.0D0
LL3(3)=1.0D0
LL3(4)=0.0D0
LL4(1)=0.0D0
LL4(2)=0.0D0
LL4(3)=0.0D0
LL4(4)=1.0D0

allocate(volume(no_ele))
do NE=1,no_ele
   do i=1,node
      np=ele_ind(NE,i) 
      hq(i)=HP(np)
      xq(i)=x(np)
      yq(i)=y(np)
      zq(i)=z(np)
   end do

   do i=1,node
      D1=LL1(i)
      D2=LL2(i)
      D3=LL3(i)
      D4=LL4(i)

      DJAC=0.0d0
      do kk=1,4
         IF (KK.EQ.1)THEN
            K1=2
            K2=3
            K3=4
         ELSEIF(KK.EQ.2)THEN
            K1=1
            K2=3
            K3=4
         ELSEIF(KK.EQ.3)THEN
            K1=1
            K2=2
            K3=4
         ELSE
            K1=1
            K2=2
            K3=3
         ENDIF
         A(KK)=(-1.0D0)**(KK+1)*(XQ(K1)*YQ(K2)*ZQ(K3)+&
             & YQ(K1)*ZQ(K2)*XQ(K3)+ZQ(K1)*XQ(K2)*YQ(K3)-&
             & XQ(K3)*YQ(K2)*ZQ(K1)-YQ(K3)*ZQ(K2)*XQ(K1)-&
             & ZQ(K3)*XQ(K2)*YQ(K1))
         B(KK)=(-1.0D0)**KK*(YQ(K1)*ZQ(K2)+&
             & YQ(K2)*ZQ(K3)+YQ(K3)*ZQ(K1)-&
             & YQ(K3)*ZQ(K2)-YQ(K2)*ZQ(K1)-&
             & YQ(K1)*ZQ(K3))
         C(KK)=(-1.0D0)**(KK+1)*(XQ(K1)*ZQ(K2)+&
             & XQ(K2)*ZQ(K3)+XQ(K3)*ZQ(K1)-&
             & XQ(K3)*ZQ(K2)-XQ(K2)*ZQ(K1)-&
             & XQ(K1)*ZQ(K3))
         D(KK)=(-1.0D0)**KK*(XQ(K1)*YQ(K2)+&
             & XQ(K2)*YQ(K3)+XQ(K3)*YQ(K1)-&
             & XQ(K3)*YQ(K2)-XQ(K2)*YQ(K1)-&
             & XQ(K1)*YQ(K3))
         DJAC=DJAC+A(KK)
      end do
      N(1)=D1
      N(2)=D2
      N(3)=D3
      N(4)=D4

      do KK=1,4
         DNX(KK)=B(KK)/DJAC
         DNY(KK)=C(KK)/DJAC
         DNZ(KK)=D(KK)/DJAC
      end do
	  DJAC=DABS(DJAC)/6.0d0
      volume(NE)=DJAC
      DJAC=DJAC*0.25d0
      HKG(i)=0.0

      do IQ=1,NODE
         HKG(i)=HKG(i)+HQ(IQ)*N(IQ)
      end do

      ! SATKX =KrockX !KrockX(NE)
      ! SATKY =KrockY !KrockY(NE)
      ! SATKZ =KrockZ !KrockZ(NE)
      SATKX =KrockX(NE)
      SATKY =KrockY(NE)
      SATKZ =KrockZ(NE)
      SATKXY=0.0d0
      SATKXZ=0.0d0
      SATKYZ=0.0d0

      ! THETSE=poro_rock !poro_rock(NE) !WCSS-WCRR
      THETSE=poro_rock(NE) !WCSS-WCRR

      AKXQP=SATKX*DJAC
      AKYQP=SATKY*DJAC
      AKZQP=SATKZ*DJAC
      AKXYQP=SATKXY*DJAC
      AKXZQP=SATKXZ*DJAC
      AKYZQP=SATKYZ*DJAC

      RQ=0.0d0
	  QA=0.0d0
	  QB=0.0d0

      do IQ=1,NODE
         RQ(IQ)=RQ(IQ)+1.0d0*(DNX(IQ)*AKXZQP+AKYZQP*DNY(IQ)+&
                           & AKZQP*DNZ(IQ))*1.0d0 
         do JQ=1,NODE
            QA(IQ,JQ)=QA(IQ,JQ)
            QB(IQ,JQ)=QB(IQ,JQ)+DNX(IQ)*(AKXQP*DNX(JQ)+AKXYQP*DNY(JQ)+&
                    & AKXZQP*DNZ(JQ)) + DNY(IQ)*(AKXYQP*DNX(JQ)+&
                    & AKYQP*DNY(JQ)+AKYZQP*DNZ(JQ)) + DNZ(IQ) *&
                    & (AKXZQP*DNX(JQ)+AKYZQP*DNY(JQ)+AKZQP*DNZ(JQ))
         end do
      end do

      do IQ=1,node
         NI=ele_ind(NE,IQ)
         do JQ=1,node
           NJ=ele_ind(NE,JQ)
           F(NI)=F(NI)+(QA(IQ,JQ)-0.0d0*QB(IQ,JQ))*HP(NJ)
           do IEM=1,NLRN(NI)
              if (NJ==ture_LRN(NI,IEM)) then
                 goto 567
              end if
           end do
       567 SK_2D(NI,IEM)=SK_2D(NI,IEM)+QA(IQ,JQ)+QB(IQ,JQ)
         end do
      end do
 
   end do
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

do i=1,no_ele
    write(5002) volume(i)
end do
close(5002)
deallocate(volume)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BC2
do i=1,no_BC2_face
    call BC2_R12(&
        & x(BC2_NB1(i)),y(BC2_NB1(i)),z(BC2_NB1(i)),&
        & x(BC2_NB2(i)),y(BC2_NB2(i)),z(BC2_NB2(i)),&
        & x(BC2_NB3(i)),y(BC2_NB3(i)),z(BC2_NB3(i)),&
        & BC2_value(i),SATKXZ,SATKYZ,SATKZ,&
        & F(BC2_NB1(i)),F(BC2_NB2(i)),F(BC2_NB3(i)))
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BC2

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BC1
do i=1,no_BC1_node
   F(BC1_NB(i))=BC1_value(i) !-z(BC1_NB(i))
   do IEM=1,NLRN(BC1_NB(i))
      if (BC1_NB(i)==ture_LRN(BC1_NB(i),IEM)) then
         SK_2D(BC1_NB(i),IEM)=1.0d0
      else
         SK_2D(BC1_NB(i),IEM)=0.0d0
      end if
   end do
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BC1


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
iter=1000 
tol=EPSLON !1.e-14
iprint=6

allocate(AGMG_A(no_AGMG_A),AGMG_JA(no_AGMG_A),AGMG_IA(no_node+1))
AGMG_A=0.0d0
AGMG_JA=0
AGMG_IA=0

cal_NB=0
do i=1,no_node
   chech_ind=0
   do j=1,NLRN(i)
      ! if (dabs(SK_2D(i,j))>EPSLON) then
         cal_NB=cal_NB+1
         AGMG_A(cal_NB)=SK_2D(i,j)
         AGMG_JA(cal_NB)=ture_LRN(i,j)
         if (chech_ind==0) then
            AGMG_IA(i)=cal_NB
            chech_ind=1
         end if
      ! end if
   end do
end do

AGMG_IA(no_node+1)=cal_NB+1

write(*,*) 'finish cal stiffness matrix'

! call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,F,RES_U,0,iprint,1,iter,tol)
call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,F,RES_U,0,iprint,2,iter,tol)
!open(550,file='RES_U.dat',form="binary")
!read(550) (RES_U(i),i=1,no_node)
!close(550)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! steaty state flow finish

!!!!!!!!!!!!!!!!!!!!!!!!!!! set fracture domain NB of node
no_fracture_node=0
allocate(node_check(no_node),new_2d_ele_ind(no_fracture_ele,3))
node_check=0
do NI=1,no_fracture_ele
    do j=1,3
        node_check(face_ind(NI,j))=1
    end do
end do
do i=1,no_node
    if (node_check(i)/=0) then
        no_fracture_node=no_fracture_node+1
        node_check(i)=no_fracture_node
    end if
end do
allocate(xn3(no_fracture_node),yn3(no_fracture_node),zn3(no_fracture_node))
allocate(xn2(no_fracture_node),yn2(no_fracture_node))
allocate(vxn2(no_fracture_node),vyn2(no_fracture_node))
allocate(return_OR_nodeNB(no_fracture_node))

do i=1,no_node
    if (node_check(i)/=0) then
    return_OR_nodeNB(node_check(i))=i
    end if
end do
    
do i=1,no_fracture_ele
    do j=1,3
        new_2d_ele_ind(i,j)=node_check(face_ind(i,j))
    end do
end do
i=0
!!!!!!!!!!!!!!!!!!!!!!!!!!! set fracture domain NB of node
!write(*,*) 'max_no_nei=',max_no_nei
!open(10,file='FEM_AGMG_max_no_nei.dat')
!write(10,*) '0,1,2,3-D element, node, max_no_nei'
!write(10,*) '0,0,',no_fracture_node,no_node,no_node,max_no_nei
!close(10)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!calculate Darcy's velocity (I-Hsien LEE, 20170617)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! creat fractured stiffness matrix
deallocate(F)
allocate(vx(no_node),vy(no_node),vz(no_node))
allocate(Fx(no_node),Fy(no_node),Fz(no_node))

SK_2D=0.0d0
Fx=0.0d0
Fy=0.0d0
Fz=0.0d0
vx=0.0d0
vy=0.0d0
vz=0.0d0

DATA LL1/1.0D0, 0.0D0, 0.0D0, 0.0d0/
DATA LL2/0.0D0, 1.0D0, 0.0D0, 0.0d0/
DATA LL3/0.0D0, 0.0D0, 1.0D0, 0.0d0/
!open(3001,file='2D_coordinate_eleind.dat',form="binary")
open(30,file='unit_vector.dat')
! allocate(unit_vx1(no_face_type),unit_vy1(no_face_type),unit_vz1(no_face_type))
! allocate(unit_vx2(no_face_type),unit_vy2(no_face_type),unit_vz2(no_face_type))
allocate(unit_vx1(no_fracture),unit_vy1(no_fracture),unit_vz1(no_fracture))
allocate(unit_vx2(no_fracture),unit_vy2(no_fracture),unit_vz2(no_fracture))
write(*,*) 'allocate(unit_vx1(no_face_type),unit_vy1(no_face_type),unit_vz1(no_face_type))'
write(*,"(A,I0)") 'no_face_type= ',no_face_type
write(*,"(A,I0)") 'no_fracture= ',no_fracture
do i=1,no_fracture
    read(30,*) k,unit_vx1(k),unit_vy1(k),unit_vz1(k)
    read(30,*) k,unit_vx2(k),unit_vy2(k),unit_vz2(k)
   !  write(*,*) 'fracure #i=',i
end do
close(30)
write(*,*) 'read unit vxyz OK'

open(3001,file='2D_coordinate_eleind.dat')
do NI=1,no_fracture_ele
    do i=1,no_2D_node
        read(3001,*) x2(i),y2(i)
    end do
    do i=1,no_2D_ele
        read(3001,*) (ele_2D_ind(j),j=1,3)
    end do
    do i=1,no_2D_ele
        read(3001,*) (global_ele_2D_ind(j),j=1,3)
    end do   
    !x(165)+x2(2)*unit_vx1(face_ID(NI))+y2(2)*unit_vx2(face_ID(NI))
    do M=1,no_2D_ele
        SATKX =Kfracture(NI)
        SATKY =Kfracture(NI)
        SATKZ =Kfracture(NI)
        SATKXY=0.0d0
        SATKXZ=0.0d0
        SATKYZ=0.0d0
        WCSS=0.4d0
        WCRR=0.0d0
        THETSE=poro_fracture(M) !WCSS-WCRR
        
        do IQ=1,tri_NODE
            XE(IQ)=x2(ele_2D_ind(IQ))
            YE(IQ)=y2(ele_2D_ind(IQ))
            NP=global_ele_2D_ind(IQ)
            HQ(IQ)=RES_U(NP)
        end do
        call GRADI3(XE,YE,DN,AREA)
        
        do kg=1,tri_NODE
            DNX3(kg)=DN(kg,1)*unit_vx1(face_ID(NI))+DN(kg,2)*unit_vx2(face_ID(NI))
            DNY3(kg)=DN(kg,1)*unit_vy1(face_ID(NI))+DN(kg,2)*unit_vy2(face_ID(NI))
            DNZ3(kg)=DN(kg,1)*unit_vz1(face_ID(NI))+DN(kg,2)*unit_vz2(face_ID(NI))
        end do
        
        do kg=1,tri_NODE
            MI=global_ele_2D_ind(kg)
            do JQ=1,tri_NODE
                NJ=global_ele_2D_ind(JQ)
                do IEM=1,NLRN(MI)
                    if (NJ==ture_LRN(MI,IEM)) then
                        goto 577
                    end if
                end do
577             if (kg==JQ) then
                    SK_2D(MI,IEM) &
                        & = SK_2D(MI,IEM) &
                        & + (1.0d0/6.0d0)*AREA
                else
                    SK_2D(MI,IEM) &
                        & = SK_2D(MI,IEM) &
                        & + (1.0d0/12.0d0)*AREA
                end if
            end do  
        end do
        DO kg=1,tri_NODE
            DO JQ=1,tri_NODE
                FX(global_ele_2D_ind(kg))=FX(global_ele_2D_ind(kg))-(SATKX *DNX3(JQ) +SATKXY*DNY3(JQ) +SATKXZ*DNZ3(JQ))*(1.0d0/3.0d0)*AREA*HQ(JQ)
                FY(global_ele_2D_ind(kg))=FY(global_ele_2D_ind(kg))-(SATKXY*DNX3(JQ) +SATKY *DNY3(JQ) +SATKYZ*DNZ3(JQ))*(1.0d0/3.0d0)*AREA*HQ(JQ)
                FZ(global_ele_2D_ind(kg))=FZ(global_ele_2D_ind(kg))-(SATKXZ*DNX3(JQ) +SATKYZ*DNY3(JQ) +SATKZ *DNZ3(JQ))*(1.0d0/3.0d0)*AREA*HQ(JQ)
            END DO
        END DO
    end do
end do
!close(3001,status='delete')
close(3001)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! creat fractured stiffness matrix


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! creat matrix stiffness matrix
LL1(1)=1.0D0
LL1(2)=0.0D0
LL1(3)=0.0D0
LL1(4)=0.0D0
LL2(1)=0.0D0
LL2(2)=1.0D0
LL2(3)=0.0D0
LL2(4)=0.0D0
LL3(1)=0.0D0
LL3(2)=0.0D0
LL3(3)=1.0D0
LL3(4)=0.0D0
LL4(1)=0.0D0
LL4(2)=0.0D0
LL4(3)=0.0D0
LL4(4)=1.0D0

do NE=1,no_ele
   do i=1,node
      np=ele_ind(NE,i) 
      HQ(i)=RES_U(np)
      xq(i)=x(np)
      yq(i)=y(np)
      zq(i)=z(np)
   end do

   do i=1,node
      D1=LL1(i)
      D2=LL2(i)
      D3=LL3(i)
      D4=LL4(i)

      DJAC=0.0d0
      do kk=1,4
         IF (KK.EQ.1)THEN
            K1=2
            K2=3
            K3=4
         ELSEIF(KK.EQ.2)THEN
            K1=1
            K2=3
            K3=4
         ELSEIF(KK.EQ.3)THEN
            K1=1
            K2=2
            K3=4
         ELSE
            K1=1
            K2=2
            K3=3
         ENDIF
         A(KK)=(-1.0D0)**(KK+1)*(XQ(K1)*YQ(K2)*ZQ(K3)+&
             & YQ(K1)*ZQ(K2)*XQ(K3)+ZQ(K1)*XQ(K2)*YQ(K3)-&
             & XQ(K3)*YQ(K2)*ZQ(K1)-YQ(K3)*ZQ(K2)*XQ(K1)-&
             & ZQ(K3)*XQ(K2)*YQ(K1))
         B(KK)=(-1.0D0)**KK*(YQ(K1)*ZQ(K2)+&
             & YQ(K2)*ZQ(K3)+YQ(K3)*ZQ(K1)-&
             & YQ(K3)*ZQ(K2)-YQ(K2)*ZQ(K1)-&
             & YQ(K1)*ZQ(K3))
         C(KK)=(-1.0D0)**(KK+1)*(XQ(K1)*ZQ(K2)+&
             & XQ(K2)*ZQ(K3)+XQ(K3)*ZQ(K1)-&
             & XQ(K3)*ZQ(K2)-XQ(K2)*ZQ(K1)-&
             & XQ(K1)*ZQ(K3))
         D(KK)=(-1.0D0)**KK*(XQ(K1)*YQ(K2)+&
             & XQ(K2)*YQ(K3)+XQ(K3)*YQ(K1)-&
             & XQ(K3)*YQ(K2)-XQ(K2)*YQ(K1)-&
             & XQ(K1)*YQ(K3))
         DJAC=DJAC+A(KK)
      end do
      N(1)=D1
      N(2)=D2
      N(3)=D3
      N(4)=D4
      do KK=1,4
         DNX(KK)=B(KK)/DJAC
         DNY(KK)=C(KK)/DJAC
         DNZ(KK)=D(KK)/DJAC
      end do
	  DJAC=DABS(DJAC)/6.0d0
      DJAC=DJAC*0.25d0

      ! SATKX =KrockX !KrockX(NE)
      ! SATKY =KrockY !KrockY(NE)
      ! SATKZ =KrockZ !KrockZ(NE)
      SATKX =KrockX(NE)
      SATKY =KrockY(NE)
      SATKZ =KrockZ(NE)
      SATKXY=0.0d0
      SATKXZ=0.0d0
      SATKYZ=0.0d0

      ! THETSE=poro_rock !poro_rock(NE) !WCSS-WCRR
      THETSE=poro_rock(NE) !WCSS-WCRR

      AKXQP=SATKX
      AKYQP=SATKY
      AKZQP=SATKZ
      AKXYQP=SATKXY
      AKXZQP=SATKXZ
      AKYZQP=SATKYZ

      QRX=0.0d0
	  QRY=0.0d0
	  QRZ=0.0d0
      QB=0.0d0

      DO IQ=1,NODE
         NI=ele_ind(NE,IQ)
         DO JQ=1,NODE
            NJ=ele_ind(NE,JQ)
			do IEM=1,NLRN(NI)
               if (NJ==ture_LRN(NI,IEM)) then
                  goto 578
               end if
            end do
      578   SK_2D(NI,IEM)=SK_2D(NI,IEM) + N(IQ)*N(JQ)*DJAC
            FX(ele_ind(NE,IQ))=FX(ele_ind(NE,IQ))-N(IQ)*(HQ(JQ)*(AKXQP*DNX(JQ)+AKXYQP*DNY(JQ)+AKXZQP*DNZ(JQ)))*DJAC
            FY(ele_ind(NE,IQ))=FY(ele_ind(NE,IQ))-N(IQ)*(HQ(JQ)*(AKXYQP*DNX(JQ)+AKYQP*DNY(JQ)+AKYZQP*DNZ(JQ)))*DJAC
            FZ(ele_ind(NE,IQ))=FZ(ele_ind(NE,IQ))-N(IQ)*(HQ(JQ)*(AKXZQP*DNX(JQ)+AKYZQP*DNY(JQ)+AKZQP*DNZ(JQ)))*DJAC
         end do
      end do
   end do
end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! creat matrix stiffness matrix


AGMG_A=0.0d0
AGMG_JA=0
AGMG_IA=0

cal_NB=0
do i=1,no_node
   chech_ind=0
   do j=1,NLRN(i)
      ! if (dabs(SK_2D(i,j))>EPSLON) then
         cal_NB=cal_NB+1
         AGMG_A(cal_NB)=SK_2D(i,j)
         AGMG_JA(cal_NB)=ture_LRN(i,j)
         if (chech_ind==0) then
            AGMG_IA(i)=cal_NB
            chech_ind=1
         end if
      ! end if
   end do
end do

AGMG_IA(no_node+1)=cal_NB+1

write(*,*) 'finish cal velocity stiffness matrix'

!!!!!!!!!!!!!!!!!!!!!!!!!!! set fracture domain NB of node
write(*,*) 'max_no_nei=',max_no_nei
open(10,file='FEM_AGMG_max_no_nei.dat')
write(10,*) '0,1,2,3-D element, node, non zero, max_no_nei'
write(10,*) '0,0,',no_fracture_node,no_node,no_node,cal_NB,max_no_nei
close(10)


iter=1000 
call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,FY,vy,0,iprint,1,iter,tol)
write(*,*) 'vy'
iter=1000
call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,FX,vx,0,iprint,1,iter,tol)
write(*,*) 'vx'
iter=1000
call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,FZ,vz,0,iprint,1,iter,tol)
write(*,*) 'vz'
!open(550,file='vxy3D.dat',form="binary")
!read(550) (vx(i),i=1,no_node)
!read(550) (vy(i),i=1,no_node)
!read(550) (vz(i),i=1,no_node)
!close(550)

allocate(vx2D(no_fracture_node),vy2D(no_fracture_node),vz2D(no_fracture_node))
allocate(vx_2Dto3D(no_fracture_node),vy_2Dto3D(no_fracture_node),vz_2Dto3D(no_fracture_node))
open(3003,file='2D_velocity.dat')
NI=0
do NI=1,no_fracture_ele    
    do j=1,3
        NB=new_2d_ele_ind(NI,j)
        NB=new_2d_ele_ind(NI,j)
        vx2D(NB)=vx(return_OR_nodeNB(NB))
        vy2D(NB)=vy(return_OR_nodeNB(NB))
        vz2D(NB)=vz(return_OR_nodeNB(NB))
        
        vx_2Dto3D(NB)=(&
            & EQa1(face_ID(NI))*(org_x(face_ID(NI))+vx2D(NB))+&
            & EQb1(face_ID(NI))*(org_y(face_ID(NI))+vy2D(NB))+&
            & EQc1(face_ID(NI))*(org_z(face_ID(NI))+vz2D(NB))-&
            & EQd1(face_ID(NI)))/&
            &((EQa1(face_ID(NI))*EQa1(face_ID(NI))+EQb1(face_ID(NI))*EQb1(face_ID(NI))+EQc1(face_ID(NI))*EQc1(face_ID(NI)))**0.5d0)
        vy_2Dto3D(NB)=(&
            & EQa2(face_ID(NI))*(org_x(face_ID(NI))+vx2D(NB))+&
            & EQb2(face_ID(NI))*(org_y(face_ID(NI))+vy2D(NB))+&
            & EQc2(face_ID(NI))*(org_z(face_ID(NI))+vz2D(NB))-&
            & EQd2(face_ID(NI)))/&
            &((EQa2(face_ID(NI))*EQa2(face_ID(NI))+EQb2(face_ID(NI))*EQb2(face_ID(NI))+EQc2(face_ID(NI))*EQc2(face_ID(NI)))**0.5d0)
        write(3003,*) vx_2Dto3D(NB),vy_2Dto3D(NB)
    end do
end do
close(3003)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DFN velocity
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cal no_node_neigh
!deallocate(NLRN,ture_LRN)
!
!max_no_nei=4000
!allocate(NLRN(no_fracture_node),LRN(no_fracture_node,max_no_nei))
!write(*,*) 'allocate NLRN and LRN'
!
!NLRN=0
!LRN=0
!
!do M=1,no_fracture_ele
!   !write(*,*) 'M=',M
!   do i=1,tri_NODE
!      IEM=new_2d_ele_ind(M,i)
!      !write(*,*) 'IEM=',IEM
!      do j=1,tri_NODE
!         IEMJ=new_2d_ele_ind(M,j)
!         !write(*,*) 'IEMJ=',IEMJ
!         if (NLRN(IEM)==0) then
!            NLRN(IEM)=NLRN(IEM)+1
!            !write(*,*) 'NLRN(IEM)=',NLRN(IEM)
!            LRN(IEM,1)=IEMJ
!            !write(*,*) 'LRN(IEM,1)=',LRN(IEM,1)
!         else
!            do k=1,NLRN(IEM)
!               Na=LRN(IEM,k)
!               !write(*,*) 'LRN(IEM,k)=',LRN(IEM,k)
!               if (Na==IEMJ) goto 101
!            end do
!            NLRN(IEM)=NLRN(IEM)+1
!            !write(*,*) 'type 2 NLRN(IEM)=',NLRN(IEM)
!            NLRNN=NLRN(IEM)
!            !write(*,*) 'NLRNN=',NLRNN
!            LRN(IEM,NLRNN)=IEMJ
!            !write(*,*) 'LRN(IEM,NLRNN)=',LRN(IEM,NLRNN)
!         end if
!  101 end do
!  end do
!end do
!write(*,*) 'creat LRN'
!
!max_no_nei=0
!no_AGMG_A=0
!do i=1,no_fracture_node
!   if (max_no_nei<NLRN(i)) max_no_nei=NLRN(i)
!   no_AGMG_A=no_AGMG_A+NLRN(i)
!end do
!allocate(ture_LRN(no_fracture_node,max_no_nei))
!ture_LRN=0
!write(*,*) 'max_no_nei=',max_no_nei
!
!
!do i=1,no_fracture_node
!   allocate(rank_NB(NLRN(i)),rank_ind(NLRN(i)))
!   do j=1,NLRN(i)
!      rank_ind(j)=j
!      rank_NB(j)=LRN(i,j)
!   end do
!   call Quick_sort_integer(rank_NB,rank_ind,NLRN(i))
!   do j=1,NLRN(i)
!      ture_LRN(i,j)=rank_NB(j)
!   end do
!   deallocate(rank_NB,rank_ind)
!end do
!deallocate(LRN)
!
!
!deallocate(SK_2D,Fx,Fy)
!allocate(SK_2D(no_fracture_node,max_no_nei))
!allocate(Fx(no_fracture_node),Fy(no_fracture_node))
allocate(fracture_x(no_fracture_node),fracture_y(no_fracture_node),fracture_z(no_fracture_node),fracture_h(no_fracture_node))
!
!SK_2D=0.0d0
!Fx=0.0d0
!Fy=0.0d0
!vx2D=0.0d0
!vy2D=0.0d0
!vz2D=0.0d0
!
!fracture_x=0.0d0
!fracture_y=0.0d0
!fracture_z=0.0d0
!fracture_h=0.0d0
!
!no_2D_node=3
!no_2D_ele=1
!
!
!
do NI=1,no_fracture_ele
    no_2D_ele=1
    do j=1,tri_NODE
        global_ele_2D_ind(j)=face_ind(Ni,j)
        ele_2D_ind(j)=j
    end do
    
    no_2D_node=3
    do j=1,tri_NODE
        x2(j)=x(face_ind(Ni,j))
        y2(j)=y(face_ind(Ni,j))
        z2(j)=z(face_ind(Ni,j))
    end do
!    
    if (fracture_1st_ele_check(face_ID(NI))==0) then
        call plane_EQ(x2(1),y2(1),z2(1),x2(2),y2(2),z2(2),x2(3),y2(3),z2(3),Nx(face_ID(NI)),Ny(face_ID(NI)),Nz(face_ID(NI)),Nd(face_ID(NI)))
        !write(45,"(I,1x,4(E,1x))") face_ID(NI),Nx(face_ID(NI)),Ny(face_ID(NI)),Nz(face_ID(NI)),Nd(face_ID(NI))
        fracture_1st_ele_check(face_ID(NI))=1
        do i=1,1
            do j=1,tri_NODE
                tx(j)=x2(j)
                ty(j)=y2(j)
                tz(j)=z2(j)            
            end do
        end do
        org_x(face_ID(NI))=tx(1)
        org_y(face_ID(NI))=ty(1)
        org_z(face_ID(NI))=tz(1)
        
        mx=(tx(1)+tx(2))/2.0d0
        my=(ty(1)+ty(2))/2.0d0
        mz=(tz(1)+tz(2))/2.0d0
        call Circumcenter_new(tx(1),ty(1),tz(1),tx(2),ty(2),tz(2),tx(3),ty(3),tz(3),ox,oy,oz)
        
        a(1)=tx(2)-tx(1)
        b(1)=ty(2)-ty(1)
        c(1)=tz(2)-tz(1)
        d(1)=a(1)*tx(1)+b(1)*ty(1)+c(1)*tz(1)
        a(2)=mx-ox
        b(2)=my-oy
        c(2)=mz-oz
        d(2)=a(2)*tx(1)+b(2)*ty(1)+c(2)*tz(1)
        if ((a(2)*a(2)+b(2)*b(2)+c(2)*c(2))<=EPSLON) then          
            mx=(tx(1)+tx(3))/2.0d0
            my=(ty(1)+ty(3))/2.0d0
            mz=(tz(1)+tz(3))/2.0d0
            
            a(1)=tx(3)-tx(1)
            b(1)=ty(3)-ty(1)
            c(1)=tz(3)-tz(1)
            d(1)=a(1)*tx(1)+b(1)*ty(1)+c(1)*tz(1)
            
            a(2)=mx-ox
            b(2)=my-oy
            c(2)=mz-oz
            d(2)=a(2)*tx(1)+b(2)*ty(1)+c(2)*tz(1)
        end if
        
        do i=1,2
            unit_vector=(a(i)*a(i)+b(i)*b(i)+c(i)*c(i))**0.5d0
            !write(30,"(I,1x,4(E,1x))") face_ID(NI),a(i)/unit_vector,b(i)/unit_vector,c(i)/unit_vector,d(i)/unit_vector
        end do
        
        EQa1(face_ID(NI))=a(1)
        EQb1(face_ID(NI))=b(1)
        EQc1(face_ID(NI))=c(1)
        EQd1(face_ID(NI))=d(1)
        EQa2(face_ID(NI))=a(2)
        EQb2(face_ID(NI))=b(2)
        EQc2(face_ID(NI))=c(2)
        EQd2(face_ID(NI))=d(2)
    end if
    
    do i=1,no_2D_node
        temp_x=(EQa1(face_ID(NI))*x2(i)+EQb1(face_ID(NI))*y2(i)+EQc1(face_ID(NI))*z2(i)-EQd1(face_ID(NI)))/((EQa1(face_ID(NI))*EQa1(face_ID(NI))+EQb1(face_ID(NI))*EQb1(face_ID(NI))+EQc1(face_ID(NI))*EQc1(face_ID(NI)))**0.5d0)
        temp_y=(EQa2(face_ID(NI))*x2(i)+EQb2(face_ID(NI))*y2(i)+EQc2(face_ID(NI))*z2(i)-EQd2(face_ID(NI)))/((EQa2(face_ID(NI))*EQa2(face_ID(NI))+EQb2(face_ID(NI))*EQb2(face_ID(NI))+EQc2(face_ID(NI))*EQc2(face_ID(NI)))**0.5d0)
        x2(i)=temp_x
        y2(i)=temp_y
    end do
    
    call determinant_value(x2(ele_2D_ind(1)),y2(ele_2D_ind(1)),&
        & x2(ele_2D_ind(2)),y2(ele_2D_ind(2)),&
        & x2(ele_2D_ind(3)),y2(ele_2D_ind(3)),unit_value)
    if (unit_value<EPSLON) then
        k=ele_2D_ind(2)
        ele_2D_ind(2)=ele_2D_ind(3)
        ele_2D_ind(3)=k
        k=global_ele_2D_ind(2)
        face_ind(Ni,2)=face_ind(Ni,3)
        global_ele_2D_ind(2)=global_ele_2D_ind(3)
        face_ind(Ni,3)=k
        global_ele_2D_ind(3)=k       
        unit_value=-unit_value
    end if
    
    do i=1,3
        fracture_x(node_check(global_ele_2D_ind(i)))=x(face_ind(Ni,i))
        fracture_y(node_check(global_ele_2D_ind(i)))=y(face_ind(Ni,i))
        fracture_z(node_check(global_ele_2D_ind(i)))=z(face_ind(Ni,i))
        fracture_h(node_check(global_ele_2D_ind(i)))=RES_U(global_ele_2D_ind(i))
    end do
!
!   SATKX =Kfracture(NI)
!   SATKY =Kfracture(NI)
!   SATKXY=0.0d0
!   
!   do IQ=1,tri_NODE
!       XE(IQ)=x2(ele_2D_ind(IQ))
!       YE(IQ)=y2(ele_2D_ind(IQ))
!       NP=global_ele_2D_ind(IQ)
!       HQ(IQ)=RES_U(NP)
!   end do
!   call GRADI3(XE,YE,DN,AREA)
!   
!   do kg=1,tri_NODE
!       NII=new_2d_ele_ind(NI,kg)
!       do JQ=1,tri_NODE
!           NJJ=new_2d_ele_ind(NI,JQ)
!           do IEM=1,NLRN(NII)
!               if (NJJ==ture_LRN(NII,IEM)) then
!                   goto 710
!               end if
!           end do
!710        if (kg==JQ) then
!               SK_2D(NII,IEM)=SK_2D(NII,IEM)-(1.0d0/6.0d0)*AREA
!           else
!               SK_2D(NII,IEM)=SK_2D(NII,IEM)-(1.0d0/12.0d0)*AREA
!           end if
!           
!           Fx(NII)=Fx(NII)-(SATKX*DN(JQ,1)+SATKXY*DN(JQ,2))*(1.0d0/3.0d0)*AREA*HQ(JQ)
!           Fy(NII)=Fy(NII)-(SATKXY*DN(JQ,1)+SATKY*DN(JQ,2))*(1.0d0/3.0d0)*AREA*HQ(JQ) 
!           
!           I=0
!       end do
!   end do     
end do
!
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!tol=EPSLON !1.e-14
!iprint=6
!
!deallocate(AGMG_A,AGMG_JA,AGMG_IA)
!
!allocate(AGMG_A(no_AGMG_A),AGMG_JA(no_AGMG_A),AGMG_IA(no_fracture_node+1))
!AGMG_A=0.0d0
!AGMG_JA=0
!AGMG_IA=0
!
!cal_NB=0
!do i=1,no_fracture_node
!   chech_ind=0
!   do j=1,NLRN(i)
!      if (dabs(SK_2D(i,j))>EPSLON) then
!         cal_NB=cal_NB+1
!         AGMG_A(cal_NB)=SK_2D(i,j)
!         AGMG_JA(cal_NB)=ture_LRN(i,j)
!         if (chech_ind==0) then
!            AGMG_IA(i)=cal_NB
!            chech_ind=1
!         end if
!      end if
!   end do
!end do
!
!AGMG_IA(no_fracture_node+1)=cal_NB+1
!
!write(*,*) 'finish cal stiffness matrix'
!
!iter=500
!!call dagmg(no_fracture_node,AGMG_A,AGMG_JA,AGMG_IA,-Fx,vx2D,0,iprint,1,iter,tol)
!write(*,*) 'DFN vx'
!iter=500
!!call dagmg(no_fracture_node,AGMG_A,AGMG_JA,AGMG_IA,-Fy,vy2D,0,iprint,1,iter,tol)
!write(*,*) 'DFN vy'
!write(*,*) 'finish DFN velocity AGMG'
!!open(550,file='vxy2D.dat',form="binary")
!!read(550) (vx2D(i),i=1,no_fracture_node)
!!read(550) (vy2D(i),i=1,no_fracture_node)
!close(550)

!open(50,file='tecplot_2D_corr_vel.dat')
!write(50,"(A17)") 'Title="XY2D_plot"'
!WRITE(50,"(A85)") 'VARIABLES="x(m)","y(m)","z(m)","HT(m)","vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
!WRITE(50,*) "ZONE N=",no_fracture_node,",E=",no_fracture_ele,", F=FEpoint, ET=triangle"
!do i=1,no_fracture_ele
!    do j=1,3
!        vx_2Dto3D(new_2d_ele_ind(i,j))=vx2D(new_2d_ele_ind(i,j))*unit_vx1(face_ID(i))+vy2D(new_2d_ele_ind(i,j))*unit_vx2(face_ID(i))
!        vy_2Dto3D(new_2d_ele_ind(i,j))=vx2D(new_2d_ele_ind(i,j))*unit_vy1(face_ID(i))+vy2D(new_2d_ele_ind(i,j))*unit_vy2(face_ID(i))
!        vz_2Dto3D(new_2d_ele_ind(i,j))=vx2D(new_2d_ele_ind(i,j))*unit_vz1(face_ID(i))+vy2D(new_2d_ele_ind(i,j))*unit_vz2(face_ID(i))
!    end do
!end do

!do i=1,no_fracture_node
!    temp=vx_2Dto3D(i)**2.0d0+vy_2Dto3D(i)**2.0d0+vz_2Dto3D(i)**2.0d0
!    write(50,"(8(G0,1x))") fracture_x(i),fracture_y(i),fracture_z(i),fracture_h(i),vx_2Dto3D(i),vy_2Dto3D(i),vz_2Dto3D(i),temp
!end do
!do i=1,no_fracture_ele
!    write(50,"(3(I,1x))") (new_2d_ele_ind(i,j),j=1,3)
!    do j=1,3
!        write(3003,*) vx2D(new_2d_ele_ind(i,j)),vy2D(new_2d_ele_ind(i,j))
!    end do
!end do
!close(50)
!close(3003)



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DFN velocity
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!





!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!open(3001,file='2D_coordinate_eleind.dat',form="binary")
open(3001,file='2D_coordinate_eleind.dat')
!open(3002,file='2D_planeEQ.dat',form="binary")
!open(3003,file='2D_velocity.dat',form="binary")
open(3003,file='2D_velocity.dat')

do NI=1,no_fracture_ele
   !do i=1,no_2D_node
   !   !read(3001) x2(i),y2(i)
   !   read(3001,*) x2(i),y2(i)
   !end do
   !do i=1,no_2D_ele
   !   !read(3001) (ele_2D_ind(j),j=1,3)
   !   read(3001,*) (ele_2D_ind(j),j=1,3)
   !end do
   !do i=1,no_2D_ele
   !   !read(3001) (global_ele_2D_ind(j),j=1,3)
   !   read(3001,*) (global_ele_2D_ind(j),j=1,3)
   !   !read(3002) a(1),b(1),c(1),d(1)
   !   !read(3002) a(2),b(2),c(2),d(2)
   !end do

   !do j=1,tri_NODE
   !vx2(j)=org_x(face_ID(NI)) +vx(global_ele_2D_ind(j))
   !vy2(j)=org_y(face_ID(NI)) +vy(global_ele_2D_ind(j))
   !vz2(j)=org_z(face_ID(NI)) +vz(global_ele_2D_ind(j))
   !end do

   !do i=1,no_2D_node
   !    call plot_point_3dplane(vx2(i),vy2(i),vz2(i),Nx(face_ID(NI)),Ny(face_ID(NI)),Nz(face_ID(NI)),Nd(face_ID(NI)),vpx,vpy,vpz)
   !    temp_x=(EQa1(face_ID(NI))*vpx+EQb1(face_ID(NI))*vpy+EQc1(face_ID(NI))*vpz-EQd1(face_ID(NI)))/((EQa1(face_ID(NI))*EQa1(face_ID(NI))+EQb1(face_ID(NI))*EQb1(face_ID(NI))+EQc1(face_ID(NI))*EQc1(face_ID(NI)))**0.5d0)
   !    temp_y=(EQa2(face_ID(NI))*vpx+EQb2(face_ID(NI))*vpy+EQc2(face_ID(NI))*vpz-EQd2(face_ID(NI)))/((EQa2(face_ID(NI))*EQa2(face_ID(NI))+EQb2(face_ID(NI))*EQb2(face_ID(NI))+EQc2(face_ID(NI))*EQc2(face_ID(NI)))**0.5d0)
   !    !temp_x=(a(1)*vx2(i)+b(1)*vy2(i)+c(1)*vz2(i)-d(1))/((a(1)*a(1)+b(1)*b(1)+c(1)*c(1))**0.5d0)
   !    !temp_y=(a(2)*vx2(i)+b(2)*vy2(i)+c(2)*vz2(i)-d(2))/((a(2)*a(2)+b(2)*b(2)+c(2)*c(2))**0.5d0)
   !    vx2(i)=-temp_x
   !    vy2(i)=-temp_y
   !end do
   do i=1,no_2D_node
      !write(3003,*) vx2(i),vy2(i)
      !vx(global_ele_2D_ind(i))=vx2(ele_2D_ind(i))
      !vy(global_ele_2D_ind(i))=vy2(ele_2D_ind(i))
      !x(global_ele_2D_ind(i))=x2(ele_2D_ind(i))
      !y(global_ele_2D_ind(i))=y2(ele_2D_ind(i))
      xn3(new_2d_ele_ind(NI,i))=x(global_ele_2D_ind(i))
      yn3(new_2d_ele_ind(NI,i))=y(global_ele_2D_ind(i))
      zn3(new_2d_ele_ind(NI,i))=z(global_ele_2D_ind(i))
      !xn2(new_2d_ele_ind(NI,i))=x2(ele_2D_ind(i))
      !yn2(new_2d_ele_ind(NI,i))=y2(ele_2D_ind(i))
      !vxn2(new_2d_ele_ind(NI,i))=vx2(ele_2D_ind(i))
      !vyn2(new_2d_ele_ind(NI,i))=vy2(ele_2D_ind(i))
   end do
end do

close(3001)
!close(3002)
close(3003)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


open(99,file='TWOD_PLANT_VELOCITY.DAT')
write(99,"(A17)") 'Title="XY2D_plot"'
write(99,"(A51)") 'VARIABLES="X(m)", "Y(m)","H(m)","VX(m/d)","VY(m/d)"'
write(99,*) 'ZONE N=',no_fracture_node,',E=',no_fracture_ele,', F=FEpoint, ET=triangle'
do i=1,no_fracture_node
    write(99,"(5(E,1x))") fracture_x(i),fracture_y(i),fracture_h(i),vx2D(i),vy2D(i)
end do
do i=1,no_fracture_ele
    write(99,"(3(I,1x))") (new_2d_ele_ind(i,j),j=1,3)
end do
close(99)

open(99,file='FEM_3d_input.dat')
write(99,"(A22)") 'no_frac,no_node,no_ele'
write(99,*) no_fracture,no_fracture_node,no_fracture_ele
write(99,"(A20)") '(COORDINATES: NODES)'
do i=1,no_fracture_node
    write(99,"(I,1x,3(E,1x))") i,xn3(i),yn3(i),zn3(i)
end do
write(99,"(A26)") '(CONNECTIVITIES: ELEMENTS)'
do i=1,no_fracture_ele
    write(99,"(I,1x,E,1x,3(I,1x))") i,Kfracture(i),(new_2d_ele_ind(i,j),j=1,3)
end do
close(99)

!open(2901,file='mesh_plot.dat',form="binary")
open(2901,file='mesh_plot.dat')
write(2901,*) 'Title="XY2D_plot"'
WRITE(2901,"(A85)") 'VARIABLES="x(m)","y(m)","z(m)","HT(m)","vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
!WRITE(2901,"(A38)") 'VARIABLES="x(m)","y(m)","z(m)","HT(m)"' !,"vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
WRITE(2901,*) "ZONE N=",no_node,",E=",no_ele,", F=FEpoint, ET=TETRAHEDRON"
do i=1,no_node
   write(2901,"(8(E,1x))") x(i),y(i),z(i),RES_U(i),vx(i),vy(i),vz(i),(vx(i)*vx(i)+vy(i)*vy(i)+vz(i)*vz(i))**0.5d0
   !write(2901,"(4(E,1x))") x(i),y(i),z(i),RES_U(i) !,vx(i),vy(i),vz(i),(vx(i)*vx(i)+vy(i)*vy(i)+vz(i)*vz(i))**0.5d0
end do
do i=1,no_ele
   write(2901,"(4(I,1x))") (ele_ind(i,j),j=1,NODE)
end do
close(2901)

!open(3011,file='mesh_plot_2D.dat',form="binary")
!write(3011) 'Title="XY2D_plot"'
!WRITE(3011) 'VARIABLES="x(m)","y(m)","z(m)","HT(m)","vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
!WRITE(3011) "ZONE N=",no_node,",E=",no_fracture_ele,", F=FEpoint, ET=triangle"
!do i=1,no_node
!   write(3011) x(i),y(i),z(i),RES_U(i),vx(i),vy(i),vz(i),(vx(i)*vx(i)+vy(i)*vy(i)+vz(i)*vz(i))**0.5d0
!end do
!do i=1,no_fracture_ele
!   write(3011) (face_ind(i,j),j=1,3)
!end do


open(3011,file='mesh_plot_2D.dat')
write(3011,*) 'Title="XY2D_plot"'
WRITE(3011,"(A85)") 'VARIABLES="x(m)","y(m)","z(m)","HT(m)","vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
!WRITE(3011,"(A38)") 'VARIABLES="x(m)","y(m)","z(m)","HT(m)"' !,"vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"'
WRITE(3011,*) "ZONE N=",no_node,",E=",no_fracture_ele,", F=FEpoint, ET=triangle"
do i=1,no_node
   write(3011,"(8(E,1x))") x(i),y(i),z(i),RES_U(i),vx(i),vy(i),vz(i),(vx(i)*vx(i)+vy(i)*vy(i)+vz(i)*vz(i))**0.5d0
   !write(3011,"(4(E,1x))") x(i),y(i),z(i),RES_U(i) !,vx(i),vy(i),vz(i),(vx(i)*vx(i)+vy(i)*vy(i)+vz(i)*vz(i))**0.5d0
end do
do i=1,no_fracture_ele
   write(3011,"(3(I,1x))") (face_ind(i,j),j=1,3)
end do
close(3011)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!calculate Darcy's velocity (I-Hsien LEE, 20170617)

open(100, file='flowTH.bin', form='unformatted')
write(100) RES_U
write(100) vx
write(100) vy
write(100) vz
close(100)
write(*,*) 'finish AGMG'

! open(10,file='FEM_AGMG_MESH.dat',form="binary")
! write(10) no_node,no_ele
! do i=1,no_node
!    write(10) x(i),y(i),z(i)
! end do
! do i=1,no_ele
!    write(10) (ele_ind(i,k),k=1,4)
! end do
! close(10)


! open(10,file='FEM_AGMG_head.dat',form='binary')
! write(10) (RES_U(i),i=1,no_node)
! close(10)


! open(10,file='FEM_AGMG_velocity.dat',form='binary')
! do i=1,no_node
!     write(10) vx(i),vy(i),vz(i)
! end do
! close(10)

! open(10,file='FEM_fracture_ele_HK.dat',form="binary")
! write(10) no_fracture,no_face_type
! write(10) (NB_fracture_TETGEN(i),i=1,no_fracture)
! write(10) no_fracture_ele  !modify IHL 20190104
! NI=0
! do i=1,no_fracture_ele
!    NI=NI+1
!    write(10) (face_ind(NI,j),j=1,tri_NODE),Kfracture(NI),face_ID(NI),poro_fracture(NI)
! end do
! close(10)

! open(66,file='HYDRO_mesh.dat')
! write(66,"(A34)")'DATA SET 7 NODAL POINT COORDINATES'
! write(66,"((I,1x))") no_node
! do i=1,no_node
!    write(66,"(I,1x,A3,1x,3(E,1x),A5)") i,'0,0',x(i),y(i),z(i),'0,0,0'
! end do
! write(66,"(A17)") '0,0,0,0,0,0,0,0,0'
! write(66,"(A29)")'DATA SET 8 ELEMENT INCIDENCES'
! write(66,"((I,1x))") no_ele
! do i=1,no_ele
!    write(66,"(I,1x,A3,1x,4(I,1x),A9)") i,'0,0',(ele_ind(i,j),j=1,NODE),'0,0,0,0,0'
! end do
! write(66,"(A23)") '0,0,0,0,0,0,0,0,0,0,0,0'
! write(66,*)
! write(66,*)
! write(66,*)
! write(66,"(A41)") 'DATA SET 27 Velocity and Moisture Content'
! do i=1,no_node
!     write(66,"(I,1x,A3,1x,(3(E,1x),A5))") i,'0,0',vx(i),vy(i),vz(i),'0,0,0'
!     write(66,"(A17)") '0,0,0,0,0,0,0,0,0'
! end do
! close(66)

end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


subroutine cal_other_VXYZ(no_node,no_AGMG_A,AGMG_A,AGMG_JA,AGMG_IA,F,v,iprint,iter,tol)
INTEGER*4 no_node,no_AGMG_A
REAL*8 AGMG_A(no_AGMG_A)
INTEGER*4 AGMG_JA(no_AGMG_A),AGMG_IA(no_node+1)
REAL*8 F(no_node),v(no_node)
INTEGER*4 iprint,iter
REAL*8 tol

!call dagmg(no_node,AGMG_A,AGMG_JA,AGMG_IA,F,v,0,iprint,1,iter,tol)

RETURN
END




subroutine Circumcenter_new(x1,y1,z1,x2,y2,z2,x3,y3,z3,Cx,Cy,Cz)
integer*4 i,j,k
real*8 x1,x2,x3,y1,y2,y3,z1,z2,z3
real*8 X12,X21,X13,X31,X23,X32
real*8 Y12,Y21,Y13,Y31,Y23,Y32
real*8 Z12,Z21,Z13,Z31,Z23,Z32
real*8 a,b,c
real*8 cx,cy,cz,Cr,crx,cry,crz,dis

X12=x1-x2
X21=x2-x1
X13=x1-x3
X31=x3-x1
X23=x2-x3
X32=x3-x2

Y12=Y1-Y2
Y21=Y2-Y1
Y13=Y1-Y3
Y31=Y3-Y1
Y23=Y2-Y3
Y32=Y3-Y2

Z12=Z1-Z2
Z21=Z2-Z1
Z13=Z1-Z3
Z31=Z3-Z1
Z23=Z2-Z3
Z32=Z3-Z2

a=0.0
b=0.0
c=0.0

call cross_V_new(x12,y12,z12,x23,y23,z23,crx,cry,crz)
call length_new(crx,cry,crz,dis)
dis=(2.0*dis*dis)
a=(x23*x23+y23*y23+z23*z23)*(x12*x13+y12*y13+z12*z13)/dis
b=(x13*x13+y13*y13+z13*z13)*(x21*x23+y21*y23+z21*z23)/dis
c=(x12*x12+y12*y12+z12*z12)*(x31*x32+y31*y32+z31*z32)/dis

cx=a*x1+b*x2+c*x3
cy=a*y1+b*y2+c*y3
cz=a*z1+b*z2+c*z3

!call dis_AB_new(x1,y1,z1,cx,cy,cz,cr)

return
end 

subroutine cross_V_new(x1,y1,z1,x2,y2,z2,x3,y3,z3)
real*8 x1,y1,z1,x2,y2,z2,x3,y3,z3

x3=y1*z2-z1*y2
y3=-x1*z2+z1*x2
z3=x1*y2-y1*x2

return
end 

subroutine length_new(x,y,z,dis)
real*8 x,y,z,dis
dis=(x*x+y*y+z*z)**0.5

return
end 

subroutine cal_concentration_distrbution(x,y,z,cx,cy,cz,varC,c)
real*8 x,y,z,cx,cy,cz,varC,c
c=dexp(((x-cx)*(x-cx)+(y-cy)*(y-cy)+(z-cz)*(z-cz))/(-2.0d0*varC))
return
end 

subroutine determinant_value(OR_a1,OR_a2,OR_b1,OR_b2,OR_c1,OR_c2,V)
real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,V
real*8 OR_a1,OR_a2,OR_a3,OR_b1,OR_b2,OR_b3,OR_c1,OR_c2,OR_c3
real*8 a0,b0,c0
v=0.0d0
v=(OR_a1*OR_b2+OR_b1*OR_c2+OR_c1*OR_a2)-(OR_c1*OR_b2+OR_b1*OR_a2+OR_a1*OR_c2)
return 
end

subroutine determinant_value_new_2d(OR_a1,OR_a2,OR_a3,OR_b1,OR_b2,OR_b3,OR_c1,OR_c2,OR_c3,V)
real*8 a1,a2,a3,b1,b2,b3,c1,c2,c3,V
real*8 OR_a1,OR_a2,OR_a3,OR_b1,OR_b2,OR_b3,OR_c1,OR_c2,OR_c3
real*8 a0,b0,c0
!a1,a2,a3
!b1,b2,b3
!c1,c2,c3
v=0.0
a1=OR_a1
a2=OR_a2
a3=OR_a3
b1=OR_b1
b2=OR_b2
b3=OR_b3
c1=OR_c1
c2=OR_c2
c3=OR_c3

!v=(a1*b2*c3+b1*c2*a3+c1*a2*b3)-(c1*b2*a3+b1*a2*c3+a1*c2*b3)

if (dabs(a3)<1.0D-12 .and. dabs(b3)<1.0D-12 .and. dabs(c3)<1.0D-12) then !�i�H�B�zxy����
   a0=1.0                                              ! z =0.0 
   b0=1.0
   c0=1.0
   v=(a1*b2*c0+b1*c2*a0+c1*a2*b0)-(c1*b2*a0+b1*a2*c0+a1*c2*b0)
else
   v=(a1*b2*c3+b1*c2*a3+c1*a2*b3)-(c1*b2*a3+b1*a2*c3+a1*c2*b3)
end if

return 
end 


subroutine triang_area(x1,y1,x2,y2,x3,y3,area)
real*8 x1,y1,x2,y2,x3,y3,area
area= (x1*y2+x2*y3+x3*y1-x2*y1-x3*y2-x1*y3)
return
    end

    
subroutine plane_EQ(x1,y1,z1,x2,y2,z2,x3,y3,z3,Nx,Ny,Nz,Nd)
!f(x,y,z)= ax+by+cz=d
real*8 x1,y1,z1,x2,y2,z2,x3,y3,z3,Nx,Ny,Nz,Nd
real*8 vx,vy,vz,kx,ky,kz,tn

vx=x2-x1
vy=y2-y1
vz=z2-z1

kx=x3-x1
ky=y3-y1
kz=z3-z1

Nx=+(vy*kz-ky*vz)
Ny=-(vx*kz-kx*vz)
Nz=+(vx*ky-kx*vy)


if (dabs(Nx)>1.0d0) then
   tn=Nx
   Nx=Nx/tn
   Ny=Ny/tn
   Nz=Nz/tn
end if

if (dabs(Ny)>1.0d0) then
   tn=Ny
   Nx=Nx/tn
   Ny=Ny/tn
   Nz=Nz/tn
end if

if (dabs(Nz)>1.0d0) then
   tn=Nz
   Nx=Nx/tn
   Ny=Ny/tn
   Nz=Nz/tn
end if

Nd=Nx*x1+Ny*y1+Nz*z1
return
end
    
    
subroutine plot_point_3dplane(px,py,pz,Nx,Ny,Nz,Nd,x,y,z)
real*8 x,y,z,Nx,Ny,Nz,Nd,px,py,pz
real*8 t

t=(Nd-Nx*px-Ny*py-Nz*pz)/(Nx+Ny+Nz)
x=Nx*t+px
y=Ny*t+py
z=Nz*t+pz

return
end

    
subroutine BC2_R12(&
        & x1,y1,z1,&
        & x2,y2,z2,&
        & x3,y3,z3,&
        & V,KXZ,KYZ,KZ,&
        & F1,F2,F3)

integer*4 i,j,k,KG,IQ
real*8 x1,y1,z1, x2,y2,z2, x3,y3,z3,V
real*8 KXZ,KYZ,KZ
real*8 F1,F2,F3

real*8 DXDDL2,DYDDL2,DZDDL2,DXDDL3,DYDDL3,DZDDL3
real*8 DETX,DETY,DETZ,DET1,DET
real*8 P, L1(3),L2(3),L3(3),N(3),WG(3)
real*8 F1K,F2K,R1Q(3),R2Q(3)
real*8 F2Q,DCOSLB(3)

DATA WG/0.333333333333333D0, 0.333333333333333D0,0.333333333333333D0/

P=1.0D0
L1(1)=1.0D0
L1(2)=0.0D0
L1(3)=0.0D0
L2(1)=0.0D0
L2(2)=1.0D0
L2(3)=0.0D0
L3(1)=0.0D0
L3(2)=0.0D0
L3(3)=1.0D0

call cal_DCOSLB(x1,y1,z1, x2,y2,z2, x3,y3,z3,DCOSLB)

DXDDL2=X2-X1
DYDDL2=Y2-Y1
DZDDL2=Z2-Z1
DXDDL3=X3-X1
DYDDL3=Y3-Y1
DZDDL3=Z3-Z1
DETX=DYDDL2*DZDDL3-DYDDL3*DZDDL2
DETY=DXDDL2*DZDDL3-DXDDL3*DZDDL2
DETZ=DXDDL2*DYDDL3-DXDDL3*DYDDL2
DET1=DSQRT(DETX*DETX+DETY*DETY+DETZ*DETZ)*0.5D0

do KG=1,3
    N(1)=L1(KG)
    N(2)=L2(KG)
    N(3)=L3(KG)
    DET=DET1*WG(KG)
    
    F1K=0.0D0
    F2K=0.0
    F2Q=DCOSLB(1)*KXZ+DCOSLB(2)*KYZ+DCOSLB(3)*KZ
    do IQ=1,3
        F1K=F1K+V*N(IQ)
        F2K=F2K+F2Q*N(IQ)
    end do
    
    do IQ=1,3
          R1Q(IQ)=R1Q(IQ)+N(IQ)*F1K*DET
          R2Q(IQ)=R2Q(IQ)+N(IQ)*F2K*DET
    end do
    
    F1=F1-R1Q(1)+R2Q(1)
    F2=F2-R1Q(2)+R2Q(2)
    F3=F3-R1Q(3)+R2Q(3)
end do


return
end
    
subroutine cal_DCOSLB(x1,y1,z1, x2,y2,z2, x3,y3,z3,DCOSLB)
real*8 x1,y1,z1, x2,y2,z2, x3,y3,z3,DCOSLB(3)
real*8 A1,A2,A3,B1,B2,B3
real*8 AB23,AB31,AB12,AREA

A1=x2-x1
A2=y2-y1
A3=z2-z1
B1=x3-x1
B2=y3-y1
B3=z3-z1

AB23=A2*B3-A3*B2
AB31=A3*B1-A1*B3
AB12=A1*B2-A2*B1
AREA=(AB23*AB23+AB31*AB31+AB12*AB12)**0.5d0
DCOSLB(1)=AB23/AREA
DCOSLB(2)=AB31/AREA
DCOSLB(3)=AB12/AREA

return
end
   
subroutine modify_ele_NB(no_ele,ele_ind,n1,n2,n3)
integer*4 no_ele,i,j,QQ
integer*4 ele_ind(no_ele,4)
integer*4 n1,n2,n3
integer*4 KGB(3,4)
integer*4 ni,nj

KGB(1,1)=4
KGB(2,1)=3
KGB(3,1)=2
KGB(1,2)=4
KGB(2,2)=1
KGB(3,2)=3
KGB(1,3)=4
KGB(2,3)=2
KGB(3,3)=1
KGB(1,4)=1
KGB(2,4)=2
KGB(3,4)=3

QQ=0
do i=1,no_ele
    do j=1,4
    if (  &
        & (ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(3,j))==n3) .or. &
        & (ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(3,j))==n2) .or. &
        & (ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(3,j))==n3) .or. &
        & (ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(3,j))==n1) .or. &
        & (ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(3,j))==n2) .or. &
        & (ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(3,j))==n1) .or. &
        
        & (ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(2,j))==n3) .or. &
        & (ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(2,j))==n2) .or. &
        & (ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(2,j))==n3) .or. &
        & (ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(2,j))==n1) .or. &
        & (ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(2,j))==n2) .or. &
        & (ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(2,j))==n1) .or. &
   
        & (ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(3,j))==n3) .or. &
        & (ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(3,j))==n2) .or. &
        & (ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(3,j))==n3) .or. &
        & (ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(3,j))==n1) .or. &
        & (ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(3,j))==n2) .or. &
        & (ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(3,j))==n1) .or. &
   
        & (ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(1,j))==n3) .or. &
        & (ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(1,j))==n2) .or. &
        & (ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(1,j))==n3) .or. &
        & (ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(1,j))==n1) .or. &
        & (ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(1,j))==n2) .or. &
        & (ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(1,j))==n1) .or. &
   
        & (ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(2,j))==n3) .or. &
        & (ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(1,j))==n3 .and. ele_ind(i,KGB(2,j))==n2) .or. &
        & (ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(2,j))==n3) .or. &
        & (ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(2,j))==n1) .or. &
        & (ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(1,j))==n1 .and. ele_ind(i,KGB(2,j))==n2) .or. &
        & (ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(1,j))==n2 .and. ele_ind(i,KGB(2,j))==n1) .or. &
   
        & (ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(1,j))==n3) .or. &
        & (ele_ind(i,KGB(3,j))==n1 .and. ele_ind(i,KGB(2,j))==n3 .and. ele_ind(i,KGB(1,j))==n2) .or. &
        & (ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(1,j))==n3) .or. &
        & (ele_ind(i,KGB(3,j))==n2 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(1,j))==n1) .or. &
        & (ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(2,j))==n1 .and. ele_ind(i,KGB(1,j))==n2) .or. &
        & (ele_ind(i,KGB(3,j))==n3 .and. ele_ind(i,KGB(2,j))==n2 .and. ele_ind(i,KGB(1,j))==n1)) then
        QQ=1
        ni=i
        nj=j
        goto 110
    end if
    end do
end do

110 if (QQ==1) then
        n1=ele_ind(ni,KGB(1,nj))
        n2=ele_ind(ni,KGB(2,nj))
        n3=ele_ind(ni,KGB(3,nj))
    end if
    
   
return
end
   

    
    
    
    

import os
import json
import calendar
from datetime import date
import pandas as pd
from ctypes import *
from numpy.core.fromnumeric import shape
from pysimgeo.projection import projection
from pysimgeo.ctypes import utils as ctypesUtils
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe
import numpy as np

sim = cdll.LoadLibrary(os.path.dirname(os.path.abspath(__file__)) + "/lib.so")
# print(os.path.dirname(os.path.abspath(__file__)) + "/lib.so")
class Geometry( Structure ):
    pass

class FabReadFor( Structure ):
    pass

def FabFileRead():

    ndpointer = np.ctypeslib.ndpointer

    class FabReadFor( Structure ):
        _fields_ = [
		( "cooX", c_double ),        
		( "cooY", c_double ),
        ( "no_frac", c_int ),        
        ( "frac_no_node", c_int ),        
        ( "frac_no_ele", c_int ),        
        ( "frac_no_sege", c_int ),        
		( "frac_ele_ind", ndpointer(dtype=np.int32,ndim=2)),
		( "fracID", ndpointer(dtype=np.int32,ndim=1)),
        ( "frac_sege", ndpointer(dtype=np.int32,ndim=2)),
		( "FracNXYZd", ndpointer(dtype=np.float64,ndim=2)),
		( "frac_XYZ", ndpointer(dtype=np.float64,ndim=2))
	]

    FabData = FabReadFor()
    FabData.memShapes = []

    # print("test BF")
    null_fds, save = pipe.suppress()
    sim.FAB_reader(byref(FabData))
    pipe.resume(null_fds, save)    
    # print("test AF")
    FabData.memShapes.append({"field_name": "no_frac", "dtype": np.int32, "shape": None })
    FabData.memShapes.append({"field_name": "frac_no_node", "dtype": np.int32, "shape": None })
    FabData.memShapes.append({"field_name": "frac_no_ele", "dtype": np.int32, "shape": None })
    FabData.memShapes.append({"field_name": "frac_no_sege", "dtype": np.int32, "shape": None })
    FabData.memShapes.append({"field_name": "cooX", "dtype": np.float64, "shape": None })
    FabData.memShapes.append({"field_name": "cooY", "dtype": np.float64, "shape": None })
    
    FabData.memShapes.append({"field_name": "frac_ele_ind", "dtype": np.int32, "shape": [FabData.frac_no_ele,3] })
    FabData.memShapes.append({"field_name": "fracID", "dtype": np.int32, "shape": [FabData.frac_no_ele] })
    FabData.memShapes.append({"field_name": "frac_sege", "dtype": np.int32, "shape": [FabData.frac_no_sege,2] })

    FabData.memShapes.append({"field_name": "FracNXYZd", "dtype": np.float64, "shape": [FabData.no_frac,4] })
    FabData.memShapes.append({"field_name": "frac_XYZ", "dtype": np.float64, "shape": [FabData.frac_no_node,3] })

    FabDataDict = utils.getdict(FabData)

    return FabDataDict    


if __name__ == "__main__":
    trimesh = FabMesh()
module FAB
implicit none
contains


subroutine FAB_reader(FABinput_inf) bind(c, name="FAB_reader")
use, intrinsic :: ISO_C_BINDING
implicit none
integer*4 i,j,k,NB,oldNB,ii

type, BIND(C) :: FABinput
    real (C_DOUBLE) cooX,cooY
    integer (C_INT) no_frac,frac_no_node,frac_no_ele,frac_no_sege
    type (C_PTR) frac_ele_ind,fracID,frac_sege
    type (C_PTR) FracNXYZd,frac_XYZ
end type FABinput

real*8 cooTXY(2)
integer*4 no_frac,frac_no_node,frac_no_ele
character(256) fabFile,coo_transFile,word
integer Reason

type (FABinput), intent(out) :: FABinput_inf

integer (C_INT), ALLOCATABLE, target, save :: frac_ele_ind(:,:),fracID(:),frac_sege(:,:)
real (C_DOUBLE), ALLOCATABLE, target, save :: FracNXYZd(:,:),frac_XYZ(:,:)
integer*4 frac_no_sege
integer*4, allocatable:: calPlaneEqCheck(:),nodeCheckPlane(:)
integer*4 e(3)

open(10,file='fabRead_sp.dat')
read(10,"(A256)") fabFile
read(10,"(A256)") coo_transFile
close(10)

if (coo_transFile(1:10)/='          ') then
    open(10,file=coo_transFile)
    do i=1,2
        read(10,"(A256)") word
        read(word(3:256),*) cooTXY(i)
    end do
    close(10)
else
    cooTXY=0.0d0    
end if

open(10,file=fabFile)
do while(.true.)
    read(10,"(A256)",IOSTAT=Reason) word
    if (Reason==-1) exit
    if (word(1:14)=='BEGIN FRACTURE') then
        frac_no_node = 0
        frac_no_ele = 0
        frac_no_sege=0
        do i=1,no_frac
            read(10,*) k,NB
            frac_no_node = frac_no_node + NB
            frac_no_ele = frac_no_ele + NB -2
            do j=1,NB+1
                read(10,*)
            end do
        end do
        frac_no_sege=frac_no_node
        if (ALLOCATED(frac_sege)) DEALLOCATE(frac_sege)
        ALLOCATE(frac_sege(2,frac_no_sege))
        if (ALLOCATED(frac_ele_ind)) DEALLOCATE(frac_ele_ind)
        ALLOCATE(frac_ele_ind(3,frac_no_ele))
        if (ALLOCATED(fracID)) DEALLOCATE(fracID)
        ALLOCATE(fracID(frac_no_ele))
        if (ALLOCATED(calPlaneEqCheck)) DEALLOCATE(calPlaneEqCheck)
        ALLOCATE(calPlaneEqCheck(no_frac))
        if (ALLOCATED(nodeCheckPlane)) DEALLOCATE(nodeCheckPlane)
        ALLOCATE(nodeCheckPlane(frac_no_node))
        if (ALLOCATED(FracNXYZd)) DEALLOCATE(FracNXYZd)
        ALLOCATE(FracNXYZd(4,no_frac))
        if (ALLOCATED(frac_XYZ)) DEALLOCATE(frac_XYZ)
        ALLOCATE(frac_XYZ(3,frac_no_node))        
        FracNXYZd=0.0d0
        frac_sege=0
    end if    
    if (word(1:12)=='BEGIN FORMAT') then
        do while(.true.)
            read(10,"(A256)",IOSTAT=Reason) word
            if (word(1:18)=='    No_Fractures =') then
                read(word(19:256),*) no_frac
                frac_no_node=frac_no_ele*4
            end if
            if (word(1:10)=='END FORMAT') then
                goto 111
            end if
        end do      
    end if
111 i=0
end do
close(10)

open(10,file=fabFile)
do while(.true.)
    read(10,"(A256)",IOSTAT=Reason) word
    if (Reason==-1) exit
    if (word(1:14)=='BEGIN FRACTURE') then
        frac_no_node=0
        frac_no_ele=0
        frac_no_sege=0
        oldNB=-1
        do i=1,no_frac
            read(10,*) k,NB
            do j=1,NB
                frac_no_node=frac_no_node+1
                read(10,*) k,(frac_XYZ(ii,frac_no_node),ii=1,3)
                do ii=1,2
                    frac_XYZ(ii,frac_no_node)=frac_XYZ(ii,frac_no_node)+cooTXY(ii)
                end do
            end do
            do j=1,NB-2
                frac_no_ele=frac_no_ele+1
                fracID(frac_no_ele)=i
                frac_ele_ind(1,frac_no_ele)=oldNB+1
                frac_ele_ind(2,frac_no_ele)=oldNB+1+j
                frac_ele_ind(3,frac_no_ele)=oldNB+1+j+1
            end do       
            do j=1,NB-1
                frac_no_sege=frac_no_sege+1
                frac_sege(1,frac_no_sege)=frac_no_sege-1
                frac_sege(2,frac_no_sege)=frac_no_sege
            end do
            frac_no_sege=frac_no_sege+1
            frac_sege(1,frac_no_sege)=frac_no_sege-1
            frac_sege(2,frac_no_sege)=oldNB+1                
            oldNB=frac_no_node-1
            read(10,*)
        end do
        goto 112
    end if
    j=0
end do
112 close(10)  


open(4000,file='TestFabReader')
calPlaneEqCheck=0
nodeCheckPlane=0
do i = 1, frac_no_ele
    e(1) = frac_ele_ind(1,i)+1
    e(2) = frac_ele_ind(2,i)+1
    e(3) = frac_ele_ind(3,i)+1
    
    if (calPlaneEqCheck(fracID(i))==0) then
        ! write(4000,"(A,I0)") 'i = ',i
        ! write(4000,"(3(G0,1x))") frac_XYZ(1,e(1)),frac_XYZ(2,e(1)),frac_XYZ(3,e(1))
        ! write(4000,"(3(G0,1x))") frac_XYZ(1,e(2)),frac_XYZ(2,e(2)),frac_XYZ(3,e(2))
        ! write(4000,"(3(G0,1x))") frac_XYZ(1,e(3)),frac_XYZ(2,e(3)),frac_XYZ(3,e(3))        
        ! write(4000,"(3(I0,1x))") (frac_ele_ind(j,i),j=1,3)
        call plane_EQ(&
        & frac_XYZ(1,e(1)),frac_XYZ(2,e(1)),frac_XYZ(3,e(1)),&
        & frac_XYZ(1,e(2)),frac_XYZ(2,e(2)),frac_XYZ(3,e(2)),&
        & frac_XYZ(1,e(3)),frac_XYZ(2,e(3)),frac_XYZ(3,e(3)),&
        & FracNXYZd(1,fracID(i)),FracNXYZd(2,fracID(i)),FracNXYZd(3,fracID(i)),FracNXYZd(4,fracID(i)))
        do j=1,3
            nodeCheckPlane(e(j))=1
        end do
        calPlaneEqCheck(fracID(i))=1
    else
        do j=1,3
            if (nodeCheckPlane(e(j))==0)then
                call CheckNodeXYZinPlane(FracNXYZd(1,fracID(i)),FracNXYZd(2,fracID(i)),FracNXYZd(3,fracID(i)),FracNXYZd(4,fracID(i)),frac_XYZ(1,e(j)),frac_XYZ(2,e(j)),frac_XYZ(3,e(j)))
                nodeCheckPlane(e(j))=1
            end if
        end do        
    end if    
end do


FABinput_inf%frac_ele_ind = C_LOC(frac_ele_ind)
FABinput_inf%fracID = C_LOC(fracID)
FABinput_inf%frac_sege = C_LOC(frac_sege)
FABinput_inf%FracNXYZd = C_LOC(FracNXYZd)
FABinput_inf%frac_XYZ = C_LOC(frac_XYZ)
FABinput_inf%cooX=cooTXY(1)
FABinput_inf%cooY=cooTXY(2)
FABinput_inf%no_frac=no_frac
FABinput_inf%frac_no_node=frac_no_node
FABinput_inf%frac_no_ele=frac_no_ele


! do i=1,no_frac
!     write(4000,*) (FracNXYZd(j,i),j=1,4)
! end do
write(4000,*) 'frac_no_sege=',frac_no_sege
do i=1,frac_no_sege
    write(4000,*) (frac_sege(j,i),j=1,2)
end do
close(4000)
return
end subroutine FAB_reader

end module FAB


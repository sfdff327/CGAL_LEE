from ctypes import *
import ctypes
import sys
import os
import numpy as np
from pysimgeo.ctypes import utils
from pysimgeo.pipe import pipe

sim = cdll.LoadLibrary(os.path.dirname(os.path.abspath(__file__)) + "/lib/struct.so")
ndpointer = np.ctypeslib.ndpointer

def main():
	
	x = 2
	y = 3
	z = 4

	class Param( Structure ):
		_fields_ = [( "len_x", c_int ),
				( "len_y", c_int ),
				( "len_z", c_int ),
				( "pi", c_double ),
				( "a", POINTER(c_int)),
				( "b", POINTER(c_int * x)),
				( "c", POINTER(c_int * x * y)),
				( "d", POINTER(c_double)),
				( "e", POINTER(c_double * x)),
				( "f", POINTER(c_double * x * y))]

	class Result( Structure ):
		_fields_ = [( "len_x", c_int ),
				( "len_y", c_int ),
				( "len_z", c_int ),
				( "pi", c_double ),
				( "aa", ndpointer(dtype=np.int32,ndim=1)),
				( "bb", ndpointer(dtype=np.int32,ndim=2)),
				( "cc", ndpointer(dtype=np.int32,ndim=3)),
				( "dd", ndpointer(dtype=np.float64,ndim=1)),
				( "ee", ndpointer(dtype=np.float64,ndim=2)),
				( "ff", ndpointer(dtype=np.float64,ndim=3))]	


	param = Param()	
	param.memShapes = []
	result = Result()
	result.memShapes = []

	paramC = Param()	

	param.len_x = x
	param.memShapes.append({"field_name": "len_x", "dtype": np.int32, "shape": None })
	param.len_y = y
	param.memShapes.append({"field_name": "len_y", "dtype": np.int32, "shape": None })
	param.len_z = z
	param.memShapes.append({"field_name": "len_z", "dtype": np.int32, "shape": None })
	param.pi = 3.14159
	param.memShapes.append({"field_name": "pi", "dtype": np.float64, "shape": None })
	
	param.a = (c_int * x)()	
	param.memShapes.append({"field_name": "a", "dtype": np.int32, "shape": [x] })
	param.b = ( (c_int * x) * y )()
	param.memShapes.append({"field_name": "b", "dtype": np.int32, "shape": [y,x] })
	param.c = ( ( ( (c_int * x) * y ) * z ) )()
	param.memShapes.append({"field_name": "c", "dtype": np.int32, "shape": [z,y,x] })	
	param.d = (c_double * x)()	
	param.memShapes.append({"field_name": "d", "dtype": np.float64, "shape": [x] })
	param.e = ( (c_double * x) * y )()
	param.memShapes.append({"field_name": "e", "dtype": np.float64, "shape": [y,x] })
	param.f = ( ( ( (c_double * x) * y ) * z ) )()
	param.memShapes.append({"field_name": "f", "dtype": np.float64, "shape": [z,y,x] })

	#[x]	
	for i in range(x):
		param.a[i] = 100*(i+1)
		param.d[i] = 100.0*(i+1)

	#[y][x]
	for i in range(x):
		for j in range(y):	
			param.b[j][i] = 10*(i+1) + 100*(j+1)
			param.e[j][i] = 10.0*(i+1) + 100.0*(j+1)

	#[z][y][x]
	for i in range(x):
		for j in range(y):
			for k in range(z):	
				param.c[k][j][i] = 1*(i+1) + 10*(j+1) + 100*(k+1)
				param.f[k][j][i] = 1.0*(i+1) + 10.0*(j+1) + 100.0*(k+1)
	
	paramDict = utils.getdict(param)
	utils.getctypes(paramDict, paramC)

	for i in range(x):
		assert param.a[i] == paramDict["a"][i] and paramDict["a"][i] == paramC.a[i]
		assert param.d[i] == paramDict["d"][i] and paramDict["d"][i] == paramC.d[i]

	for i in range(x):
		for j in range(y):
			assert param.b[j][i] == paramDict["b"][j][i] and paramDict["b"][j][i] == paramC.b[j][i]
			assert param.e[j][i] == paramDict["e"][j][i] and paramDict["e"][j][i] == paramC.e[j][i]

	for i in range(x):
		for j in range(y):
			for k in range(z):
				assert param.c[k][j][i] == paramDict["c"][k][j][i] and paramDict["c"][k][j][i] == paramC.c[k][j][i]
				assert param.f[k][j][i] == paramDict["f"][k][j][i] and paramDict["f"][k][j][i] == paramC.f[k][j][i]

	null_fds, save = pipe.suppress()
	sim.structtest(byref(paramC), byref(result))
	pipe.resume(null_fds, save)


	result.memShapes.append({"field_name": "len_x", "dtype": np.int32, "shape": None })
	result.memShapes.append({"field_name": "len_y", "dtype": np.int32, "shape": None })
	result.memShapes.append({"field_name": "len_z", "dtype": np.int32, "shape": None })
	result.memShapes.append({"field_name": "pi", "dtype": np.float64, "shape": None })
	result.memShapes.append({"field_name": "aa", "dtype": np.int32, "shape": [result.len_x] })
	result.memShapes.append({"field_name": "bb", "dtype": np.int32, "shape": [result.len_y,result.len_x] })
	result.memShapes.append({"field_name": "cc", "dtype": np.int32, "shape": [result.len_z,result.len_y,result.len_x] })	
	result.memShapes.append({"field_name": "dd", "dtype": np.float64, "shape": [result.len_x] })
	result.memShapes.append({"field_name": "ee", "dtype": np.float64, "shape": [result.len_y,result.len_x] })
	result.memShapes.append({"field_name": "ff", "dtype": np.float64, "shape": [result.len_z,result.len_y,result.len_x] })


	resultDict = utils.getdict(result)


	
	for i in range(x):
		assert resultDict["aa"][i] == param.a[i]
		assert resultDict["dd"][i] == param.d[i]

	for i in range(x):
		for j in range(y):
			assert resultDict["bb"][j][i] == param.b[j][i]
			assert resultDict["ee"][j][i] == param.e[j][i]

	for i in range(x):
		for j in range(y):
			for k in range(z):
				assert resultDict["cc"][k][j][i] == param.c[k][j][i]
				assert resultDict["ff"][k][j][i] == param.f[k][j][i]

	assert resultDict["len_x"] == param.len_x
	assert resultDict["len_y"] == param.len_y
	assert resultDict["len_z"] == param.len_z
	assert resultDict["pi"] == param.pi

if __name__ == "__main__":
	result = main()	




import numpy as np
from meshpy.tet import MeshInfo, build,Options
import pyvista as pv
from pyvista import CellType
import meshpy.tet as tet
import os


def check_stlHole(stlpoints,faces,face_markers):

  stlLenPs=len(stlpoints)
  maxCoor=np.max(stlpoints,axis=0)
  minCoor=np.min(stlpoints,axis=0)
  LenCoor=(maxCoor-minCoor)*5
  print(maxCoor)
  print(minCoor)
  print(LenCoor)

  points=stlpoints.tolist()

  points.append([minCoor[0]-LenCoor[0],minCoor[1]-LenCoor[1],minCoor[2]]-LenCoor[2])
  points.append([maxCoor[0]+LenCoor[0],minCoor[1]-LenCoor[1],minCoor[2]]-LenCoor[2])
  points.append([maxCoor[0]+LenCoor[0],maxCoor[1]+LenCoor[1],minCoor[2]]-LenCoor[2])
  points.append([minCoor[0]-LenCoor[0],maxCoor[1]+LenCoor[1],minCoor[2]]-LenCoor[2])
  points.append([minCoor[0]-LenCoor[0],minCoor[1]-LenCoor[1],minCoor[2]]+LenCoor[2])
  points.append([maxCoor[0]+LenCoor[0],minCoor[1]-LenCoor[1],minCoor[2]]+LenCoor[2])
  points.append([maxCoor[0]+LenCoor[0],maxCoor[1]+LenCoor[1],minCoor[2]]+LenCoor[2])
  points.append([minCoor[0]-LenCoor[0],maxCoor[1]+LenCoor[1],minCoor[2]]+LenCoor[2])



  # faces=meshUSG_coarsening.faces.reshape(-1,4)[:,1:4].tolist()
  # face_markers=np.linspace(start=0,stop=meshUSG_coarsening.n_cells,num=meshUSG_coarsening.n_cells, endpoint=False,dtype=np.int64).tolist()
  faces=faces.tolist()
  # faces.append([stlLenPs+0,stlLenPs+3,stlLenPs+2,stlLenPs+1])
  # faces.append([stlLenPs+4,stlLenPs+5,stlLenPs+6,stlLenPs+7])
  # faces.append([stlLenPs+0,stlLenPs+4,stlLenPs+7,stlLenPs+3])
  # faces.append([stlLenPs+1,stlLenPs+2,stlLenPs+6,stlLenPs+5])
  # faces.append([stlLenPs+0,stlLenPs+1,stlLenPs+5,stlLenPs+4])
  # faces.append([stlLenPs+2,stlLenPs+3,stlLenPs+7,stlLenPs+6])  
  faces.append([stlLenPs+0,stlLenPs+3,stlLenPs+2])
  faces.append([stlLenPs+0,stlLenPs+2,stlLenPs+1])
  faces.append([stlLenPs+4,stlLenPs+5,stlLenPs+6])
  faces.append([stlLenPs+4,stlLenPs+6,stlLenPs+7])  
  faces.append([stlLenPs+0,stlLenPs+4,stlLenPs+7])
  faces.append([stlLenPs+0,stlLenPs+7,stlLenPs+3])
  faces.append([stlLenPs+1,stlLenPs+2,stlLenPs+6])
  faces.append([stlLenPs+1,stlLenPs+6,stlLenPs+5])
  faces.append([stlLenPs+0,stlLenPs+1,stlLenPs+5])
  faces.append([stlLenPs+0,stlLenPs+5,stlLenPs+4])
  faces.append([stlLenPs+2,stlLenPs+3,stlLenPs+7])
  faces.append([stlLenPs+2,stlLenPs+7,stlLenPs+6])


  face_markers=face_markers.tolist()
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  face_markers.append(-1)
  # print(points)
  # print(faces)
  # print(face_markers)
  
  # no_ele=len(faces)
  # cells=np.full(shape=(no_ele,4),fill_value=3,dtype=np.int64)
  # celltypes = np.full(no_ele, CellType.TRIANGLE, dtype=np.uint8)
  # cells[:,1:4]=np.array(faces)[:,0:3]
  # mesh = pv.UnstructuredGrid(cells, celltypes, points)
  # mesh['face_markers']=face_markers
  # mesh.save('stl.vtu') 
  
  points=tuple(map(tuple, points))


  mtr=np.zeros(len(points),dtype=np.float64)
  tetgenF='test'
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(len(mtr))+' 1\n')
    for i in range(0,len(mtr)):
      f.write(str(mtr[i])+'\n')
    f.close()  
  
  # print(faces[1262])
  # delFacesIndex=[1261,1263,1891,1894,1981,1988,1986]
  # delFacesIndex=[1261,1653,1891,2204,1894,2207,1981,2208,1988,1986,2201]
  # for index in delFacesIndex:
  #   del faces[index]
  #   del face_markers[index]

  # print(faces[1262])
  # quit()
  # delFacesIndex=[]
  # for i in range(0,len(faces)):
  #   if 268 in faces[i] and 267 in faces[i] and 885 in faces[i]:
  #     print(i)
  #     delFacesIndex.append(i)

    
  # print(delFacesIndex)
  # quit()

  # print('faces=',faces)
  # print('face_markers=',face_markers)

  # print(points)
  # print(len(points))
  print('len(faces)=',len(faces))
  print('len(face_markers)=',len(face_markers))

  # print(mtr)
  mesh_info = MeshInfo()
  mesh_info.set_points(points)
  mesh_info.set_facets(faces,face_markers) 
  mesh_info.load_mtr(tetgenF) 
  switches='pqAmnfMT1e-16'
  switches='pAmnfMT1e-16'
  # switches='d'
  print('refinement switches=',switches)
  mesh = build(mesh_info, options=Options(switches=switches))
  points=np.array(mesh.points)
  elements=np.array(mesh.elements)
  element_attributes=np.array(mesh.element_attributes)
  print(points)
  print(elements)
  no_ele=len(elements)
  cells=np.full(shape=(no_ele,5),fill_value=4,dtype=np.int64)
  celltypes = np.full(no_ele, CellType.TETRA, dtype=np.uint8)
  cells[:,1:5]=elements[:,0:4]
  mesh = pv.UnstructuredGrid(cells, celltypes, points)
  mesh['element_attributes']=element_attributes
  mesh.save('tetgen.vtu')


def setRegions(river3D_Points,Regional3D_Points,RegionalOcean3D_Points,Site3D_Points,surfBotz,MT1CenterPoints,MT2CenterPoints,DisposalTunnelCenterPoints,DisposalHoleCenterPoints):
  regions=[]
  rivertag=3
  regionaltag=0
  regionalOceantag=1
  sittag=2

  MTtag=101
  DTtag=102
  DHtag=103

  river3D_Points=np.array(river3D_Points)
  Regional3D_Points=np.array(Regional3D_Points)
  RegionalOcean3D_Points=np.array(RegionalOcean3D_Points)
  Site3D_Points=np.array(Site3D_Points)
  MT1CenterPoints=np.array(MT1CenterPoints)
  MT2CenterPoints=np.array(MT2CenterPoints)
  DisposalTunnelCenterPoints=np.array(DisposalTunnelCenterPoints)
  DisposalHoleCenterPoints=np.array(DisposalHoleCenterPoints)

  surfBotz=abs(surfBotz)

  # for i in range(0,len(river3D_Points)):
  #   region=[
  #     river3D_Points[i,0],
  #     river3D_Points[i,1],
  #     river3D_Points[i,2]-surfBotz/5.0,
  #     rivertag,500]
  #   regions.append(region)
  #   region=[
  #     river3D_Points[i,0],
  #     river3D_Points[i,1],
  #     river3D_Points[i,2]-surfBotz/2.0-surfBotz,
  #     rivertag+1,500]
  #   regions.append(region)
  '''Regional tag'''
  # for i in range(0,len(Regional3D_Points)):
  #   region=[
  #     Regional3D_Points[i,0],
  #     Regional3D_Points[i,1],
  #     Regional3D_Points[i,2]-surfBotz/5.0,
  #     regionaltag,500]
  #   regions.append(region)
  #   region=[
  #     Regional3D_Points[i,0],
  #     Regional3D_Points[i,1],
  #     Regional3D_Points[i,2]-surfBotz/2.0-surfBotz,
  #     regionaltag+1,500]
  #   regions.append(region)

  '''RegionalOcean tag'''
  # for i in range(0,len(RegionalOcean3D_Points)):
  #   region=[
  #     RegionalOcean3D_Points[i,0],
  #     RegionalOcean3D_Points[i,1],
  #     RegionalOcean3D_Points[i,2]-surfBotz/2.0,
  #     regionalOceantag,500]
  #   regions.append(region)
  #   region=[
  #     RegionalOcean3D_Points[i,0],
  #     RegionalOcean3D_Points[i,1],
  #     RegionalOcean3D_Points[i,2]-surfBotz/2.0-surfBotz,
  #     regionalOceantag+1,500]
  #   regions.append(region)

  '''Site tag'''
  # for i in range(0,len(Site3D_Points)):
  #   region=[
  #     Site3D_Points[i,0],
  #     Site3D_Points[i,1],
  #     Site3D_Points[i,2]-surfBotz/5.0,
  #     sittag,500]
  #   regions.append(region)
  #   region=[
  #     Site3D_Points[i,0],
  #     Site3D_Points[i,1],
  #     Site3D_Points[i,2]-surfBotz/2.0-surfBotz,
  #     sittag+1,500]
  #   regions.append(region)
  
  '''MT DT DH tag'''
  for i in range(0,len(MT1CenterPoints)):
    region=[
      MT1CenterPoints[i,0],
      MT1CenterPoints[i,1],
      MT1CenterPoints[i,2],
      MTtag,500]
    regions.append(region)
  for i in range(0,len(MT2CenterPoints)):
    region=[
      MT2CenterPoints[i,0],
      MT2CenterPoints[i,1],
      MT2CenterPoints[i,2],
      MTtag,500]
    regions.append(region)
  for i in range(0,len(DisposalTunnelCenterPoints)):
    region=[
      DisposalTunnelCenterPoints[i,0],
      DisposalTunnelCenterPoints[i,1],
      DisposalTunnelCenterPoints[i,2],
      DTtag,500]
    regions.append(region)
  for i in range(0,len(DisposalHoleCenterPoints)):
    region=[
      DisposalHoleCenterPoints[i,0],
      DisposalHoleCenterPoints[i,1],
      DisposalHoleCenterPoints[i,2],
      DHtag,500]
    regions.append(region)
  

  return regions
  
def TetgenGen(Output_path,testSwitches,points,faces,facet_markers,mtr,regions):
  '''Create Tetgen mesh'''
  points=tuple(map(tuple, points))
  
  tetgenF='test'
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(len(mtr))+' 1\n')
    for i in range(0,len(mtr)):
      f.write(str(mtr[i])+'\n')
    f.close() 

  mesh_info =tet.MeshInfo()
  mesh_info.set_points(points)
  mesh_info.set_facets(faces,facet_markers) 
  mesh_info.load_mtr(tetgenF) 
  
  mesh_info.regions.resize(len(regions))
  for i in range(0,len(regions)):
    mesh_info.regions[i] = regions[i]
    # print(regions[i])
  print("set regions OK")
  
  if testSwitches:    
    switches='pAmnMT1e-16' #test
  else:
    # switches='pAmnfMT1e-16' #get all facets
    switches='pqAmnMT1e-16' #get tag facets
    # switches='pqAmnfM'
    # switches='pAmnfM'
  print('refinement switches=',switches)

  mesh = tet.build(mesh_info, options=tet.Options(switches=switches))

  points=np.array(mesh.points)
  elements=np.array(mesh.elements)
  element_attributes=np.array(mesh.element_attributes)
  mtr=np.array(mesh.point_metric_tensors)
  print(points)
  print(elements)  
  no_ele=len(elements)
  cells=np.full(shape=(no_ele,5),fill_value=4,dtype=np.int64)
  celltypes = np.full(no_ele, CellType.TETRA, dtype=np.uint8)
  cells[:,1:5]=elements[:,0:4]
  tet3dDomain = pv.UnstructuredGrid(cells, celltypes, points)
  tet3dDomain['mtr']=mtr
  tet3dDomain['element_attributes']=element_attributes
  tet3dDomain.save(os.path.join(Output_path,'tet3dDomain.vtu'))
  print(tet3dDomain)

  saveSurfaceSoil=False
  if saveSurfaceSoil:
    surInd=np.where(np.logical_or(tet3dDomain['element_attributes']==104,tet3dDomain['element_attributes']==105))
    surTet3dDomain=tet3dDomain.extract_cells(surInd).extract_surface()
    surTet3dDomain.save(os.path.join(Output_path,'dem3d.stl'))

  faces=np.array(mesh.faces)
  face_markers=np.array(mesh.face_markers)
  no_face_ele=len(faces)
  faceCells=np.full(shape=(no_face_ele,4),fill_value=3,dtype=np.int64)
  celltypes = np.full(no_face_ele, CellType.TRIANGLE, dtype=np.uint8)
  faceCells[:,1:4]=faces[:,0:3]
  tet3dDomainface = pv.UnstructuredGrid(faceCells, celltypes, points)
  tet3dDomainface['mtr']=mtr
  tet3dDomainface['face_markers']=face_markers
  tet3dDomainface.save(os.path.join(Output_path,'tet3dDomainface.vtu'))
  
import gmsh
import sys
import pyvista as pv
import numpy as np
import pytriangle
import copy

def Domain3DfacetsCreate(Output_path,facetArray_name,topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,RegionalOceanFaceTag,SiteFaceTag,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D,surfBotz,siteBotZ,regionalBotZ):
  
  gmsh.initialize()
  gmsh.model.add("Harea3D")

  # unquite=np.unique(topDomainTri_2D['element_attributes'])
  # print(unquite) #[ -3   1   2 101]

  domain_tag={
    "regionalSoil":1,
    "regionalMatrix":2,
    "siteSoil":3,
    "siteMatrix":4,
    "1":[],
    "2":[],
    "3":[],
    "4":[],
    }

  points=topDomainTri_3D.points.tolist()
  faces=topDomainTri_3D.cells.reshape(-1,4)[:,1:4].tolist()
  face_markers=topDomainTri_2D['element_attributes'].tolist()
  mtr=np.full(shape=len(points),fill_value=200,dtype=np.float64).tolist()  

  pointNb=-1
  for i in range(0,len(points)):
    pointNb=pointNb+1
    gmsh.model.geo.addPoint(
      points[i][0],points[i][1],points[i][2],mtr[i],pointNb)

  linetag=-1
  curves=[]
  for i in range(0,len(faces)):
    curve=[]
    for j in range(0,len(faces[i])):
      linetag=linetag+1
      if (j < len(faces[i])-1):
        # print(faces[i][j], faces[i][j+1], linetag)
        gmsh.model.geo.addLine(faces[i][j], faces[i][j+1], linetag)      
      else:
        gmsh.model.geo.addLine(faces[i][-1], faces[i][0], linetag)
      curve.append(linetag)
    curves.append(curve)

  facetMarkers_unique=np.unique(topDomainTri_2D['element_attributes'])
  planetags={}
  for i in range(0,len(facetMarkers_unique)):
    planetags[str(facetMarkers_unique[i])]=[]
  
  
  curvetag=0
  for i in range(0,len(curves)):
    curvetag=curvetag+1
    gmsh.model.geo.addCurveLoop(curves[i], curvetag)
    # gmsh.model.geo.addPlaneSurface([curvetag], curvetag) 
    planetags[str(face_markers[i])].append(curvetag)

  for i in range(0,len(facetMarkers_unique)):
    gmsh.model.geo.addPlaneSurface(planetags[str(facetMarkers_unique[i])], facetMarkers_unique[i])
    if facetMarkers_unique[i]==riverFaceRegionalTag or facetMarkers_unique[i]==RegionalFaceTag or facetMarkers_unique[i]==RegionalOceanFaceTag:
      domain_tag["1"].append(facetMarkers_unique[i])
    elif facetMarkers_unique[i]==riverFaceSiteTag or facetMarkers_unique[i]==SiteFaceTag:
      domain_tag["3"].append(facetMarkers_unique[i])
      


  '''Add surface soil coordinates of the site.'''
  '''used surfBotz set z coordinate to create surface facet'''
  surfMtr=100
  surfBot_Index=[]
  for i in range(0,topDomainTri_3D.n_points):
    pointNb=pointNb+1
    surfBot_Index.append(pointNb)
    gmsh.model.geo.addPoint(
      points[i][0],points[i][1],points[i][2]+surfBotz,surfMtr,pointNb)

  tempfaces=copy.deepcopy(topDomainTri_3D.cells.reshape(-1,4)[:,1:4])
  tempfaces=tempfaces+topDomainTri_3D.n_points
  tempface_markers=copy.deepcopy(topDomainTri_2D['element_attributes'])
  tempface_markers=tempface_markers+100
  # print(np.unique(tempface_markers)) #[101 102 130 131 201]

  curves=[]
  for i in range(0,len(tempfaces)):
    curve=[]
    for j in range(0,len(tempfaces[i])):
      linetag=linetag+1
      if (j < len(tempfaces[i])-1):
        gmsh.model.geo.addLine(tempfaces[i][j], tempfaces[i][j+1], linetag)      
      else:
        gmsh.model.geo.addLine(tempfaces[i][-1], tempfaces[i][0], linetag)
      curve.append(linetag)
    curves.append(curve)

  temp_facetMarkers_unique=np.unique(tempface_markers)
  # print("temp_facetMarkers_unique=",temp_facetMarkers_unique)

  for i in range(0,len(temp_facetMarkers_unique)):
    planetags[str(temp_facetMarkers_unique[i])]=[]  

  for i in range(0,len(curves)):
    curvetag=curvetag+1
    gmsh.model.geo.addCurveLoop(curves[i], curvetag)
    planetags[str(tempface_markers[i])].append(curvetag)
  
  for i in range(0,len(temp_facetMarkers_unique)):
    gmsh.model.geo.addPlaneSurface(planetags[str(temp_facetMarkers_unique[i])], temp_facetMarkers_unique[i])
    if temp_facetMarkers_unique[i]==riverFaceRegionalTag+100 or temp_facetMarkers_unique[i]==RegionalFaceTag+100 or temp_facetMarkers_unique[i]==RegionalOceanFaceTag+100 :
      domain_tag["1"].append(temp_facetMarkers_unique[i])
      domain_tag["2"].append(temp_facetMarkers_unique[i])
    elif temp_facetMarkers_unique[i]==riverFaceSiteTag+100 or temp_facetMarkers_unique[i]==SiteFaceTag+100:
      domain_tag["3"].append(temp_facetMarkers_unique[i])
      domain_tag["4"].append(temp_facetMarkers_unique[i])


  '''Add bottom point coordinates of the site.'''
  '''used site_Index to create site facet'''
  siteMtr=100
  siteBot_Index=[]
  for i in range(0, len(site_Index)):
    pointNb=pointNb+1
    siteBot_Index.append(pointNb)
    gmsh.model.geo.addPoint(
      points[site_Index[i]][0],points[site_Index[i]][1],siteBotZ,siteMtr,pointNb) 

  iniMark=-20
  for i in range(0, len(site_Index)):
    # get top surface segement
    if i < len(site_Index)-1:
      startNb=site_Index[i]
      nestNb=site_Index[i+1]
    elif i == len(site_Index)-1:
      startNb=site_Index[-1]
      nestNb=site_Index[0]
    lineMarker=iniMark+i*-1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)      

    '''Set the surface soil face at the site scale.'''
    faces=[]
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)

    '''Set the matrix face at the site scale.'''
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    face.append(nestNb)
    face.append(startNb)
    faces.append(face)

    curves=[]
    for j in range(0,len(faces)):
      curve=[]
      for k in range(0,len(faces[j])):
        linetag=linetag+1
        if (k < len(faces[j])-1):
          gmsh.model.geo.addLine(faces[j][k], faces[j][k+1], linetag)      
        else:
          gmsh.model.geo.addLine(faces[j][-1], faces[j][0], linetag)
        curve.append(linetag)
      curves.append(curve)

    for j in range(0,len(curves)):
      marker=SiteFaceTag+1+j+i*10
      planetags[str(marker)]=[] 
      curvetag=curvetag+1
      gmsh.model.geo.addCurveLoop(curves[j], curvetag)
      planetags[str(marker)].append(curvetag)
    
    for j in range(0,len(curves)):
      marker=SiteFaceTag+1+j+i*10
      gmsh.model.geo.addPlaneSurface(planetags[str(marker)], marker)
      if j==0:
        domain_tag["3"].append(marker)
      elif j==1:
        domain_tag["4"].append(marker)



  '''Add Regional point coordinates of the site.'''
  '''Top Regional'''
  RegionMrt=500
  if True:
    # print('topRegional_R_Index=',topRegional_R_Index)
    topRegional_R_Bot_Index=[]
    for i in range(0, len(topRegional_R_Index)):
      pointNb=pointNb+1
      topRegional_R_Bot_Index.append(pointNb)
      gmsh.model.geo.addPoint(
      points[topRegional_R_Index[i]][0],points[topRegional_R_Index[i]][1],regionalBotZ,RegionMrt,pointNb)

    topRegional_L_Bot_Index=[]
    for i in range(0, len(topRegional_L_Index)):
      pointNb=pointNb+1
      topRegional_L_Bot_Index.append(pointNb)
      gmsh.model.geo.addPoint(
      points[topRegional_L_Index[i]][0],points[topRegional_L_Index[i]][1],regionalBotZ,RegionMrt,pointNb)


  print(domain_tag)

  quit()

def gmshSurfaceGen(Output_path,testSwitches,points,faces,sege_group,mtr,tet_regions):
  # points,mtr,pointNb,faces
  # bc1dEdges,bc1dTags,bc2dFaces,bc2dtags,group1d,group1dTags,group2d,group2dTags

  # print(topDomainTri_2D.array_names)
  facetArray_name='element_attributes'
  # boundMarkers=np.unique(topDomain2Dfacet[facetArray_name])

  # points=topDomainTri_3D.points[:,0:3].tolist()
  # faces=topDomainTri_3D.cells.reshape(-1,4)[:,1:4].tolist()
  # sege_group=topDomainTri_2D[facetArray_name].tolist()
  # mtr=np.full(shape=len(points),fill_value=20).tolist()
  pointNb=np.linspace(start=0,stop=len(points),num=len(points),endpoint=False,dtype=np.int64)
  # print(faces)
  # # print(sege_group)
  # quit()
  # startNb=0
  # nestNb=1
  # lineMarker=3
  # siteOrderSegement=pytriangle.orderSegement(riverBound,'sege_group',startNb,nestNb,lineMarker)
  # print(siteOrderSegement)
  # quit()
  
  # points=np.array(points)
  # faces=np.array(faces)
  # sege_group=np.array(sege_group)
  # mtr=np.array(mtr)
  # pointNb=np.array(pointNb)


  gmsh.initialize()
  gmsh.model.add("Harea")
  # gmsh.option.setNumber("General.Verbosity", 0)
  
  for i in range(0,len(points)):
    gmsh.model.geo.addPoint(
      points[i][0],
      points[i][1],
      points[i][2],
      mtr[i],
      pointNb[i])
    
  linetag=-1
  curves=[]
  for i in range(0,len(faces)):
    curve=[]
    for j in range(0,len(faces[i])):
      linetag=linetag+1
      if (j < len(faces[i])-1):
        gmsh.model.geo.addLine(faces[i][j], faces[i][j+1], linetag)
        # print(testfaces[i][j], testfaces[i][j+1], linetag)        
      else:
        gmsh.model.geo.addLine(faces[i][-1], faces[i][0], linetag)
        # print(testfaces[i][-1], testfaces[i][0], linetag)
      curve.append(linetag)
    curves.append(curve)
  # print(curves)

  curvetag=0
  for i in range(0,len(curves)):
    curvetag=curvetag+1
    gmsh.model.geo.addCurveLoop(curves[i], curvetag)
    # print(curves[i], curvetag)
    gmsh.model.geo.addPlaneSurface([curvetag], curvetag)
  
  gmsh.model.geo.synchronize()

  
  # gmsh.model.addPhysicalGroup(1, [3, 7], 101)
  # gmsh.model.addPhysicalGroup(1, [1, 5], 102)
  # gmsh.model.addPhysicalGroup(2, [1], 10)
  # gmsh.model.addPhysicalGroup(2, [2], 11)

  gmsh.model.mesh.generate(3)

  gmsh.model.mesh.removeDuplicateNodes()
  gmsh.model.mesh.removeDuplicateElements()

  # if '-nopopup' not in sys.argv:
  #   gmsh.fltk.run()

  # print(dir(gmsh.model))
  nodeTags, nodeCoords, _ = gmsh.model.mesh.getNodes()
  print(nodeTags)
  print(nodeCoords)
  elementType = gmsh.model.mesh.getElementType("triangle", 1)
  print(elementType)
  faceNodes = gmsh.model.mesh.getElementFaceNodes(elementType, 3)
  edgeNodes = gmsh.model.mesh.getElementEdgeNodes(elementType)
  print(faceNodes)
  print(edgeNodes)

  gmsh.write('gmehTest.msh')
  gmsh.finalize()



  mesh=pv.read('gmehTest.msh')
  mesh.save('gmehTest.vtu')
  quit()
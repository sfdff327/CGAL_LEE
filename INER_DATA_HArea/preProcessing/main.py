import json
import os

import numpy as np
import pymeshfix as mf
import pyTetgen
import pyvista as pv
import readGeoJSON
from mesh import Mesh
from pymeshfix import MeshFix
from pymeshfix._meshfix import PyTMesh
from pymeshfix.examples import planar_mesh
from pyvista import CellType

from gstools import Exponential,Gaussian,Matern
from gstools import krige, vario_estimate_unstructured

import riverCreate

import pygmsh

work_path = os.path.dirname(os.path.realpath(__file__))
layoutCAD_path = os.path.join(work_path,'../Repository Layout')
Object_path = os.path.join(work_path,'../C_水文特性_1130705','水文地質單元','Site scale object')
DEM_path = os.path.join(work_path,'../DEM')
Output_path = os.path.join(work_path,'output')



def moveSTL(meshStl,meshStlNew,IDa,IDb,scale):
  # IDa往IDb前進scale
  mesh=pv.read(meshStl)
  vector=mesh.points[212]-mesh.points[5]
  lenVector=np.linalg.norm(mesh.points[212]-mesh.points[5])
  vector=vector/lenVector
  points=mesh.points
  faces=mesh.faces
  for i in range(0,3):
    points[:,i]=points[:,i]+vector[i]*scale
  meshnew=pv.PolyData(points,faces)
  meshnew.save(meshStlNew)


def coastlineCreate(dtm):

  '''get coastline'''
  n_contours=1
  rng=[0,0]
  output, coastline = dtm.contour_banded(n_contours, rng=rng)
  coastline = dtm.contour(
    isosurfaces=1,
    scalars='z',
    compute_normals=False,
    compute_gradients=False,
    compute_scalars=True,
    rng=[0,0],
    preference='point',
    method='contour',)
  coastline['z']=coastline.points[:,2]
  center_coastline=coastline.cell_centers()
  lines=coastline.lines.reshape(-1,3)[:,1:3]
  coastline.save('coastline.vtk')

  calPoint=np.zeros(coastline.n_points,dtype=np.int64)
  for line in lines:
    for j in range(0,2):
      calPoint[line[j]]=calPoint[line[j]]+1
  coastline_StartStopId=[]
  for i in range(0,coastline.n_points):
    if calPoint[i]==1:
      coastline_StartStopId.append(i)
  print('coastline_StartStopId=',coastline_StartStopId)

  cellsPoint=[]
  a=coastline.points[coastline_StartStopId[0]]
  cellsPoint.append(a.tolist())
  cellId=coastline.find_closest_cell(a)
  points=center_coastline.points

  while(len(points)>1):
    a=center_coastline.points[cellId]
    points=np.delete(points, cellId, 0)
    center_coastline=pv.PolyData(points)
    cellId=center_coastline.find_closest_cell(a)
    cellsPoint.append(a.tolist())
  center_coastline=pv.PolyData(cellsPoint)
  center_coastline.save(os.path.join(Output_path,'center_coastline.vtk'))

def vtkRegionCenters(vtkAll):
  points=[]
  vtkConn=vtkAll.connectivity(extraction_mode='all')
  u=np.unique(vtkConn['RegionId'])
  for i in range(0,len(u)):
    ind=np.where(vtkConn['RegionId']==u[i])
    objects=vtkConn.extract_cells(ind)
    points.append(objects.center)
  return points

def main():

  print('work_path=',work_path)

  '''Read DTM stl'''
  dtmFileName='DEM_inner40_object_regional_v1_1_10_ShiftXY.stl'
  dtmStl=os.path.join(DEM_path,dtmFileName)
  dtm=pv.read(dtmStl)
  dtm['z']=dtm.points[:,2]
  dtmpoints2D=np.zeros((dtm.n_points,3),dtype=np.float64)
  dtmpoints2D[:,0:2]=dtm.points[:,0:2]
  dem2D=pv.PolyData(dtmpoints2D)
  dem2D['z']=dtm['z']

  '''Read Site scale object stl'''
  Site_HRD1_Gne_StlFileName='Site_HRD1_Gne_ShiftXY.stl'
  Site_HRD1_Gne_StlFileName=os.path.join(Object_path,Site_HRD1_Gne_StlFileName)
  Site_HRD1_GneStl=pv.read(Site_HRD1_Gne_StlFileName)
  Site_HRD1_Mar_StlFileName='Site_HRD1_Mar_ShiftXY.stl'
  Site_HRD1_Mar_StlFileName=os.path.join(Object_path,Site_HRD1_Mar_StlFileName)
  Site_HRD1_MarStl=pv.read(Site_HRD1_Mar_StlFileName)
  points=np.zeros((4,3),dtype=np.float64)
  points[0]=Site_HRD1_MarStl.points[529]
  points[1]=Site_HRD1_MarStl.points[8417]
  points[2]=Site_HRD1_GneStl.points[6384]
  points[3]=Site_HRD1_GneStl.points[572]
  lines=[[2,0,1],[2,1,2],[2,2,3],[2,3,0]]
  celltypes = np.full(len(lines), CellType.LINE, dtype=np.uint8)
  lines=np.array(lines).ravel()
  siteBound=pv.UnstructuredGrid(lines, celltypes, points)
  siteBound.save(os.path.join(Output_path,'siteBound.vtu'))  

  '''check center_coastline file exists'''
  if not os.path.isfile(os.path.join(Output_path,'center_coastline.vtk')):
    print('start to create center_coastline vtk')
    coastlineCreate(dtm)

  case=1
  if case==1:    
    center_coastline=pv.read(os.path.join(Output_path,'center_coastline.vtk'))
    Domain_bounds=[[[3972.94,10094.20],[1549.81,5029.50]]]
    insidePoints=[[3981.34,6815.56]]
    riverBound=riverCreate.riverShp2VTU(Output_path,Domain_bounds,insidePoints,center_coastline,siteBound,dem2D)
  else:
    riverBound=pv.read(os.path.join(Output_path,'riverBound.vtu'))

  '''if run riverBound, must to check center_coastlineByRiver'''
  points=center_coastline.points
  points[528]=riverBound.points[148]
  points[531]=riverBound.points[149]
  cells=np.full(shape=(len(points),2),fill_value=1,dtype=np.int64)
  cells[:,1]=np.linspace(start=0,stop=len(points),num=len(points),endpoint=False,dtype=np.int64)
  celltypes = np.full(len(cells), CellType.VERTEX, dtype=np.uint8)
  cells=cells.ravel()
  center_coastlineByRiver=pv.UnstructuredGrid(cells, celltypes, points)
  center_coastlineByRiver.save(os.path.join(Output_path,'center_coastlineByRiver.vtu'))


  '''Set Top Domain facets'''
  case=1
  if case==1:
    topRegional_scaleDomainR=[[7350.97,10123.8],[6066.44,8199.25],[3972.94,10094.20]]
    topRegional_scaleDomainL=[[1549.81,5029.50],[234.28,4430.0]]
    bottomRegional_scaleDomain=[[4593.47,773.25],[11770.10,7069.25]]
    riverMaxArea=50000000 # 50000.0 # riverMaxArea=25.0*25.0/4.0
    RegionalMaxArea=200000000.0 # 5000000.0 # RegionalMaxArea=10000.0
    SiteMaxArea=500000000.0 # 50000000.0 # SiteMaxArea=1000.0
    riverFaceRegionalTag=int(30)
    riverFaceSiteTag=int(31)
    RegionalFaceTag=int(1)
    RegionalOceanFaceTag=int(2)    
    
    SiteFaceTag=int(1001)
    riverRegionPoints=np.array(
      [[6072.06956226, 4785.74660037],
       [6048.08485242, 4804.80946765],
       [4051.06575167, 6689.36838925],
       [3292.64449156, 4978.93546805]])
    riverSitePoints=np.array([[5711.0967379 , 5156.84514156]])
    RegionalPoints=np.array(
      [[5625.0148718 , 7697.73196172],
       [2383.11435132, 5430.6402615 ]])
    RegionalOceanPoints=np.array(
      [[9339.32758459, 6276.51672377]])
    SitePoints=np.array(
      [[4765.31979172, 6302.37661989],
      [3931.18536452, 5781.9057275 ],
      [4938.39248324, 5058.49548485]])
    
    topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D=riverCreate.topDomainCreate(Output_path,riverMaxArea,RegionalMaxArea,SiteMaxArea,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,RegionalOceanFaceTag,SiteFaceTag,riverRegionPoints,riverSitePoints,RegionalPoints,RegionalOceanPoints,SitePoints,riverBound,topRegional_scaleDomainR,topRegional_scaleDomainL,bottomRegional_scaleDomain,siteBound,center_coastlineByRiver,dem2D)
  else:
    topDomain2Dfacet=pv.read(os.path.join(Output_path,'topDomain2Dfacet.vtu'))
    topDomainTri_2D=pv.read(os.path.join(Output_path,'topDomainTri_2D.vtu'))
    topDomainTri_3D=pv.read(os.path.join(Output_path,'topDomainTri_3D.vtu'))
    npz_file = np.load('topDomain.npz')
    topRegional_R_Index, topRegional_L_Index, bottomRegional_Index, site_Index, coastline_Index = npz_file['topRegional_R_Index'], npz_file['topRegional_L_Index'], npz_file['bottomRegional_Index'], npz_file['site_Index'], npz_file['coastline_Index']
  print("Top Domain OK")

  river3D_RegionalPoints=riverCreate.interpolateSurfaceZ(riverRegionPoints,dem2D)
  river3D_SitePoints=riverCreate.interpolateSurfaceZ(riverSitePoints,dem2D)
  Regional3D_Points=riverCreate.interpolateSurfaceZ(RegionalPoints,dem2D)
  RegionalOcean3D_Points=riverCreate.interpolateSurfaceZ(RegionalOceanPoints,dem2D)
  Site3D_Points=riverCreate.interpolateSurfaceZ(SitePoints,dem2D)

  case=1
  if case==1:
    # tri_facet_markers index doc
    # -2 is site bound
    # 3 is river bound
    # 4 is coastline
    # 10  11  12  13  14  bottomRegional_Index
    # 20  21  22  topRegional_R_Index
    # 23 topRegional_L_Index
    facetArray_name=topDomain2Dfacet.array_names[0]
    boundMarkers=np.unique(topDomain2Dfacet[facetArray_name])
    
    surfBotz=-20.0
    siteBotZ=-1500.0 # set site domain bottom coordinates
    regionalBotZ=-3000.0
    tet_points,tet_faces,tet_facet_markers,tet_mtr=riverCreate.Domain3DfacetsCreate(Output_path,facetArray_name,topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,SiteFaceTag,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D,surfBotz,siteBotZ,regionalBotZ)
    print("3D domain Regional and site facets OK")

    tet_points,tet_faces,tet_facet_markers,tet_mtr=pygmsh.Domain3DfacetsCreate(Output_path,facetArray_name,topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,RegionalOceanFaceTag,SiteFaceTag,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D,surfBotz,siteBotZ,regionalBotZ)

  case=1
  if case==1:
    '''Read DH stl'''
    DisposalHoleFileName='DH.stl'
    DisposalHoleStl=os.path.join(layoutCAD_path,DisposalHoleFileName)
    DisposalHoleStlNew='DisposalHoleStlNew.stl'
    DisposalHole=pv.read(DisposalHoleStl)
    vector=np.array([0.0,0.0,1.0])
    scale=-0.5
    points=DisposalHole.points
    faces=DisposalHole.faces
    for i in range(0,3):
      points[:,i]=points[:,i]+vector[i]*scale
    DisposalHole=pv.PolyData(points,faces)
    DisposalHole.save(DisposalHoleStlNew)
    DisposalHoleCenterPoints=vtkRegionCenters(DisposalHole)

    DisposalTunnelFileName='DT.stl'
    DisposalTunnelStl=os.path.join(layoutCAD_path,DisposalTunnelFileName)
    # DisposalTunnel=pv.read(DisposalTunnelStl)
    DisposalTunnelStlNew='DisposalTunnelStlNew.stl'
    IDa=7692
    IDb=7705
    scale=0.5
    moveSTL(DisposalTunnelStl,DisposalTunnelStlNew,IDa,IDb,scale)
    DisposalTunnel=pv.read(DisposalTunnelStlNew)
    DisposalTunnelCenterPoints=vtkRegionCenters(DisposalTunnel)

    MT1FileName='MT_part1.stl'
    MT1Stl=os.path.join(layoutCAD_path,MT1FileName)
    MT1vtk=pv.read(MT1Stl)
    MT1CenterPoints=vtkRegionCenters(MT1vtk)
    
    MT2FileName='MT_part2.stl'
    MT2Stl=os.path.join(layoutCAD_path,MT2FileName)
    MT2StlNew='MT2StlNew.stl'
    IDa=212
    IDb=5
    scale=0.5
    moveSTL(MT2Stl,MT2StlNew,IDa,IDb,scale)
    MT2vtk=pv.read(MT2StlNew)
    MT2CenterPoints=vtkRegionCenters(MT2vtk) 

    stlFiles=[MT1Stl,MT2StlNew,DisposalTunnelStlNew,DisposalHoleStlNew]
    DisposalMtl=[30.0,30.0,20.0,1.5]
    # DisposalMtl=[100.0,100.0,100.0,1.5]
    my_mesh=Mesh(stlFiles)
    my_mesh.solveIntersections()
    out_coords=my_mesh.get_out_coords().reshape(-1,3)
    out_tris=my_mesh.get_out_tris().reshape(-1,3)
    out_labels=np.int64(my_mesh.get_out_labels_bit())
    DF_mtr=np.full(shape=len(out_coords),fill_value=DisposalMtl[0],dtype=np.float64)
    DF_mtrCehck=np.zeros(shape=len(out_coords),dtype=np.int64)
    '''set DH Mtr'''
    indLabelDH=np.where(out_labels>=1000)[0]
    for i in range(len(indLabelDH)):
      for j in range(0,3):
        nb=out_tris[indLabelDH[i]][j]
        if DF_mtrCehck[nb]==0:
          DF_mtr[nb]=DisposalMtl[3]
          DF_mtrCehck[nb]=1
    '''set DT Mtr'''
    indLabelDT=np.where(out_labels>=100)[0]
    for i in range(len(indLabelDT)):
      if indLabelDT[i] not in indLabelDH:
        for j in range(0,3):
          nb=out_tris[indLabelDT[i]][j]
          if DF_mtrCehck[nb]==0:
            DF_mtr[nb]=DisposalMtl[2]
            DF_mtrCehck[nb]=1
    no_ele=len(out_tris)
    ele_ind_new=np.full((no_ele,4),3,dtype=np.int64)
    ele_ind_new[0:len(out_tris),1:4]=out_tris[0:len(out_tris),0:3]
    ele_ind_new=np.hstack(ele_ind_new)
    celltypes = np.full(no_ele, CellType.TRIANGLE, dtype=np.uint8)
    # mesh = pv.UnstructuredGrid(ele_ind_new, celltypes, out_coords)
    mesh = pv.PolyData(out_coords,ele_ind_new)
    mesh['DF_mtr']=DF_mtr
    mesh['out_labels']=out_labels
        
    DF_points=out_coords
    DF_faces=out_tris
    DF_face_markers=out_labels
    # DF_mtr=np.full(shape=len(out_coords),fill_value=3.0,dtype=np.float64)
    # quit()

  addDFMesh=False
  addDFMesh=True
  if addDFMesh:
    tempNpoints=len(tet_points)
    tet_points=np.concatenate((np.array(tet_points),np.array(DF_points)),axis=0).tolist()
    tet_facet_markers=np.concatenate((np.array(tet_facet_markers),np.array(DF_face_markers)),axis=0).tolist()
    tet_mtr=np.concatenate((np.array(tet_mtr),np.array(DF_mtr)),axis=0).tolist()
    for i in range(0,len(DF_faces)):
      temp=(np.array(DF_faces[i])+tempNpoints)
      temp=temp.tolist()
      tet_faces.append(temp)

  print("3D domain meshGen input OK")

  case=1
  if case==1:
    testSwitches=False    
    testSwitches=True
    
    tet_regions=pyTetgen.setRegions(river3D_Points,Regional3D_Points,RegionalOcean3D_Points,Site3D_Points,surfBotz,MT1CenterPoints,MT2CenterPoints,DisposalTunnelCenterPoints,DisposalHoleCenterPoints)
    
    pygmsh.gmshSurfaceGen(Output_path,testSwitches,tet_points,tet_faces,tet_facet_markers,tet_mtr,tet_regions)

    # pyTetgen.TetgenGen(Output_path,testSwitches,tet_points,tet_faces,tet_facet_markers,tet_mtr,tet_regions)

  
  quit()


  #old codeing

  '''top bc edges'''
  dtmBound=dtm.extract_feature_edges(feature_angle=30.0,boundary_edges=True,non_manifold_edges=False,feature_edges=False,manifold_edges=False,clear_data=False,)
  dtmBound.save(os.path.join(Output_path,'dtmBound.vtk'))
  points=np.zeros((dtmBound.n_points,3),dtype=np.float64)
  points[:,0:2]=dtmBound.points[:,0:2]  
  lines=dtmBound.lines
  celltypes = np.full(dtmBound.n_cells, CellType.LINE, dtype=np.uint8)
  dtmBound2D= pv.UnstructuredGrid(lines, celltypes, points)
  # dtmBound2D=pv.PolyData(points,lines)
  dtmBound2D['z']=dtmBound['z']
  dtmBound2D.save(os.path.join(Output_path,'dtmBound2D.vtu'))
  print(dtmBound2D)


  quit()




  '''Read RegionalScale'''
  HRD1_Mar_ShiftXYFileName='HRD1_Mar_ShiftXY.stl'
  HRD1_MarStl=os.path.join(work_path,'../C_水文特性_1130705','水文地質單元','Regional scale object',HRD1_Mar_ShiftXYFileName)
  HRD1_Mar=pv.read(HRD1_MarStl)




  


  # AccessDecline
if __name__ == '__main__':
  
  main()
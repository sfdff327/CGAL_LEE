import numpy as np
import pyvista as pv
import json
import readGeoJSON
from pyvista import CellType
import os
from mesh import Mesh

import math
import numpy as np
import meshpy
import meshpy.triangle as triangle

def trianglePointConn(xy):
  points=[]
  for i in range(0,len(xy)):
    points.append((xy[i][0],xy[i][1]))
  return points

def triangleFaceConn(segements):
  facets=[]
  for i in range(0,len(segements)):
    facets.append((segements[i][0],segements[i][1]))
  return facets

maxReg0Area_ = 3

def order2d(ind):
  if ind ==[0]:
    return [1,2]
  elif ind ==[1]:
    return [2,0]
  elif ind ==[2]:
    return [0,1]

def needs_refinement(vertices, area):
  global maxReg0Area_
  vert_origin, vert_destination, vert_apex = vertices
  bary_x = (vert_origin.x + vert_destination.x + vert_apex.x) / 3
  bary_y = (vert_origin.y + vert_destination.y + vert_apex.y) / 3

  dist_center = math.sqrt(bary_x**2 + bary_y**2)
  max_area = 100*(math.fabs(0.002 * (dist_center-0.3)) + 0.0001)

  #print('     ', vert_origin.y, ' ', vert_destination.y, ' ', vert_apex.y, ' ', max_area)
  #print(maxReg0Area_)
  coarsenfac = 70*bary_y*bary_y/300/300 + 1
  return area > coarsenfac*maxReg0Area_
  #return area > max_area


def createMesh(points,facets,sege_group,maxArea):
  '''
  # triangle MeshInfo
  '''
  global maxReg0Area_ # Dirty trick to passing the maxArea to 'needs_refinement'
  info = triangle.MeshInfo()
  info.set_points(points)
  info.set_facets(facets, facet_markers=sege_group)
  info.regions.resize(2)  
  centroid=[150.0, 10.0],[150.0, -10.0]

  info.regions[0] = [
            centroid[0][0], centroid[0][1],  # coordinate
            1,  # lower element tag
            maxArea,  # max area
            ]
  info.regions[1] = [
            centroid[1][0], centroid[1][1],  # coordinate
            10,  # lower element tag
            maxArea*30,  # max area
            ]
  maxReg0Area_=maxArea

  '''
  # triangle MeshInfo
  '''
  tri = triangle.build(info,verbose=False, refinement_func=needs_refinement, attributes=True, volume_constraints=True, max_volume=None, allow_boundary_steiner=True, allow_volume_steiner=True, quality_meshing=True, generate_edges=None, generate_faces=True, min_angle=None)

  return tri

def getBounds(tri,bcNodeMark_Left,bcNodeMark_Right,bcNodeMark_Bot,bcNodeMark_Top,bcNodeMark_Mid):
  tri_points = np.array(tri.points)
  tri_faces = np.array(tri.elements)
  tri_element_attributes = np.array(tri.element_attributes)
  tri_facets = np.array(tri.facets)
  tri_facet_markers = np.array(tri.facet_markers)

  '''Left Boundary'''
  ind=list(zip(*np.where(tri_facet_markers==bcNodeMark_Left)))
  leftNb=np.zeros((len(ind)*2),dtype=np.int64)
  for i in range(0,len(ind)):
    leftNb[i*2]=tri_facets[ind[i]][0]
    leftNb[i*2+1]=tri_facets[ind[i]][1]
  leftNb=np.unique(leftNb)
  # print("leftNb=",leftNb)

  '''Bottom Boundary'''
  ind=list(zip(*np.where(tri_facet_markers==bcNodeMark_Bot)))
  BotNb=np.zeros((len(ind)*2),dtype=np.int64)
  for i in range(0,len(ind)):
    BotNb[i*2]=tri_facets[ind[i]][0]
    BotNb[i*2+1]=tri_facets[ind[i]][1]
  BotNb=np.unique(BotNb)
  # print("BotNb=",BotNb)

  '''Right Boundary'''
  ind=list(zip(*np.where(tri_facet_markers==bcNodeMark_Right)))
  RightNb=np.zeros((len(ind)*2),dtype=np.int64)
  for i in range(0,len(ind)):
    RightNb[i*2]=tri_facets[ind[i]][0]
    RightNb[i*2+1]=tri_facets[ind[i]][1]
  RightNb=np.unique(RightNb)
  # print("RightNb=",RightNb)  

  '''Top Boundary'''
  ind=list(zip(*np.where(tri_facet_markers==bcNodeMark_Top)))
  TopNb=np.zeros((len(ind)*2),dtype=np.int64)
  for i in range(0,len(ind)):
    TopNb[i*2]=tri_facets[ind[i]][0]
    TopNb[i*2+1]=tri_facets[ind[i]][1]
  TopNb=np.unique(TopNb)
  # print("TopNb=",TopNb)  

  '''Middle Boundary'''
  ind=list(zip(*np.where(tri_facet_markers==bcNodeMark_Mid)))
  MidNb=np.zeros((len(ind)*2),dtype=np.int64)
  for i in range(0,len(ind)):
    MidNb[i*2]=tri_facets[ind[i]][0]
    MidNb[i*2+1]=tri_facets[ind[i]][1]
  MidNb=np.unique(MidNb)
  # print("MidNb=",MidNb)   

  return [leftNb,BotNb,RightNb,TopNb,MidNb]

def lineSegementStartSearch(lines,ind):
  nb=[]
  for i in range(0,len(ind)):
    nb.append(lines[ind[i][0],0])
    nb.append(lines[ind[i][0],1])
  values, counts = np.unique(nb, return_counts=True)
  print(values)
  print(counts)
  for i in range(0,len(values)):
    if counts[i]==1:
      startNb=values[i]
      break
  return startNb

def orderSegement(facetVtk,arrayName,startNb,stopNb,marker):
  # print(startNb,stopNb)
  lines=facetVtk.cells.reshape(-1,3)[:,1:3]
  markerInd=np.argwhere(facetVtk[arrayName]==marker).flatten()
  # print('markerInd=',markerInd)
  nextNb=startNb
  order=[]
  lineIndChack=np.zeros(len(markerInd),dtype=int)
  while(nextNb!=stopNb):
    # print('nextNb=',nextNb)
    for i in range(0,len(markerInd)):    
      if nextNb in lines[markerInd[i]] and lineIndChack[i]==0:
        lineIndChack[i]=1
        ind = np.where(lines[markerInd[i]]==nextNb)[0]
        # print(i,markerInd[i],lines[markerInd[i]],ind)
        order.append(nextNb)
        if ind ==0:
          nextNb=lines[markerInd[i],1]
        elif ind ==1:
          nextNb=lines[markerInd[i],0]
        # print('nextNb=',nextNb)
        # if len(order)>5:
          # quit()
  order.append(nextNb)
  return order
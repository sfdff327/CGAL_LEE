import numpy as np
import pyvista as pv
import json
import readGeoJSON
from pyvista import CellType
import os
from mesh import Mesh

import pytriangle
import meshpy.triangle as triangle

import meshpy.tet as tet

import copy

def interpolateSurfaceZ(tagPoints,dem2D):
  points=dem2D.points
  maxC=np.max(points,axis=0)
  minC=np.min(points,axis=0)
  radius=np.linalg.norm(maxC-minC)

  tag3D_Points=np.zeros((len(tagPoints),3),dtype=np.float64)
  tag3D_Points[:,0:2]=np.array(tagPoints)[:,0:2]
  tag=pv.PolyData(tag3D_Points)
  tag=tag.interpolate(dem2D,radius=radius,n_points=3)
  tag3D_Points[:,2]=tag['z']

  return tag3D_Points


def riverShp2VTU(Output_path,Domain_bounds,insidePoints,center_coastline,siteBound,dem2D):
  riverF=r'../C_水文特性_1130705/RIV_v1.1/RIV_v1.json'
  with open(riverF, 'r', encoding="utf-8") as f:
    riverJSON = json.loads(f.read())
    f.close()
  results= readGeoJSON.readGeoJSONcoor(riverJSON)
  # print(results)
  points = np.zeros((len(results['lineXY'])-2,3),dtype=np.float64) 
  cells = np.full(shape=(len(results['lineAB']),3),fill_value=2,dtype=np.int64)
  celltypes = np.full(len(results['lineAB']), CellType.LINE, dtype=np.uint8)
  nb=-1
  for i in range(0,27):
    nb=nb+1
    points[nb,0]=results['lineXY'][i][0]
    points[nb,1]=results['lineXY'][i][1]
  for i in range(28,52):
    nb=nb+1
    points[nb,0]=results['lineXY'][i][0]
    points[nb,1]=results['lineXY'][i][1]    
  nb=-1
  for i in range(0,35):
    nb=nb+1
    cells[nb,1]=i
    cells[nb,2]=int(i+1)
  for i in range(36,50):
    nb=nb+1
    cells[nb,1]=i
    cells[nb,2]=int(i+1)    
  nb=nb+1
  cells[nb,1]=int(50)
  cells[nb,2]=int(26)  
  cells=cells.ravel()
  riverVtk= pv.UnstructuredGrid(cells, celltypes, points)
  riverVtk.save(os.path.join(Output_path,'riverVtk.vtu'))

  lines=riverVtk.cells.reshape(-1,3)[:,1:3] 
  
  width=25.0 
  mesh=pv.PolyData()
  nb=-1
  for line in lines:    
    nb=nb+1
    p=[]
    a=points[line[0]]
    b=points[line[1]]
    vector=b-a
    dis=np.linalg.norm(a-b)
    unitV=vector/dis
    verticalUnitV=np.array([-unitV[1],unitV[0],0.0])
    # print(unitV,width,dis)
    c1=a+verticalUnitV*width/2.0
    c4=a-verticalUnitV*width/2.0
    c2=c1+unitV*dis
    c3=c4+unitV*dis
    p.append([c1[0],c1[1],c1[2]])
    p.append([c2[0],c2[1],c2[2]])
    p.append([c3[0],c3[1],c3[2]])
    p.append([c4[0],c4[1],c4[2]])
    cell=[4,0,1,2,3]
    plane=pv.PolyData(p,cell)
    plane['nb']=nb
    mesh=mesh+plane
    # plane.save('oneplane.vtk')
    # quit()
    
  mesh.save('rectangleRiver.stl')

  my_mesh=Mesh(['rectangleRiver.stl'])
  my_mesh.solveIntersections()
  out_coords=my_mesh.get_out_coords().reshape(-1,3)
  out_tris=my_mesh.get_out_tris().reshape(-1,3)
  out_labels=np.int64(my_mesh.get_out_labels_bit())
  no_ele=len(out_tris)
  ele_ind_new=np.full((no_ele,4),3,dtype=np.int64)
  ele_ind_new[0:len(out_tris),1:4]=out_tris[0:len(out_tris),0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  celltypes = np.full(no_ele, CellType.TRIANGLE, dtype=np.uint8)
  mesh = pv.PolyData(out_coords,ele_ind_new)
  mesh['out_labels']=out_labels
  mesh = mesh.decimate(0.8)
  # mesh.save('rectangleRiver.vtk')

  points=mesh.points
  out_tris=mesh.faces.reshape(-1,4)[:,1:4].tolist()

  out_tris.append([134,130,131])
  out_tris.append([10,140,139])
  out_tris.append([141,136,138])
  out_tris.append([137,135,8])
  out_tris.append([6,7,133])
  out_tris.append([132,129,127])
  out_tris.append([125,128,124])
  out_tris.append([126,123,122])
  out_tris.append([119,121,120])
  out_tris.append([118,116,115])
  out_tris.append([113,117,114])
  out_tris.append([112,110,109])
  out_tris.append([111,107,106])
  out_tris.append([108,105,104])
  out_tris.append([103,101,102])
  out_tris.append([28,29,31])
  out_tris.append([34,35,38])
  out_tris.append([41,40,44])
  out_tris.append([95,92,91])
  out_tris.append([93,89,88])
  out_tris.append([90,85,87])
  out_tris.append([84,80,81])
  out_tris.append([79,78,77])
  out_tris.append([76,16,15])
  out_tris.append([12,14,75])
  out_tris.append([74,71,72])
  out_tris.append([73,68,70])
  out_tris.append([69,67,66])
  out_tris.append([65,64,63])
  out_tris.append([62,61,60])
  out_tris.append([59,58,57])
  out_tris.append([22,20,25])
  out_tris.append([27,24,30])
  out_tris.append([36,37,39])
  out_tris.append([43,42,46])
  out_tris.append([47,45,48])
  out_tris.append([49,50,51])
  out_tris.append([52,53,2])
  # print(out_tris)
  out_tris=np.array(out_tris)
  ele_ind_new=np.full((len(out_tris),4),3,dtype=np.int64)
  ele_ind_new[0:len(out_tris),1:4]=out_tris[0:len(out_tris),0:3]
  # print(ele_ind_new)
  ele_ind_new=np.hstack(ele_ind_new)
  # print(ele_ind_new)
  celltypes = np.full(len(out_tris), CellType.TRIANGLE, dtype=np.uint8)
  # river = pv.UnstructuredGrid(ele_ind_new, celltypes, points)
  river = pv.PolyData(points,ele_ind_new)
  river.save(os.path.join(Output_path,'reRectangleRiver.vtk'))


  riverBound=river.extract_feature_edges(feature_angle=30.0,boundary_edges=True,non_manifold_edges=False,feature_edges=False,manifold_edges=False,clear_data=False,)
  riverBound.save('temriverBound.vtk')

  

  from skspatial.objects import Line as skspatial_Line
  from skspatial.objects import LineSegment as skspatial_LineSegment
  from skspatial.objects import Vector as skspatial_Vector

  # from sympy import *
  from sympy.geometry import Point2D,Point,Line,Segment,Polygon,intersection

  points=riverBound.points

  xy=riverBound.points[:,0:2]
  newPoints=copy.deepcopy(riverBound.points)

  segements=riverBound.lines.reshape(-1,3)[:,1:3]

  ''' set river polygon
  # stopNb=segements[0,0]
  # nestNb=segements[0,1]
  # points=[]
  # points.append(Point2D(xy[stopNb,0],xy[stopNb,1]))
  # points.append(Point2D(xy[nestNb,0],xy[nestNb,1]))
  # n2=nestNb
  # indexUsed=[0]
  # while n2!=stopNb:
  #   for i in range(1,len(segements)):
  #     if i not in indexUsed:
  #       n1=segements[i,0]
  #       n2=segements[i,1]
  #       if nestNb==n1:
  #         indexUsed.append(i)          
  #         points.append(Point2D(xy[n2,0],xy[n2,1]))
  #         nestNb=n2
  #         break
  #       elif nestNb==n2:
  #         indexUsed.append(i)          
  #         points.append(Point2D(xy[n1,0],xy[n1,1]))
  #         nestNb=n1
  #         break
  # t = tuple(points)
  # riverPolygon = Polygon(*t) 
  '''
  
  insidePoints=np.array(insidePoints)
  # for i in range(0,len(Domain_bounds)):
  for i in range(0,1):
    array=np.array(Domain_bounds[i])
    # print(len(array))
    for k in range(0,len(array)-1):
      a=array[k+1]-array[k+0] 
      ain=insidePoints[i]-array[k+0]
      crossIndex=np.cross(a, ain)
      # print('i=',i,'k=',k,'crossIndex=',crossIndex)

      for j in range(0,len(segements)):
        b=xy[segements[j,0]]-array[k+0]
        crossAB=np.cross(a, b)
        c=xy[segements[j,1]]-array[k+0]
        crossAC=np.cross(a, c)
        if crossAB*crossAC<0:
          p11=Point(array[k+1][0], array[k+1][1])
          p12=Point(array[k+0][0], array[k+0][1])
          p21=Point(xy[segements[j,0]][0], xy[segements[j,0]][1])
          p22=Point(xy[segements[j,1]][0], xy[segements[j,1]][1])
          l1 = Segment(p11,p12)
          l2 = Segment(p21,p22)
          if len(intersection(l1, l2))>0:
            pp1 = intersection(l1, l2)[0]
            point_intersection=[float(pp1[0]),float(pp1[1])]
            if crossAB*crossIndex<0:
              newPoints[segements[j,0],0]=point_intersection[0]
              newPoints[segements[j,0],1]=point_intersection[1]
            elif crossAC*crossIndex<0:
              newPoints[segements[j,1],0]=point_intersection[0]
              newPoints[segements[j,1],1]=point_intersection[1]
            # print(i,j,point_intersection)
            # quit()

  p11=Point(siteBound.points[2][0], siteBound.points[2][1])
  p12=Point(siteBound.points[3][0], siteBound.points[3][1])
  p21=Point(xy[segements[39,0]][0], xy[segements[39,0]][1])
  p22=Point(xy[segements[39,1]][0], xy[segements[39,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP10=[float(pp1[0]),float(pp1[1])]

  p11=Point(siteBound.points[2][0], siteBound.points[2][1])
  p12=Point(siteBound.points[3][0], siteBound.points[3][1])
  p21=Point(xy[segements[40,0]][0], xy[segements[40,0]][1])
  p22=Point(xy[segements[40,1]][0], xy[segements[40,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP9=[float(pp1[0]),float(pp1[1])]

  p11=Point(siteBound.points[1][0], siteBound.points[1][1])
  p12=Point(siteBound.points[2][0], siteBound.points[2][1])
  p21=Point(xy[segements[29,0]][0], xy[segements[29,0]][1])
  p22=Point(xy[segements[29,1]][0], xy[segements[29,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP8=[float(pp1[0]),float(pp1[1])]

  p11=Point(siteBound.points[1][0], siteBound.points[1][1])
  p12=Point(siteBound.points[2][0], siteBound.points[2][1])
  p21=Point(xy[segements[121,0]][0], xy[segements[121,0]][1])
  p22=Point(xy[segements[121,1]][0], xy[segements[121,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP7=[float(pp1[0]),float(pp1[1])]


  p11=Point(siteBound.points[0][0], siteBound.points[0][1])
  p12=Point(siteBound.points[1][0], siteBound.points[1][1])
  p21=Point(xy[segements[78,0]][0], xy[segements[78,0]][1])
  p22=Point(xy[segements[78,1]][0], xy[segements[78,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP6=[float(pp1[0]),float(pp1[1])]

  p11=Point(siteBound.points[0][0], siteBound.points[0][1])
  p12=Point(siteBound.points[1][0], siteBound.points[1][1])
  p21=Point(xy[segements[79,0]][0], xy[segements[79,0]][1])
  p22=Point(xy[segements[79,1]][0], xy[segements[79,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP5=[float(pp1[0]),float(pp1[1])]


  p11=Point(center_coastline.points[528][0], center_coastline.points[528][1])
  p12=Point(center_coastline.points[529][0], center_coastline.points[529][1])
  p21=Point(xy[segements[3,0]][0], xy[segements[3,0]][1])
  p22=Point(xy[segements[3,1]][0], xy[segements[3,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP4=[float(pp1[0]),float(pp1[1])]

  p11=Point(center_coastline.points[530][0], center_coastline.points[530][1])
  p12=Point(center_coastline.points[531][0], center_coastline.points[531][1])
  p21=Point(xy[segements[38,0]][0], xy[segements[38,0]][1])
  p22=Point(xy[segements[38,1]][0], xy[segements[38,1]][1])
  l1 = Segment(p11,p12)
  l2 = Segment(p21,p22)
  pp1 = intersection(l1, l2)[0]
  newRiverP3=[float(pp1[0]),float(pp1[1])]

  newPoints=newPoints.tolist()
  newPoints.append([newRiverP10[0],newRiverP10[1],0.0])
  newPoints.append([newRiverP9[0],newRiverP9[1],0.0]) 
  newPoints.append([newRiverP8[0],newRiverP8[1],0.0])
  newPoints.append([newRiverP7[0],newRiverP7[1],0.0]) 
  newPoints.append([newRiverP6[0],newRiverP6[1],0.0])
  newPoints.append([newRiverP5[0],newRiverP5[1],0.0]) 
  newPoints.append([newRiverP4[0],newRiverP4[1],0.0])
  newPoints.append([newRiverP3[0],newRiverP3[1],0.0]) 
  newPoints.append([center_coastline.points[529][0],center_coastline.points[529][1],center_coastline.points[529][2]]) 
  newPoints.append([center_coastline.points[530][0],center_coastline.points[530][1],center_coastline.points[530][2]]) 
  
  riverLines=riverBound.lines.reshape(-1,3).tolist()
  popList=[38,3,79,78,121,29,40,39]
  popList=np.unique(popList)
  for i in range(len(popList)-1,-1,-1):
    riverLines.pop(popList[i])

  riverLines.append([2,54,int(len(newPoints)-10)])
  riverLines.append([2,int(len(newPoints)-10),5])
  riverLines.append([2,55,int(len(newPoints)-9)])
  riverLines.append([2,int(len(newPoints)-9),53])
  riverLines.append([2,int(len(newPoints)-9),int(len(newPoints)-10)])

  riverLines.append([2,41,int(len(newPoints)-8)])
  riverLines.append([2,int(len(newPoints)-8),38])
  riverLines.append([2,42,int(len(newPoints)-7)])
  riverLines.append([2,int(len(newPoints)-7),40])
  riverLines.append([2,int(len(newPoints)-7),int(len(newPoints)-8)])

  riverLines.append([2,107,int(len(newPoints)-5)])
  riverLines.append([2,int(len(newPoints)-5),106])
  riverLines.append([2,102,int(len(newPoints)-6)])
  riverLines.append([2,int(len(newPoints)-6),105])
  riverLines.append([2,int(len(newPoints)-6),int(len(newPoints)-5)])

  riverLines.append([2,53,int(len(newPoints)-3)])
  riverLines.append([2,int(len(newPoints)-3),4])
  riverLines.append([2,3,int(len(newPoints)-4)])
  riverLines.append([2,int(len(newPoints)-4),5])

  riverLines.append([2,int(len(newPoints)-4),int(len(newPoints)-2)])
  riverLines.append([2,int(len(newPoints)-2),int(len(newPoints)-1)])
  riverLines.append([2,int(len(newPoints)-1),int(len(newPoints)-3)])

  # riverLines.append([2,21,25])
  # for i in range(0,len(riverLines)):
  #   print(riverLines[i])
  # quit()

  celltypes = np.full(len(riverLines), CellType.LINE, dtype=np.uint8)
  riverLines=np.array(riverLines).ravel()
  riverBound=pv.UnstructuredGrid(riverLines, celltypes, newPoints)
                 
  # quit()

  xy=riverBound.points[:,0:2]
  points=pytriangle.trianglePointConn(xy)
  segements=riverBound.cells.reshape(-1,3)[:,1:3]
 
  sege_group=np.full(shape=len(segements),fill_value=3,dtype=int).tolist()
  facets=pytriangle.triangleFaceConn(segements)
  sege_group[5]=22
  sege_group[138]=-22
  sege_group[143]=-21
  sege_group[148]=-20
  sege_group[153]=4
  sege_group[154]=4
  sege_group[155]=4
  # print(facets)
  # quit()
  riverBound['sege_group']=sege_group
  riverBound.save(os.path.join(Output_path,'riverBound.vtu')) 
  
  info = triangle.MeshInfo()
  info.set_points(points)
  info.set_facets(facets, facet_markers=sege_group)
  

  max_volume=100.0
  tri = triangle.build(info,verbose=False,  attributes=True, volume_constraints=True, max_volume=max_volume, allow_boundary_steiner=True, allow_volume_steiner=True, quality_meshing=True, generate_edges=None, generate_faces=True, min_angle=None)
  

  tri_points = np.array(tri.points)
  points=np.zeros((len(tri_points),3),dtype=np.float64)
  points[:,0:2]=tri_points[:,0:2]
  tri_faces = np.array(tri.elements)


  ele_ind_new=np.full((len(tri_faces),4),3,dtype=np.int64)
  ele_ind_new[:,1:4]=tri_faces[:,0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  

  celltypes = np.full(len(tri_faces), CellType.TRIANGLE, dtype=np.uint8)
  riverTriangle = pv.UnstructuredGrid(ele_ind_new, celltypes, points)
  riverTriangle.save(os.path.join(Output_path,'riverTriangle.vtu'))

  radius=40
  riverTriangle_2D = riverTriangle.interpolate(dem2D,sharpness=2,n_points=5, radius=radius)
  riverTriangle_2D.save(os.path.join(Output_path,'riverTriangle_2D.vtu'))
  points=riverTriangle_2D.points
  cells=riverTriangle_2D.cells  
  riverTriangle_2DStl = pv.PolyData(points,cells )
  riverTriangle_2DStl['z']=riverTriangle_2D['z']
  riverTriangle_2DStl.save(os.path.join(Output_path,'riverTriangle_2D.stl'))
  points[:,2]=riverTriangle_2D['z']  
  celltypes = np.full(riverTriangle_2D.n_cells, CellType.TRIANGLE, dtype=np.uint8)
  riverTriangle_3D = pv.UnstructuredGrid(cells, celltypes, points)
  riverTriangle_3D['z']=riverTriangle_2D['z']
  riverTriangle_3D.save(os.path.join(Output_path,'riverTriangle_3D.vtu'))
  riverTriangle_3DStl = pv.PolyData(points,cells )
  riverTriangle_3DStl['z']=riverTriangle_3D['z']
  riverTriangle_3DStl.save(os.path.join(Output_path,'riverTriangle_3D.stl'))
  return riverBound
  # quit()

def topDomainCreate(Output_path,riverMaxArea,RegionalMaxArea,SiteMaxArea,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,RegionalOceanFaceTag,SiteFaceTag,riverRegionPoints,riverSitePoints,RegionalPoints,RegionalOceanPoints,SitePoints,riverBound,topRegional_scaleDomainR,topRegional_scaleDomainL,bottomRegional_scaleDomain,siteBound,center_coastlineByRiver,dem2D):
  '''River'''
  points=riverBound.points[:,0:2].tolist()
  facets=riverBound.cells.reshape(-1,3)[:,1:3].tolist()
  sege_group=riverBound['sege_group'].tolist()
  # print('facets=',facets)

  '''top Regional'''
  topRegional_R_Index=[]
  for i in range(0,len(topRegional_scaleDomainR)):
    points.append(topRegional_scaleDomainR[i])
    topRegional_R_Index.append(len(points)-1)
  topRegional_L_Index=[]
  for i in range(0,len(topRegional_scaleDomainL)):
    points.append(topRegional_scaleDomainL[i])
    topRegional_L_Index.append(len(points)-1)


  '''bottom Regional'''
  bottomRegional_Index=[]
  for i in range(0,len(bottomRegional_scaleDomain)):
    points.append(bottomRegional_scaleDomain[i])
    bottomRegional_Index.append(len(points)-1)

  '''site scale'''
  site_Index=[]
  for i in range(0,siteBound.n_points):
    points.append(siteBound.points[i][0:2].tolist())
    site_Index.append(len(points)-1)  


  '''coastline'''
  coastline_Index=[]
  for i in range(0,528):
    points.append(center_coastlineByRiver.points[i][0:2].tolist())
    coastline_Index.append(len(points)-1)
  for i in range(528,532):
    coastline_Index.append(-1)
  for i in range(532,center_coastlineByRiver.n_points):
    points.append(center_coastlineByRiver.points[i][0:2].tolist())
    coastline_Index.append(len(points)-1)
  #   print(points[-1],coastline_Index[-1])
  # print(points)
  # print(coastline_Index)

  '''top region facets'''
  for i in range(0,len(topRegional_R_Index)-1):
    facets.append([topRegional_R_Index[i],topRegional_R_Index[i+1]])
    sege_group.append(20+i)
  facets.append([topRegional_R_Index[-1],8])
  sege_group.append(22)
  facets.append([10,topRegional_L_Index[0]])
  sege_group.append(22)
  for i in range(0,len(topRegional_L_Index)-1):
    facets.append([topRegional_L_Index[i],topRegional_L_Index[i+1]])
    sege_group.append(23+i)

  facets.append([topRegional_R_Index[0],coastline_Index[0]])
  sege_group.append(10)
  facets.append([topRegional_L_Index[-1],coastline_Index[-1]])
  sege_group.append(14)
  # facets.append([topRegional_R_Index[0],bottomRegional_Index[-1]])
  # sege_group.append(1)
  # facets.append([topRegional_L_Index[-1],bottomRegional_Index[0]])
  # sege_group.append(1)
  # print(topRegional_R_Index[0],coastline_Index[0])
  # print(topRegional_L_Index[-1],coastline_Index[-1])
  # quit()

  '''bottom region facets'''
  for i in range(0,len(bottomRegional_Index)-1):
    facets.append([bottomRegional_Index[i],bottomRegional_Index[i+1]])
    sege_group.append(12+i)
  facets.append([bottomRegional_Index[0],coastline_Index[-1]])
  sege_group.append(13)
  facets.append([bottomRegional_Index[-1],coastline_Index[0]])
  sege_group.append(11)

  '''site scale facets'''
  facets.append([site_Index[0],146])
  sege_group.append(-20)
  facets.append([147,site_Index[1]])
  sege_group.append(-20)
  facets.append([site_Index[1],144])
  sege_group.append(-21)
  facets.append([145,site_Index[2]])
  sege_group.append(-21)
  facets.append([site_Index[2],143])
  sege_group.append(-22)
  facets.append([142,site_Index[3]])
  sege_group.append(-22)
  facets.append([site_Index[3],site_Index[0]])
  sege_group.append(-23)

  # print("coastline_Index=",coastline_Index)
  '''coastline facets'''
  for i in range(0,527):
    facets.append([coastline_Index[i],coastline_Index[i+1]])
    sege_group.append(4)
  for i in range(532,len(coastline_Index)-1):
    facets.append([coastline_Index[i],coastline_Index[i+1]])
    sege_group.append(4)  
  facets.append([coastline_Index[527],148])
  sege_group.append(4)
  facets.append([coastline_Index[532],149])
  sege_group.append(4)


  '''create domain vtu'''
  points2d=np.array(points)
  facets=np.array(facets)
  newPoints=np.zeros((len(points),3),dtype=np.float64)
  newPoints[:,0:2]=points2d[:,0:2]
  cells=np.full(shape=(len(facets),3),fill_value=2,dtype=np.int64)
  cells[:,1:3]=facets[:,0:2]
  celltypes = np.full(len(cells), CellType.LINE, dtype=np.uint8)
  cells=np.array(cells).ravel()
  topDomain=pv.UnstructuredGrid(cells, celltypes, newPoints)
  topDomain['facet_markers']=sege_group
  topDomain.save(os.path.join(Output_path,'topDomain.vtu'))
  # quit()

  # print('len(points)=',len(points))
  # print('len(facets)=',len(facets))
  # print('len(sege_group)=',len(sege_group))
  # quit()
  info = triangle.MeshInfo()  
  info.set_points(points)
  info.set_facets(facets, facet_markers=sege_group)
  # info.set_facets(facets)
    
  # riverPoints=np.array(
  #   [[6072.06956226, 4785.74660037],
  #    [6048.08485242, 4804.80946765],
  #    [5711.0967379 , 5156.84514156],
  #    [4051.06575167, 6689.36838925],
  #    [3292.64449156, 4978.93546805]])
  # RegionalPoints=np.array(
  #   [[5625.0148718 , 7697.73196172,],
  #    [2383.11435132, 5430.6402615 ,],
  #    [9339.32758459, 6276.51672377,]])
  # SitePoints=np.array(
  #   [[4765.31979172, 6302.37661989,],
  #    [3931.18536452, 5781.9057275 ,],
  #    [4938.39248324, 5058.49548485,]])
  # riverMaxArea=25.0*25.0/4.0
  # RegionalMaxArea=10000.0
  # SiteMaxArea=1000.0


  info.regions.resize(len(riverRegionPoints)+len(riverSitePoints)+len(RegionalPoints)+len(RegionalOceanPoints)+len(SitePoints))

  nb=-1
  for i in range(0,len(riverRegionPoints)):
    nb=nb+1
    info.regions[nb] = [
              riverRegionPoints[i,0], riverRegionPoints[i,1],  # coordinate
              riverFaceRegionalTag,  # lower element tag
              riverMaxArea,  # max area
              ]    
  for i in range(0,len(riverSitePoints)):
    nb=nb+1
    info.regions[nb] = [
              riverSitePoints[i,0], riverSitePoints[i,1],  # coordinate
              riverFaceSiteTag,  # lower element tag
              riverMaxArea,  # max area
              ]    
  for i in range(0,len(RegionalPoints)):
    nb=nb+1
    info.regions[nb] = [
              RegionalPoints[i,0], RegionalPoints[i,1],  # coordinate
              RegionalFaceTag,  # lower element tag
              RegionalMaxArea,  # max area
              ]
  for i in range(0,len(RegionalOceanPoints)):
    nb=nb+1
    info.regions[nb] = [
              RegionalOceanPoints[i,0], RegionalOceanPoints[i,1],  # coordinate
              RegionalOceanFaceTag,  # lower element tag
              RegionalMaxArea,  # max area
              ]
  for i in range(0,len(SitePoints)):
    nb=nb+1
    info.regions[nb] = [
              SitePoints[i,0], SitePoints[i,1],  # coordinate
              SiteFaceTag,  # lower element tag
              SiteMaxArea,  # max area
              ]    
  
  max_volume=np.max([riverMaxArea,RegionalMaxArea,SiteMaxArea])
  
  tri = triangle.build(info,verbose=False,  attributes=True, volume_constraints=True, max_volume=max_volume, allow_boundary_steiner=True, allow_volume_steiner=True, quality_meshing=True, generate_edges=None, generate_faces=True, min_angle=None)

  tri_points = np.array(tri.points)
  points=np.zeros((len(tri_points),3),dtype=np.float64)
  points[:,0:2]=tri_points[:,0:2]
  tri_faces = np.array(tri.elements)
  ele_ind_new=np.full((len(tri_faces),4),3,dtype=np.int64)
  ele_ind_new[:,1:4]=tri_faces[:,0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  celltypes = np.full(len(tri_faces), CellType.TRIANGLE, dtype=np.uint8)
  tri_element_attributes = np.array(tri.element_attributes)
  topDomainTri = pv.UnstructuredGrid(ele_ind_new, celltypes, points)
  topDomainTri['element_attributes']=np.array(tri_element_attributes,dtype=np.int64)
  # topDomainTri.save(os.path.join(Output_path,'topDomainTri.vtu'))

  tri_facets = np.array(tri.facets)
  celltypes = np.full(len(tri_facets), CellType.LINE, dtype=np.uint8)
  tri_facets_new=np.full((len(tri_facets),3),2,dtype=np.int64)
  tri_facets_new[:,1:3]=tri_facets[:,0:2]
  tri_facets_new=np.hstack(tri_facets_new)
  tri_facet_markers = np.array(tri.facet_markers)
  topDomain2Dfacet = pv.UnstructuredGrid(tri_facets_new, celltypes, points)
  topDomain2Dfacet['facet_markers']=tri_facet_markers
  topDomain2Dfacet.save(os.path.join(Output_path,'topDomain2Dfacet.vtu'))

  '''為了取得個區域內的點座標'''
  # print('river points')
  # print((topDomainTri.points[1738]+topDomainTri.points[1572])/2.0)
  # print((topDomainTri.points[1252]+topDomainTri.points[2817])/2.0)
  # print(topDomainTri.points[2253])
  # print(topDomainTri.points[1566])
  # print(topDomainTri.points[2626])
  # print(topDomainTri.points[1288])
  # print('Regional points')
  # print(topDomainTri.points[8227])
  # print(topDomainTri.points[7530])
  # print(topDomainTri.points[5910])
  # print('Site points')
  # print(topDomainTri.points[3784])
  # print(topDomainTri.points[3694])
  # print(topDomainTri.points[4017])
  # quit()
  radius=40
  topDomainTri_2D = topDomainTri.interpolate(dem2D,sharpness=2,n_points=5, radius=radius)
  topDomainTri_2D.save(os.path.join(Output_path,'topDomainTri_2D.vtu'))
  points=topDomainTri_2D.points
  cells=topDomainTri_2D.cells  

  points[:,2]=topDomainTri_2D['z']  
  celltypes = np.full(topDomainTri_2D.n_cells, CellType.TRIANGLE, dtype=np.uint8)
  topDomainTri_3D = pv.UnstructuredGrid(cells, celltypes, points)
  topDomainTri_3D['z']=topDomainTri_2D['z']
  topDomainTri_3D.save(os.path.join(Output_path,'topDomainTri_3D.vtu'))

  topDomainTri_3D.extract_surface().save(os.path.join(Output_path,'topDomainTri_3D.stl'))

  np.savez_compressed('topDomain.npz',topRegional_R_Index=topRegional_R_Index,topRegional_L_Index=topRegional_L_Index,bottomRegional_Index=bottomRegional_Index,site_Index=site_Index,coastline_Index=coastline_Index)

  return topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D
  
def Domain3DfacetsCreate(Output_path,facetArray_name,topRegional_R_Index,topRegional_L_Index,bottomRegional_Index,site_Index,coastline_Index,riverFaceRegionalTag,riverFaceSiteTag,RegionalFaceTag,SiteFaceTag,topDomain2Dfacet,topDomainTri_2D,topDomainTri_3D,surfBotz,siteBotZ,regionalBotZ):
  points=topDomainTri_3D.points.tolist()
  faces=topDomainTri_3D.cells.reshape(-1,4)[:,1:4].tolist()
  facet_markers=topDomainTri_2D['element_attributes'].tolist()

  '''Add surface soil coordinates of the site.'''
  '''used surfBotz set z coordinate to create surface facet'''
  if True:
    surfBot_Index=[]
    for i in range(0,topDomainTri_3D.n_points):
      points.append([points[i][0],points[i][1],points[i][2]+surfBotz])
      surfBot_Index.append(len(points)-1)

    tempfaces=copy.deepcopy(topDomainTri_3D.cells.reshape(-1,4)[:,1:4])
    tempfaces=tempfaces+topDomainTri_3D.n_points
    tempfacet_markers=copy.deepcopy(topDomainTri_2D['element_attributes'])
    tempfacet_markers=tempfacet_markers+100
    for i in range(0,len(tempfaces)):
      faces.append([tempfaces[i,0],tempfaces[i,1],tempfaces[i,2]])
      facet_markers.append(tempfacet_markers[i])



  '''Add bottom point coordinates of the site.'''
  '''used site_Index to create site facet'''
  if True:
    siteBot_Index=[]
    for i in range(0, len(site_Index)):
      points.append([points[site_Index[i]][0],points[site_Index[i]][1],siteBotZ])
      siteBot_Index.append(len(points)-1)
    # print('siteBot_Index=',siteBot_Index)

    iniMark=-20
    for i in range(0, len(site_Index)-1):
      # get top surface segement
      startNb=site_Index[i]
      nestNb=site_Index[i+1]
      lineMarker=iniMark+i*-1
      siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)      
      marker=SiteFaceTag+1+i

      # Set the surface soil face at the site scale.
      face=copy.deepcopy(siteOrderSegement)
      for j in reversed(range(0,len(siteOrderSegement))):
        face.append(surfBot_Index[siteOrderSegement[j]])
      faces.append(face)
      facet_markers.append(marker)

      # Set the matrix face at the site scale.
      face=[]
      for j in range(0,len(siteOrderSegement)):
        face.append(surfBot_Index[siteOrderSegement[j]])
      face.append(siteBot_Index[i+1])
      face.append(siteBot_Index[i])
      faces.append(face)
      facet_markers.append(marker)
    
    startNb=site_Index[-1]
    nestNb=site_Index[0]
    lineMarker=iniMark+(len(site_Index)-1)*-1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=marker+1
    # Set the surface soil face at the site scale.
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)

    # Set the matrix face at the site scale.
    marker=marker+1
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    # face=siteOrderSegement
    face.append(siteBot_Index[0])
    face.append(siteBot_Index[-1])
    faces.append(face)
    facet_markers.append(marker)

    # Set the bottom faces at the site scale.
    marker=marker+1
    face=[]
    for i in reversed(range(0,len(siteBot_Index))):
      face.append(siteBot_Index[i])
    faces.append(face)
    facet_markers.append(marker)

    # iniMark=-20
    # for i in range(0, len(site_Index)-1):
    #   startNb=site_Index[i]
    #   nestNb=site_Index[i+1]
    #   marker=iniMark+i*-1
    #   siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,marker)
    #   face=siteOrderSegement
    #   face.append(siteBot_Index[i+1])
    #   face.append(siteBot_Index[i])
    #   faces.append(face)
    #   facet_markers.append(marker)
    # startNb=site_Index[-1]
    # nestNb=site_Index[0]
    # marker=iniMark+(len(site_Index)-1)*-1
    # siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,marker)
    # face=siteOrderSegement
    # face.append(siteBot_Index[0])
    # face.append(siteBot_Index[-1])
    # faces.append(face)
    # facet_markers.append(marker)

    # face=[]
    # for i in reversed(range(0,len(siteBot_Index))):
    #   face.append(siteBot_Index[i])
    # faces.append(face)
    # marker=marker-1
    # facet_markers.append(marker)

  '''Add Regional point coordinates of the site.'''
  '''Top Regional'''
  if True:
    # print('topRegional_R_Index=',topRegional_R_Index)
    topRegional_R_Bot_Index=[]
    for i in range(0, len(topRegional_R_Index)):
      points.append([points[topRegional_R_Index[i]][0],points[topRegional_R_Index[i]][1],regionalBotZ])
      topRegional_R_Bot_Index.append(len(points)-1)
    topRegional_L_Bot_Index=[]
    for i in range(0, len(topRegional_L_Index)):
      points.append([points[topRegional_L_Index[i]][0],points[topRegional_L_Index[i]][1],regionalBotZ])
      topRegional_L_Bot_Index.append(len(points)-1)

    iniMark=20
    for i in range(0, len(topRegional_R_Index)-1):
      startNb=topRegional_R_Index[i]
      nestNb=topRegional_R_Index[i+1]
      lineMarker=iniMark+i
      siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
      marker=-2

      # Set the surface soil face at the regional scale.
      face=copy.deepcopy(siteOrderSegement)
      for j in reversed(range(0,len(siteOrderSegement))):
        face.append(surfBot_Index[siteOrderSegement[j]])
      faces.append(face)
      facet_markers.append(marker)

      # Set the matrix face at the regional scale.
      face=[]
      for j in range(0,len(siteOrderSegement)):
        face.append(surfBot_Index[siteOrderSegement[j]])
      face.append(topRegional_R_Bot_Index[i+1])
      face.append(topRegional_R_Bot_Index[i])
      faces.append(face)
      facet_markers.append(marker)

    startNb=topRegional_R_Index[-1]
    nestNb=topRegional_L_Index[0]
    lineMarker=lineMarker+1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=-2
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
        face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)
    
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    # face=siteOrderSegement
    face.append(topRegional_L_Bot_Index[0])
    face.append(topRegional_R_Bot_Index[-1])
    faces.append(face)
    facet_markers.append(marker)

    iniMark=lineMarker+1
    for i in range(0, len(topRegional_L_Index)-1):
      startNb=topRegional_L_Index[i]
      nestNb=topRegional_L_Index[i+1]
      lineMarker=iniMark+i
      siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
      marker=-2

      face=copy.deepcopy(siteOrderSegement)
      for j in reversed(range(0,len(siteOrderSegement))):
        face.append(surfBot_Index[siteOrderSegement[j]])
      faces.append(face)
      facet_markers.append(marker)

      face=[]
      for j in range(0,len(siteOrderSegement)):
        face.append(surfBot_Index[siteOrderSegement[j]])
      face.append(topRegional_L_Bot_Index[i+1])
      face.append(topRegional_L_Bot_Index[i])
      faces.append(face)
      facet_markers.append(marker)


    '''Bottom Regional'''

    coastline_Bot_Index=[]
    points.append([points[coastline_Index[0]][0],points[coastline_Index[0]][1],regionalBotZ])
    coastline_Bot_Index.append(len(points)-1)
    points.append([points[coastline_Index[-1]][0],points[coastline_Index[-1]][1],regionalBotZ])
    coastline_Bot_Index.append(len(points)-1)

    bottomRegional_Bot_Index=[]
    for i in range(0,len(bottomRegional_Index)):
      points.append([points[bottomRegional_Index[i]][0],points[bottomRegional_Index[i]][1],regionalBotZ])
      bottomRegional_Bot_Index.append(len(points)-1)
  
    startNb=topRegional_R_Index[0]
    nestNb=coastline_Index[0]
    lineMarker=10
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=-1
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    face.append(coastline_Bot_Index[0])
    face.append(topRegional_R_Bot_Index[0])
    faces.append(face)
    facet_markers.append(marker)


    startNb=coastline_Index[0]
    nestNb=bottomRegional_Index[-1]
    lineMarker=lineMarker+1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=-1
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    face.append(bottomRegional_Bot_Index[-1])
    face.append(coastline_Bot_Index[0])
    faces.append(face)
    facet_markers.append(marker)

    iniMark=lineMarker+1
    for i in range(0, len(bottomRegional_Index)-1):
      startNb=bottomRegional_Index[i]
      nestNb=bottomRegional_Index[i+1]
      lineMarker=iniMark+i
      siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
      marker=-1
      face=copy.deepcopy(siteOrderSegement)
      for j in reversed(range(0,len(siteOrderSegement))):
        face.append(surfBot_Index[siteOrderSegement[j]])
      faces.append(face)
      facet_markers.append(marker)
      face=[]
      for j in range(0,len(siteOrderSegement)):
        face.append(surfBot_Index[siteOrderSegement[j]])
      face.append(bottomRegional_Bot_Index[i+1])
      face.append(bottomRegional_Bot_Index[i])
      faces.append(face)
      facet_markers.append(marker)

    startNb=bottomRegional_Index[0]
    nestNb=coastline_Index[-1]
    lineMarker=lineMarker+1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=-1
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    face.append(coastline_Bot_Index[-1])
    face.append(bottomRegional_Bot_Index[0]) 
    faces.append(face)
    facet_markers.append(marker)


    startNb=coastline_Index[-1]
    nestNb=topRegional_L_Index[-1]
    lineMarker=lineMarker+1
    siteOrderSegement=pytriangle.orderSegement(topDomain2Dfacet,facetArray_name,startNb,nestNb,lineMarker)
    marker=-1
    face=copy.deepcopy(siteOrderSegement)
    for j in reversed(range(0,len(siteOrderSegement))):
      face.append(surfBot_Index[siteOrderSegement[j]])
    faces.append(face)
    facet_markers.append(marker)
    face=[]
    for j in range(0,len(siteOrderSegement)):
      face.append(surfBot_Index[siteOrderSegement[j]])
    face.append(topRegional_L_Bot_Index[-1])
    face.append(coastline_Bot_Index[-1])  
    faces.append(face)
    facet_markers.append(marker)


    face=[]
    marker=0
    for i in reversed(range(0,len(topRegional_R_Bot_Index))):
      face.append(topRegional_R_Bot_Index[i])
    face.append(coastline_Bot_Index[0])
    for i in reversed(range(0,len(bottomRegional_Bot_Index))):
      face.append(bottomRegional_Bot_Index[i])
    face.append(coastline_Bot_Index[-1])
    for i in reversed(range(0,len(topRegional_L_Bot_Index))):
      face.append(topRegional_L_Bot_Index[i])
    faces.append(face)
    facet_markers.append(marker)

  mtr=np.full(shape=len(points),fill_value=200,dtype=np.float64)

  cells=[]
  for i in range(0,len(faces)):
    # cell=[]
    cells.append(len(faces[i]))
    for j in range(0,len(faces[i])):
      cells.append(faces[i][j])

  polydata=pv.PolyData(points,cells)
  polydata['facet_markers']=facet_markers
  polydata.save(os.path.join(Output_path,'tet3dPolydata.vtk'))

  return points,faces,facet_markers,mtr



  





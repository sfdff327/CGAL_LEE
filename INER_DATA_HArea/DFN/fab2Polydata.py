import numpy as np
import pandas as pd
import pyvista as pv


def main(fabFilename):
  csv=pd.read_csv(fabFilename)
  print(csv)
  Transmissivity=[]
  Storativity=[]
  aperture=[]
  points=[]
  faces=[]
  nb=0
  while(nb<len(csv)):
    no_pointsIncell=csv['no_node'][nb]
    Transmissivity.append(csv['Transmissivity'][nb])
    Storativity.append(csv['Storativity'][nb])
    aperture.append(csv['aperture'][nb])
    faces.append(no_pointsIncell)
    for i in range(0,no_pointsIncell):
      nb=nb+1
      points.append([csv['Transmissivity'][nb],csv['Storativity'][nb],csv['aperture'][nb]])
      faces.append(len(points)-1)

    # print('no_pointsIncell=',no_pointsIncell)
    # print('Transmissivity=',Transmissivity)
    # print('Storativity=',Storativity)
    # print('aperture=',aperture)
    # print('points=',points)
    # print('faces=',faces)
    # print("================")
    # print("================")    
    # if nb>10:
    #   quit()
    nb=nb+1

  mesh=pv.PolyData(points,faces)
  mesh['Transmissivity']=Transmissivity
  mesh['Storativity']=Storativity
  mesh['aperture']=aperture
  mesh.save(fabFilename.split('.cs')[0]+'.vtk')

  
  # AccessDecline
if __name__ == '__main__':
  
  fabFilename='SiteScale_fracture.csv'
  main(fabFilename)
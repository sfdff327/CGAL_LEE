import json
import pandas as pd
import io
import os
import numpy as np
import matplotlib.pyplot as plt
# from matplotlib import pyplot as plt
import matplotlib
import datetime

import pyvista as pv
# zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/times_new_roman.ttf')
# zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/times new roman bold italic.ttf')
zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/kaiu.ttf')
color=["#00FF00","#FF0000","#000000","#FF00FF","#FFA600"]
linestyle_tuple = [
     ('solid',                 (0, ())),
     ('dotted',                (0, (2, 2))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))), 
     ('dashdotted',            (0, (2, 5, 5, 2))),         
     ('long dash with offset', (5, (10, 3))),          
     ('loosely dashed',        (0, (5, 10))),
     ('dashed',                (0, (5, 5))),
     ('densely dashed',        (0, (5, 1))),

     ('loosely dashdotted',    (0, (3, 10, 1, 10))),
     ('densely dashdotted',    (0, (3, 1, 1, 1))),


     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
linestyles=[]
for i, (name, linestyle) in enumerate(linestyle_tuple):
  linestyles.append(linestyle)

# mpl.rcParams['font.family'] = ['Times New Roman']

poro=np.float64(1e-5)

def find_closest(arr, val):
  idx = np.abs(arr - val).argmin()
  return idx

def appendData(
  i,DT_Fr,DT_PathLength,DT_tw,
  Hybrid_loc_Fr,Hybrid_loc_path,Hybrid_loc_time,
  Hybrid_Fr,Hybrid_PathLength,Hybrid_travelTime,
  DT_Fr_00,  DT_Fr_01,  DT_Fr_02,  DT_Fr_03,  DT_Fr_04,
  DT_Path_00,  DT_Path_01,  DT_Path_02,  DT_Path_03,  DT_Path_04,
  DT_Time_00,  DT_Time_01,  DT_Time_02,  DT_Time_03,  DT_Time_04,
  Hy_Fr_00,  Hy_Fr_01,  Hy_Fr_02,  Hy_Fr_03,  Hy_Fr_04,  
  Hy_Path_00,  Hy_Path_01,  Hy_Path_02,  Hy_Path_03,  Hy_Path_04,  
  Hy_Time_00,  Hy_Time_01,  Hy_Time_02,  Hy_Time_03,  Hy_Time_04,
  HORFr_00,  HORFr_01,  HORFr_02,  HORFr_03,  HORFr_04,
  HORPath_00,  HORPath_01,  HORPath_02,  HORPath_03,  HORPath_04,
  HORTime_00,  HORTime_01,  HORTime_02,  HORTime_03,  HORTime_04,):
  if (i==0):
    DT_Fr_00.append(DT_Fr)
    Hy_Fr_00.append(Hybrid_loc_Fr)
    HORFr_00.append(Hybrid_Fr)
    DT_Path_00.append(DT_PathLength)
    Hy_Path_00.append(Hybrid_loc_path)
    HORPath_00.append(Hybrid_PathLength)
    DT_Time_00.append(DT_tw)
    Hy_Time_00.append(Hybrid_loc_time)
    HORTime_00.append(Hybrid_travelTime)
  elif (i==1):
    DT_Fr_01.append(DT_Fr)
    Hy_Fr_01.append(Hybrid_loc_Fr)
    HORFr_01.append(Hybrid_Fr)
    DT_Path_01.append(DT_PathLength)
    Hy_Path_01.append(Hybrid_loc_path)
    HORPath_01.append(Hybrid_PathLength)
    DT_Time_01.append(DT_tw)
    Hy_Time_01.append(Hybrid_loc_time)
    HORTime_01.append(Hybrid_travelTime)
  elif (i==2):
    DT_Fr_02.append(DT_Fr)
    Hy_Fr_02.append(Hybrid_loc_Fr)
    HORFr_02.append(Hybrid_Fr)
    DT_Path_02.append(DT_PathLength)
    Hy_Path_02.append(Hybrid_loc_path)
    HORPath_02.append(Hybrid_PathLength)
    DT_Time_02.append(DT_tw)
    Hy_Time_02.append(Hybrid_loc_time)
    HORTime_02.append(Hybrid_travelTime)   
  elif (i==3):
    DT_Fr_03.append(DT_Fr)
    Hy_Fr_03.append(Hybrid_loc_Fr)
    HORFr_03.append(Hybrid_Fr)
    DT_Path_03.append(DT_PathLength)
    Hy_Path_03.append(Hybrid_loc_path)
    HORPath_03.append(Hybrid_PathLength)
    DT_Time_03.append(DT_tw)
    Hy_Time_03.append(Hybrid_loc_time)
    HORTime_03.append(Hybrid_travelTime)
  elif (i==4):
    DT_Fr_04.append(DT_Fr)
    Hy_Fr_04.append(Hybrid_loc_Fr)
    HORFr_04.append(Hybrid_Fr)
    DT_Path_04.append(DT_PathLength)
    Hy_Path_04.append(Hybrid_loc_path)
    HORPath_04.append(Hybrid_PathLength)
    DT_Time_04.append(DT_tw)
    Hy_Time_04.append(Hybrid_loc_time)
    HORTime_04.append(Hybrid_travelTime)


  return DT_Fr_00,  DT_Fr_01,  DT_Fr_02,  DT_Fr_03,  DT_Fr_04,\
  DT_Path_00,  DT_Path_01,  DT_Path_02,  DT_Path_03,  DT_Path_04,\
  DT_Time_00,  DT_Time_01,  DT_Time_02,  DT_Time_03,  DT_Time_04,\
  Hy_Fr_00,  Hy_Fr_01,  Hy_Fr_02,  Hy_Fr_03,  Hy_Fr_04,  \
  Hy_Path_00,  Hy_Path_01,  Hy_Path_02,  Hy_Path_03,  Hy_Path_04,  \
  Hy_Time_00,  Hy_Time_01,  Hy_Time_02,  Hy_Time_03,  Hy_Time_04,\
  HORFr_00,  HORFr_01,  HORFr_02,  HORFr_03,  HORFr_04,\
  HORPath_00,  HORPath_01,  HORPath_02,  HORPath_03,  HORPath_04,\
  HORTime_00,  HORTime_01,  HORTime_02,  HORTime_03,  HORTime_04

# plotHistogram(
#     Title+" DT "+"Q1 performance Measurement",
#     "DT"+"_performanceMeasurementQ1_"+Title+".png",
#     DT_Time_00,DT_Path_00,DT_Fr_00,
#     DT_Time_01,DT_Path_01,DT_Fr_01,
#     DT_Time_02,DT_Path_02,DT_Fr_02,
#     DT_Time_03,DT_Path_03,DT_Fr_03,
#     DT_Time_04,DT_Path_04,DT_Fr_04,
#     )

def plotHistogram(
    suptitle,
    file,
    DT_Time_00,DT_Path_00,DT_Fr_00,
    DT_Time_01,DT_Path_01,DT_Fr_01,
    DT_Time_02,DT_Path_02,DT_Fr_02,
    DT_Time_03,DT_Path_03,DT_Fr_03,
    DT_Time_04,DT_Path_04,DT_Fr_04,):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  '''DT 00'''
  i=0
  DT_Time_00=np.array(DT_Time_00)
  DT_Path_00=np.array(DT_Path_00)
  DT_Fr_00=np.array(DT_Fr_00)
  LogtravelTime=np.log10(DT_Time_00[~np.isnan(DT_Time_00)])      
  LogPathLength=np.log10(DT_Path_00[~np.isnan(DT_Path_00)])   
  LogFr=np.log10(DT_Fr_00[~np.isnan(DT_Fr_00)])   
  Qnb="base case" 
  hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  hist,bin_edges = np.histogram(LogFr,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)  

  '''DT 01'''
  i=1
  DT_Time_01=np.array(DT_Time_01)
  DT_Path_01=np.array(DT_Path_01)
  DT_Fr_01=np.array(DT_Fr_01)
  LogtravelTime=np.log10(DT_Time_01[~np.isnan(DT_Time_01)])      
  LogPathLength=np.log10(DT_Path_01[~np.isnan(DT_Path_01)])   
  LogFr=np.log10(DT_Fr_01[~np.isnan(DT_Fr_01)])   
  Qnb="case "+str(i)
  hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  hist,bin_edges = np.histogram(LogFr,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)  

  '''DT 02'''
  i=2
  DT_Time_02=np.array(DT_Time_02)
  DT_Path_02=np.array(DT_Path_02)
  DT_Fr_02=np.array(DT_Fr_02)
  LogtravelTime=np.log10(DT_Time_02[~np.isnan(DT_Time_02)])      
  LogPathLength=np.log10(DT_Path_02[~np.isnan(DT_Path_02)])   
  LogFr=np.log10(DT_Fr_02[~np.isnan(DT_Fr_02)])   
  Qnb="case "+str(i) 
  hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  hist,bin_edges = np.histogram(LogFr,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb) 

  '''DT 03'''
  i=3
  DT_Time_03=np.array(DT_Time_03)
  DT_Path_03=np.array(DT_Path_03)
  DT_Fr_03=np.array(DT_Fr_03)  
  LogtravelTime=np.log10(DT_Time_03[~np.isnan(DT_Time_03)])      
  LogPathLength=np.log10(DT_Path_03[~np.isnan(DT_Path_03)])   
  LogFr=np.log10(DT_Fr_03[~np.isnan(DT_Fr_03)])   
  Qnb="case "+str(i) 
  hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  hist,bin_edges = np.histogram(LogFr,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb) 

  '''DT 04'''
  i=4
  DT_Time_04=np.array(DT_Time_04)
  DT_Path_04=np.array(DT_Path_04)
  DT_Fr_04=np.array(DT_Fr_04)   
  LogtravelTime=np.log10(DT_Time_04[~np.isnan(DT_Time_04)])      
  LogPathLength=np.log10(DT_Path_04[~np.isnan(DT_Path_04)])   
  LogFr=np.log10(DT_Fr_04[~np.isnan(DT_Fr_04)])   
  Qnb="case "+str(i) 
  hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  hist,bin_edges = np.histogram(LogFr,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb) 

  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(suptitle)  
  # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  plt.savefig(file)
  plt.close() 


def DarcyTools_Hybrid_PT_pathTimeFr(darcytoolsFolderChar,folderChar,dfnFile,Title):

  DT_Ur_indcolumn=13
  DT_Qeq_indcolumn=16
  DT_PathLength_indcolumn=int(18)
  DT_tw_indcolumn=int(19)
  DT_Fr_indcolumn=int(20)

  excelF=darcytoolsFolderChar+'0/'+'case_1_Q1_performance.xlsx'
  exdf = pd.read_excel(excelF, sheet_name=None,engine='openpyxl')
  dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
  dfDarcyToolsOR = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
  DT_column_names = list(dfDarcyTools.columns) 
  # print(dfDarcyToolsOR[DT_column_names[DT_tw]]) 
  # print(dfDarcyToolsOR[DT_column_names[DT_tw]][5]) 
  # print(dfDarcyToolsOR[DT_column_names[DT_tw]][dfDarcyToolsOR.index[0]]) 
  # print(dfDarcyToolsOR)
  # quit()


  '''
  # 判斷哪些3D DH被完全截切
  # 比較darcyTool與HybridTW差異
  '''
  repository_DFNF=darcytoolsFolderChar+'0/'+'repository_DFN.txt'
  df_repository_DFNF = pd.read_csv(repository_DFNF,header=3,sep='\s+')


  dfn_poly=pv.read(folderChar+'0/'+dfnFile)
  ptStartsF=folderChar+'0/'+'Q1PT.vtk'
  ptStarts= pv.read(ptStartsF)
  
  # print(ptStarts.array_names) #['Ur', 'STL', 'FAB']

  ptStarts['dt_STL']=np.zeros(ptStarts.n_points,dtype=np.int64)
  ptStarts['dt_FAB']=np.zeros(ptStarts.n_points,dtype=np.int64)

  '''
  Q1
  ''' 
  DT_Fr_00=[]
  DT_Fr_01=[]
  DT_Fr_02=[]
  DT_Fr_03=[]
  DT_Fr_04=[]
  DT_Path_00=[]
  DT_Path_01=[]
  DT_Path_02=[]
  DT_Path_03=[]
  DT_Path_04=[]
  DT_Time_00=[]
  DT_Time_01=[]
  DT_Time_02=[]
  DT_Time_03=[]
  DT_Time_04=[]

  Hy_Fr_00=[]
  Hy_Fr_01=[]
  Hy_Fr_02=[]
  Hy_Fr_03=[]
  Hy_Fr_04=[]
  Hy_Path_00=[]
  Hy_Path_01=[]
  Hy_Path_02=[]
  Hy_Path_03=[]
  Hy_Path_04=[]
  Hy_Time_00=[]
  Hy_Time_01=[]
  Hy_Time_02=[]
  Hy_Time_03=[]
  Hy_Time_04=[]

  HORFr_00=[]
  HORFr_01=[]
  HORFr_02=[]
  HORFr_03=[]
  HORFr_04=[]
  HORPath_00=[]
  HORPath_01=[]
  HORPath_02=[]
  HORPath_03=[]
  HORPath_04=[]
  HORTime_00=[]
  HORTime_01=[]
  HORTime_02=[]
  HORTime_03=[]
  HORTime_04=[]  

  for j in range(0,len(dfDarcyToolsOR)):
  # for j in range(33,len(dfDarcyToolsOR)):
    print("j=",j,len(dfDarcyToolsOR))
    whereId = np.array(np.where((ptStarts['STL']== dfDarcyToolsOR.index[j]))).reshape(-1)
    if len(whereId)>0: #代表同時都有完全截切
      char='DH-'+str(dfDarcyToolsOR.index[j]+1)
      char_ind = np.array(np.where((df_repository_DFNF['Name']== char))).reshape(-1)
      ptStarts['dt_STL'][whereId[-1]]=dfDarcyToolsOR.index[j]
      ptStarts['dt_FAB'][whereId[-1]]=dfn_poly['No'][int(abs(df_repository_DFNF['iFPC'][char_ind[-1]])-1)]-1
      # print(dfDarcyToolsOR.index[j])
      # print(dfn_poly['No'][int(abs(df_repository_DFNF['iFPC'][char_ind[-1]])-1)]-1)
      # print(whereId[-1])
      SeedId=whereId[-1]
      index_darcyTools=dfDarcyToolsOR.index[j]
 
      for i in range(0,5):
        Q1PerformaceMeasurementFile=folderChar+str(i)+"/Q1PerformaceMeasurement.vtk"
        Q1PerformaceMeasurement=Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)
        travelTime=np.zeros(Q1PerformaceMeasurement.n_cells)
        PathLength=np.zeros(Q1PerformaceMeasurement.n_cells)
        Fr=np.zeros(Q1PerformaceMeasurement.n_cells)

        # travelTime=[]
        # PathLength=[]
        # Fr=[]
        if (SeedId in Q1PerformaceMeasurement["SeedIds"]):
          ind=Q1PerformaceMeasurement["SeedIds"]!=SeedId
          oneStreamline=Q1PerformaceMeasurement.remove_cells(ind,inplace=False)
          Hybrid_PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
          Hybrid_travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
          Hybrid_Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))

          excelF=darcytoolsFolderChar+str(i)+'/'+'case_1_Q1_performance.xlsx'
          exdf = pd.read_excel(excelF, sheet_name=None,engine='openpyxl')
          dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)        
          # dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
          # print(dfDarcyTools)
          DT_column_names = list(dfDarcyTools.columns)        
          DT_PathLength=dfDarcyTools[DT_column_names[DT_PathLength_indcolumn]][index_darcyTools]
          DT_tw=dfDarcyTools[DT_column_names[DT_tw_indcolumn]][index_darcyTools]
          DT_Fr=dfDarcyTools[DT_column_names[DT_Fr_indcolumn]][index_darcyTools]
          
          if not np.isnan(DT_PathLength):       
            # print("i=",i)
            # print("Hybrid_travelTime=",Hybrid_travelTime)
            # print("Hybrid_PathLength=",Hybrid_PathLength)
            # print("Hybrid_Fr=",Hybrid_Fr)
            # print("DT_PathLength=",DT_PathLength)
            # print("DT_tw=",DT_tw)
            # print("DT_Fr=",DT_Fr)
            idx=find_closest(oneStreamline["PathLength"], DT_PathLength)
            # print('idx=',idx)
            # print('Hybrid path=',oneStreamline["PathLength"][idx])
            # print("Hybrid time=",oneStreamline["travelTime"][idx]/(86400*365))
            # print("Hybrid Fr=",oneStreamline["Fr"][idx]/(86400*365))
            appendData(
            i,DT_Fr,DT_PathLength,DT_tw,
            oneStreamline["Fr"][idx]/(86400*365),oneStreamline["PathLength"][idx],oneStreamline["travelTime"][idx]/(86400*365),
            Hybrid_Fr,Hybrid_PathLength,Hybrid_travelTime,
            DT_Fr_00,  DT_Fr_01,  DT_Fr_02,  DT_Fr_03,  DT_Fr_04,
            DT_Path_00,  DT_Path_01,  DT_Path_02,  DT_Path_03,  DT_Path_04,
            DT_Time_00,  DT_Time_01,  DT_Time_02,  DT_Time_03,  DT_Time_04,
            Hy_Fr_00,  Hy_Fr_01,  Hy_Fr_02,  Hy_Fr_03,  Hy_Fr_04,  
            Hy_Path_00,  Hy_Path_01,  Hy_Path_02,  Hy_Path_03,  Hy_Path_04,  
            Hy_Time_00,  Hy_Time_01,  Hy_Time_02,  Hy_Time_03,  Hy_Time_04,
            HORFr_00,  HORFr_01,  HORFr_02,  HORFr_03,  HORFr_04,
            HORPath_00,  HORPath_01,  HORPath_02,  HORPath_03,  HORPath_04,
            HORTime_00,  HORTime_01,  HORTime_02,  HORTime_03,  HORTime_04,)

  print('DT_Fr_00=',DT_Fr_00)
  print('DT_Fr_01=',DT_Fr_01)
  print('DT_Fr_02=',DT_Fr_02)
  print('DT_Fr_03=',DT_Fr_03)
  print('DT_Fr_04=',DT_Fr_04)

  plotHistogram(
    Title+" DT "+"Q1 performance Measurement",
    "DT"+"_performanceMeasurementQ1_"+Title+".png",
    DT_Time_00,DT_Path_00,DT_Fr_00,
    DT_Time_01,DT_Path_01,DT_Fr_01,
    DT_Time_02,DT_Path_02,DT_Fr_02,
    DT_Time_03,DT_Path_03,DT_Fr_03,
    DT_Time_04,DT_Path_04,DT_Fr_04,
    )
  
  plotHistogram(
    Title+" HybridTW "+"Q1 performance Measurement",
    "HybridTW"+"_performanceMeasurementQ1_"+Title+".png",
    Hy_Time_00,Hy_Path_00,Hy_Fr_00,
    Hy_Time_01,Hy_Path_01,Hy_Fr_01,
    Hy_Time_02,Hy_Path_02,Hy_Fr_02,
    Hy_Time_03,Hy_Path_03,Hy_Fr_03,
    Hy_Time_04,Hy_Path_04,Hy_Fr_04,
    )  

  plotHistogram(
    Title+" HybridTW ALL "+"Q1 performance Measurement",
    "HybridTW_ALL"+"_performanceMeasurementQ1_"+Title+".png",
    HORTime_00,HORPath_00,HORFr_00,
    HORTime_01,HORPath_01,HORFr_01,
    HORTime_02,HORPath_02,HORFr_02,
    HORTime_03,HORPath_03,HORFr_03,
    HORTime_04,HORPath_04,HORFr_04,    
    )    
  
  # DT_Time_01=np.array(DT_Time_00)-np.array(DT_Time_01)
  # DT_Time_02=np.array(DT_Time_00)-np.array(DT_Time_02)
  # DT_Time_03=np.array(DT_Time_00)-np.array(DT_Time_03)
  # DT_Time_04=np.array(DT_Time_00)-np.array(DT_Time_04)
  # DT_Time_00=np.array(DT_Time_00)-np.array(DT_Time_00)
  # DT_Path_01=np.array(DT_Path_00)-np.array(DT_Path_01)
  # DT_Path_02=np.array(DT_Path_00)-np.array(DT_Path_02)
  # DT_Path_03=np.array(DT_Path_00)-np.array(DT_Path_03)
  # DT_Path_04=np.array(DT_Path_00)-np.array(DT_Path_04)
  # DT_Path_00=np.array(DT_Path_00)-np.array(DT_Path_00)
  # DT_Fr_01=np.array(DT_Fr_00)-np.array(DT_Fr_01)
  # DT_Fr_02=np.array(DT_Fr_00)-np.array(DT_Fr_02)
  # DT_Fr_03=np.array(DT_Fr_00)-np.array(DT_Fr_03)
  # DT_Fr_04=np.array(DT_Fr_00)-np.array(DT_Fr_04)
  # DT_Fr_00=np.array(DT_Fr_00)-np.array(DT_Fr_00)
  # plotHistogram(
  #   Title+" DT_diff "+"Q1 performance Measurement",
  #   "DT_diff"+"_performanceMeasurementQ1_"+Title+".png",
  #   DT_Time_00,DT_Path_00,DT_Fr_00,
  #   DT_Time_01,DT_Path_01,DT_Fr_01,
  #   DT_Time_02,DT_Path_02,DT_Fr_02,
  #   DT_Time_03,DT_Path_03,DT_Fr_03,
  #   DT_Time_04,DT_Path_04,DT_Fr_04,
  #   )
  # quit() 

  # bins=400
  # fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  
  # '''
  # Q1
  # '''  
  # for i in range(0,5):
  #   Q1PerformaceMeasurementFile=folderChar+str(i)+"/Q1PerformaceMeasurement.vtk"
  #   print(Q1PerformaceMeasurementFile)
  #   Q1PerformaceMeasurement=Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)
  #   travelTime=np.zeros(Q1PerformaceMeasurement.n_cells)
  #   PathLength=np.zeros(Q1PerformaceMeasurement.n_cells)
  #   Fr=np.zeros(Q1PerformaceMeasurement.n_cells)
  #   travelTime=[]
  #   PathLength=[]
  #   Fr=[]

  #   nb=-1
  #   for SeedId in Q1PerformaceMeasurement["SeedIds"]:
  #     ind=Q1PerformaceMeasurement["SeedIds"]!=SeedId
  #     print("i",i,"SeedId=",SeedId,len(Q1PerformaceMeasurement["SeedIds"]))
  #     oneStreamline=Q1PerformaceMeasurement.remove_cells(ind,inplace=False)
  #     # nb=nb+1
  #     # travelTime[nb]=oneStreamline["travelTime"][oneStreamline.n_points-1]/86400*365
  #     # PathLength[nb]=oneStreamline["PathLength"][oneStreamline.n_points-1]
  #     # Fr[nb]=oneStreamline["Fr"][oneStreamline.n_points-1]/86400*365
  #     if oneStreamline["PathLength"][oneStreamline.n_points-1]>1000 and oneStreamline["PathLength"][oneStreamline.n_points-1]<100000:
  #       travelTime.append(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
  #       PathLength.append(oneStreamline["PathLength"][oneStreamline.n_points-1])
  #       Fr.append(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        
  #   travelTime=np.array(travelTime)
  #   PathLength=np.array(PathLength)
  #   Fr=np.array(Fr)
  #   LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
  #   LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])   
  #   LogFr=np.log10(Fr[~np.isnan(Fr)])      

  #   if i==0:
  #     Qnb="base case"
  #   else:
  #     Qnb="case "+str(i) 

  #   hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
  #   cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  #   ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  #   hist,bin_edges = np.histogram(LogFr,bins = bins)
  #   cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  #   ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
  #   hist,bin_edges = np.histogram(LogPathLength,bins = bins)
  #   cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  #   ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  # ax1.set_xlabel("log10 year")
  # ax1.set_ylabel("Fraction(-)")
  # ax1.set_title("Travel Time")
  # # ax1.legend()
  # ax2.set_xlabel("log10 year/m")
  # ax2.set_ylabel("Fraction(-)")
  # ax2.set_title("F$_{r}$")  
  # # ax2.legend()
  # ax3.set_xlabel("log10 m")
  # ax3.set_ylabel("Fraction(-)")
  # ax3.set_title("Path Length")
  # ax3.legend(frameon=False)
  # fig.suptitle(Title+" Q1 performance Measurement")  
  # # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  # plt.savefig("performanceMeasurementQ1_"+Title+".png")
  # plt.close()    


def Q1eqFluxCompare(folderChar,Title):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  
  '''
  Q1
  '''  
  for i in range(0,5):
    Q1PerformaceMeasurementFile=folderChar+str(i)+"/Q1PerformaceMeasurement.vtk"
    Q1PerformaceMeasurement=Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)
    travelTime=np.zeros(Q1PerformaceMeasurement.n_cells)
    PathLength=np.zeros(Q1PerformaceMeasurement.n_cells)
    Fr=np.zeros(Q1PerformaceMeasurement.n_cells)
    travelTime=[]
    PathLength=[]
    Fr=[]

    nb=-1
    for SeedId in Q1PerformaceMeasurement["SeedIds"]:
      ind=Q1PerformaceMeasurement["SeedIds"]!=SeedId
      oneStreamline=Q1PerformaceMeasurement.remove_cells(ind,inplace=False)
      # nb=nb+1
      # travelTime[nb]=oneStreamline["travelTime"][oneStreamline.n_points-1]/86400*365
      # PathLength[nb]=oneStreamline["PathLength"][oneStreamline.n_points-1]
      # Fr[nb]=oneStreamline["Fr"][oneStreamline.n_points-1]/86400*365
      if oneStreamline["PathLength"][oneStreamline.n_points-1]>1000 and oneStreamline["PathLength"][oneStreamline.n_points-1]<100000:
        travelTime.append(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength.append(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr.append(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])   
    LogFr=np.log10(Fr[~np.isnan(Fr)])      

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q1 performance Measurement")  
  # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  plt.savefig("performanceMeasurementQ1_"+Title+".png")
  plt.close()    

def getQ2Endpoint(folderChar,out_filename):
  data={
    "travelTime":{},
    "Fr":{},
    "PathLength":{}
  }
  for i in range(0,5):
    PerformaceMeasurementFile=folderChar+str(i)+"/Q2PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    travelTime=np.zeros(PerformaceMeasurement.n_cells)
    PathLength=np.zeros(PerformaceMeasurement.n_cells)
    Fr=np.zeros(PerformaceMeasurement.n_cells)

    nb=-1
    for SeedId in PerformaceMeasurement["SeedIds"]:
      # print(SeedId,PerformaceMeasurement.n_cells)
      ind=PerformaceMeasurement["SeedIds"]==SeedId
      oneStreamline=PerformaceMeasurement.extract_cells(ind)   
      nb=nb+1
      travelTime[nb]=np.max(oneStreamline["travelTime"])/(86400.*365.)
      PathLength[nb]=np.max(oneStreamline["PathLength"])
      Fr[nb]=np.max(oneStreamline["Fr"])/(86400*365)
    data["travelTime"]["case "+str(i)]=travelTime.tolist()
    data["PathLength"]["case "+str(i)]=PathLength.tolist()
    data["Fr"]["case "+str(i)]=Fr.tolist()
  out_filename = io.open(out_filename, mode="w", encoding="utf-8")
  # print(data)
  json.dump(data, out_filename, indent=2,ensure_ascii=False)       

def Q2eqFluxCompare(folderChar,Title,Q2DensityFlowPM):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)

  # print(Q2DensityFlowPM["travelTime"]["case 0"][0:5])
  # print(len(Q2DensityFlowPM["travelTime"]["case 0"]))
  # quit()

  '''
  Q2
  '''  
  for i in range(0,5):
    travelTime=[]
    PathLength=[]
    Fr=[]
    for j in range(0,len(Q2DensityFlowPM["travelTime"]["case "+str(i)])):
      if Q2DensityFlowPM["PathLength"]["case "+str(i)][j]>1000 and Q2DensityFlowPM["PathLength"]["case "+str(i)][j]<100000:
        travelTime.append(Q2DensityFlowPM["travelTime"]["case "+str(i)][j])
        PathLength.append(Q2DensityFlowPM["PathLength"]["case "+str(i)][j])
        Fr.append(Q2DensityFlowPM["Fr"]["case "+str(i)][j])
    
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])      
    LogFr=np.log10(Fr[~np.isnan(Fr)])      

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q2 performance Measurement")
  # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  plt.savefig("performanceMeasurementQ2_"+Title+".png")
  plt.close()      


def Q3eqFluxCompare(folderChar,Title):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  '''
  Q3
  '''  
  for i in range(0,5):
    PerformaceMeasurementFile=folderChar+str(i)+"/Q3PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    travelTime=np.zeros(PerformaceMeasurement.n_cells)
    PathLength=np.zeros(PerformaceMeasurement.n_cells)
    travelTime=[]
    PathLength=[]
    Fr=[]
    nb=-1
    for SeedId in PerformaceMeasurement["SeedIds"]:
      ind=PerformaceMeasurement["SeedIds"]!=SeedId
      oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
      # nb=nb+1
      # travelTime[nb]=oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365)
      # PathLength[nb]=oneStreamline["PathLength"][oneStreamline.n_points-1]
      if oneStreamline["PathLength"][oneStreamline.n_points-1]>1000 and oneStreamline["PathLength"][oneStreamline.n_points-1]<100000:
        travelTime.append(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength.append(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr.append(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])   
    LogFr=np.log10(Fr[~np.isnan(Fr)])   

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q3 performance Measurement")
  plt.savefig("performanceMeasurementQ3_"+Title+".png")
  plt.close()    


def DarcyToolsHybrid_ini_Velocity(darcytoolsFolderChar,folderChar,xlabel,xlabelUr,ylabel,Title):

  DT_Ur_indcolumn=13
  DT_Qeq_indcolumn=16

  '''
  Q1 Qeq
  '''
  '''# HybridTW'''
  plotTitle=Title+" - Q1 initial Q$_{eq}$_HybridTW"
  savePngFile="Q1_ptQeq_"+Title+"_HybridTW.png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q1EqFlux"][~np.isnan(Q123FullConnEqFlux["Q1EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''# DarcyTools'''
  plotTitle=Title+" - Q1 initial Q$_{eq}$_DarcyTools"
  savePngFile="Q1_ptQeq_"+Title+"_DarcyTools.png"
  for i in range(0,5):    
    excelF=darcytoolsFolderChar+str(i)+'/'+'case_1_Q1_performance.xlsx'
    dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
    dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
    DT_column_names = list(dfDarcyTools.columns)
    urLen=dfDarcyTools[DT_column_names[DT_Qeq_indcolumn]]
    # urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)]
    logur=np.log10(localUr)   
    bins=400
    if i==0:
      Qnb="base case_DarcyTools"
    else:
      Qnb="case "+str(i)+"_DarcyTools" 
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q2 Qeq
  '''
  '''# HybridTW'''
  plotTitle=Title+" - Q2 initial Q$_{eq}$_HybridTW"
  savePngFile="Q2_ptQeq_"+Title+"_HybridTW.png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q2EqFlux"][~np.isnan(Q123FullConnEqFlux["Q2EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()


  '''# DarcyTools'''
  plotTitle=Title+" - Q2 initial Q$_{eq}$__DarcyTools"
  savePngFile="Q2_ptQeq_"+Title+"_DarcyTools.png"
  for i in range(0,5):    
    excelF=darcytoolsFolderChar+str(i)+'/'+'case_1_Q2_performance.xlsx'
    dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
    dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
    DT_column_names = list(dfDarcyTools.columns)    
    urLen=dfDarcyTools[DT_column_names[DT_Qeq_indcolumn]]
    # urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)]
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case_DarcyTools"
    else:
      Qnb="case "+str(i)+"_DarcyTools" 
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q1 Ur
  '''
  '''# HybridTW'''
  plotTitle=Title+" - Q1 initial U$_{r}$_HybridTW"
  savePngFile="Q1_ptUr_"+Title+"_HybridTW.png"
  for i in range(0,5):    
    PTFile=folderChar+str(i)+'/Q1PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case_HybridTW"
    else:
      Qnb="case "+str(i)+"_HybridTW"  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''# DarcyTools'''
  plotTitle=Title+" - Q1 initial U$_{r}$_DarcyTools"
  savePngFile="Q1_ptUr_"+Title+"_DarcyTools.png"
  for i in range(0,5):
    excelF=darcytoolsFolderChar+str(i)+'/'+'case_1_Q1_performance.xlsx'
    dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
    dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
    DT_column_names = list(dfDarcyTools.columns)        
    urLen=dfDarcyTools[DT_column_names[DT_Ur_indcolumn]]
    # urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)]
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case_DarcyTools"
    else:
      Qnb="case "+str(i)+"_DarcyTools" 
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()
  # quit()

  '''
  Q2 Ur
  '''
  '''# HybridTW'''
  plotTitle=Title+" - Q2 initial U$_{r}$_HybridTW"
  savePngFile="Q2_ptUr_"+Title+"_HybridTW.png"
  for i in range(0,5):    
    PTFile=folderChar+str(i)+'/Q2PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case_HybridTW"
    else:
      Qnb="case "+str(i)+"_HybridTW"  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''# DarcyTools'''
  plotTitle=Title+" - Q2 initial U$_{r}$_DarcyTools"
  savePngFile="Q2_ptUr_"+Title+"_DarcyTools.png"
  for i in range(0,5):    
    excelF=darcytoolsFolderChar+str(i)+'/'+'case_1_Q2_performance.xlsx'
    dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
    dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
    DT_column_names = list(dfDarcyTools.columns)
    urLen=dfDarcyTools[DT_column_names[DT_Ur_indcolumn]]
    # urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)]
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case_DarcyTools"
    else:
      Qnb="case "+str(i)+"_DarcyTools" 
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()
  # quit()









def initialVelocityCompare(folderChar,xlabel,xlabelUr,ylabel,Title):
  '''
  Q1 Qeq
  '''
  plotTitle=Title+" - Q1 initial Q$_{eq}$"
  savePngFile="Q1_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q1EqFlux"][~np.isnan(Q123FullConnEqFlux["Q1EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q1 Ur
  '''
  plotTitle=Title+" - Q1 initial U$_{r}$"
  savePngFile="Q1_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q1PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q2 Qeq
  '''
  plotTitle=Title+" - Q2 initial Q$_{eq}$"
  savePngFile="Q2_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q2EqFlux"][~np.isnan(Q123FullConnEqFlux["Q2EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q2 Ur
  '''
  plotTitle=Title+" - Q2 initial U$_{r}$"
  savePngFile="Q2_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q2PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q3 Qeq
  '''
  plotTitle=Title+" - Q3 initial Q$_{eq}$"
  savePngFile="Q3_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)    
    EQFlux=Q123FullConnEqFlux["Q3EqFlux"][~np.isnan(Q123FullConnEqFlux["Q3EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    npPT=np.zeros((bins,6),dtype=np.float64)  
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    # npPT[:,0]=bin_edges[1:]
    # npPT[:,1]=cdf[:]
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q3 Ur
  '''
  plotTitle=Title+" - Q3 initial U$_{r}$"
  savePngFile="Q3_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q3PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

def readEachPM(folderChar,Title):
  for k in range(0,5):
    Qnb="Q1"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)    
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q1EqFlux"] *(86400*365)
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==508 and QPt["FAB"][j]==16 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q1")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)

    Qnb="Q2"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q2EqFlux"] *(86400*365)
    print("Q2 EQFlux")
    print(EQFlux)    
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==508 and QPt["FAB"][j]==68 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close        
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q2")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)

    Qnb="Q3"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q3EqFlux"] *(86400*365)
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==80 and QPt["FAB"][j]==16 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q3")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)        
    # quit()

def PlotPT(folderChar,Title):
  for k in range(0,5):
    Qnb="Q1"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close

    Qnb="Q2"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close

    Qnb="Q3"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close
  # quit()

def loadPerformaceMeasurement(darcytoolsFolderChar,hybridFolderChar,dfnFile):
  q123Folder=r'../../HybridTW/001_stlToMesh'
  dh=pv.read(q123Folder+r'/dh.vtk')
  cells=dh.faces.reshape(dh.n_cells,4)
  celltypes = np.full(shape=dh.n_cells,fill_value=pv.CellType.TRIANGLE,dtype=np.int8)
  dh=pv.UnstructuredGrid(cells, celltypes, dh.points)
  dh.save("dh.vtu")
  # quit()
  
  '''
  # 判斷哪些3D DH被完全截切
  # 比較darcyTool與HybridTW差異
  '''
  repository_DFNF=darcytoolsFolderChar+'0/'+'repository_DFN.txt'
  df_repository_DFNF = pd.read_csv(repository_DFNF,header=3,sep='\s+')
  # print(df_repository_DFNF)
  # quit()

  excelF=darcytoolsFolderChar+'0/'+'case_1_Q1_performance_base.xlsx'
  # print(excelF)
  exdf = pd.read_excel(excelF, sheet_name=None,engine='openpyxl')
  no_sheet=len(exdf) 
  dfDarcyTools = pd.read_excel(excelF,sheet_name= 0,engine='openpyxl',skiprows=0)
  dfDarcyTools = dfDarcyTools[dfDarcyTools['X - End coordinate'].notna()]
  # DT_column_names = list(dfDarcyTools.columns.values)
  # DT_column_names = dfDarcyTools.columns.values.tolist()
  DT_column_names = list(dfDarcyTools.columns)
  # print(DT_column_names)

  dfn_poly=pv.read(hybridFolderChar+'0/'+dfnFile)
  # print(dfn_poly['No'])
  dfnArea=dfn_poly.triangulate(inplace=False)
  cells=dfnArea.faces.reshape(dfnArea.n_cells,4)
  celltypes = np.full(shape=dfnArea.n_cells,fill_value=pv.CellType.TRIANGLE,dtype=np.int8)
  dfnArea=pv.UnstructuredGrid(cells, celltypes, dfnArea.points)
  area=dfnArea.compute_cell_quality(quality_measure='area')
  # print(np.min(area['CellQuality']))
  # print(area.cells)
  area.save("dfnArea.vtu")
  # quit()

  ptStartsF=hybridFolderChar+'0/'+'Q1PT.vtk'
  ptStarts= pv.read(ptStartsF)
  
  # print(ptStarts.array_names) #['Ur', 'STL', 'FAB']

  ptStarts['dt_STL']=np.zeros(ptStarts.n_points,dtype=np.int64)
  ptStarts['dt_FAB']=np.zeros(ptStarts.n_points,dtype=np.int64)

  ''' # 比對dfDarcyTools dh編號是否存在於df_Hybrid'''
  sameDH=[]
  diffDH=[]
  for i in range(0,len(dfDarcyTools)):
    ind = np.array(np.where((ptStarts['STL']== dfDarcyTools.index[i]))).reshape(-1)
    if len(ind)>0: #代表同時都有完全截切
      char='DH-'+str(dfDarcyTools.index[i]+1)
      char_ind = np.array(np.where((df_repository_DFNF['Name']== char))).reshape(-1)
      ptStarts['dt_STL'][ind[-1]]=dfDarcyTools.index[i]
      ptStarts['dt_FAB'][ind[-1]]=dfn_poly['No'][int(abs(df_repository_DFNF['iFPC'][char_ind[-1]])-1)]-1
      # print("DT vs Hy STL=",ptStarts['dt_STL'][ind[-1]]+1,ptStarts['STL'][ind[-1]]+1, 'FAB=',ptStarts['dt_FAB'][ind[-1]]+1,ptStarts['FAB'][ind[-1]]+1)
      if (ptStarts['dt_FAB'][ind[-1]]==ptStarts['FAB'][ind[-1]]):
        sameDH.append(ptStarts['dt_STL'][ind[-1]]+1)
      else:
        print("DT vs Hy STL=",ptStarts['dt_STL'][ind[-1]]+1,ptStarts['STL'][ind[-1]]+1, 'FAB=',ptStarts['dt_FAB'][ind[-1]]+1,ptStarts['FAB'][ind[-1]]+1)
        diffDH.append(ptStarts['dt_STL'][ind[-1]]+1)
      # quit()
  print("total DH=",len(dfDarcyTools))
  print("sameDH=",sameDH)
  print("diffDH=",diffDH)


  for i in range(0,ptStarts.n_points):
    ind = np.array(np.where((dfDarcyTools.index== ptStarts['STL'][i]))).reshape(-1)
    if len(ind)<=0: #代表只有Hybrid有完全截切
      print("hybrid not in DT: DH is #",ptStarts['STL'][i]+1,'Frac is #',ptStarts['FAB'][i]+1)

def getSeedIdStreamline2Vtu(SeedId,folderChar,Q123,Title):
  from pyvista import CellType

  for i in range(0,5):
    Q1PerformaceMeasurementFile=folderChar+str(i)+"/"+Q123+"PerformaceMeasurement.vtk"
    print(Q1PerformaceMeasurementFile)
    Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)



    ind=Q1PerformaceMeasurement["SeedIds"]!=SeedId
    # print(ind)
    oneStreamline=Q1PerformaceMeasurement.remove_cells(ind,inplace=False)
    points=oneStreamline.points
    cells=oneStreamline.cells
    nb=0
    for SeedId in oneStreamline["SeedIds"]:
      # print("SeedId=",SeedId)
      no_edge=cells[nb]
      start=cells[nb+1]
      end=cells[nb+cells[nb]]
      # print("start=",start)
      # print("end=",end)
      temp = np.linspace((2,start,start+1),(2,end-1,end),no_edge,dtype=np.int64)
      # print(temp)
      if nb==0:
        lines=temp
      else:
        lines=np.concatenate((lines,temp),axis=0)
      nb=nb+cells[nb]+1
    
    cell_type = np.full(shape=len(lines),fill_value=CellType.LINE,dtype= np.int8)
    grid = pv.UnstructuredGrid(lines, cell_type, points)
    file=Q123+'_'+Title+'_'+str(i)+'_'+str(SeedId)+'.vtu'
    grid['totalHead']=oneStreamline['totalHead']
    grid['travelTime']=oneStreamline['travelTime']/(86400*365)
    grid['PathLength']=oneStreamline['PathLength']
    grid['Fr']=oneStreamline['Fr']
    grid.save(file)



def streamline2Vtu(folderChar,Q123,Title):
  from pyvista import CellType
  # a = np.linspace((2,0,1),(2,9,10),10,dtype=np.int64)
  # b = np.linspace((2,11,12),(2,19,20),10,dtype=np.int64)
  # a = np.concatenate((a,b),axis=0)
  # print("a=",a)
  # print("b=",b)
  # print("c=",c)
  # quit()

  for i in range(0,5):
    Q1PerformaceMeasurementFile=folderChar+str(i)+"/"+Q123+"PerformaceMeasurement.vtk"
    print(Q1PerformaceMeasurementFile)
    Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)

    totalHead=Q1PerformaceMeasurement['totalHead']
    travelTime=Q1PerformaceMeasurement['travelTime']
    PathLength=Q1PerformaceMeasurement['PathLength']
    Fr=Q1PerformaceMeasurement['Fr']

    points=Q1PerformaceMeasurement.points
    cells=Q1PerformaceMeasurement.cells

    nb=0
    for SeedId in Q1PerformaceMeasurement["SeedIds"]:
      # print("SeedId=",SeedId)
      no_edge=cells[nb]
      start=cells[nb+1]
      end=cells[nb+cells[nb]]
      # print("start=",start)
      # print("end=",end)
      temp = np.linspace((2,start,start+1),(2,end-1,end),no_edge,dtype=np.int64)
      # print(temp)
      if nb==0:
        lines=temp
      else:
        lines=np.concatenate((lines,temp),axis=0)
      nb=nb+cells[nb]+1
    
    cell_type = np.full(shape=len(lines),fill_value=CellType.LINE,dtype= np.int8)
    grid = pv.UnstructuredGrid(lines, cell_type, points)
    file=Q123+'_'+Title+'_'+str(i)+'.vtu'
    grid['totalHead']=Q1PerformaceMeasurement['totalHead']
    grid['travelTime']=Q1PerformaceMeasurement['travelTime']/(86400*365)
    grid['PathLength']=Q1PerformaceMeasurement['PathLength']
    grid['Fr']=Q1PerformaceMeasurement['Fr']
    grid.save(file)

    '''
    SeedIds=0    
    nb=0
    數量:cells[nb]=cells[0]=812
    eleind = cells[nb+1]:cells[nb+cells[nb]]=cells[1]:cells[812]=0:811
    0:811

    SeedIds=1
    nb=nb+cells[nb]+1=0+812+1=813
    數量:cells[nb]=cells[813]=836
    eleind = nb+1 : nb+cells[nb] = 
    814 : 813+836 = 814 : 1649
    812:1647

    SeedIds=2
    nb=nb+cells[nb]+1=813+836+1=1650
    eleind = nb+1 : nb+cells[nb] = 
    1650 : 1650+778 = 1650 : 2428
    1648:2425
    '''

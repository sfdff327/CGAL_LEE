import numpy as np
import pyvista as pv
import triMesh3D
import pandas as pd

if __name__ == "__main__":
  # 0. 母岩水力傳導係數:1.0e-10 cm/s 斷層水力傳導係數 5.0e-6 cm/s
  # 1. 母岩水力傳導係數:4.1e-12 cm/s
  # 2. 母岩水力傳導係數:1.0e-09 cm/s
  # 3. 斷層水力傳導係數:1.0e-04 cm/s
  # 4. 斷層水力傳導係數:1.0e-08 cm/s

  '''水文參數設定'''
  Kfracture=5.0e-5
  # poro_fracture=0.4
  poro_fracture=1.5e-2 #F2's poro
  fracture_alpha_w=1.0
  aperture=5.0e-6
  alpha=0.008
  sa=0.0
  density=1.0*(1.0+alpha*sa)
  # Krock=1.0e-10
   
  matrix_alpha_w=4.8
  Ksurface=1.0e-5
  poro_surface=1.e-3 
  surface_alpha_w=4.8


  F1F2K=5.0e-6 # 基準案例
  # F1F2K=1.0e-04 # 3號案例 改斷層
  # F1F2K=1.0e-08 # 4號案例 改斷層
  poro_F1F2K=0.4
  F1F2K_alpha_w=1.0 

  Krock=[1.0e-10,3.3e-8] # 基準案例'matrix', 'edz'
  # Krock=[4.1e-12,3.3e-8] # 1號案例只有改母岩 'matrix', 'edz'
  # Krock=[1.0e-09,3.3e-8] # 2號案例只有改母岩 'matrix', 'edz'
  poro_rock=[1.0e-5,1.0e-4] # 'matrix', 'edz'
  edzThickness=0.3

  #########
  regionName= ['matrix', 'edz', 'mt', 'dt', 'dh']
  regionTag = [       5,   100,  101,   102, 103]
  addRegion = 2

  profileMesh=pv.read('profileMesh.vtk')
  meshTetraRefinementHybridFolw=pv.read('meshTetraRefinementHybridFolwCon.vtu')
  meshTetraRefinementHybridFolw['z']=meshTetraRefinementHybridFolw.cell_centers().points[:,2:3]
  ind = np.argwhere(meshTetraRefinementHybridFolw["element_attributes"]!=-1)
  topMesh=meshTetraRefinementHybridFolw.remove_cells(ind,inplace=False)
  print(np.max(meshTetraRefinementHybridFolw['z']),np.min(meshTetraRefinementHybridFolw['z']))
  ind = np.argwhere(meshTetraRefinementHybridFolw['z']<-10.0)
  seafaceMesh=meshTetraRefinementHybridFolw.remove_cells(ind,inplace=False)
  seafaceMesh.save("seafaceMesh.vtk")
  topNodeNb = np.unique(np.concatenate((topMesh['nodeNb'], seafaceMesh['nodeNb'])))

  # volQulity=meshTetraRefinementHybridFolw.compute_cell_quality(quality_measure='volume', null_value=-1.0, progress_bar=False)
  # print(volQulity.array_names)
  # print(np.unique(volQulity['CellQuality']))
  # print("vol=",volQulity['CellQuality'][429374])
  # quit()
  # print(meshTetraRefinementHybridFolw.array_names)
  # ['nodeNb', 'mtr', 'vectors', 'velocityLen', 'totalHead', 'eleNb', 'element_attributes']  

  bcTopCon=0.0
  topNodeNbCon=np.full(shape=len(topNodeNb),fill_value=bcTopCon,dtype=np.float64)
  bcProfileCon=3.2  
  profileMesh['bcProfileCon']=np.full(shape=profileMesh.n_points,fill_value=bcProfileCon,dtype=np.float64)
  print('start FEM_AGMG_ADE_BC1_inf')
  with open('FEM_AGMG_ADE_BC1_inf.dat','w',encoding = 'utf-8') as f:
    # np.savetxt(f, np.column_stack((topMesh['nodeNb'][0:]+1,topMesh['bcTopCon'][0:])),fmt=["%u","%e"])
    np.savetxt(f, np.column_stack((topNodeNb[0:]+1,topNodeNbCon[0:])),fmt=["%u","%e"])
    np.savetxt(f, np.column_stack((profileMesh['nodeNb'][0:]+1,profileMesh['bcProfileCon'][0:])),fmt=["%u","%e"])
    f.close()
  # print("profileMesh.n_points=",profileMesh.n_points)
  # print(123448 in profileMesh['nodeNb'])

  meshTetraRefinementHybridFolw['vectors']=meshTetraRefinementHybridFolw['vectors']*86400.0
  print('start FEM_AGMG_velocity')
  with open('FEM_AGMG_velocity.dat','w',encoding = 'utf-8') as f:
    np.savetxt(f, np.column_stack((meshTetraRefinementHybridFolw['vectors'][0:,0],meshTetraRefinementHybridFolw['vectors'][0:,1],meshTetraRefinementHybridFolw['vectors'][0:,2])),delimiter=' ',)
    f.close()

  print('input_initial_C.dat')
  with open('input_initial_C.dat','w',encoding = 'utf-8') as f:
    np.savetxt(f, np.column_stack((meshTetraRefinementHybridFolw['nodeNb'][0:]+1,meshTetraRefinementHybridFolw['con'][0:])),fmt=["%u","%e"])
    f.close()

  quit()

  
  bcTopCon=0.0
  bcProfileCon=3.2  


  edzclipMeshTetraRefinement=pv.read('edzclipMeshTetraRefinement.vtk')
  faultclipMeshTetraRefinement=pv.read('faultclipMeshTetraRefinement.vtk')
  edzMeshEleNb=np.unique(edzclipMeshTetraRefinement["eleNb"])
  faultMeshEleNb=np.unique(faultclipMeshTetraRefinement["eleNb"])

  xyz=meshTetraRefinementHybridFolw.points
  ele_ind=meshTetraRefinementHybridFolw.cells.reshape(-1,5)
  ind=meshTetraRefinementHybridFolw["element_attributes"]!=-1
  surface=meshTetraRefinementHybridFolw.remove_cells(ind,inplace=False)

  '''# type=0 -> Krock[0] and poro_rock[0]'''
  '''# type=1 -> Ksurface and poro_surface'''
  '''# type=2 -> F1F2K and poro_F1F2K'''
  '''# type=3 -> Krock[1] and poro_rock[1]'''
  meshMatType=np.zeros(shape=meshTetraRefinementHybridFolw.n_cells,dtype=np.int64)
  meshMatType[surface["eleNb"][:]] = 1
  meshMatType[faultMeshEleNb[:]] = 2
  meshMatType[edzMeshEleNb[:]] = 3

  TC1=1.0e2
  TC2=1.0e0
  bcCon=0.35
  matStartNb=10001

  FEM3dmF=r'hybridFEM.3dm'
  hkNB=10001  
  case=0
  if case==0:
    delCell=[]
  elif case==1:
    delCell=triMesh3D.checkDJAC(meshTetraRefinementHybridFolw)
  delCell=np.unique(delCell)
  triMesh3D.createFemwater3dm(FEM3dmF,meshTetraRefinementHybridFolw,matStartNb,meshMatType,delCell)
  # quit()

  FEM_ADE_3bcF=r'hybridFEM_ade.3bc'  
  FEM_ADE_coniniF=r'femini.con'
  triMesh3D.createFemwater_ADE3bc(FEM_ADE_3bcF,FEM_ADE_coniniF,meshTetraRefinementHybridFolw,matStartNb,meshMatType,profileMesh,bcCon,Ksurface,poro_surface,Krock,poro_rock,F1F2K,poro_F1F2K,TC1,TC2,delCell)

  FEMHeadF=r'fem.phd'
  FEMVelocityF=r'fem.vel'
  triMesh3D.writeHeadVelocity(FEMHeadF,FEMVelocityF,meshTetraRefinementHybridFolw,delCell)
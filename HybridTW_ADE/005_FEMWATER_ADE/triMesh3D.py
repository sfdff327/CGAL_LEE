import numpy as np
import pyvista as pv
import vtk
from scipy.io import FortranFile

def createWedgeMesh(tri,thickness):
  '''Create a wedge mesh from a triangular mesh.'''
  tri_points = np.array(tri.points)
  points=np.zeros((len(tri.points)*2,3))

  points[0:len(tri.points),0:1]=tri_points[:,0:1]
  points[len(tri.points):len(tri.points)*2,0:1]=tri_points[:,0:1]
  points[0:len(tri.points),2:3]=tri_points[:,1:2]
  points[len(tri.points):len(tri.points)*2,2:3]=tri_points[:,1:2]
  points[0:len(tri.points),1:2]=np.float64(thickness/2.0)
  points[len(tri.points):len(tri.points)*2,1:2]=-np.float64(thickness/2.0)
  # print("points")
  # print(points)

  tri_faces = np.array(tri.elements)
  # print("len(tri_points)=",len(tri_points))
  # print("tri_faces")
  # print(tri_faces)
  cells=np.full(shape=(len(tri_faces),7),fill_value=6,dtype=np.int64)
  tempface=tri_faces+np.int64(len(tri.points))
  cells[:,1:4]=tri_faces[:,0:3]
  cells[:,4:7]=tempface[:,0:3]
  # print("cells")
  # print(cells)

  no_cells=len(cells)
  cells=np.array(cells).ravel()
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_WEDGE

  wedge=pv.UnstructuredGrid(cells, celltypes, points)
  wedge.save('wedge.vtk')
  return wedge

def createFemwater3bc(FEM3bcF,wedge,hkNB,leftNb,BotNb,RightNb,TopNb,MidNb,LeftTotalHead,RightTotalHead):
  with open(FEM3bcF,'w',encoding = 'utf-8') as f:
    f.write('3DFEMWBC\n')
    f.write('T1\n')
    f.write('T2\n')
    f.write('T3\n')
    f.write('OP1 10\n')
    f.write('OP2  0   0   1   0   1  22\n')
    f.write('OP3  1.0 1.0 1.0 0.01 1.5 0.005 0.6667\n')
    f.write('OP4 1\n')
    f.write('IP1  400  25 40000 1e-7 1e-7 1\n')
    f.write('IP2  40 400 0.01\n')
    f.write('IP3  10 0.5 0.01 0.05\n')
    f.write('PT1  1   1   1   2\n')
    f.write('TC1  0.1000000000E+21\n')
    f.write('TC2 0  0.1000000000E+02\n')
    f.write('OC1  0   0   0   0\n')
    f.write('OC2  4   2   3   5  7\n')
    f.write('OC3  0   0   1\n')
    f.write('OC4  3   1  4  5\n')
    f.write('MP1  0\n')
    f.write('MP3  1000.0 0.0 73206249984.0 0.0\n')
    f.write('MP4  1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0\n')
    f.write('XY1 41 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 0.06631251790320901\n')
    f.write('-14.70490163 0.11114320626676581\n')
    f.write('-9.583194398 0.1630802637753569\n')
    f.write('-7.02234078 0.2076151652203082\n')
    f.write('-5.741913971 0.23403982870751538\n')
    f.write('-5.101700567 0.2473827025441112\n')
    f.write('-4.461487162 0.2601655723085497\n')
    f.write('-4.14138046 0.26616669736443405\n')
    f.write('-3.821273758 0.27181000250573034\n')
    f.write('-3.501167056 0.27702700126303637\n')
    f.write('-3.181060353 0.2817573217160853\n')
    f.write('-2.860953651 0.2859522488004665\n')
    f.write('-2.540846949 0.28957783887615113\n')
    f.write('-2.380793598 0.2911711723216832\n')
    f.write('-2.220740247 0.29261731644080835\n')
    f.write('-2.060686896 0.2939170998484099\n')
    f.write('-1.900633545 0.2950725555110137\n')
    f.write('-1.660553518 0.29654251648264257\n')
    f.write('-1.470490163 0.29749154934290845\n')
    f.write('-1.310436812 0.29815177391126185\n')
    f.write('-1.150383461 0.29869279985919617\n')
    f.write('-0.530176726 0.29983508366070083\n')
    f.write('-0.45015005 0.2998935995935242\n')
    f.write('-0.370123374 0.29993701888271906\n')
    f.write('-0.330110037 0.2999536463585668\n')
    f.write('-0.290096699 0.299967211381742\n')
    f.write('-0.250083361 0.2999779707615234\n')
    f.write('-0.210070023 0.2999861934190637\n')
    f.write('-0.170056686 0.29999216291168845\n')
    f.write('-0.150050017 0.29999439621917445\n')
    f.write('-0.130043348 0.29999618118433824\n')
    f.write('-0.110036679 0.29999755940367967\n')
    f.write('-0.09003001 0.2999985746033832\n')
    f.write('-0.070023341 0.299999273174614\n')
    f.write('-0.050016672 0.2999997050095863\n')
    f.write('-0.040013338 0.2999998377856117\n')
    f.write('-0.030010003 0.29999992496675437\n')
    f.write('-0.020006669 0.29999997468783535\n')
    f.write('-0.010003334 0.2999999960502565\n')
    f.write('0.0 0.3\n')
    f.write('XY1 42 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 4.1636829493015676e-05\n')
    f.write('-14.70490163 0.0028262091254001543\n')
    f.write('-9.583194398 0.02593607506692531\n')
    f.write('-7.02234078 0.0937488021972619\n')
    f.write('-5.741913971 0.17831865588601323\n')
    f.write('-5.101700567 0.24247811857839685\n')
    f.write('-4.461487162 0.3241783594518115\n')
    f.write('-4.14138046 0.37175048865498855\n')
    f.write('-3.821273758 0.423537424789336\n')
    f.write('-3.501167056 0.4790671165984366\n')
    f.write('-3.181060353 0.5376268856777273\n')
    f.write('-2.860953651 0.5982632288340116\n')
    f.write('-2.540846949 0.6598005879145358\n')
    f.write('-2.380793598 0.6904878077229055\n')
    f.write('-2.220740247 0.720878097775814\n')
    f.write('-2.060686896 0.7507806245847292\n')
    f.write('-1.900633545 0.7799995616816539\n')
    f.write('-1.660553518 0.8221099458581201\n')
    f.write('-1.470490163 0.8535900675471687\n')
    f.write('-1.310436812 0.8785544845354475\n')
    f.write('-1.150383461 0.9018901447670155\n')
    f.write('-0.530176726 0.9730197249798019\n')
    f.write('-0.45015005 0.9795016192385343\n')
    f.write('-0.370123374 0.985248254481638\n')
    f.write('-0.330110037 0.9878298864197548\n')
    f.write('-0.290096699 0.9902069770852884\n')
    f.write('-0.250083361 0.9923705333315591\n')
    f.write('-0.210070023 0.9943099033487155\n')
    f.write('-0.170056686 0.9960120435992922\n')
    f.write('-0.150050017 0.9967691263674248\n')
    f.write('-0.130043348 0.9974602342261187\n')
    f.write('-0.110036679 0.998082293136071\n')
    f.write('-0.09003001 0.9986315315239164\n')
    f.write('-0.070023341 0.9991031336362268\n')
    f.write('-0.050016672 0.99949057709555\n')
    f.write('-0.040013338 0.9996499028146539\n')
    f.write('-0.030010003 0.9997841223668206\n')
    f.write('-0.020006669 0.999890783723227\n')
    f.write('-0.010003334 0.9999659225998017\n')
    f.write('0.0 1.0\n')
    f.write('XY1 43 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 -0.0029310493649055756\n')
    f.write('-14.70490163 -0.010140575233213775\n')
    f.write('-9.583194398 -0.017390647060776795\n')
    f.write('-7.02234078 -0.020637386925570978\n')
    f.write('-5.741913971 -0.020841290971464616\n')
    f.write('-5.101700567 -0.019966576245679345\n')
    f.write('-4.461487162 -0.018747264641414293\n')
    f.write('-4.14138046 -0.017629450136586948\n')
    f.write('-3.821273758 -0.016297686754793487\n')
    f.write('-3.501167056 -0.014777323963281524\n')
    f.write('-3.181060353 -0.013104777432561174\n')
    f.write('-2.860953651 -0.011326192338467935\n')
    f.write('-2.540846949 -0.009955014597176938\n')
    f.write('-2.380793598 -0.00903538795089107\n')
    f.write('-2.220740247 -0.008120938421349023\n')
    f.write('-2.060686896 -0.0072191906972555505\n')
    f.write('-1.900633545 -0.006122795761052128\n')
    f.write('-1.660553518 -0.004993244806532407\n')
    f.write('-1.470490163 -0.004125028087374485\n')
    f.write('-1.310436812 -0.003380285039669773\n')
    f.write('-1.150383461 -0.0018417790988752515\n')
    f.write('-0.530176726 -0.0007312053398713617\n')
    f.write('-0.45015005 -0.0005425601982378881\n')
    f.write('-0.370123374 -0.00041554834198760746\n')
    f.write('-0.330110037 -0.0003390125356494166\n')
    f.write('-0.290096699 -0.0002688948315534146\n')
    f.write('-0.250083361 -0.00020549791522772123\n')
    f.write('-0.210070023 -0.0001491875727527767\n')
    f.write('-0.170056686 -0.00011162815189271428\n')
    f.write('-0.150050017 -8.921850827775976e-05\n')
    f.write('-0.130043348 -6.888799636912226e-05\n')
    f.write('-0.110036679 -5.0743064901238374e-05\n')
    f.write('-0.09003001 -3.491691849383213e-05\n')
    f.write('-0.070023341 -2.1584551245434318e-05\n')
    f.write('-0.050016672 -1.327317726236377e-05\n')
    f.write('-0.040013338 -8.71520774760657e-06\n')
    f.write('-0.030010003 -4.970450949120961e-06\n')
    f.write('-0.020006669 -2.135529917307536e-06\n')
    f.write('-0.010003334 -3.9484270711422e-07\n')
    f.write('0.0 -3.9484270711422e-07\n')
    # f.write('MP2 100 10.0 10.0 10.0 0.0 0.0 0.0 0.0 0.3\n') # 10.0 = hk
    # f.write('MP5 100 0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n') # 1.0D-11 =  dispersion
    # f.write('SP1 100 41 42 43\n')
  
    for i in range(0,wedge.n_cells):
      hk=wedge['hk'][i]
      f.write('MP2 '+str(hkNB+i)+' '+str(hk)+' '+str(hk)+' '+str(hk)+' '+'0.0 0.0 0.0 0.0 0.3\n')
      f.write('MP5 '+str(hkNB+i)+' '+'0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n')
      f.write('SP1 '+str(hkNB+i)+' '+'41 42 43\n')
      # quit()

    ctHeadNb=1000
    ctHeadNb=ctHeadNb+1
    LeftHeadNb=ctHeadNb
    f.write('XY1 '+str(LeftHeadNb)+' 1 0 0 0 0.0 constant\n')
    f.write('0.0 '+str(LeftTotalHead)+'\n')
    ctHeadNb=ctHeadNb+1
    RightHeadNb=ctHeadNb
    f.write('XY1 '+str(RightHeadNb)+' 1 0 0 0 0.0 constant\n')
    f.write('0.0 '+str(RightTotalHead)+'\n')
    f.write('RS3 0.0 0.0\n')

    twoD_n_points=wedge.n_points/2
    for i in range(0,len(leftNb)):
      f.write('DB1 '+str(int(leftNb[i]+1))+' '+str(LeftHeadNb)+'\n')
      f.write('DB1 '+str(int(leftNb[i]+1+twoD_n_points))+' '+str(LeftHeadNb)+'\n')
    for i in range(0,len(RightNb)):
      f.write('DB1 '+str(int(RightNb[i])+1)+' '+str(RightHeadNb)+'\n')
      f.write('DB1 '+str(int(RightNb[i]+1+twoD_n_points))+' '+str(RightHeadNb)+'\n')


    f.write('ICH 1\n')
    f.write('ICC 0 0.0\n')
    f.write('ICS 0\n')
    f.write('ICT 0.0\n')
    f.write('ICF 0 0 0\n')
    f.write('END\n')  
    f.close()

def createFemwater_ADE3bc(FEM_ADE_3bcF,FEM_ADE_coniniF,meshTetraRefinementHybridFolw,matStartNb,meshMatType,profileMesh,bcCon,Ksurface,poro_surface,Krock,poro_rock,F1F2K,poro_F1F2K,TC1,TC2,delCell):
  
  with open(FEM_ADE_3bcF,'w',encoding = 'utf-8') as f:
    f.write('3DFEMWBC\n')
    f.write('T1\n')
    f.write('T2\n')
    f.write('T3\n')
    f.write('OP1 1\n')
    f.write('OP2  1   1   1   0   1  22\n')
    f.write('OP3  1.0 1.0 1.0 0.01 1.5 0.005 0.6667\n')
    f.write('OP4 1\n')
    f.write('IP1  400  25 40000 1e-7 1e-7 1\n')
    f.write('IP2  40 400 0.01\n')
    f.write('IP3  10 0.5 0.01 0.05\n')
    f.write('PT1  1   1   1   2\n')
    f.write('TC1  '+str(TC1)+'\n')
    f.write('TC2 0 '+str(TC2)+'\n')
    f.write('OC1  0   0   0   1\n')
    f.write('OC2  5   2   3   4  5  7\n')
    f.write('OC3  0   0   1\n')
    f.write('OC4  3   1  4  5\n')
    f.write('MP1  0\n')
    f.write('MP3  1000.0 0.0 73206249984.0 0.0\n')
    f.write('MP4  1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0\n')
    f.write('XY1 41 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 0.06631251790320901\n')
    f.write('-14.70490163 0.11114320626676581\n')
    f.write('-9.583194398 0.1630802637753569\n')
    f.write('-7.02234078 0.2076151652203082\n')
    f.write('-5.741913971 0.23403982870751538\n')
    f.write('-5.101700567 0.2473827025441112\n')
    f.write('-4.461487162 0.2601655723085497\n')
    f.write('-4.14138046 0.26616669736443405\n')
    f.write('-3.821273758 0.27181000250573034\n')
    f.write('-3.501167056 0.27702700126303637\n')
    f.write('-3.181060353 0.2817573217160853\n')
    f.write('-2.860953651 0.2859522488004665\n')
    f.write('-2.540846949 0.28957783887615113\n')
    f.write('-2.380793598 0.2911711723216832\n')
    f.write('-2.220740247 0.29261731644080835\n')
    f.write('-2.060686896 0.2939170998484099\n')
    f.write('-1.900633545 0.2950725555110137\n')
    f.write('-1.660553518 0.29654251648264257\n')
    f.write('-1.470490163 0.29749154934290845\n')
    f.write('-1.310436812 0.29815177391126185\n')
    f.write('-1.150383461 0.29869279985919617\n')
    f.write('-0.530176726 0.29983508366070083\n')
    f.write('-0.45015005 0.2998935995935242\n')
    f.write('-0.370123374 0.29993701888271906\n')
    f.write('-0.330110037 0.2999536463585668\n')
    f.write('-0.290096699 0.299967211381742\n')
    f.write('-0.250083361 0.2999779707615234\n')
    f.write('-0.210070023 0.2999861934190637\n')
    f.write('-0.170056686 0.29999216291168845\n')
    f.write('-0.150050017 0.29999439621917445\n')
    f.write('-0.130043348 0.29999618118433824\n')
    f.write('-0.110036679 0.29999755940367967\n')
    f.write('-0.09003001 0.2999985746033832\n')
    f.write('-0.070023341 0.299999273174614\n')
    f.write('-0.050016672 0.2999997050095863\n')
    f.write('-0.040013338 0.2999998377856117\n')
    f.write('-0.030010003 0.29999992496675437\n')
    f.write('-0.020006669 0.29999997468783535\n')
    f.write('-0.010003334 0.2999999960502565\n')
    f.write('0.0 0.3\n')
    f.write('XY1 42 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 4.1636829493015676e-05\n')
    f.write('-14.70490163 0.0028262091254001543\n')
    f.write('-9.583194398 0.02593607506692531\n')
    f.write('-7.02234078 0.0937488021972619\n')
    f.write('-5.741913971 0.17831865588601323\n')
    f.write('-5.101700567 0.24247811857839685\n')
    f.write('-4.461487162 0.3241783594518115\n')
    f.write('-4.14138046 0.37175048865498855\n')
    f.write('-3.821273758 0.423537424789336\n')
    f.write('-3.501167056 0.4790671165984366\n')
    f.write('-3.181060353 0.5376268856777273\n')
    f.write('-2.860953651 0.5982632288340116\n')
    f.write('-2.540846949 0.6598005879145358\n')
    f.write('-2.380793598 0.6904878077229055\n')
    f.write('-2.220740247 0.720878097775814\n')
    f.write('-2.060686896 0.7507806245847292\n')
    f.write('-1.900633545 0.7799995616816539\n')
    f.write('-1.660553518 0.8221099458581201\n')
    f.write('-1.470490163 0.8535900675471687\n')
    f.write('-1.310436812 0.8785544845354475\n')
    f.write('-1.150383461 0.9018901447670155\n')
    f.write('-0.530176726 0.9730197249798019\n')
    f.write('-0.45015005 0.9795016192385343\n')
    f.write('-0.370123374 0.985248254481638\n')
    f.write('-0.330110037 0.9878298864197548\n')
    f.write('-0.290096699 0.9902069770852884\n')
    f.write('-0.250083361 0.9923705333315591\n')
    f.write('-0.210070023 0.9943099033487155\n')
    f.write('-0.170056686 0.9960120435992922\n')
    f.write('-0.150050017 0.9967691263674248\n')
    f.write('-0.130043348 0.9974602342261187\n')
    f.write('-0.110036679 0.998082293136071\n')
    f.write('-0.09003001 0.9986315315239164\n')
    f.write('-0.070023341 0.9991031336362268\n')
    f.write('-0.050016672 0.99949057709555\n')
    f.write('-0.040013338 0.9996499028146539\n')
    f.write('-0.030010003 0.9997841223668206\n')
    f.write('-0.020006669 0.999890783723227\n')
    f.write('-0.010003334 0.9999659225998017\n')
    f.write('0.0 1.0\n')
    f.write('XY1 43 40 0 0 0 0.0 top_mc_Clay_loam\n')
    f.write('-30 -0.0029310493649055756\n')
    f.write('-14.70490163 -0.010140575233213775\n')
    f.write('-9.583194398 -0.017390647060776795\n')
    f.write('-7.02234078 -0.020637386925570978\n')
    f.write('-5.741913971 -0.020841290971464616\n')
    f.write('-5.101700567 -0.019966576245679345\n')
    f.write('-4.461487162 -0.018747264641414293\n')
    f.write('-4.14138046 -0.017629450136586948\n')
    f.write('-3.821273758 -0.016297686754793487\n')
    f.write('-3.501167056 -0.014777323963281524\n')
    f.write('-3.181060353 -0.013104777432561174\n')
    f.write('-2.860953651 -0.011326192338467935\n')
    f.write('-2.540846949 -0.009955014597176938\n')
    f.write('-2.380793598 -0.00903538795089107\n')
    f.write('-2.220740247 -0.008120938421349023\n')
    f.write('-2.060686896 -0.0072191906972555505\n')
    f.write('-1.900633545 -0.006122795761052128\n')
    f.write('-1.660553518 -0.004993244806532407\n')
    f.write('-1.470490163 -0.004125028087374485\n')
    f.write('-1.310436812 -0.003380285039669773\n')
    f.write('-1.150383461 -0.0018417790988752515\n')
    f.write('-0.530176726 -0.0007312053398713617\n')
    f.write('-0.45015005 -0.0005425601982378881\n')
    f.write('-0.370123374 -0.00041554834198760746\n')
    f.write('-0.330110037 -0.0003390125356494166\n')
    f.write('-0.290096699 -0.0002688948315534146\n')
    f.write('-0.250083361 -0.00020549791522772123\n')
    f.write('-0.210070023 -0.0001491875727527767\n')
    f.write('-0.170056686 -0.00011162815189271428\n')
    f.write('-0.150050017 -8.921850827775976e-05\n')
    f.write('-0.130043348 -6.888799636912226e-05\n')
    f.write('-0.110036679 -5.0743064901238374e-05\n')
    f.write('-0.09003001 -3.491691849383213e-05\n')
    f.write('-0.070023341 -2.1584551245434318e-05\n')
    f.write('-0.050016672 -1.327317726236377e-05\n')
    f.write('-0.040013338 -8.71520774760657e-06\n')
    f.write('-0.030010003 -4.970450949120961e-06\n')
    f.write('-0.020006669 -2.135529917307536e-06\n')
    f.write('-0.010003334 -3.9484270711422e-07\n')
    f.write('0.0 -3.9484270711422e-07\n')
    # f.write('MP2 100 10.0 10.0 10.0 0.0 0.0 0.0 0.0 0.3\n') # 10.0 = hk
    # f.write('MP5 100 0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n') # 1.0D-11 =  dispersion
    # f.write('SP1 100 41 42 43\n')
    
    '''# type=0 -> Krock[0] and poro_rock[0]'''
    '''# type=1 -> Ksurface and poro_surface'''
    '''# type=2 -> F1F2K and poro_F1F2K'''
    '''# type=3 -> Krock[1] and poro_rock[1]'''
    f.write('MP2 '+str(matStartNb+0)+' '+str(Krock[0])+' '+str(Krock[0])+' '+str(Krock[0])+' '+'0.0 0.0 0.0 0.0 '+str(poro_rock[0])+'\n')
    f.write('MP5 '+str(matStartNb+0)+' '+'0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n')
    f.write('SP1 '+str(matStartNb+0)+' '+'41 42 43\n')
    f.write('MP2 '+str(matStartNb+1)+' '+str(Ksurface)+' '+str(Ksurface)+' '+str(Ksurface)+' '+'0.0 0.0 0.0 0.0 '+str(poro_surface)+'\n')
    f.write('MP5 '+str(matStartNb+1)+' '+'0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n')
    f.write('SP1 '+str(matStartNb+1)+' '+'41 42 43\n')
    f.write('MP2 '+str(matStartNb+2)+' '+str(F1F2K)+' '+str(F1F2K)+' '+str(F1F2K)+' '+'0.0 0.0 0.0 0.0 '+str(poro_F1F2K)+'\n')
    f.write('MP5 '+str(matStartNb+2)+' '+'0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n')
    f.write('SP1 '+str(matStartNb+2)+' '+'41 42 43\n')
    f.write('MP2 '+str(matStartNb+3)+' '+str(Krock[1])+' '+str(Krock[1])+' '+str(Krock[1])+' '+'0.0 0.0 0.0 0.0 '+str(poro_rock[1])+'\n')
    f.write('MP5 '+str(matStartNb+3)+' '+'0.0 0.0 1.0D-11 1.0D-11 1.0D-11 1.0 0.0 0.0 0.0 0.0 1.0\n')
    f.write('SP1 '+str(matStartNb+3)+' '+'41 42 43\n')
    
    conNb=1000
    f.write('XY1 '+str(conNb)+' 1 0 0 0 0.0 constant\n')
    f.write('0.0 '+str(bcCon)+'\n')
    f.write('RS3 0.0 0.0\n')

    for i in range(0,profileMesh.n_points):
      f.write('DB2 '+str(int(profileMesh['nodeNb'][i]+1))+' '+str(conNb)+'\n') 
    f.write('ICH 1\n')
    f.write('ICC 1 0.0\n')
    f.write('ICS 0\n')
    f.write('ICT 0.0\n')
    f.write('ICF 0 0 1\n')
    f.write('END\n')  
    f.close()

  with open(FEM_ADE_coniniF,'w',encoding = 'utf-8') as f:  
    f.write('DATASET\n')
    f.write('OBJTYPE mesh3d\n')
    f.write('BEGSCL\n')
    f.write('ND'+str(meshTetraRefinementHybridFolw.n_points)+'\n')
    f.write('NC'+str(int(meshTetraRefinementHybridFolw.n_cells-len(delCell)))+'\n') 
    f.write('NAME concentration\n')                            
    f.write('TS   0 0.00000000E+000\n')  
    for i in range(0,meshTetraRefinementHybridFolw.n_points):
      if i in profileMesh['nodeNb']:
        f.write(str(bcCon)+'\n')
      else:
        f.write(str(0)+'\n')
    f.write('ENDENDDS\n')
    f.close()  

def writeHeadVelocity(FEMHeadF,FEMVelocityF,HybridFolw,delCell):
  with open(FEMHeadF,'w',encoding = 'utf-8') as f:  
    f.write('DATASET\n')
    f.write('OBJTYPE mesh3d\n')
    f.write('BEGSCL\n')
    f.write('ND'+str(HybridFolw.n_points)+'\n')
    f.write('NC'+str(int(HybridFolw.n_cells-len(delCell)))+'\n') 
    f.write('NAME pressure_head\n')                            
    f.write('TS   0 0.00000000E+000\n')  
    for i in range(0,HybridFolw.n_points):
      f.write(str(HybridFolw["totalHead"][i])+'\n')
    f.write('ENDENDDS\n')
    f.close()
  with open(FEMVelocityF,'w',encoding = 'utf-8') as f:  
    f.write('DATASET\n')
    f.write('OBJTYPE mesh3d\n')
    f.write('BEGVEC\n')
    f.write('ND'+str(HybridFolw.n_points)+'\n')
    f.write('NC'+str(int(HybridFolw.n_cells-len(delCell)))+'\n') 
    f.write('NAME nodal_velocity\n')                            
    f.write('TS   0 0.00000000E+000\n')  
    for i in range(0,HybridFolw.n_points):
      f.write(str(HybridFolw["vectors"][i][0])+' '+str(HybridFolw["vectors"][i][1])+' '+str(HybridFolw["vectors"][i][2])+' '+'\n')
    f.write('ENDENDDS\n')
    f.close()

  # ['nodeNb', 'mtr', 'vectors', 'velocityLen', 'totalHead', 'eleNb', 'element_attributes']
  # NAME nodal_velocity

def createFemwater3dm(FEM3dmF,wedge,matStartNb,meshMatType,delCell):
  '''Create a femwater 3dm input file from a wedge mesh.'''
  with open(FEM3dmF,'w',encoding = 'utf-8') as f:
    f.write('3DFEMGEO\n')
    f.write('T1\n')
    f.write('T2\n')
    f.write('T3\n')
    import copy
    cells=copy.deepcopy(wedge.cells.reshape(-1,5)[:,1:5])+1
    nb=0
    for i in range(wedge.n_cells):
      if i not in delCell:
        nb=nb+1
        f.write('GE4 '
              +str(nb)+' '
              +str(cells[i][0])+' '
              +str(cells[i][1])+' '
              +str(cells[i][2])+' '
              +str(cells[i][3])+' '
              +str(matStartNb+meshMatType[i])+'\n')
    for i in range(wedge.n_points):
      f.write('GN '
              +str(i+1)+' '
              +str(wedge.points[i][0])+' '
              +str(wedge.points[i][1])+' '
              +str(wedge.points[i][2])+'\n')
    f.write('END\n')
    f.close()
  
def createFemwaterFws(filename):
  '''Create a femwater fws input file.'''
  with open('fem.fws','w',encoding = 'utf-8') as f:
    f.write('FEMSUP\n')
    f.write('REFTIME  s 0 1900 1 1 0 0 0\n')
    f.write('GEOM	"'+filename+'.3dm"\n')
    f.write('BCFT	"'+filename+'.3bc"\n')
    f.write('PRTF	"fem.out"\n')
    f.write('PSOL	"fem.phd"\n')
    f.write('VSOL	"fem.vel"\n')
    f.write('UNITS "m"	"d"	"kg"	"N"	"mg/l"\n')
    f.close()


def checkDJAC(mesh):
  import sys
  epsilon=sys.float_info.epsilon
  SQEPS=epsilon**0.5
  # print("SQEPS=",SQEPS)
  # quit()

  points=mesh.points
  order=np.array([[2,3,4],[1,3,4],[1,2,4],[1,2,3]])-1
  ele_ind=mesh.cells.reshape(-1,5)[0:,1:5]
  print(ele_ind)
  delCell=[]
  XQ=np.zeros(4,dtype=np.float64)
  YQ=np.zeros(4,dtype=np.float64)
  ZQ=np.zeros(4,dtype=np.float64)
  
  for m in range(0,mesh.n_cells):

    XQ=np.array([points[ele_ind[m,0],0],
        points[ele_ind[m,1],0],
        points[ele_ind[m,2],0],
        points[ele_ind[m,3],0]])
    YQ=np.array([points[ele_ind[m,0],1],
        points[ele_ind[m,1],1],
        points[ele_ind[m,2],1],
        points[ele_ind[m,3],1]])
    ZQ=np.array([points[ele_ind[m,0],2],
        points[ele_ind[m,1],2],
        points[ele_ind[m,2],2],
        points[ele_ind[m,3],2]])      

    ind=0
    for KG in range(0,len(ele_ind[m])):      
      DJAC=np.float64(0.0)
      if ind==1:
        continue
      for kk in range(0,len(ele_ind[m])):    
          K1=order[kk][0]
          K2=order[kk][1]
          K3=order[kk][2]
          DJAC=DJAC+(np.float64(-1.0)**np.float64(kk+2.0))*(XQ[K1]*YQ[K2]*ZQ[K3]+YQ[K1]*ZQ[K2]*XQ[K3]+ZQ[K1]*XQ[K2]*YQ[K3]-XQ[K3]*YQ[K2]*ZQ[K1]-YQ[K3]*ZQ[K2]*XQ[K1]-ZQ[K3]*XQ[K2]*YQ[K1])
          # print("m,KG,kk=",m,KG,kk)
          # print("A(KK)=",(np.float64(-1.0)**np.float64(kk+2.0))*(
          #   XQ[K1]*YQ[K2]*ZQ[K3]+
          #   YQ[K1]*ZQ[K2]*XQ[K3]+
          #   ZQ[K1]*XQ[K2]*YQ[K3]-
          #   XQ[K3]*YQ[K2]*ZQ[K1]-
          #   YQ[K3]*ZQ[K2]*XQ[K1]-
          #   ZQ[K3]*XQ[K2]*YQ[K1]))
          # print("DJAC=",DJAC)
          # print("**",np.float64(-1.0)**np.float64(kk+2.0))
          # print("XQ[K1]*YQ[K2]*ZQ[K3]=",XQ[K1]*YQ[K2]*ZQ[K3])
          # print("YQ[K1]*ZQ[K2]*XQ[K3]=",YQ[K1]*ZQ[K2]*XQ[K3])
          # print("ZQ[K1]*XQ[K2]*YQ[K3]=",ZQ[K1]*XQ[K2]*YQ[K3])
          # print("XQ[K3]*YQ[K2]*ZQ[K1]=",XQ[K3]*YQ[K2]*ZQ[K1])
          # print("YQ[K3]*ZQ[K2]*XQ[K1]=",YQ[K3]*ZQ[K2]*XQ[K1])
          # print("ZQ[K3]*XQ[K2]*YQ[K1]=",ZQ[K3]*XQ[K2]*YQ[K1])
      if DJAC <= np.float64(SQEPS*10.0):
        delCell.append(m)
        ind=1        
        print("at element = ", DJAC,m)
        # print("=================")
        continue
    # if m==testID:
    #   for i in range(0,4):
    #     print(XQ[i],YQ[i],ZQ[i])
    #     print(ele_ind[m,0:])
    #   print("DJAC=",DJAC)
    #   quit()
  print(delCell)
  return delCell
     
    
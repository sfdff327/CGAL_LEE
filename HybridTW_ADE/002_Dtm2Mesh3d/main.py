import numpy as np
import re
import json
import geojson
import pandas as pd
from pathlib import Path
import pickle
import copy
import pyvista as pv
# from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt

import math

import dem
import readTetgen
import pyToTecplot
import meshTool
import hybridFlow
import plotPyvista
import pyFabSTL
import os

import meshpy
import meshpy.triangle as triangle
from meshpy.tet import MeshInfo, build,Options

import readGeoJSON
from scipy.io import FortranFile
from pathlib import Path
import time




from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()
def createSmallDomain(domainCenter,dx,dy,dz):
  xyz=np.zeros((26,3),dtype=np.float64)
  xyz[0]=domainCenter-np.array([dx/2,dy/2,dz/2])
  
  xyz[1]=xyz[0]+np.array([dx,0.,0.])
  xyz[2]=xyz[0]+np.array([dx,dy,0.])
  xyz[3]=xyz[0]+np.array([0.,dy,0.])
  xyz[4]=xyz[0]+np.array([0.,0.,dz])
  xyz[5]=xyz[1]+np.array([0.,0.,dz])
  xyz[6]=xyz[2]+np.array([0.,0.,dz])
  xyz[7]=xyz[3]+np.array([0.,0.,dz])

  xyz[8] =xyz[0]+np.array([dx/2,0.,0.])
  xyz[9] =xyz[0]+np.array([dx,dy/2,0.])
  xyz[10]=xyz[0]+np.array([dx/2,dy,0.])
  xyz[11]=xyz[0]+np.array([0.,dy/2,0.])

  xyz[12]=xyz[4]+np.array([dx/2,0.,0.])
  xyz[13]=xyz[4]+np.array([dx,dy/2,0.])
  xyz[14]=xyz[4]+np.array([dx/2,dy,0.])
  xyz[15]=xyz[4]+np.array([0.,dy/2,0.])

  xyz[16]=xyz[0]+np.array([0.,0.,250.])
  xyz[17]=xyz[16]+np.array([dx/2.,0.,0.])
  xyz[18]=xyz[16]+np.array([dx,0.,0.])
  xyz[19]=xyz[16]+np.array([dx,dy/2,0.])
  xyz[20]=xyz[16]+np.array([dx,dy,0.])
  xyz[21]=xyz[16]+np.array([dx/2.,dy,0.])
  xyz[22]=xyz[16]+np.array([0.,dy,0.])
  xyz[23]=xyz[16]+np.array([0.,dy/2,0.])

  xyz[24]=xyz[0]+np.array([dx/2.,dy/2,0.])
  xyz[25]=xyz[4]+np.array([dx/2.,dy/2,0.])

  faces=np.hstack([
[4, 0,11,24,8],[4,11,3,10,24],[4,8,24,9,1],[4, 24,10,2,9],[4,4,15,23,16],[4,16,23,11,0],
[4,15,7,22,23],[4,23,22,3,11],[4,5,12,17,18],[4, 18,17,8,1],[4,12,4,16,17],[4, 17,16,0,8],
[4,6,13,19,20],[4, 20,19,9,2],[4,13,5,18,19],[4, 19,18,1,9],[4,7,14,21,22],[4,22,21,10,3],
[4,14,6,20,21],[4,21,20,2,10],[4,4,12,25,15],[4,15,25,14,7],[4,12,5,13,25],[4,25,13,6,14],])
  smallDomain=pv.PolyData(xyz,faces)
  nb=-np.linspace(0,24,num=24,endpoint=False,dtype=int)
  smallDomain=pv.PolyData(xyz,faces)
  smallDomain['faceNb']=nb
  smallDomain.save('smallDomain.vtk')
  # print(smallDomain.points)
  # print(smallDomain.faces)
  
  return smallDomain

def testTetgen():
  from meshpy.tet import MeshInfo, build

  mesh_info = MeshInfo()
  mesh_info.set_points(
      [
          (0, 0, 0),
          (2, 0, 0),
          (2, 2, 0),
          (0, 2, 0),
          (0, 0, 12),
          (2, 0, 12),
          (2, 2, 12),
          (0, 2, 12),
      ]
  )
  mesh_info.set_facets(
      [
          [0, 1, 2, 3],
          [4, 5, 6, 7],
          [0, 4, 5, 1],
          [1, 5, 6, 2],
          [2, 6, 7, 3],
          [3, 7, 4, 0],
      ]
  )
  with open('test.mtr','w',encoding = 'utf-8') as f:
    f.write(str(8)+' 1\n')
    for i in range(0,8):
      f.write(str(1.0+i)+'\n')
    f.close()
  mesh_info.load_mtr('test')  
  tetgenSwitches='pmMnAT1e-16'
  print('tetgenSwitches=',tetgenSwitches)
  mesh = build(mesh_info, options=Options(switches=tetgenSwitches))
  print("MeshCoarsening point=",len(mesh.points))
  print("MeshCoarsening elements=",len(mesh.elements))
  for i in range(0,len(mesh.points)):
    print(mesh.point_metric_tensors[i])
  quit()

if __name__ == "__main__":
  from mesh import Mesh
  start = time.time()

  # testTetgen()  

  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  q123Folder=r'../../HybridTW/001_stlToMesh'
  FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  EpsgIn=3825
  EpsgOut=4326
  STLminZ=-2000.0
  

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
#   quit()
  
  CsvFile=r'../../INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
  domainFile=r'../../INER_DATA/Domain2D/domainboundary_large.xy'
  nx=201
  ny=139
  # CsvFile=r'../../INER_DATA/Domain2D/DTM_largescale_263x_263y.dat'
  # nx=263
  # ny=263

  case=0
  if (case==1):
    '''
    # 讀取500m DEM
    '''    
    xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
    xyzCsv = xyzCsv.fillna('')
    xyzCsv.columns=["x", "y", "z"]   
    # print(xyzCsv) 
    # quit()
    ''' # 讀取模擬範圍,並且依據dem計算表面高程 '''    
    xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
    xyzDomain = xyzDomain.fillna('')
    xyzDomain.columns=["x", "y"]
    domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
    print("read dem and doamin files")
    domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
#   quit()
  domain2d=pv.read('domain2d.vtk')
  demClip2d=pv.read('demClip2d.vtk')
  largeDem=pv.read('largeDem.vtk')
  # testPLOTfilename='demClip2d.dat'
  # demClip2d=demClip2d.triangulate()
  # xyz=demClip2d.points
  # ele_ind=demClip2d.cells.reshape(-1,4)
  # pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind)
  # quit()


  case=0
  if (case==1):   
    STLminZ=np.float64(STLminZ)
    '''
    use demClip2d create 3d dem
    '''
    demClip3dsurf=plotPyvista.dem2dTo3d(demClip2d)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
    fault_1=pv.read(StlFile)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
    fault_2=pv.read(StlFile)
    fault_1Mtr=100.0
    fault_2Mtr=20.0
    fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,fault_1,fault_2,fault_1Mtr,fault_2Mtr)
    boxMtr=500.0
    poly3d=plotPyvista.mergeOutsideBox(depthSurfaceSoil,demClip3dsurf,STLminZ,boxMtr)    
  else:
    demClip3dsurf=pv.read('demClip3dsurf.vtk')
    poly3d=pv.read('poly3d.vtk')
    fault12=pv.read('fault12.vtk')
  # print(fault12.array_names)
  # print(poly3d.array_names)
  # print('poly3d, demclip3d and fault12')
  # quit()

  sp= 50.         #18.     #50.
  smallsp=50. #200. #50.         #18.     #50.
  dfnsp =20. #50. #5. #20.     #12.5 #18. #sp #50. #sp/10.
  edzsp=20. #50. #5. #20.
  max_angle=181.0 #160. #175.5
  min_area=0. #270. #55. #30.

  case=2
  if (case==1):
    dfndataF=r'dfnData.csv'
    dfndata = pd.read_csv(dfndataF)
    no_node=dfndata['no_node']
    Transmissivity=dfndata['Transmissivity']
    Storativity=dfndata['Storativity']
    aperture=dfndata['aperture']
    fractureK=Transmissivity/aperture
    fracNb=0
    lineNb=0
    len_dfndata=len(no_node)
    filenameList=[]
    dfnmesh = pv.PolyData()
    checkStartMesh=0
    while(lineNb<len_dfndata):      
      fracNb=fracNb+1
      filename=os.path.join(path, str(fracNb)+'.stl')
      filenameList.append(filename)
      # print('filename=',filename)
      no_each_frac_node=no_node[lineNb]
      fractureK=Transmissivity[lineNb]/aperture[lineNb]
      fractureAperture=aperture[lineNb]
      # print(fracNb,lineNb,no_each_frac_node)
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)
      faces=np.linspace(start=-1,stop=no_each_frac_node,num=no_each_frac_node+1,endpoint=False,dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])
      # print(xyz)
      lineNb=lineNb+1+no_each_frac_node
      dfnpoly=pv.PolyData(xyz,faces)
      dfnpoly.translate([delta_x,delta_y,0],inplace=True)
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['No']=[fracNb]
      dfnmesh=dfnmesh+dfnpoly
    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    print('fracNb=',fracNb)
  elif (case==2):
    dfnPoly=pv.read('LargeAreaDFN.vtk')
  elif (case==0):
    dfnPoly=pv.read('dfnPoly.vtk') 
  print("dfnPoly.array_names=",dfnPoly.array_names)
  # print("dfnPoly",dfnPoly)
  # print(dfnPoly['eachStartLineNB'])
  # print(dfnPoly['No'])
  # print(dfnPoly['vtkOriginalCellIds'])
  # quit() 


  case=0
  if (case==1):
    noface=np.max(dfnPoly["No"])
    ind=np.zeros(noface,dtype=np.int64)
    fractureK=np.zeros(noface,dtype=np.float64)
    aperture=np.zeros(noface,dtype=np.float64)
    for i in range(0,dfnPoly.n_cells):
      # if (ind[dfnPoly["No"][i]-1]==0):
      #  fractureK[dfnPoly["No"][i]-1]=dfnPoly["fractureK"][i]
      #  aperture[dfnPoly["No"][i]-1]=dfnPoly["aperture"][i]
      if (ind[i]==0):
       fractureK[i]=dfnPoly["fractureK"][i]
       aperture[i]=dfnPoly["aperture"][i]
    np.savez('dfnPolyNp', 
    fractureK=fractureK, 
    aperture=aperture)
    print("save dfnPolyNp OK")  
  dfnPolyNp = np.load('dfnPolyNp.npz')
  print("dfnPolyNp OK")     
  # quit()  

  # dfnOrUsg=pyFabSTL.fabRead(None,[],'DFN_all.dat',[])
  # dfnOrUsgArea=dfnOrUsg.compute_cell_quality(quality_measure='area')
  # isGoodArea=dfnOrUsgArea['CellQuality']>0
  # print(np.sum(isGoodArea!=0))
  # isGoodArea=isGoodArea.tolist()
  isGoodArea=[]


  # 0. 母岩水力傳導係數:1.0e-10 cm/s 斷層水力傳導係數 5.0e-6 cm/s
  # 1. 母岩水力傳導係數:4.1e-12 cm/s
  # 2. 母岩水力傳導係數:1.0e-09 cm/s
  # 3. 斷層水力傳導係數:1.0e-04 cm/s
  # 4. 斷層水力傳導係數:1.0e-08 cm/s

  '''水文參數設定'''
  Kfracture=5.0e-5
  # poro_fracture=0.4
  poro_fracture=1.5e-2 #F2's poro
  fracture_alpha_w=1.0
  aperture=5.0e-6
  alpha=0.008
  sa=3.2
  density=1.0*(1.0+alpha*sa)
  # Krock=1.0e-10
   
  matrix_alpha_w=4.8
  Ksurface=1.0e-5
  poro_surface=1.e-3 
  surface_alpha_w=4.8


  F1F2K=5.0e-6 # 基準案例
  # F1F2K=1.0e-04 # 3號案例 改斷層
  # F1F2K=1.0e-08 # 4號案例 改斷層
  poro_F1F2K=0.4
  F1F2K_alpha_w=1.0 

  Krock=[1.0e-10,3.3e-8] # 基準案例'matrix', 'edz'
  # Krock=[4.1e-12,3.3e-8] # 1號案例只有改母岩 'matrix', 'edz'
  # Krock=[1.0e-09,3.3e-8] # 2號案例只有改母岩 'matrix', 'edz'
  poro_rock=[1.0e-5,1.0e-4] # 'matrix', 'edz'
  edzThickness=0.3

  #########
  regionName= ['matrix', 'edz', 'mt', 'dt', 'dh']
  regionTag = [       5,   100,  101,   102, 103]
  addRegion = 2


  pyFabSTL.createEDZ()
  # print("noTestFrac=",np.max(dfnPoly['No']))
  print("noTestFrac=",dfnPoly.n_cells)
  dfnOrUsg=dfnPoly
  # print("dfnOrUsg",dfnOrUsg)
  # quit()

  '''讀取原始FAB與STL轉換後的vtk'''
  # q123Folder=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
  # ReposirotyType =1 全部  =2 簡單案例
  ReposirotyType=1
  if (ReposirotyType==1):
    dh=pv.read(q123Folder+r'/dh.vtk')
    dt=pv.read(q123Folder+r'/dt.vtk')
    mt=pv.read(q123Folder+r'/mt.vtk')
  elif (ReposirotyType==2):
    dh,dt,mt=pyFabSTL.readOneReposiroty()
    dh.save('dh.stl')
    dt.save('dt.stl')
    mt.save('mt.stl')
  # quit()    

  ''' 
  # edz重建
  '''
  edz=pv.read(r'edzNew.vtk')
  reposiroty=edz+dh+dt+mt
  coormax=np.max(reposiroty.points,axis=0)
  coormin=np.min(reposiroty.points,axis=0)
  lencoor=coormax-coormin
  domainCenter=(coormax+coormin)/2
  reposiroty=pv.Cube(center=domainCenter, x_length=lencoor[0], y_length=lencoor[1], z_length=lencoor[2], bounds=None, clean=True)
  # print(reposiroty)
  # reposiroty=reposiroty.triangulate()
  # testPLOTfilename=r'edz.dat'
  # pyToTecplot.tecplotPyvistaUSG(testPLOTfilename,reposiroty.points,reposiroty.faces.reshape(-1,4)) 
  # quit()

  
  case=0
  if (case==1):
    edz_centers=pyFabSTL.stlTetgenCenter(edz,10.)
    print("edz_centers OK")
    dt_centers=pyFabSTL.stlTetgenCenter(dt,2.5)
    print("dt_centers OK")
    mt_centers=pyFabSTL.stlTetgenCenter(mt,2.5)
    print("mt_centers OK")

    
    dh_centers=dh.cell_centers()
    dh_centers['mtr']=np.full(dh_centers.n_points,1.,dtype=np.float64)
    dt_centers.save('dt_centers.vtk')
    mt_centers.save('mt_centers.vtk')
    edz_centers.save('edz_centers.vtk')
    dh_centers.save('dh_centers.vtk')
  else:
    dt_centers=pv.read('dt_centers.vtk')
    mt_centers=pv.read('mt_centers.vtk')
    edz_centers=pv.read('edz_centers.vtk')
    dh_centers=pv.read('dh_centers.vtk')
  reposiroty=dt_centers+mt_centers+dh_centers+edz_centers
  # reposiroty=dt_centers+mt_centers+dh_centers
  # quit()

  
  

  '''
  # 計算dfn與處置設施交結三角形
  '''
  dfncoorMax=np.max(dfnOrUsg.points,axis=0)
  dfncoorMin=np.min(dfnOrUsg.points,axis=0)
  edzcoorMax=np.max(edz.points,axis=0)
  edzcoorMin=np.min(edz.points,axis=0)

  coormax=np.zeros(3,dtype=np.float64)
  coormin=np.zeros(3,dtype=np.float64)
  for i in range(0,3):
    if dfncoorMax[i]>=edzcoorMax[i]:
      coormax[i]=dfncoorMax[i]
    else:
      coormax[i]=edzcoorMax[i]
    if dfncoorMin[i]<=edzcoorMin[i]:
      coormin[i]=dfncoorMin[i]
    else:
      coormin[i]=edzcoorMin[i]
  lencoor=coormax-coormin
  print('lencoor=',lencoor)
  domainCenter=(coormax+coormin)/2
  domainCenter[2]=np.float64(-500.0)
  smallDomain=createSmallDomain(domainCenter,lencoor[0]+250.0,lencoor[0]+250.0,500)

  case=1
  if (case==1):    
    innerOBJ=pyFabSTL.mesh_arrangement(dfnOrUsg,smallDomain,edz,smallsp,dfnsp,edzsp,max_angle,min_area) 
  else:
    innerOBJ=pv.read("innerOBJ.vtk")
  testPLOTfilename='innerOBJ.dat'
  innerOBJ=innerOBJ.triangulate()
  xyz=innerOBJ.points
  ele_ind=innerOBJ.faces.reshape(-1,4)
  pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,innerOBJ['labels'])    
  # quit()
  '''
  # 辨別innerOBJ三角形,哪些屬於dfn
  '''
  # print('dfnOrUsg.array_names')
  # print(dfnOrUsg.array_names)
  case=1
  if (case==1):
    innerMegered=pyFabSTL.find_closestCell(innerOBJ,dfnOrUsg,dh,edz,dt,mt) 
  else:
    innerMegered=pv.read('innerMegered.vtk')      
  '''
  # labels ==1 代表裂隙, closest_cell代表裂隙編號 
  # mask01 = innerMegered['closest_cell'] !=-1
  # mask02 = innerMegered['labels'] ==1 
  '''
  # quit()
  

  
  smallDomain=innerMegered
  smallDomain.save('smallDomain.vtk')
  
  edzRegionTag=100
  case=1
  if (case==1):
    mesh_coarsening=pyFabSTL.runTetgen_coarsening(smallDomain,poly3d,fault12,edz_centers,edz,edzRegionTag,smallsp,dfnsp,edzsp)
  meshUSG_coarsening = np.load('meshUSG_coarsening.npz')
  print("runTetgen_coarsening OK")
  # quit()

  case=1
  if (case==1):
    meshTetraCoarsening,meshTetraPolyCoarsening=pyFabSTL.createPvUSG_TETRA_byNpz(meshUSG_coarsening)
    meshTetraCoarsening.save('meshTetraCoarsening.vtk')
    meshTetraPolyCoarsening.save('meshTetraPolyCoarsening.vtk')
  else:
    meshTetraCoarsening=pv.read('meshTetraCoarsening.vtk')
    meshTetraPolyCoarsening=pv.read('meshTetraPolyCoarsening.vtk')
  print("save coarsening OK")
  # quit()


  case=1
  if (case==1):
    '''
    # element_attributes = -1表土層, -2母岩, -3小區域
    '''
    pyFabSTL.runTetgen_refines(smallDomain,edzRegionTag,meshUSG_coarsening,meshTetraCoarsening)
  meshUSG_refinement = np.load('meshUSG_refinement.npz')
  print("runTetgen_refines OK")

  case=1
  if (case==1):
    meshTetraRefinement,meshTetraPolyRefinement=pyFabSTL.createPvUSG_TETRA_byNpz(meshUSG_refinement)
    meshTetraRefinement.save('meshTetraRefinement.vtk')
    meshTetraPolyRefinement.save('meshTetraPolyRefinement.vtk')
  else:
    meshTetraRefinement=pv.read('meshTetraRefinement.vtk')
    meshTetraPolyRefinement=pv.read('meshTetraPolyRefinement.vtk')
  print("save Refinement OK")

  case=1
  if (case==1):
    bounds=edz.bounds
    center=meshTetraRefinement.cell_centers() 
    edzclipMeshTetraRefinement =center.clip_surface(
      edz, 
      invert=True, value=0.0, 
      compute_distance=True, 
      progress_bar=False, crinkle=False)
    edzclipMeshTetraRefinement.save('edzclipMeshTetraRefinement.vtk')
  else:
    edzclipMeshTetraRefinement=pv.read('edzclipMeshTetraRefinement.vtk')
  edzMeshEleNb=np.unique(edzclipMeshTetraRefinement["eleNb"])
  print("edzMeshEleNb OK")

  case=1
  if (case==1):
    center=meshTetraRefinement.cell_centers()
    faultclipMeshTetraRefinement =center.clip_surface(
      fault12, 
      invert=True, value=sp/3, 
      compute_distance=True, progress_bar=False, crinkle=False)
    faultclipMeshTetraRefinement.save('faultclipMeshTetraRefinement.vtk')
  else:
    faultclipMeshTetraRefinement=pv.read('faultclipMeshTetraRefinement.vtk')  
  faultMeshEleNb=np.unique(faultclipMeshTetraRefinement["eleNb"])
  print("faultMeshEleNb OK")

  # pyToTecplot.TetrahedronUSG_zCenter('edzclipMeshTetraRefinement.dat',edzclipMeshTetraRefinement.points,edzclipMeshTetraRefinement.cells.reshape(-1,5),edzclipMeshTetraRefinement["eleNb"])
  # pyToTecplot.TetrahedronUSG_zCenter('faultclipMeshTetraRefinement.dat',faultclipMeshTetraRefinement.points,faultclipMeshTetraRefinement.cells.reshape(-1,5),faultclipMeshTetraRefinement["eleNb"])
  # edzTriangulate=edz.triangulate()
  # pyToTecplot.tecplotPyvistaUSG_z('edz.dat',edzTriangulate.points,edzTriangulate.faces.reshape(-1,4),edzTriangulate.points[:,2])

  case=1
  if (case==1):
    '''
    # 執行HybridFlow input files
    # 產生複合域水流輸入檔
    '''
    hybridFlow.writeInputDensity(
      density,meshTetraRefinement,meshTetraPolyRefinement,
      edzMeshEleNb,faultMeshEleNb,
      dfnOrUsg,dfnPolyNp,Kfracture,poro_fracture,
      Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K,demClip2d)
    os.system('./DualDomain_flow')

  case=1
  if (case==1):
    meshTetraRefinementHybridFolw=hybridFlow.readFlowTH(meshTetraRefinement)
    meshTetraRefinementHybridFolw.save("meshTetraRefinementHybridFolw.vtk")
  else:
    meshTetraRefinementHybridFolw=pv.read("meshTetraRefinementHybridFolw.vtk")


  case=1
  if (case==1):
    edzmeshTetraRefinementHybridFolw=hybridFlow.localMeshByNb(meshTetraRefinementHybridFolw,edzMeshEleNb)
    edzmeshTetraRefinementHybridFolw.save("edzmeshTetraRefinementHybridFolw.vtk")
    faultmeshTetraRefinementHybridFolw=hybridFlow.localMeshByNb(meshTetraRefinementHybridFolw,faultMeshEleNb)
    faultmeshTetraRefinementHybridFolw.save("faultmeshTetraRefinementHybridFolw.vtk")
  else:
    edzmeshTetraRefinementHybridFolw=pv.read('edzmeshTetraRefinementHybridFolw.vtk')
    faultmeshTetraRefinementHybridFolw=pv.read('faultmeshTetraRefinementHybridFolw.vtk')
  print("localMeshByNb OK")

  case=1
  if (case==1):
    outputfile=r'edzmeshTetraRefinementHybridFolw.dat'
    hybridFlow.tecplotHybridFlow(outputfile,edzmeshTetraRefinementHybridFolw)
    outputfile=r'faultmeshTetraRefinementHybridFolw.dat'
    hybridFlow.tecplotHybridFlow(outputfile,faultmeshTetraRefinementHybridFolw)  
    print("tecplotHybridFlow OK")
  quit()

  '''
  # 讀取完全截切節點
  # 各節點差值獲得速度，最大速度位置釋放質點
  '''
  case=0
  if (case==1):
    # q123Folder=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
    print("q123Folder=",q123Folder)
    noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123=hybridFlow.readConnSTL(q123Folder)
    Q1PT,Q2PT,Q3PT,Q1FullConnNb,Q2FullConnNb,Q3FullConnNb,Q1EqFlux,Q2EqFlux,Q3EqFlux=hybridFlow.FindMaxFluxNode(q123Folder,FABResfolder,noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123,meshTetraRefinementHybridFolw,poro_fracture,edzThickness,poro_rock[1])
    Q1PT.save('Q1PT.vtk')
    Q2PT.save('Q2PT.vtk')
    Q3PT.save('Q3PT.vtk')
    np.savez('Q123FullConnEqFlux', 
    Q1FullConnNb=np.array(Q1FullConnNb),
    Q2FullConnNb=np.array(Q2FullConnNb),
    Q3FullConnNb=np.array(Q3FullConnNb),
    Q1EqFlux=np.array(Q1EqFlux),
    Q2EqFlux=np.array(Q2EqFlux),
    Q3EqFlux=np.array(Q3EqFlux))    
  else:
    Q1PT=pv.read('Q1PT.vtk')
    Q2PT=pv.read('Q2PT.vtk')
    Q3PT=pv.read('Q3PT.vtk')
  Q123FullConnEqFlux = np.load('Q123FullConnEqFlux.npz')
  print("read Q123FullConnEqFlux.npz OK")
  # quit()

  '''
  # 測試PT 必定需要修改的程式
  '''
  case=0
  if (case==1):
    max_steps=500000
    terminal_speed=np.min(meshTetraRefinementHybridFolw["velocityLen"])
    print("terminal_speed=",terminal_speed)
    initial_step_length=dfnsp/100
    min_step_length=dfnsp/2
    max_step_length=dfnsp    

    Q1_streamlines=hybridFlow.streamlines_from_source(meshTetraRefinementHybridFolw,Q1PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])
    Q1_streamlines.points_to_double()
    Q1_streamlines.save('Q1_streamlines.vtk')
    print('streamlines Q1 OK')

    Q2_streamlines=hybridFlow.streamlines_from_source(meshTetraRefinementHybridFolw,Q2PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])  
    Q2_streamlines.points_to_double()
    Q2_streamlines.save('Q2_streamlines.vtk')
    print('streamlines Q2 OK')
    Q3_streamlines=hybridFlow.streamlines_from_source(meshTetraRefinementHybridFolw,Q3PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])
    Q3_streamlines.points_to_double()
    Q3_streamlines.save('Q3_streamlines.vtk')
    print('streamlines Q3 OK')
  else:
    Q1_streamlines=pv.read('Q1_streamlines.vtk')
    Q2_streamlines=pv.read('Q2_streamlines.vtk')
    Q3_streamlines=pv.read('Q3_streamlines.vtk')
    
  '''
  # 判斷streamline是否在edz
  '''
  case=0
  if (case==1):
    Q1_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q1_streamlines)
    Q2_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q2_streamlines)
    Q3_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q3_streamlines)
    Q1_streamlines_edz.save("Q1_streamlines_edz.vtk")
    Q2_streamlines_edz.save("Q2_streamlines_edz.vtk")
    Q3_streamlines_edz.save("Q3_streamlines_edz.vtk")
  else:
    Q1_streamlines_edz=pv.read("Q1_streamlines_edz.vtk")
    Q2_streamlines_edz=pv.read("Q2_streamlines_edz.vtk")
    Q3_streamlines_edz=pv.read("Q3_streamlines_edz.vtk")
  print("isPtInEdz OK")

  '''
  # 判斷streamline是否在F1F2
  '''
  case=0
  if (case==1):
    Q1_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q1_streamlines_edz)
    Q2_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q2_streamlines_edz)
    Q3_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q3_streamlines_edz)
    Q1_streamlines_edz_fault.save("Q1_streamlines_edz_fault.vtk")
    Q2_streamlines_edz_fault.save("Q2_streamlines_edz_fault.vtk")
    Q3_streamlines_edz_fault.save("Q3_streamlines_edz_fault.vtk")
  else:
    Q1_streamlines_edz_fault=pv.read("Q1_streamlines_edz_fault.vtk")
    Q2_streamlines_edz_fault=pv.read("Q2_streamlines_edz_fault.vtk")
    Q3_streamlines_edz_fault=pv.read("Q3_streamlines_edz_fault.vtk")
  print("isPtInFault12 OK")  


  '''
  # 判斷streamline是否在表土層
  '''
  case=0
  if (case==1):
    surfaceBool=meshTetraCoarsening['element_attributes']!=-1
    surfaceMesh=meshTetraCoarsening.remove_cells(surfaceBool,inplace=False)
    Q1_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q1_streamlines_edz_fault)
    Q2_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q2_streamlines_edz_fault)
    Q3_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q3_streamlines_edz_fault)
    Q1_streamlines_edz_fault_surface.save("Q1_streamlines_edz_fault_surface.vtk")
    Q2_streamlines_edz_fault_surface.save("Q2_streamlines_edz_fault_surface.vtk")
    Q3_streamlines_edz_fault_surface.save("Q3_streamlines_edz_fault_surface.vtk")
  else:
    Q1_streamlines_edz_fault_surface=pv.read("Q1_streamlines_edz_fault_surface.vtk")
    Q2_streamlines_edz_fault_surface=pv.read("Q2_streamlines_edz_fault_surface.vtk")
    Q3_streamlines_edz_fault_surface=pv.read("Q3_streamlines_edz_fault_surface.vtk")
  print("isPtInSurface OK") 

  '''
  # 判斷streamline是否在dfn
  '''
  case=0
  if (case==1):
    Q1_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q1_streamlines_edz_fault_surface)
    Q2_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q2_streamlines_edz_fault_surface)
    Q3_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q3_streamlines_edz_fault_surface)
    Q1_streamlines_edz_fault_surface_dfn.save("Q1_streamlines_edz_fault_surface_dfn.vtk")
    Q2_streamlines_edz_fault_surface_dfn.save("Q2_streamlines_edz_fault_surface_dfn.vtk")
    Q3_streamlines_edz_fault_surface_dfn.save("Q3_streamlines_edz_fault_surface_dfn.vtk")
  else:
    Q1_streamlines_edz_fault_surface_dfn=pv.read("Q1_streamlines_edz_fault_surface_dfn.vtk")
    Q2_streamlines_edz_fault_surface_dfn=pv.read("Q2_streamlines_edz_fault_surface_dfn.vtk")
    Q3_streamlines_edz_fault_surface_dfn=pv.read("Q3_streamlines_edz_fault_surface_dfn.vtk")
  print("isPtInDFN OK")

  '''
  # 計算功能測度值
  '''  
  case=1
  if (case==1):  
    Q1PerformaceMeasurement=hybridFlow.performanceMeasurement(dfnOrUsg,Q1_streamlines_edz_fault_surface_dfn,dfnsp,poro_fracture,poro_surface,poro_rock,poro_F1F2K,fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q1PerformaceMeasurement.save("Q1PerformaceMeasurement.vtk")
  else:
    Q1PerformaceMeasurement=pv.read("Q1PerformaceMeasurement.vtk")
  print("Q1PerformaceMeasurement OK")

  case=0
  if (case==1):  
    Q2PerformaceMeasurement=hybridFlow.performanceMeasurement(dfnOrUsg,Q2_streamlines_edz_fault_surface_dfn,dfnsp,poro_fracture,poro_surface,poro_rock,poro_F1F2K,fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q2PerformaceMeasurement.save("Q2PerformaceMeasurement.vtk")
  else:
    Q2PerformaceMeasurement=pv.read("Q2PerformaceMeasurement.vtk")
  print("Q2PerformaceMeasurement OK")

  case=0
  if (case==1):
    Q3PerformaceMeasurement=hybridFlow.performanceMeasurement(dfnOrUsg,Q3_streamlines_edz_fault_surface_dfn,dfnsp,poro_fracture,poro_surface,poro_rock,poro_F1F2K,fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q3PerformaceMeasurement.save("Q3PerformaceMeasurement.vtk")
  else:    
    Q3PerformaceMeasurement=pv.read("Q3PerformaceMeasurement.vtk")
  print("Q3PerformaceMeasurement OK")




  end = time.time()


  # 輸出結果
  print("執行時間：%f 秒" % (end - start))
  
from ctypes import *
import numpy
import sys
lib = CDLL('./libpy_mesh.so');
class Mesh(object):
    def __init__(self, val):
        lib.mesh_new.argtypes = (py_object,)
        lib.mesh_new.restype = c_void_p
        lib.mesh_solveIntersections.argtypes=(c_void_p,)
        lib.mesh_solveIntersections.restype = None
        
        lib.mesh_solveIntersections_nolabel.argtypes=(c_void_p,)
        lib.mesh_solveIntersections_nolabel.restype = None
        
        lib.mesh_exact_solveIntersections_nolabel.argtypes=(c_void_p,)
        lib.mesh_exact_solveIntersections_nolabel.restype = None
        
        lib.mesh_exact_solveIntersections.argtypes=(c_void_p,)
        lib.mesh_exact_solveIntersections.restype = None
        
        lib.mesh_get_out_coords.argtypes = [c_void_p,]
        lib.mesh_get_out_coords.restype = py_object
        
        lib.mesh_get_out_tris.argtypes = [c_void_p,]
        lib.mesh_get_out_tris.restype = py_object
        
        lib.mesh_get_out_labels.argtypes = [c_void_p,]
        lib.mesh_get_out_labels.restype = py_object
        
        lib.out_coords_size.argtypes=[c_void_p]
        lib.out_coords_size.restype = c_int
        
        lib.out_tris_size.argtypes=[c_void_p]
        lib.out_tris_size.restype = c_int
        
        lib.out_labels_size.argtypes=[c_void_p]
        lib.out_labels_size.restype = c_int
        
        self.obj = lib.mesh_new(val)
    def solveIntersections(self):
        lib.mesh_solveIntersections(self.obj)
    def solveIntersections_nolabel(self):
        lib.mesh_solveIntersections_nolabel(self.obj)
    def exact_solveIntersections_nolabel(self):
        lib.mesh_exact_solveIntersections_nolabel(self.obj)
    def exact_solveIntersections(self):
        lib.mesh_exact_solveIntersections(self.obj)    
    def get_out_coords(self):
        '''out_coords=[]
        size=lib.out_coords_size(self.obj)
        for i in range(size):
            out_coords.append(numpy.float64(lib.mesh_get_out_coords(self.obj,i)))
            #print(type(lib.mesh_get_out_coords(self.obj,i)))
            #print(sys.getsizeof(lib.mesh_get_out_coords(self.obj,i)))
        return out_coords'''
        ret_list=lib.mesh_get_out_coords(self.obj)
        return numpy.array(ret_list,dtype="float64")
    def get_out_tris(self):
        out_tris=lib.mesh_get_out_tris(self.obj)
            
        return numpy.array(out_tris)
       
    
    def get_out_labels(self):
        out_labels=lib.mesh_get_out_labels(self.obj)
        return numpy.array(out_labels)
    def get_out_labels_bit(self):
        out_labels=lib.mesh_get_out_labels(self.obj)
        out_labels_bin=[]
        for i in out_labels:
            out_labels_bin.append(format(i, "b"))
        return numpy.array(out_labels_bin)
        

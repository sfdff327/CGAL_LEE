import numpy as np
import re
import json
import geojson
import pandas as pd
from pathlib import Path
import pickle
import copy
import pyvista as pv
# from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt

import math

# import dem
# import readTetgen
# import pyToTecplot
# import meshTool
# import hybridFlow
# import plotPyvista
# import pyFabSTL
import os

import meshpy
import meshpy.triangle as triangle
from meshpy.tet import MeshInfo, build,Options

# import readGeoJSON
from scipy.io import FortranFile
from pathlib import Path
import time

if __name__ == "__main__":
  # from mesh import Mesh
  start = time.time()

  # testTetgen()  

  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  q123Folder=r'../../HybridTW/001_stlToMesh'
  FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)

  dfndataF=r'dfnData.csv'
  dfndata = pd.read_csv(dfndataF)
  no_node=dfndata['no_node']
  Transmissivity=dfndata['Transmissivity']
  Storativity=dfndata['Storativity']
  aperture=dfndata['aperture']
  fractureK=Transmissivity/aperture
  
  case=0
  if (case==1):
    fracNb=0
    lineNb=0
    eachStartLineNB=[]
    len_dfndata=len(no_node)
    # filenameList=[]
    dfnmesh = pv.PolyData()
    checkStartMesh=0
    while(lineNb<len_dfndata):      
      fracNb=fracNb+1
      # print("lineNb=",lineNb)
      eachStartLineNB.append(lineNb)
      # filename=os.path.join(path, str(fracNb)+'.stl')
      # filenameList.append(filename)
      # print('filename=',filename)
      no_each_frac_node=no_node[lineNb]
      fractureK=Transmissivity[lineNb]/aperture[lineNb]
      fractureAperture=aperture[lineNb]
      fractureTransmissivity=Transmissivity[lineNb]
      fractureStorativity=Storativity[lineNb]
      # print(fracNb,lineNb,no_each_frac_node)
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)      
      faces=np.linspace(start=-1,stop=no_each_frac_node,num=no_each_frac_node+1,endpoint=False,dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])
      # print(xyz)
      lineNb=lineNb+1+no_each_frac_node
      dfnpoly=pv.PolyData(xyz,faces)
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['transmissivity']=[fractureTransmissivity]
      dfnpoly['storativity']=[fractureStorativity]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['No']=[fracNb]
      dfnmesh=dfnmesh+dfnpoly
    dfnmesh=dfnmesh.compute_cell_quality('area')
    dfnmesh['eachStartLineNB']=np.array(eachStartLineNB)
    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    print('fracNb=',fracNb)
    print("area=",dfnPoly.area)
  else:
    dfnPoly=pv.read('dfnPoly.vtk')
  dfnPoly=dfnPoly.translate([delta_x,delta_y,0],inplace=True) 


  ind=np.logical_and(dfnPoly["CellQuality"]>30000.0, dfnPoly["CellQuality"]<100000.0)
  # ind=dfnPoly["CellQuality"]>90000.0
  # print(len(ind))
  # print(ind)
  # quit()

  extract_cells=dfnPoly.extract_cells(ind)
  poly=pv.PolyData(extract_cells.points,extract_cells.cells)
  poly['fractureK']=extract_cells['fractureK']
  poly['transmissivity']=extract_cells['transmissivity']
  poly['storativity']=extract_cells['storativity']
  poly['aperture']=extract_cells['aperture']
  poly['No']=extract_cells['No']

  # print("dfnPoly=",dfnPoly)
  # print("extract_cells=",extract_cells)
  # print("extract_cells['transmissivity']",extract_cells['transmissivity'])
  # print("extract_cells['storativity']",extract_cells['storativity'])
  # print("extract_cells['aperture']",extract_cells['aperture'])
  # print("extract_cells['No']",extract_cells['No'])
  # extract_cells.save('LargeAreaDFN.vtk')
  poly.save('LargeAreaDFN.vtk')
  # quit() 

  with open('LargeAreaDFN.csv','w',encoding = 'utf-8') as f:
    f.write('no_node,Transmissivity,Storativity,aperture\n')
    lineNb=0
    cellNb=0  
    while(lineNb<len(extract_cells.cells)):
      f.write(str(extract_cells.cells[lineNb])+','+str(extract_cells['transmissivity'][cellNb])+','+str(extract_cells['storativity'][cellNb])+','+str(extract_cells['aperture'][cellNb])+'\n')
      for i in range(lineNb+1,lineNb+1+extract_cells.cells[lineNb]):
       f.write(str(i-lineNb)+','+str(extract_cells.points[extract_cells.cells[i],0])+','+str(extract_cells.points[extract_cells.cells[i],1])+','+str(extract_cells.points[extract_cells.cells[i],2])+'\n')
      lineNb=lineNb+1+extract_cells.cells[lineNb]
      cellNb=cellNb+1
    f.close()   

  
  with open('LargeAreaDFN.fab','w',encoding = 'utf-8') as f:
    f.write('BEGIN FORMAT\n')
    f.write('    Format = Ascii\n')
    f.write('    XAxis = East\n')
    f.write('    Length_Unit = Meter\n')
    f.write('    Scale = 950.0000153\n')
    f.write('    No_Fractures ='+str(extract_cells.n_cells)+'\n')
    f.write('    No_TessFractures =        0\n')
    f.write('    No_Properties = 3\n')
    f.write('END FORMAT\n')
    f.write('\n')
    f.write('BEGIN PROPERTIES\n')
    f.write('    Prop1    =    (Real*4)    "Transmissivity"\n')
    f.write('    Prop2    =    (Real*4)    "Storativity"\n')
    f.write('    Prop3    =    (Real*4)    "Aperture"\n')
    f.write('END PROPERTIES\n')
    f.write('\n')

    f.write('BEGIN SETS\n')    
    f.write('    Set1    =    "Region_Near_conn_1"\n')    
    f.write('END SETS\n')    
    f.write('\n')

    f.write('BEGIN FRACTURE\n')
    lineNb=0
    cellNb=0
    while(lineNb<len(extract_cells.cells)):
      f.write(str(cellNb+1)+'    '+str(extract_cells.cells[lineNb])+'    1    '+str(extract_cells['transmissivity'][cellNb])+'    '+str(extract_cells['storativity'][cellNb])+'    '+str(extract_cells['aperture'][cellNb])+'\n')
      for i in range(lineNb+1,lineNb+1+extract_cells.cells[lineNb]):
       f.write(str(i-lineNb)+'   '+str(extract_cells.points[extract_cells.cells[i],0])+'    '+str(extract_cells.points[extract_cells.cells[i],1])+'    '+str(extract_cells.points[extract_cells.cells[i],2])+'    '+'\n')
      f.write('0   '+str(poly.cell_normals[cellNb,0])+'    '+str(poly.cell_normals[cellNb,1])+'    '+str(poly.cell_normals[cellNb,2])+'\n')   
      # print(lineNb)
      # print(extract_cells.cells[lineNb])
      # print(extract_cells.cells[lineNb+1:lineNb+1+extract_cells.cells[lineNb]])
      # if lineNb>50:
      #   quit()
      lineNb=lineNb+1+extract_cells.cells[lineNb]
      cellNb=cellNb+1
    f.write('END FRACTURE\n')
    f.write('\n')    

    f.write('BEGIN TESSFRACTURE\n')
    f.write('END TESSFRACTURE\n')
    f.close()


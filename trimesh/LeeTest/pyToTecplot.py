import io

def FemToTecplot3D(tecplot_filename,xyz,ele_ind):
    with open(tecplot_filename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,et=triangle\n')
        for i in range(0,len(xyz)):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+' '+str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+'\n')            
    return 0

def mf2005HeadToTecplot(tecplot_filename,ncol,nrow,nlay,grid_x,grid_y,grid_z,head,ibound):
    with open(tecplot_filename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","h(m)","Ibound"\n')
        f.write('Zone I='+str(ncol)+',J='+str(nrow)+',K='+str(nlay)+',F=point\n')
        for k in range(0, nlay):
            for j in range(0,nrow):
                for i in range(0,ncol):
                    f.write(str(grid_x[j][i])+' '+str(grid_y[j][i])+' '+str(grid_z[k][j][i])+' '+str(head[k][j][i])+' '+str(ibound[k][j][i])+'\n')
    return 0    

def mf2005ToTecplot(tecplot_filename,ncol,nrow,nlay,grid_x,grid_y,grid_z):
    with open(tecplot_filename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone I='+str(ncol)+',J='+str(nrow)+',K='+str(nlay)+',F=point\n')
        for k in range(0, nlay):
            for j in range(0,nrow):
                for i in range(0,ncol):
                    f.write(str(grid_x[j][i])+' '+str(grid_y[j][i])+' '+str(grid_z[k][j][i])+'\n')
    return 0

def main(data):
    result = transGeoJSON(data)
    return result

# if __name__== "__main__":
#     result = main(data)    

import numpy as np
import trimesh
import pyToTecplot

# load a file by name or from a buffer
# mesh = trimesh.load_mesh('../models/featuretype.STL')
mesh = trimesh.load_mesh('../models/featuretype.ply')
# to keep the raw data intact, disable any automatic processing
#mesh = trimesh.load_mesh('../models/featuretype.STL', process=False)


# is the current mesh watertight?
mesh.is_watertight
print(mesh.is_watertight)


# what's the euler number for the mesh?
mesh.euler_number


print("mesh.euler_number=",mesh.euler_number)
print("dir(mesh)",dir(mesh))



dict = mesh.to_dict()
tecplot_filename='meshPlot.dat'
pyToTecplot.FemToTecplot3D(tecplot_filename,mesh.vertices,mesh.faces)
# xyz=[]
# ele_ind=[]
# for i in range(0,len(dict["vertices"])):
#     xyz.append(dict["vertices"][i])
# for i in range(0,len(dict["faces"])):
#     ele_ind.append(dict["faces"][i])
# pyToTecplot.FemToTecplot3D(tecplot_filename,xyz,ele_ind)
# print("len(dict)=",len(dict))
# print(dict["faces"][0])
# print(dict["vertices"][0])
# print(dict["vertices"][0][0])
# print(dict["vertices"][0][1])
# print(dict["vertices"][0][2])


for subdict in dict:
    print(subdict)
quit()

# the convex hull is another Trimesh object that is available as a property
# lets compare the volume of our mesh with the volume of its convex hull
np.divide(mesh.volume, mesh.convex_hull.volume)


# since the mesh is watertight, it means there is a
# volumetric center of mass which we can set as the origin for our mesh
mesh.vertices -= mesh.center_mass

# what's the moment of inertia for the mesh?
mesh.moment_inertia

# if there are multiple bodies in the mesh we can split the mesh by
# connected components of face adjacency
# since this example mesh is a single watertight body we get a list of one mesh
mesh.split()

# preview mesh in a pyglet window from a terminal, or inline in a notebook
# mesh.show()


# facets are groups of coplanar adjacent faces
# set each facet to a random color
# colors are 8 bit RGBA by default (n,4) np.uint8
for facet in mesh.facets:
    mesh.visual.face_colors[facet] = trimesh.visual.random_color()


# transform method can be passed a (4,4) matrix and will cleanly apply the transform
mesh.apply_transform(trimesh.transformations.random_rotation_matrix())


# an axis aligned bounding box is available
mesh.bounding_box.primitive.extents


# a minimum volume oriented bounding box is available
mesh.bounding_box_oriented.primitive.extents


mesh.bounding_box_oriented.primitive.transform


# the bounding box is a trimesh.primitives.Box object, which subclasses
# Trimesh and lazily evaluates to fill in vertices and faces when requested
# mesh.bounding_box_oriented.show()


# bounding spheres and bounding cylinders of meshes are also
# available, and will be the minimum volume version of each
# except in certain degenerate cases, where they will be no worse
# than a least squares fit version of the primitive.
print(mesh.bounding_box_oriented.volume, 
      mesh.bounding_cylinder.volume,
      mesh.bounding_sphere.volume)
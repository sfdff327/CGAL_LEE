import matplotlibTool
import numpy as np
import json
def main():
  ''''''  
  ''''''  
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    xlabelQeq="log10 Q$_{eq}$ (m$^{3}$/year)"
    xlabelUr="log10 U$_{r}$ (m/year)"
    ylabel="Fraction(-)"
    Title="Sea-level"
    savePngFile='Q1_ptUr_SeaLevel.png'
    matplotlibTool.initialVelocityCompare(folderChar,xlabelQeq,xlabelUr,ylabel,Title)  
  print("run SeaLevel initialVelocityCompare OK")

  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'  
    xlabelQeq="log10 Q$_{eq}$ (m$^{3}$/year)"
    xlabelUr="log10 U$_{r}$ (m/year)"
    ylabel="Fraction(-)"
    Title="Density flow"
    QeqsavePngFile='Q1_ptQeq_Density.png'
    URsavePngFile='Q1_ptUr_Density.png'
    matplotlibTool.initialVelocityCompare(folderChar,xlabelQeq,xlabelUr,ylabel,Title)
  print("run Density initialVelocityCompare OK")
  ''''''
  ''''''

  '''
  # Q1eqFluxCompare 
  '''
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    Title="Sea-level"
    matplotlibTool.Q1eqFluxCompare(folderChar,Title)
  print("run Sea-level Q1eqFluxCompare OK")
  
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    Title="Density flow"
    matplotlibTool.Q1eqFluxCompare(folderChar,Title)
  print("run Density Q1eqFluxCompare OK")

  '''
  # 畫出每個PT,需要花比較多時間
  '''
  case=0
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    Title="Density flow"
    matplotlibTool.PlotPT(folderChar,Title)

  '''
  # 畫出每個PT,需要花比較多時間
  '''
  case=0
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    Title="Sea-level"
    matplotlibTool.PlotPT(folderChar,Title)


  Title="Densityflow"
  out_filename=Title+"_Q2.json"
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    matplotlibTool.getQ2Endpoint(folderChar,out_filename)
  with open(out_filename, 'r') as f:
    Q2DensityFlowPM= json.loads(f.read())
  case=0
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    Title="Density flow"
    matplotlibTool.Q2eqFluxCompare(folderChar,Title,Q2DensityFlowPM)    

  Title="Sea-level"
  out_filename=Title+"_Q2.json"
  case=0
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    matplotlibTool.getQ2Endpoint(folderChar,out_filename)
  with open(out_filename, 'r') as f:
    Q2DensityFlowPM= json.loads(f.read())
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    Title="Sea-level"
    matplotlibTool.Q2eqFluxCompare(folderChar,Title,Q2DensityFlowPM) 
  ''''''
  ''''''

  ''''''
  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/Large20221123_500_250_20_20_case0'
    Title="Sea-level"
    matplotlibTool.Q3eqFluxCompare(folderChar,Title)

  case=1
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    Title="Density flow"
    matplotlibTool.Q3eqFluxCompare(folderChar,Title)
  ''''''

  case=0
  if (case==1):
    folderChar=r'/data/CGAL/CGAL_LEE/HybridTW_Large/20221126_50_50_20_20_case0'
    Title="Density flow"
    matplotlibTool.readEachPM(folderChar,Title)

  return 0


if __name__ == "__main__":

  res=main()
   



    # Qnb='Q2'  
    # hist,bin_edges = np.histogram(logQ2,bins = bins)
    # cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    # npPT[:,2]=bin_edges[1:]
    # npPT[:,3]=cdf[:]
    # plt.plot(bin_edges[1:],cdf,color='green',label=Qnb)

    # Qnb='Q3'  
    # hist,bin_edges = np.histogram(logQ3,bins = bins)
    # cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    # npPT[:,4]=bin_edges[1:]
    # npPT[:,5]=cdf[:]
    # plt.plot(bin_edges[1:],cdf,color='red',label=Qnb)  

    # plt.xlabel("U(m/year)")
    # plt.ylabel("Fraction(-)")
    # plt.title("CDF for discrete distribution")
    # plt.legend()
    # plt.savefig('Q123_ptUr.png')
    # plt.close()
  # quit()




  
  # # evaluate the histogram
  # values, base = np.histogram(Q123FullConnEqFlux["Q1EqFlux"], bins=400)
  # #evaluate the cumulative
  # cumulative = np.cumsum(values)
  # # plot the cumulative function
  # plt.plot(base[:-1], cumulative, c='blue')
  # plt.xlabel("U (cm/s)", fontsize=12)
  # plt.ylabel("probability(%)", fontsize=12)
  # plt.tick_params(
  #   axis='both',
  #   color='black',
  #   width=3,
  #   length=5,
  #   direction='in',
  #   colors='black')

   
  # plt.savefig('Q1EqFlux.png')
  # quit() 


  # # print(Q123FullConnEqFlux["Q1EqFlux"])
  # # print(logQ1)
  # # plt.hist(Q123FullConnEqFlux["Q1EqFlux"],log = True)
  # # plt.rcParams["font.family"] = "Times New Roman"
  # plt.hist(logQ1,bins = 21,density=True,alpha=1.0,cumulative=True)
  # plt.xlabel("log10(velocity cm/s)", fontsize=12)
  # # plt.ylabel("probability(%)", fontsize=12)    
  # plt.ylabel("probability(%)", fontsize=12)    
  # plt.tick_params(
  #   axis='both',
  #   color='black',
  #   width=3,
  #   length=5,
  #   direction='in',
  #   colors='black')

   
  # plt.savefig('Q1EqFlux.png')  
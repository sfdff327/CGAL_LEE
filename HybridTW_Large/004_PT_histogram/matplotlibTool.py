import json
import pandas as pd
import io
import os
import numpy as np
import matplotlib.pyplot as plt
# from matplotlib import pyplot as plt
import matplotlib
import datetime

import pyvista as pv
# zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/times_new_roman.ttf')
# zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/times new roman bold italic.ttf')
zhfont1 = matplotlib.font_manager.FontProperties(fname='./Times New Roman/kaiu.ttf')
color=["#00FF00","#FF0000","#000000","#FF00FF","#FFA600"]
linestyle_tuple = [
     ('solid',                 (0, ())),
     ('dotted',                (0, (2, 2))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))), 
     ('dashdotted',            (0, (2, 5, 5, 2))),         
     ('long dash with offset', (5, (10, 3))),          
     ('loosely dashed',        (0, (5, 10))),
     ('dashed',                (0, (5, 5))),
     ('densely dashed',        (0, (5, 1))),

     ('loosely dashdotted',    (0, (3, 10, 1, 10))),
     ('densely dashdotted',    (0, (3, 1, 1, 1))),


     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
linestyles=[]
for i, (name, linestyle) in enumerate(linestyle_tuple):
  linestyles.append(linestyle)

# mpl.rcParams['font.family'] = ['Times New Roman']

poro=np.float(1e-5)

def Q1eqFluxCompare(folderChar,Title):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  
  '''
  Q1
  '''  
  for i in range(0,5):
    Q1PerformaceMeasurementFile=folderChar+str(i)+"/Q1PerformaceMeasurement.vtk"
    Q1PerformaceMeasurement=Q1PerformaceMeasurement=pv.read(Q1PerformaceMeasurementFile)
    travelTime=np.zeros(Q1PerformaceMeasurement.n_cells)
    PathLength=np.zeros(Q1PerformaceMeasurement.n_cells)
    Fr=np.zeros(Q1PerformaceMeasurement.n_cells)
    travelTime=[]
    PathLength=[]
    Fr=[]

    nb=-1
    for SeedId in Q1PerformaceMeasurement["SeedIds"]:
      ind=Q1PerformaceMeasurement["SeedIds"]!=SeedId
      oneStreamline=Q1PerformaceMeasurement.remove_cells(ind,inplace=False)
      # nb=nb+1
      # travelTime[nb]=oneStreamline["travelTime"][oneStreamline.n_points-1]/86400*365
      # PathLength[nb]=oneStreamline["PathLength"][oneStreamline.n_points-1]
      # Fr[nb]=oneStreamline["Fr"][oneStreamline.n_points-1]/86400*365
      if oneStreamline["PathLength"][oneStreamline.n_points-1]>1000 and oneStreamline["PathLength"][oneStreamline.n_points-1]<100000:
        travelTime.append(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength.append(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr.append(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])   
    LogFr=np.log10(Fr[~np.isnan(Fr)])      

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q1 performance Measurement")  
  # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  plt.savefig("performanceMeasurementQ1_"+Title+".png")
  plt.close()    

def getQ2Endpoint(folderChar,out_filename):
  data={
    "travelTime":{},
    "Fr":{},
    "PathLength":{}
  }
  for i in range(0,5):
    PerformaceMeasurementFile=folderChar+str(i)+"/Q2PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    travelTime=np.zeros(PerformaceMeasurement.n_cells)
    PathLength=np.zeros(PerformaceMeasurement.n_cells)
    Fr=np.zeros(PerformaceMeasurement.n_cells)

    nb=-1
    for SeedId in PerformaceMeasurement["SeedIds"]:
      # print(SeedId,PerformaceMeasurement.n_cells)
      ind=PerformaceMeasurement["SeedIds"]==SeedId
      oneStreamline=PerformaceMeasurement.extract_cells(ind)   
      nb=nb+1
      travelTime[nb]=np.max(oneStreamline["travelTime"])/(86400.*365.)
      PathLength[nb]=np.max(oneStreamline["PathLength"])
      Fr[nb]=np.max(oneStreamline["Fr"])/(86400*365)
    data["travelTime"]["case "+str(i)]=travelTime.tolist()
    data["PathLength"]["case "+str(i)]=PathLength.tolist()
    data["Fr"]["case "+str(i)]=Fr.tolist()
  out_filename = io.open(out_filename, mode="w", encoding="utf-8")
  # print(data)
  json.dump(data, out_filename, indent=2,ensure_ascii=False)       

def Q2eqFluxCompare(folderChar,Title,Q2DensityFlowPM):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)

  # print(Q2DensityFlowPM["travelTime"]["case 0"][0:5])
  # print(len(Q2DensityFlowPM["travelTime"]["case 0"]))
  # quit()

  '''
  Q2
  '''  
  for i in range(0,5):
    travelTime=[]
    PathLength=[]
    Fr=[]
    for j in range(0,len(Q2DensityFlowPM["travelTime"]["case "+str(i)])):
      if Q2DensityFlowPM["PathLength"]["case "+str(i)][j]>1000 and Q2DensityFlowPM["PathLength"]["case "+str(i)][j]<100000:
        travelTime.append(Q2DensityFlowPM["travelTime"]["case "+str(i)][j])
        PathLength.append(Q2DensityFlowPM["PathLength"]["case "+str(i)][j])
        Fr.append(Q2DensityFlowPM["Fr"]["case "+str(i)][j])
    
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])      
    LogFr=np.log10(Fr[~np.isnan(Fr)])      

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q2 performance Measurement")
  # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
  # plt.legend(loc='upper center', bbox_to_anchor=(1, 0.05))
  plt.savefig("performanceMeasurementQ2_"+Title+".png")
  plt.close()      


def Q3eqFluxCompare(folderChar,Title):
  bins=400
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, constrained_layout=True, sharey=True)
  '''
  Q3
  '''  
  for i in range(0,5):
    PerformaceMeasurementFile=folderChar+str(i)+"/Q3PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    travelTime=np.zeros(PerformaceMeasurement.n_cells)
    PathLength=np.zeros(PerformaceMeasurement.n_cells)
    travelTime=[]
    PathLength=[]
    Fr=[]
    nb=-1
    for SeedId in PerformaceMeasurement["SeedIds"]:
      ind=PerformaceMeasurement["SeedIds"]!=SeedId
      oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
      # nb=nb+1
      # travelTime[nb]=oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365)
      # PathLength[nb]=oneStreamline["PathLength"][oneStreamline.n_points-1]
      if oneStreamline["PathLength"][oneStreamline.n_points-1]>1000 and oneStreamline["PathLength"][oneStreamline.n_points-1]<100000:
        travelTime.append(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength.append(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr.append(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
    travelTime=np.array(travelTime)
    PathLength=np.array(PathLength)
    Fr=np.array(Fr)
    LogtravelTime=np.log10(travelTime[~np.isnan(travelTime)])      
    LogPathLength=np.log10(PathLength[~np.isnan(PathLength)])   
    LogFr=np.log10(Fr[~np.isnan(Fr)])   

    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i) 

    hist,bin_edges = np.histogram(LogtravelTime,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax1.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
    hist,bin_edges = np.histogram(LogFr,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax2.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)    
    hist,bin_edges = np.histogram(LogPathLength,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    ax3.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  ax1.set_xlabel("log10 year")
  ax1.set_ylabel("Fraction(-)")
  ax1.set_title("Travel Time")
  # ax1.legend()
  ax2.set_xlabel("log10 year/m")
  ax2.set_ylabel("Fraction(-)")
  ax2.set_title("F$_{r}$")  
  # ax2.legend()
  ax3.set_xlabel("log10 m")
  ax3.set_ylabel("Fraction(-)")
  ax3.set_title("Path Length")
  ax3.legend(frameon=False)
  fig.suptitle(Title+" Q3 performance Measurement")
  plt.savefig("performanceMeasurementQ3_"+Title+".png")
  plt.close()    


def initialVelocityCompare(folderChar,xlabel,xlabelUr,ylabel,Title):
  '''
  Q1 Qeq
  '''
  plotTitle=Title+" - Q1 initial Q$_{eq}$"
  savePngFile="Q1_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q1EqFlux"][~np.isnan(Q123FullConnEqFlux["Q1EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q1 Ur
  '''
  plotTitle=Title+" - Q1 initial U$_{r}$"
  savePngFile="Q1_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q1PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q2 Qeq
  '''
  plotTitle=Title+" - Q2 initial Q$_{eq}$"
  savePngFile="Q2_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q2EqFlux"][~np.isnan(Q123FullConnEqFlux["Q2EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q2 Ur
  '''
  plotTitle=Title+" - Q2 initial U$_{r}$"
  savePngFile="Q2_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q2PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q3 Qeq
  '''
  plotTitle=Title+" - Q3 initial Q$_{eq}$"
  savePngFile="Q3_ptQeq_"+Title+".png"
  for i in range(0,5):
    npzFile=folderChar+str(i)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q3EqFlux"][~np.isnan(Q123FullConnEqFlux["Q3EqFlux"])] *(86400*365)
    logEQFlux=np.log10(EQFlux)      
    bins=400
    npPT=np.zeros((bins,6),dtype=np.float64)  
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logEQFlux,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    # npPT[:,0]=bin_edges[1:]
    # npPT[:,1]=cdf[:]
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabel)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

  '''
  Q3 Ur
  '''
  plotTitle=Title+" - Q3 initial U$_{r}$"
  savePngFile="Q3_ptUr_"+Title+".png"
  for i in range(0,5):
    PTFile=folderChar+str(i)+'/Q3PT.vtk'
    PtVtk = pv.read(PTFile)
    ur=PtVtk["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    localUr=urLen[~np.isnan(urLen)] *(86400*365)
    # print("localUr")
    # print(localUr)
    logur=np.log10(localUr)      
    bins=400
    if i==0:
      Qnb="base case"
    else:
      Qnb="case "+str(i)  
    hist,bin_edges = np.histogram(logur,bins = bins)
    cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
    plt.plot(bin_edges[1:],cdf,linestyle=linestyles[i],color=color[i],label=Qnb)
  plt.xlabel(xlabelUr)
  plt.ylabel(ylabel)
  plt.title(plotTitle)
  plt.legend(frameon=False)
  plt.savefig(savePngFile)
  plt.close()

def readEachPM(folderChar,Title):
  for k in range(0,5):
    Qnb="Q1"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)    
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q1EqFlux"] *(86400*365)
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==508 and QPt["FAB"][j]==16 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q1")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)

    Qnb="Q2"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q2EqFlux"] *(86400*365)
    print("Q2 EQFlux")
    print(EQFlux)    
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==508 and QPt["FAB"][j]==68 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close        
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q2")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)

    Qnb="Q3"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    npzFile=folderChar+str(k)+'/Q123FullConnEqFlux.npz'
    Q123FullConnEqFlux = np.load(npzFile)
    EQFlux=Q123FullConnEqFlux["Q3EqFlux"] *(86400*365)
    QPtF=folderChar+str(k)+"/"+Qnb+"PT.vtk"
    QPt=pv.read(QPtF)
    ur=QPt["Ur"]
    urLen= np.linalg.norm(ur,axis=1)
    # print(QPt.point_data)
    for j in range(0,QPt.n_points):
      if (QPt["STL"][j]==80 and QPt["FAB"][j]==16 ):
        ind=PerformaceMeasurement["SeedIds"]!=PerformaceMeasurement["SeedIds"][j]
        oneStreamline=PerformaceMeasurement.remove_cells(ind,inplace=False)
        oneStreamline=oneStreamline.triangulate()
        point=oneStreamline.points
        lines=oneStreamline.cells.reshape(-1,3)
        PathLength=oneStreamline["PathLength"]
        travelTime=oneStreamline["travelTime"]/(86400*365)
        Fr=oneStreamline["Fr"]/(86400*365)
        with open(Qnb+"_onePT.dat",'w',encoding = 'utf-8') as f:
          f.write('Title="XY2D_plot"\n')
          f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
          f.write('Zone N='+str(oneStreamline.n_points)+',E='+str(oneStreamline.n_cells)+',F=FEpoint,ET=LINESEG\n') 
          for i in range(0, oneStreamline.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
          for i in range(0,oneStreamline.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
          f.close
        travelTime=(oneStreamline["travelTime"][oneStreamline.n_points-1]/(86400*365))
        PathLength=(oneStreamline["PathLength"][oneStreamline.n_points-1])
        Fr=(oneStreamline["Fr"][oneStreamline.n_points-1]/(86400*365))
        print("Q3")
        print("STL,FAB=",QPt["STL"][j],QPt["FAB"][j])
        print("points=",QPt.points[j])
        print("Ur=",urLen[j])
        print("Qeq=",EQFlux[j])
        print("travelTime=",travelTime)
        print("PathLength=",PathLength)
        print("Fr=",Fr)        
    # quit()

def PlotPT(folderChar,Title):
  for k in range(0,5):
    Qnb="Q1"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close

    Qnb="Q2"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close

    Qnb="Q3"
    PerformaceMeasurementFile=folderChar+str(k)+"/"+Qnb+"PerformaceMeasurement.vtk"
    PerformaceMeasurement=pv.read(PerformaceMeasurementFile)
    PerformaceMeasurement=PerformaceMeasurement.triangulate()
    point=PerformaceMeasurement.points
    lines=PerformaceMeasurement.cells.reshape(-1,3)
    PathLength=PerformaceMeasurement["PathLength"]
    travelTime=PerformaceMeasurement["travelTime"]/(86400*365)
    Fr=PerformaceMeasurement["Fr"]/(86400*365)
    with open(Qnb+Title+"_case "+str(k)+".dat",'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Path Length(m)","travel Time (year)","F<sub>r</sub> (year/m)"'+'\n')
        f.write('Zone N='+str(PerformaceMeasurement.n_points)+',E='+str(PerformaceMeasurement.n_cells)+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, PerformaceMeasurement.n_points):
            f.write(str(point[i][0])+','+str(point[i][1])+','+str(point[i][2])+','+str(PathLength[i])+','+str(travelTime[i])+','+str(Fr[i])+'\n')
        for i in range(0,PerformaceMeasurement.n_cells):
            f.write(str(lines[i][1]+1)+','+str(lines[i][2]+1)+'\n')        
        f.close
  # quit()

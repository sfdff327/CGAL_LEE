from mesh import Mesh
import numpy
import sys

my_mesh=Mesh(["dfnOrUsg.stl","edz.stl"])
my_mesh.solveIntersections()
out_coords=my_mesh.get_out_coords()
out_tris=my_mesh.get_out_tris()
out_labels=my_mesh.get_out_labels_bit()
print('%0.60f'%out_coords[0])
print(type(out_coords[0]))
print(sys.getsizeof(out_coords[0]))
print(out_labels[0])

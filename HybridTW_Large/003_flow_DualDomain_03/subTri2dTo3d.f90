subroutine triangle3dTo2DCoor(x2,y2,z2,a,b,c,d,px,py,pz)
implicit none
integer*4 i,j,k
real*8 x2(3),y2(3),z2(3)
real*8 a(4),b(4),c(4),d(4)
real*8 disCB,disPA
real*8 dv(3),v(3),t,P(3),px,py,pz
real*8 CB(3)
real*8 xDir(3),yDir(3)
real*8 xDirLen,yDirLen

CB(1)=x2(3)-x2(2)
CB(2)=y2(3)-y2(2)
CB(3)=z2(3)-z2(2)
disCB=(CB(1)*CB(1)+CB(2)*CB(2)+CB(3)*CB(3))**0.5d0

dv(1)=CB(1)/disCB
dv(2)=CB(2)/disCB
dv(3)=CB(3)/disCB

v(1)=x2(1)-x2(2)
v(2)=y2(1)-y2(2)
v(3)=z2(1)-z2(2)

t=v(1)*dv(1)+v(2)*dv(2)+v(3)*dv(3)

P(1)=x2(2)+t*dv(1)
P(2)=y2(2)+t*dv(2)
P(3)=z2(2)+t*dv(3)

xDir(1)=x2(2)-x2(3)
xDir(2)=y2(2)-y2(3)
xDir(3)=z2(2)-z2(3)

yDir(1)=x2(1)-P(1)
yDir(2)=y2(1)-P(2)
yDir(3)=z2(1)-P(3)

xDirLen=((xDir(1)*xDir(1))+(xDir(2)*xDir(2))+(xDir(3)*xDir(3)))**0.5d0
yDirLen=((yDir(1)*yDir(1))+(yDir(2)*yDir(2))+(yDir(3)*yDir(3)))**0.5d0

do i=1,3
    xDir(i)=xDir(i)/xDirLen
    yDir(i)=yDir(i)/yDirLen
end do

a(1)=xDir(1)
a(2)=yDir(1)

b(1)=xDir(2)
b(2)=yDir(2)

c(1)=xDir(3)
c(2)=yDir(3)

d(1)=a(1)*P(1)+b(1)*P(2)+c(1)*P(3)
d(2)=a(2)*P(1)+b(2)*P(2)+c(2)*P(3)

px=P(1)
py=P(2)
pz=P(3)


!disPA=((P(1)-x2(1))**2.0d0)+(P(2)-y2(1))**2.0d0)+(P(3)-z2(1))**2.0d0))**0.5d0)
!dist,oxyz=pnt2line(c, a, b)
!xDir=b-a
!yDir=np.array(oxyz-c,dtype=np.float64)
    !xDir=xDir/(np.sqrt(np.sum(np.power(xDir, 2.0))))
    !yDir=yDir/(np.sqrt(np.sum(np.power(yDir, 2.0))))






return
end subroutine triangle3dTo2DCoor
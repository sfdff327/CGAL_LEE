subroutine conjugate_gradient_iteration(no_n,A,B,x)
integer*4 i,j,k
integer*4 no_n,iter
real*8 A(no_n,no_n),B(no_n)
real*8 err,err_ind
real*8 gfun,gdeg(no_n),r(no_n)
real*8 x(no_n),alpha,OR_alpha,OR_X(no_n)

real*8 beta,beta_gdeg(no_n)

alpha=0.25
OR_alpha=alpha

beta=0.0

X=0.0
OR_X=0.0
xi=0.0
err_ind=1.0D-12
err=10.0

iter=0
do while(err>=err_ind)
   iter=iter+1
   if(mod(iter,200)==0) write(*,*) iter
   call cal_residual(A,B,x,no_n,r)
   gdeg=-r
   call cal_alpha(A,gdeg,no_n,r,alpha)

   err=0.0
   do i=1,no_n
      x(i)=x(i)-alpha*r(i)
	  err=err+(alpha*r(i))*(alpha*r(i))
   end do
   err=err**0.5
   if(err<=err_ind) goto 121

   call cal_residual(A,B,x,no_n,r)
   call cal_beta(A,gdeg,no_n,r,beta)

   err=0.0
   do i=1,no_n
      x(i)=x(i)-alpha*r(i)
	  err=err+(alpha*r(i))*(alpha*r(i))
   end do
   err=err**0.5
   if(err<=err_ind) goto 121

end do

121 i=0

write(*,*) "conjugate_gradient_iteration iter=",iter
!write(*,*) "x=",(x(i),i=1,no_n)
i=0
return
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine cal_residual(A,B,x,no_n,r)
integer*4 i,j,k,no_n
real*8 A(no_n,no_n), B(no_n),x(no_n)
real*8 r(no_n)

r=0.0

do i=1,no_n
   do j=1,no_n
      r(i)=r(i)+A(i,j)*x(j)
   end do
   r(i)=r(i)+b(i)
end do

return
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine cal_alpha(A,gdeg,no_n,r,alpha)
integer*4 i,j,k,no_n
real*8 A(no_n,no_n),gdeg(no_n)
real*8 r(no_n),alpha
real*8 up_alpha,down_alpha

up_alpha=0.0
down_alpha=0.0

do i=1,no_n
   up_alpha=up_alpha-r(i)*gdeg(i)
end do

do i=1,no_n
   do j=1,no_n
      down_alpha=down_alpha+gdeg(i)*A(i,j)*gdeg(j)
   end do
end do

alpha=up_alpha/down_alpha

return
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine cal_beta(A,gdeg,no_n,r,beta)
integer*4 i,j,k,no_n
real*8 A(no_n,no_n),gdeg(no_n)
real*8 r(no_n),beta
real*8 up_beta,down_beta

up_beta=0.0
down_beta=0.0

do i=1,no_n
   do j=1,no_n
      up_beta=up_beta+r(i)*A(i,j)*gdeg(j)
   end do
end do

do i=1,no_n
   do j=1,no_n
      down_beta=down_beta+gdeg(i)*A(i,j)*gdeg(j)
   end do
end do

beta=up_beta/down_beta

do i=1,no_n
   gdeg(i)=-r(i)+beta*gdeg(i)
end do

return
end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

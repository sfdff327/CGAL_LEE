import pathlib
import struct

import miniball
import numpy
from pygltflib import GLTF2,BufferFormat

import CreatGltf
# import pyvista

# BYTE = 5120
# UNSIGNED_BYTE = 5121
# SHORT = 5122
# UNSIGNED_SHORT = 5123
# UNSIGNED_INT = 5125
# FLOAT = 5126


# load an example gltf file from the khronos collection
# fname = pathlib.Path("glTF-Sample-Models/2.0/Box/glTF-Embedded/Box.gltf")
# fname = pathlib.Path("glTF-Sample-Models/2.0/BoxVertexColors/glTF-Embedded/BoxVertexColors.gltf")

# fname = pathlib.Path("dh.glb")
# fname = pathlib.Path("mt.gltf")

# point

fname = pathlib.Path("glTF-Asset-Generator/Output/Mesh_Indices/Mesh_Indices_07.gltf")
fname = pathlib.Path("glTF-Asset-Generator/Output/Mesh_Indices/Mesh_Indices_08.gltf")
gltf = GLTF2().load(fname)
current_scene = gltf.scenes[gltf.scene]
print("gltf=",gltf)
meshes = gltf.meshes
print("len(meshes)=",len(meshes))
for mesh in meshes:
    print(mesh)
    primitives=mesh.primitives
    for primitive in primitives:
        mode = primitive.mode
        print("mode=",primitive.mode)


buffer0 = gltf.buffers[0]
gltf.convert_buffers(BufferFormat.DATAURI)
data0 = gltf.decode_data_uri(buffer0.uri)
ele_ind = []
POSITION=[]
accessors = gltf.accessors
print("len(accessors)=",len(accessors))
for accessor in accessors:
    bufferView =gltf.bufferViews[accessor.bufferView]
    byte = int(bufferView.byteLength/accessor.count)
    print("byte=",byte)
    if (accessor.type == 'SCALAR'):
        if (mode==0):
            no_ele = int(accessor.count)
        elif (mode==1):
            no_ele = int(accessor.count/2)
        elif (mode==4):
            no_ele = int(accessor.count/3)            
        print("no_ele=",no_ele)
        for i in range(no_ele):
            if (mode==0):
                index = bufferView.byteOffset + accessor.byteOffset + i*1*byte
                d = data0[index:index+1*byte]
                # v = struct.unpack("<bbb", d)
                v = struct.unpack("<i", d)
            elif (mode ==1):
                index = bufferView.byteOffset + accessor.byteOffset + i*2*byte
                d = data0[index:index+2*byte]
                # v = struct.unpack("<bbb", d)
                v = struct.unpack("<ii", d)                 
            elif (mode ==4):
                index = bufferView.byteOffset + accessor.byteOffset + i*3*byte
                d = data0[index:index+3*byte]
                # v = struct.unpack("<bbb", d)
                v = struct.unpack("<iii", d)                
            ele_ind.append(v)
            print(i, v)   
        print("ele_ind=",ele_ind)    
    else:
        no_node = int(accessor.count)
        print("no_node=",no_node)
        for i in range(no_node):
            index1 = bufferView.byteOffset + accessor.byteOffset + i*12
            d = data0[index1:index1+12]
            v = struct.unpack("<fff", d)
            POSITION.append(v)
        print("POSITION=",POSITION)



quit()
meshs = gltf.meshes
print("len(meshs)=",len(meshs))
buffer0 = gltf.buffers[0]
# print(buffer0)
gltf.convert_buffers(BufferFormat.DATAURI)
data0 = gltf.decode_data_uri(buffer0.uri)
ele_ind = []
accessors = gltf.accessors
print("len(accessors)=",len(accessors))
accessor = accessors[0]

if (accessor.type == 'SCALAR'):
    no_ele = int(accessor.count/3)
    bufferView =gltf.bufferViews[accessor.bufferView]
    # print("no_ele=",no_ele)
    # print("bufferView.byteOffset=",bufferView.byteOffset)
    # print("accessor.byteOffset=",accessor.byteOffset)
    for i in range(no_ele):
        index = bufferView.byteOffset + accessor.byteOffset + i*6
        d = data0[index:index+6]
        # v = struct.unpack("<bbb", d)
        v = struct.unpack("<hhh", d)
        ele_ind.append(v)
        print(i, v)

POSITION=[]
NORMAL =[]
COLOR_0 =[]
TEXCOORD_0 =[]

accessor1 = accessors[1]
accessor2 = accessors[2]


no_ele = int(accessor.count/3)
no_node = int(accessor1.count)
print(no_ele,no_node)
# quit()
bufferView1 =gltf.bufferViews[accessor1.bufferView]
bufferView2 =gltf.bufferViews[accessor2.bufferView]

for i in range(no_node):
    index1 = bufferView1.byteOffset + accessor1.byteOffset + i*12
    d = data0[index1:index1+12]
    v = struct.unpack("<fff", d)
    POSITION.append(v)

    index2 = bufferView2.byteOffset + accessor2.byteOffset + i*16
    d = data0[index2:index2+16]
    v = struct.unpack("<ffff", d)
    COLOR_0.append(v)


# print("ele_ind[0][0]=",ele_ind[0][0])
# print("ele_ind[0][1]=",ele_ind[0][1])
# print("ele_ind[0][2]=",ele_ind[0][2])

print("ele_ind=")
print(ele_ind)
print("POSITION=")
print(POSITION)
print("COLOR_0=")
print(COLOR_0)
quit()

# get the first mesh in the current scene (in this example there is only one scene and one mesh)
# mesh = gltf.meshes[gltf.scenes[gltf.scene].nodes[0]]
meshs = gltf.meshes
# print("meshs=",meshs)
print("len(meshs)=",len(meshs))

buffer0 = gltf.buffers[0]
data0 = gltf.decode_data_uri(buffer0.uri)
# print(data0)

ele_ind = []
accessors = gltf.accessors
print("len(accessors)=",len(accessors))
accessor = accessors[0]
if (accessor.type == 'SCALAR'):
    no_ele = int(accessor.count/3)
    bufferView =gltf.bufferViews[accessor.bufferView]
    # print("no_ele=",no_ele)
    # print("bufferView.byteOffset=",bufferView.byteOffset)
    # print("accessor.byteOffset=",accessor.byteOffset)
    for i in range(no_ele):
        index = bufferView.byteOffset + accessor.byteOffset + i*6
        d = data0[index:index+6]
        v = struct.unpack("<hhh", d)
        ele_ind.append(v)
        # print(i, v)

POSITION=[]
NORMAL =[]
COLOR_0 =[]
TEXCOORD_0 =[]

accessor1 = accessors[1]
accessor2 = accessors[2]
accessor3 = accessors[3]
accessor4 = accessors[4]
no_node = int(accessor.count/3)
bufferView1 =gltf.bufferViews[accessor1.bufferView]
bufferView2 =gltf.bufferViews[accessor2.bufferView]
bufferView3 =gltf.bufferViews[accessor3.bufferView]
bufferView4 =gltf.bufferViews[accessor4.bufferView]
for i in range(no_node):
    index1 = bufferView1.byteOffset + accessor1.byteOffset + i*12
    d = data0[index1:index1+12]
    v = struct.unpack("<fff", d)
    POSITION.append(v)

    index2 = bufferView2.byteOffset + accessor2.byteOffset + i*12
    d = data0[index2:index2+12]
    v = struct.unpack("<fff", d)
    NORMAL.append(v)

    index3 = bufferView3.byteOffset + accessor3.byteOffset + i*16
    d = data0[index3:index3+16]
    v = struct.unpack("<ffff", d)
    # print("COLOR",v)
    COLOR_0.append(v)

    index4 = bufferView4.byteOffset + accessor4.byteOffset + i*8
    d = data0[index4:index4+8]
    v = struct.unpack("<ff", d)
    TEXCOORD_0.append(v)

# print("ele_ind[0][0]=",ele_ind[0][0])
# print("ele_ind[0][1]=",ele_ind[0][1])
# print("ele_ind[0][2]=",ele_ind[0][2])

print("POSITION[0]=",POSITION[0])
print("NORMAL[0]=",NORMAL[0])
print("COLOR_0[0]=",COLOR_0[0])
print("TEXCOORD_0[0]=",TEXCOORD_0[0])
# print("COLOR_0=",COLOR_0)

# https://stackoverflow.com/questions/15140072/how-to-map-number-to-color-using-matplotlibs-colormap

ele_ind=numpy.array(ele_ind,dtype="uint8")
POSITION= numpy.array(POSITION,dtype="float32")
print("type(POSITION)=",type(POSITION))

CreatGltf.mesh2Gltf(ele_ind,POSITION)
# gltf meshes primitives  attributes COLOR_0


quit()


no_ele = int(30)
for i in range(no_ele):
    index = i*8  # the location in the buffer of this vertex
    d = data1[index:index+8]  # the vertex data
    print("d=",d)
    v = struct.unpack("<ff", d)   # convert from base64 to three floats
    vertices.append(v)
    print(i, v)


animations = gltf.animations
asset = gltf.asset
bufferViews = gltf.bufferViews
buffers = gltf.buffers
cameras = gltf.cameras


quit()

mesh = meshs[0]
# print("mesh",mesh)
# Mesh(extensions={}, extras={}, primitives=[Primitive(extensions={}, extras={}, attributes=Attributes(POSITION=0, NORMAL=None, TANGENT=None, TEXCOORD_0=1, TEXCOORD_1=None, COLOR_0=None, JOINTS_0=None, WEIGHTS_0=None), indices=2, mode=4, material=0, targets=[])], weights=[], name='mesh0')

# get the vertices for each primitive in the mesh (in this example there is only one)
buffer2 = gltf.buffers[2]
data2 = gltf.decode_data_uri(buffer2.uri)
vertices=[]
no_ele = int(168/3)
for i in range(no_ele):
    index = i*12  # the location in the buffer of this vertex
    d = data2[index:index+12]  # the vertex data
    # print("d=",d)
    v = struct.unpack("<iii", d)   # convert from base64 to three floats
    vertices.append(v)
    # print(i, v)

buffer3 = gltf.buffers[3]
data3 = gltf.decode_data_uri(buffer3.uri)
vertices=[]
no_ele = int(30)
for i in range(no_ele):
    index = i*12  # the location in the buffer of this vertex
    d = data3[index:index+12]  # the vertex data
    print("d=",d)
    v = struct.unpack("<iii", d)   # convert from base64 to three floats
    vertices.append(v)
    print(i, v)
quit()


for i in range(len(meshs)):
    mesh = meshs[0]
    for primitive in mesh.primitives:

        # get the binary data for this mesh primitive from the buffer
        print("primitive.attributes.POSITION=",primitive.attributes.POSITION)
        accessor = gltf.accessors[primitive.attributes.POSITION]
        print("accessor=",accessor)
        bufferView = gltf.bufferViews[accessor.bufferView]
        buffer = gltf.buffers[bufferView.buffer]
        data = gltf.decode_data_uri(buffer.uri)
        buffer1 = gltf.buffers[1]
        data1 = gltf.decode_data_uri(buffer1.uri)
        buffer2 = gltf.buffers[2]
        data2 = gltf.decode_data_uri(buffer2.uri)
        print("data=",data)
        print("data1=",data1)
        print("data2=",data2)

        # pull each vertex from the binary buffer and convert it into a tuple of python floats
        vertices = []
        vertices1 =[]
        vertices2 =[]
        print("accessor.count=",accessor.count)
        for i in range(accessor.count):
            index = bufferView.byteOffset + accessor.byteOffset + i*12  # the location in the buffer of this vertex
            d = data[index:index+12]  # the vertex data
            print("d=",d)
            v = struct.unpack("<fff", d)   # convert from base64 to three floats

            d1 = data1[index:index+12]  # the vertex data
            print("d1=",d1)
            v1 = struct.unpack("<fff", d1)   # convert from base64 to three floats

            d2 = data2[index:index+12]  # the vertex data
            print("d2=",d2)
            v2 = struct.unpack("<fff", d2)   # convert from base64 to three floats

            vertices.append(v)
            vertices1.append(v1)
            vertices2.append(v2)
            print(i, v)
            print(i, v1)
            print(i, v2)
        
# print("vertices=",vertices)
print("len(vertices)=",len(vertices))

# convert a numpy array for some manipulation
S = numpy.array(vertices)

# use a third party library to perform Ritter's algorithm for finding smallest bounding sphere
C, radius_squared = miniball.get_bounding_ball(S)

# output the results
print(f"center of bounding spcdhere: {C}\nradius squared of bounding sphere: {radius_squared}")

此檔案說明c++ vector用法，[參考網站](https://shengyu7697.github.io/std-vector/)

# 目錄  
## 1. vector 初始化
這邊介紹幾種 vector 初始化，
這樣是宣告一個 int 整數類型的 vector，裡面沒有任何元素(空)，size 為 0 表示 vector 容器中沒有任何元素，capacity 也是 0，

### 1.1. vector &lt;int&gt; v;
```c++
#include <vector>
using namespace std;
int main() {
    vector<int> v;
    return 0;
}
```
### 1.1. push_back 填資料
先宣告一個空的 vector，再透過 push_back 將資料一直推進去，
```c++
vector<int> v;
v.push_back(1);
v.push_back(2);
v.push_back(3);
```
這是先建立空vector，再依序填資料。

你也可以寫成一行，但這語法需要編譯器 c++11 支援，
```c++
vector<int> v = {1, 2, 3};
```
或者是這樣寫也可以，
```c++
vector<int> v({1, 2, 3});
```

### 1.2. 複製 vector
假如要從另外一個 vector 容器複製資料過來當作初始值的話可以這樣寫，
```c++
vector<int> v1 = {1, 2, 3};
vector<int> v2 = v1;
```
或者這樣，
```c++
vector<int> v1 = {1, 2, 3};
vector<int> v2(v1);
```
也可以從傳統陣列裡複製過來當作初始值，
```c++
int n[3] = {1, 2, 3};
vector<int> v(n, n+3);
```

### 1.3. 部分複製 vector
不想複製來源 vector 全部的資料，想要指定複製 vector 的範圍的話也可以，例如我要複製 v1 vector 的第三個元素到倒數第二個元素，
```c++
vector<int> v1 = {1, 2, 3, 4, 5};
vector<int> v2(v1.begin()+2, v1.end()-1); // {3, 4}
```
如果是指定複製傳統陣列的範圍的話，可以這樣寫，
```c++
int n[5] = {1, 2, 3, 4, 5};
vector<int> v(n+2, n+4); // {3, 4}
```

### 1.4. 存取 vector 元素的用法
vector 用 [] 來隨機存取元素，第一個元素為 v[0]，索引值是 0，第二個元素為 v[1]，索引值是 1，依此類推，[] 不只可以讀取元素也可以用來修改元素，例如 v[0] = 4 像下面範例這樣寫，
```c++
vector<int> v = {1, 2, 3};
cout << v[0] << "\n"; // 1
cout << v[1] << "\n"; // 2
v[0] = 4;
cout << v[0] << "\n"; // 4
```
### 1.5. 在 vector 容器尾巴新增元素的用法
在前面已經有稍微透漏了怎麼新增 vector 元素的方法了，沒錯就是用 push_back() 這個方法，它會把元素加在 vector 容器的尾巴，
先宣告一個空的 vector，再透過 push_back 將資料一直推進去，
```c++
vector<int> v = {1, 2, 3};
v.push_back(4); // {1, 2, 3, 4}
v.push_back(5); // {1, 2, 3, 4, 5}
v.push_back(6); // {1, 2, 3, 4, 5, 6}
```

### 1.6. 在 vector 容器尾巴移除元素的用法
移除 vector 容器尾巴的元素用 pop_back()，一次只能從尾端移除一個元素，不能指定移除的數量，
```c++
vector<int> v = {1, 2, 3};
v.pop_back(); // {1, 2}
v.pop_back(); // {1}
```

### 1.7. 預先配置 vector 容器大小
如果我們一開始就知道容器的裡要放置多少個元素的話，可以透過 reserve() 來預先配置容器大小，這樣可以減少一直配置記憶體的機會，假設我要預先配置好 2 的大小的話可以這樣寫，之後一樣用 push_back 將元素推進去，

然後你可以用 capacity() 與 size() 來檢查看看，

```c++
vector<int> v;
v.reserve(2);
cout << v.size() << "\n";
cout << v.capacity() << "\n";
v.push_back(1); // {1}
v.push_back(2); // {1, 2}
v.push_back(3); // {1, 2, 3}
cout << v.size() << "\n";
cout << v.capacity() << "\n";
```
import io
import numpy as np


def tecplotPyvistaStreamFromSrc(testPLOTfilename,stream):
    no_node = len(stream.points)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone i='+str(no_node)+',F=point\n') 
        for i in range(0,len(stream.points)):
            f.write(str(stream.points[i][0])+' '+str(stream.points[i][1])+' '+str(stream.points[i][2])+'\n')        
        f.close()

def tecplotPyvistaStream(testPLOTfilename,stream,src):
    no_node = len(stream.points)+len(src.points)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone i='+str(no_node)+',F=point\n') 
        f.write(str(src.points[0][0])+' '+str(src.points[0][1])+' '+str(src.points[0][2])+'\n')
        for i in range(0,len(stream.points)):
            f.write(str(stream.points[i][0])+' '+str(stream.points[i][1])+' '+str(stream.points[i][2])+'\n')        
        f.close()

def tecplotPyvistaUSG(testPLOTfilename,xyz,ele_ind):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    # print("no_node,no_ele=",no_node,no_ele)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=TRIANGLE\n') 
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
        f.close()

def tecplotPyvistaUSG_z(testPLOTfilename,xyz,ele_ind,z):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    # print("no_node,no_ele=",no_node,no_ele)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","v(m)"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=TRIANGLE\n') 
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(z[i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
        f.close()

def TetrahedronUSG_z(testPLOTfilename,xyz,ele_ind,z):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","data"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEPOINT,ET=TETRAHEDRON\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(z[i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()

def TetrahedronUSG_zCenter(testPLOTfilename,xyz,ele_ind,z):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","data"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEBLOCK,ET=TETRAHEDRON\n') 
        f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0,no_ele):
            f.write(str(z[i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()



def TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,z):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","data"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEBLOCK,ET=TRIANGLE\n') 
        f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0,no_ele):
            f.write(str(z[i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
        f.close()




def TecplotTrimesh(outfile_name,mesh):
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"'+'\n')
        f.write('Zone N='+str(len(mesh.vertices))+',E='+str(len(mesh.faces))+',F=FEpoint,ET=triangle\n') 
        for i in range(0, len(mesh.vertices)):
            f.write(str(mesh.vertices[i][0])+','+str(mesh.vertices[i][1])+','+str(mesh.vertices[i][2])+'\n')
        for i in range(0,len(mesh.faces)):
            f.write(str(mesh.faces[i][0]+1)+','+str(mesh.faces[i][1]+1)+','+str(mesh.faces[i][2]+1)+'\n')        
        f.close

def tecplotPyvistaUSG_line_z(outfile_name,xyz,ele_ind,z):
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","data"'+'\n')
        f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+','+str(xyz[i][1])+','+str(xyz[i][2])+','+str(z[i])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')        
        f.close

def TecplotLINESEG2D(outfile_name,xy,ele_ind):
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)"'+'\n')
        f.write('Zone N='+str(len(xy))+',E='+str(len(ele_ind))+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, len(xy)):
            f.write(str(xy[i][0])+','+str(xy[i][1])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+'\n')        
        f.close

def TecplotLINESEG3D(outfile_name,xyz,ele_ind):
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"'+'\n')
        f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=LINESEG\n') 
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+','+str(xyz[i][1])+','+str(xyz[i][2])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+'\n')        
        f.close

def TecplotTriangle3D_By_eachNbEleInter(xyz,ele_ind,fracID,no_frac,eachNbEleInter,frac_segeID,frac_sege):
    for i in range(0,len(eachNbEleInter)):
        filename='fracConn_N'+str(i)+'.dat'
        checkeachNbEleInter=np.zeros(no_frac)
        print("no_frac=",no_frac)
        for j in range(0,len(eachNbEleInter[i])):
            # print(j,eachNbEleInter[i][j])
            checkeachNbEleInter[eachNbEleInter[i][j]]=1
        print(checkeachNbEleInter)
        quit()


    with open('PlotTr3D.dat','w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        # f.write('Variables="x(m)","y(m)","z(m)"'+'\n')
        # f.write('Zone T="Tri3Dmesh",N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n')
        f.write('Variables="x(m)","y(m)","z(m)","ID"'+'\n')
        f.write('Zone T="Tri3Dmesh",N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=TRIANGLE\n') 
        f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0, len(fracID)):
            f.write(str(fracID[i])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')
    return 0



def TecplotTriangle3D(xyz,ele_ind,fracID):
    with open('PlotTr3D.dat','w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        # f.write('Variables="x(m)","y(m)","z(m)"'+'\n')
        # f.write('Zone T="Tri3Dmesh",N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEpoint,ET=TRIANGLE\n')
        f.write('Variables="x(m)","y(m)","z(m)","ID"'+'\n')
        f.write('Zone T="Tri3Dmesh",N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=TRIANGLE\n') 
        f.write('varlocation=([1,2,3]=nodal,[4]=CELLCENTERED)\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][0])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][1])+'\n')
        for i in range(0, len(xyz)):
            f.write(str(xyz[i][2])+'\n')
        for i in range(0, len(fracID)):
            f.write(str(fracID[i])+'\n')
        for i in range(0,len(ele_ind)):
            f.write(str(ele_ind[i][0]+1)+','+str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')
    return 0

def TecplotMesh2D(outfile_name,mesh_points,mesh_tris,xy,SegeNb,TTIxyz,plotaddSege):
    with open(outfile_name,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)"'+'\n')
        f.write('Zone T="mesh",N='+str(len(mesh_points))+',E='+str(len(mesh_tris))+',F=FEpoint,ET=TRIANGLE\n') 
        for i in range(0, len(mesh_points)):
            f.write(str(mesh_points[i][0])+','+str(mesh_points[i][1])+'\n')
        for i in range(0,len(mesh_tris)):
            f.write(str(mesh_tris[i][0]+1)+','+str(mesh_tris[i][1]+1)+','+str(mesh_tris[i][2]+1)+'\n')
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)"'+'\n')
        f.write('Zone T="xy",N='+str(len(xy))+',E='+str(len(SegeNb))+',F=FEpoint,ET=LINESEG\n')  
        for i in range(0, len(xy)):
            f.write(str(xy[i][0])+','+str(xy[i][1])+'\n')
        for i in range(0,len(SegeNb)):
            f.write(str(SegeNb[i][0]+1)+','+str(SegeNb[i][1]+1)+'\n')                
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)"'+'\n')
        f.write('Zone T="TTI",N='+str(len(TTIxyz))+',E='+str(len(plotaddSege))+',F=FEpoint,ET=LINESEG\n')  
        for i in range(0, len(TTIxyz)):
            f.write(str(TTIxyz[i][0])+','+str(TTIxyz[i][1])+'\n')
        for i in range(0,len(plotaddSege)):
            f.write(str(plotaddSege[i][0]+1)+','+str(plotaddSege[i][1]+1)+'\n')                                          
        f.close()

def PrintSege2DPoint(xy,SegeNb):
    print("=======")
    print("len(xy)=",len(xy))
    for i in range(0, len(xy)):
        print(i,xy[i][0],xy[i][1])
    print("len(SegeNb)=",len(SegeNb))
    for i in range(0, len(SegeNb)): 
        print(i,SegeNb[i][0],SegeNb[i][1])
    print("=======")
    return 0

def mayaviInput(testPLOTfilename,xyz,u,ele_ind):
    no_node = len(xyz)
    no_ele = len(ele_ind)
    # print("no_node,no_ele=",no_node,no_ele)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","head(m)"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=TRIANGLE\n') 
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(u[i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][0])+' '+str(ele_ind[i][1])+' '+str(ele_ind[i][2])+'\n')

    return 0

def mf2005MeshToTecplot(testPLOTfilename,meshConn,grid_x,grid_y,grid_z):
    no_node = len(meshConn)*2
    no_ele = len(meshConn)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=LINESEG\n')
        for i in range(0,no_ele):
            # print("i=",i)
            ik=meshConn[i][0][0]
            ij=meshConn[i][0][1]
            ii=meshConn[i][0][2]
            jk=meshConn[i][1][0]
            jj=meshConn[i][1][1]
            ji=meshConn[i][1][2] 
            # print("ii,ij,ik=",ii,ij,ik)           
            # print("ji,jj,jk=",ji,jj,jk)           
            f.write(str(grid_x[ij][ii])+' '+str(grid_y[ij][ii])+' '+str(grid_z[ik][ij][ii])+'\n')
            # f.write(str(grid_z[ik][ij][ii]))
            f.write(str(grid_x[jj][ji])+' '+str(grid_y[jj][ji])+' '+str(grid_z[jk][jj][ji])+'\n')
            NA=-1
            NB=0
        for i in range(0,no_ele):
            NA=NA+2
            NB=NB+2
            f.write(str(NA)+' '+str(NB)+'\n')

    # print("len(meshConn[0])=",meshConn[0])
    # print("len(meshConn[0][0])=",meshConn[0][0])
    # print("len(meshConn[0][0][0])=",meshConn[0][0][0])
    # print("len(meshConn[0][0][1])=",meshConn[0][0][1])
    # print("len(meshConn[0][0][2])=",meshConn[0][0][2])


    return 0

def mf2005HeadToTecplot(tecplot_filename,ncol,nrow,nlay,grid_x,grid_y,grid_z,head,ibound):
    with open(tecplot_filename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","h(m)","Ibound"\n')
        f.write('Zone I='+str(ncol)+',J='+str(nrow)+',K='+str(nlay)+',F=point\n')
        for k in range(0, nlay):
            for j in range(0,nrow):
                for i in range(0,ncol):
                    f.write(str(grid_x[j][i])+' '+str(grid_y[j][i])+' '+str(grid_z[k][j][i])+' '+str(head[k][j][i])+' '+str(ibound[k][j][i])+'\n')
    return 0    

def mf2005ToTecplot(tecplot_filename,ncol,nrow,nlay,grid_x,grid_y,grid_z):
    with open(tecplot_filename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)"\n')
        f.write('Zone I='+str(ncol)+',J='+str(nrow)+',K='+str(nlay)+',F=point\n')
        for k in range(0, nlay):
            for j in range(0,nrow):
                for i in range(0,ncol):
                    f.write(str(grid_x[j][i])+' '+str(grid_y[j][i])+' '+str(grid_z[k][j][i])+'\n')
    return 0

def main(data):
    result = transGeoJSON(data)
    return result

# if __name__== "__main__":
#     result = main(data)    

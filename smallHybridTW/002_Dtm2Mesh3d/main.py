from calendar import c
import numpy as np
import re
import json
import geojson
import pandas as pd
from pathlib import Path
import pickle
import copy
import pyvista as pv
# from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt

import math

import dem
import readTetgen
import pyToTecplot
import meshTool
import hybridFlow
import plotPyvista
import pyFabSTL
import os
from os.path import exists

import meshpy
import meshpy.triangle as triangle
from meshpy.tet import MeshInfo, build,Options

import readGeoJSON
from scipy.io import FortranFile
from pathlib import Path
import time



from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()
def createSmallDomain(domainCenter,dx,dy,dz):
  xyz=np.zeros((26,3),dtype=np.float64)
  xyz[0]=domainCenter-np.array([dx/2,dy/2,dz/2])
  
  xyz[1]=xyz[0]+np.array([dx,0.,0.])
  xyz[2]=xyz[0]+np.array([dx,dy,0.])
  xyz[3]=xyz[0]+np.array([0.,dy,0.])
  xyz[4]=xyz[0]+np.array([0.,0.,dz])
  xyz[5]=xyz[1]+np.array([0.,0.,dz])
  xyz[6]=xyz[2]+np.array([0.,0.,dz])
  xyz[7]=xyz[3]+np.array([0.,0.,dz])

  xyz[8] =xyz[0]+np.array([dx/2,0.,0.])
  xyz[9] =xyz[0]+np.array([dx,dy/2,0.])
  xyz[10]=xyz[0]+np.array([dx/2,dy,0.])
  xyz[11]=xyz[0]+np.array([0.,dy/2,0.])

  xyz[12]=xyz[4]+np.array([dx/2,0.,0.])
  xyz[13]=xyz[4]+np.array([dx,dy/2,0.])
  xyz[14]=xyz[4]+np.array([dx/2,dy,0.])
  xyz[15]=xyz[4]+np.array([0.,dy/2,0.])

  xyz[16]=xyz[0]+np.array([0.,0.,250.])
  xyz[17]=xyz[16]+np.array([1000.,0.,0.])
  xyz[18]=xyz[16]+np.array([2000.,0.,0.])
  xyz[19]=xyz[16]+np.array([2000.,1000.,0.])
  xyz[20]=xyz[16]+np.array([2000.,2000.,0.])
  xyz[21]=xyz[16]+np.array([1000.,2000.,0.])
  xyz[22]=xyz[16]+np.array([0.,2000.,0.])
  xyz[23]=xyz[16]+np.array([0.,1000.,0.])

  xyz[24]=xyz[0]+np.array([1000.,1000.,0.])
  xyz[25]=xyz[4]+np.array([1000.,1000.,0.])

  faces=np.hstack([
[4, 0,11,24,8],[4,11,3,10,24],[4,  8,24,9,1],[4, 24,10,2,9],[4,4,15,23,16],[4,16,23,11,0],
[4,15,7,22,23],[4,23,22,3,11],[4,5,12,17,18],[4, 18,17,8,1],[4,12,4,16,17],[4, 17,16,0,8],
[4,6,13,19,20],[4, 20,19,9,2],[4,13,5,18,19],[4, 19,18,1,9],[4,7,14,21,22],[4,22,21,10,3],
[4,14,6,20,21],[4,21,20,2,10],[4,4,12,25,15],[4,15,25,14,7],[4,12,5,13,25],[4,25,13,6,14],])
  smallDomain=pv.PolyData(xyz,faces)
  nb=-np.linspace(0,24,num=24,endpoint=False,dtype=int)
  smallDomain=pv.PolyData(xyz,faces)
  smallDomain['faceNb']=nb
  smallDomain.save('smallDomain.vtk')
  # print(smallDomain.points)
  # print(smallDomain.faces)
  
  return smallDomain

if __name__ == "__main__":
  start = time.time()

  '''
  tetgen='tetgen'
  mesh_info = MeshInfo()
  vertices=[(0,0,0), (2,0,0), (2,2,0), (0,2,0),
    (0,0,12), (2,0,12), (2,2,12), (0,2,12)]
  cells=[[0,1,2,3],
    [4,5,6,7],
    [0,4,5,1],
    [1,5,6,2],
    [2,6,7,3],
    [3,7,4,0],
    [0,3,2]]
  # mesh_info.set_points([(0,0,0), (2,0,0), (2,2,0), (0,2,0),(0,0,12), (2,0,12), (2,2,12), (0,2,12),])
  # mesh_info.set_facets([[0,1,2,3],[4,5,6,7],[0,4,5,1],[1,5,6,2],[2,6,7,3],[3,7,4,0],])  
  runTetgenCheck=0
  while runTetgenCheck==0:
    mesh_info = MeshInfo()
    mesh_info.set_points(vertices)
    mesh_info.set_facets(cells)
    mesh_info.save_nodes(tetgen)
    mesh_info.save_poly(tetgen)
    mesh_info.save_neighbors(tetgen)
    os.system('tetgen -d tetgen.poly >tetgenOut')
    f = open('tetgenOut')
    delNb=[]
    for line in f.readlines():      
      if 'No faces are intersecting.' in line :
        runTetgenCheck=1
        print("mesh Generation OK")
      if ' duplicates facet #' in line:
        # nb=line.split('duplicates facet #')[1]
        nb = int(re.findall(' duplicates facet #(.*?) ', line)[0])
        nb=nb-1
        print("nb=",nb)
        if (nb not in delNb):
          delNb.append(nb)
          print("append")
        outputFileCheck = Path("tetgenOut")
        if outputFileCheck.is_file():
            os.system('rm tetgen.1.*')
            os.system('rm tetgenOut')
    print("delNb=",delNb)
    for i in range(0,len(delNb)):
      del cells[delNb[i]] 
    # quit()
  mesh = build(mesh_info, options=Options(switches='pqA'))
  print(np.array(mesh.points))
  print(np.array(mesh.elements))
  # print(dir(mesh))
  quit()
  '''

  OriginPointsF=r'../../../../INER_DATA/Domain2D/OriginPoints.txt'
  path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  q123Folder=r'../../HybridTW/001_stlToMesh'

  EpsgIn=3825
  EpsgOut=4326

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  # print("delta_x,delta_y=",delta_x,delta_y)

  case=1
  if (case==1):
    # path=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/testWriteMeshALL.poly'
    with open('polyF.dat','w',encoding = 'utf-8') as f:
      f.write(path)
      f.close()
    os.system('./ReadPoly')
    defxyzF=r'dfnXYZ.txt'
    defxyzCsv = pd.read_csv(defxyzF,sep='\\s+',header=None)
    defxyzCsv = defxyzCsv.fillna('')
    defxyzCsv.columns=["x", "y","z"]  
    points=np.zeros((len(defxyzCsv),3),dtype=np.float64)      
    for i in range(0,len(defxyzCsv)):
      points[i]=[defxyzCsv['x'][i],defxyzCsv['y'][i],defxyzCsv['z'][i]]
    dfnELEF=r'dfnELE.txt'
    dfnELECsv = pd.read_csv(dfnELEF,sep='\\s+',header=None)
    dfnELECsv = dfnELECsv.fillna('')
    dfnELECsv.columns=["e0", "e1","e2"]  
    faces=[]  
    for i in range(0,len(dfnELECsv)):
      faces.extend([3,dfnELECsv['e0'][i],dfnELECsv['e1'][i],dfnELECsv['e2'][i]])
    dfnPoly=pv.PolyData(points,faces)
    dfnPoly.save('dfnPoly.vtk')        
    # quit()   

  # dfnOrUsg=pyFabSTL.fabRead(None,[],'DFN_all.dat',[])
  # dfnOrUsgArea=dfnOrUsg.compute_cell_quality(quality_measure='area')
  # isGoodArea=dfnOrUsgArea['CellQuality']>0
  # print(np.sum(isGoodArea!=0))
  # isGoodArea=isGoodArea.tolist()
  isGoodArea=[]

  '''水文參數設定'''
  Kfracture=5.0e-5
  # poro_fracture=0.4
  poro_fracture=1.5e-2 #F2's poro
  aperture=5.0e-6
  alpha=0.008
  sa=3.2
  density=1.0*(1.0+alpha*sa)
  # Krock=1.0e-10
   
  matrix_alpha_w=4.8
  Ksurface=1.0e-5
  poro_surface=5.4e-3 
  surface_alpha_w=4.8
  F1F2K=5.0e-6
  poro_F1F2K=0.4
  F1F2K_alpha_w=1.0 

  Krock=[1.0e-10,3.3e-8] # 'matrix', 'edz'
  poro_rock=[5.4e-3,1.0e-4] # 'matrix', 'edz'



  #########
  regionName= ['matrix', 'edz', 'mt', 'dt', 'dh']
  regionTag = [       5,   100,  101,   102, 103]
  addRegion = 2

  # 
  # mesh size = sp
  pyFabSTL.createEDZ()
  sp= 50.         #18.     #50.
  dfnsp = 20.     #12.5 #18. #sp #50. #sp/10.
  max_angle=175.5 #175.5
  min_area=62. #55. #30.

  case=0 #如果要增加dfn，要設定case=1
  '''讀取原始dfn'''
  noTestFrac=8932 #8933 #4237 #20   8770,
  # for i in range(noTestFrac-10,noTestFrac+1):
  #   print(i,dfnOrUsgArea['CellQuality'][i],isGoodArea[i])
  # 8932 4237
  delFracNb=[3119,3148,3401,4111,4122,4124,4126,4128,4136,4137,4138,4140,4141,4142,4145,4146,4147,4148,4150,4201,4203,4207,4208,4210,4211,4214,4215,4216,4217,4218,4219,4220,4222,4225,4226,4227,4228,4231,4232,4234,4235,4236,4237,4301,4302,4303,4305,4306,4309,4313,4314,4320,4323,4329,4330,4331,4332,4333,4335,4336,4407,4414,
  5323,5347,6287,6288,6297,6380,6495,6499,
  7198,7450,7470,7477,7515,7518,7520,7570,7608,7700,7780,7793,
  8575,8722,8730,8750,8760,8933,
  ]
  longRunTime=[4231,4232,4236,4237,4545,4301]
  print("len(delFracNb)=",len(delFracNb))

  print("noTestFrac=",noTestFrac)
  
  if (case==1):
    dfnOrUsg=pyFabSTL.fabRead(noTestFrac,delFracNb,'DFN_part.dat',isGoodArea,printInd=True)       
  else:
    dfnOrUsg=pv.read('dfnOrUsg.vtk')

  # print(dfnOrUsg.n_points)
  # print(dfnOrUsg.n_cells)

  # dfnOrUsgmax_angle=dfnOrUsg.compute_cell_quality(quality_measure='max_angle')
  # isGoodAngle=dfnOrUsgmax_angle['CellQuality']<172.5
  # no_isGoodAngle=int(np.sum(isGoodAngle!=0))
  # frac_ele_k=[]
  # frac_ele_ap=[]
  # frac_local_ID=[]
  # faces=dfnOrUsg.faces.reshape(-1,4)
  # cells=[]
  # nodeCheck=np.zeros(dfnOrUsg.n_points,dtype=np.int32)
  # for i in range(0,no_isGoodAngle):
  #   if isGoodAngle[i]==True:
  #     cells.append(faces[i].tolist()) 
  #     frac_ele_k.append(dfnOrUsg['frac_ele_k'][i])
  #     frac_ele_ap.append(dfnOrUsg['frac_ele_ap'][i])
  #     frac_local_ID.append(dfnOrUsg['fracID'][i])
  #     for j in range(1,4):
  #       nodeCheck[faces[i].tolist()[j]]=1
  # no_points=np.sum(nodeCheck)
  # points=np.zeros((no_points,3),dtype=np.float64)
  # nb=-1
  # for i in range(0,dfnOrUsg.n_points):
  #   if (nodeCheck[i]==1):
  #     nb=nb+1
  #     points[nb]=dfnOrUsg.points[i]
  #     nodeCheck[i]=nb
  # for i in range(0,len(cells)):
  #   for j in range(1,4):
  #     cells[i][j]=nodeCheck[cells[i][j]]
  # no_cells=len(cells)
  # cells=np.array(cells).ravel()             
  # dfnOrUsg = pv.PolyData(points,cells) 
  # dfnOrUsg['frac_ele_k']=np.array(frac_ele_k)
  # dfnOrUsg['frac_ele_ap']=np.array(frac_ele_ap)
  # dfnOrUsg['fracID']=np.array(frac_local_ID)


  # print("dfnOrUsg['frac_ele_k']=",dfnOrUsg['frac_ele_k'])
  # print("dfnOrUsg['frac_ele_ap']=",dfnOrUsg['frac_ele_ap'])
  # print("dfnOrUsg['fracID']=",dfnOrUsg['fracID'])

  '''讀取原始FAB與STL轉換後的vtk'''
  # q123Folder=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
  dh=pv.read(q123Folder+r'/dh.vtk')
  dt=pv.read(q123Folder+r'/dt.vtk')
  mt=pv.read(q123Folder+r'/mt.vtk')
  ''' 
  # edz重建
  '''
  edz=pv.read(r'edzNew.vtk')
  # edz=pv.read(r'edzNew2.vtk')
  # edz=pv.read(q123Folder+r'/edz.vtk') 


  '''
  # 計算dfn與處置設施交結三角形
  '''
  # case=1
  if (case==1):
    innerOBJ=pyFabSTL.mesh_arrangement(dfnOrUsg,dh,edz,dt,mt) 
  else:
    innerOBJ=pv.read("innerOBJ.vtk")


  '''
  # 辨別innerOBJ三角形,哪些屬於dfn
  '''
  # case=1
  if (case==1):
    innerMegered=pyFabSTL.find_closestCell(innerOBJ,dfnOrUsg,dh,edz,dt,mt) 
  else:
    innerMegered=pv.read('innerMegered.vtk')  
  '''
  # labels ==1 代表裂隙, containing_cell代表裂隙編號 
  # mask01 = innerMegered['containing_cell'] !=-1
  # mask02 = innerMegered['labels'] ==1 
  '''

  coormax=np.max(dfnOrUsg.points,axis=0)
  coormin=np.min(dfnOrUsg.points,axis=0)
  domainCenter=(coormax+coormin)/2
  domainCenter[2]=-500.0
  # print(domainCenter)
  # print("=====")

  
  smallDomain=createSmallDomain(domainCenter,2000,2000,500)
  faces=smallDomain.faces

  smallDomainTop=[22]
  smallDomainBot=[1]
  smallDomainProfile=np.linspace(9,20,num=20-9+1,endpoint=True,dtype=np.int32)
  # print("smallDomainProfile=",smallDomainProfile)
  
  if (case==1):
    mesh,ini_points,ini_cells,ini_eachNoFace,ini_cellsMark,ini_mtr=pyFabSTL.runTetgen_coarsening(innerMegered,smallDomain,sp,dfnsp,max_angle,min_area,addRegion)
    meshUSG,meshUSGPoly=pyFabSTL.createPvUSG_TETRA(mesh)  
    # print(meshUSG)  
    cellsNb=np.linspace(0,len(mesh.elements),endpoint=False,num=len(mesh.elements),dtype=np.int32)
    meshUSG['cellsNb']=cellsNb
    meshUSG['element_attributes']=np.array(mesh.element_attributes)
    

    '''
    # meshUSG_clipped['cellsNb']代表edz的網格編號
    '''

    meshUSG.save('meshUSG_coarsening.vtk')
    np.savez('meshUSG_coarsening', 
    points=np.array(mesh.points), 
    elements=np.array(mesh.elements), element_attributes=np.array(mesh.element_attributes),
    faces=np.array(mesh.faces),
    face_markers=np.array(mesh.face_markers),
    ini_points=ini_points,ini_cells=np.array(ini_cells),ini_eachNoFace=np.array(ini_eachNoFace),ini_cellsMark=np.array(ini_cellsMark),ini_mtr=np.array(ini_mtr))
    print("save meshUSG_coarsening OK")
    # quit()

    '''
    with open('merge2dFaces.dat','w',encoding = 'utf-8') as f:
      f.write('Title="XY2D_plot"\n')
      f.write('Variables="x(m)","y(m)","z(m)","label"\n')
      f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.faces))+',F=feblock,et=triangle\n')
      f.write('varlocation=([4]=cellcentered)\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][0])+'\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][1])+'\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][2])+'\n')
      for i in range(0,len(mesh.faces)):
        f.write(str(mesh.face_markers[i])+'\n')
      for i in range(0,len(mesh.faces)):
        f.write(str(mesh.faces[i][0]+1)+' '+str(mesh.faces[i][1]+1)+' '+str(mesh.faces[i][2]+1)+'\n')
      f.close()
    with open('merge2dElements.dat','w',encoding = 'utf-8') as f:
      f.write('Title="XY2D_plot"\n')
      f.write('Variables="x(m)","y(m)","z(m)","label"\n')
      f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
      f.write('varlocation=([4]=cellcentered)\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][0])+'\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][1])+'\n')
      for i in range(0,len(mesh.points)):
        f.write(str(mesh.points[i][2])+'\n')
      for i in range(0,len(mesh.elements)):
        f.write(str(mesh.element_attributes[i])+'\n')
      for i in range(0,len(mesh.elements)):
        f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
      f.close()
    '''

  meshUSG_coarsening = np.load('meshUSG_coarsening.npz')
  meshUSG_coarseningVTK=pv.read('meshUSG_coarsening.vtk')

  # case=0
  if (case==1):
    print("start refine mesh")
    mesh=pyFabSTL.createMeshRefines(meshUSG_coarsening,meshUSG_coarseningVTK,edz,regionTag,addRegion)
    meshRefine_TETRA,meshRefine_TETRAPoly=pyFabSTL.createPvUSG_TETRA(mesh)
    meshRefine_TETRAPoly['face_markers']=mesh.face_markers
    meshRefine_TETRA['cellsNb']=np.linspace(0,len(mesh.elements),endpoint=False,num=len(mesh.elements))
    meshRefine_TETRA['element_attributes']=np.array(mesh.element_attributes)
    meshRefine_triangle=pyFabSTL.createPvUSG_TRIANGLE(mesh)
    meshRefine_triangle['face_markers']=np.array(mesh.face_markers)
    meshRefine_TETRA.save('meshRefine_TETRA.vtk')
    meshRefine_triangle.save('meshRefine_triangle.vtk')
    meshRefine_TETRAPoly.save('meshRefine_TETRAPoly.vtk')
  else:
    meshRefine_TETRA=pv.read('meshRefine_TETRA.vtk')
    meshRefine_triangle=pv.read('meshRefine_triangle.vtk')
    meshRefine_TETRAPoly=pv.read('meshRefine_TETRAPoly.vtk')

  

  # case=1
  if (case==1):
    # smallDomainTop=[22]
    # smallDomainBot=[1]
    ''' 
    # 搜尋上邊界node編號
    '''
    faces=meshRefine_triangle.cells.reshape(-1,4)[:,1:4]
    topBcNodeNb=[]
    botBcNodeNb=[]
    profileBcNodeNb=[]

    for i in range(0,len(smallDomainTop)):
      partMesh=meshRefine_triangle['face_markers']==-(smallDomainTop[i]+1)  
      ind=list(zip(*np.where(partMesh==True)))
      for j in range(0,len(ind)):
        for k in range(0,3):
          topBcNodeNb.append(faces[ind[j][0]][k])
    topBcNodeNb=np.unique(np.array(topBcNodeNb))
    # print('topBcNodeNb=',topBcNodeNb)

    ''' 
    # 搜尋下邊界node編號
    '''
    for i in range(0,len(smallDomainBot)):
      partMesh=meshRefine_triangle['face_markers']==-(smallDomainBot[i]+1)  
      ind=list(zip(*np.where(partMesh==True)))
      for j in range(0,len(ind)):
        for k in range(0,3):
          botBcNodeNb.append(faces[ind[j][0]][k])
    botBcNodeNb=np.unique(np.array(botBcNodeNb))
    # print('botBcNodeNb=',botBcNodeNb)

    ''' 
    # 搜尋側邊邊界node編號
    '''
    # 
    for i in range(0,len(smallDomainProfile)):
      partMesh=meshRefine_triangle['face_markers']==-(smallDomainProfile[i]+1)  
      ind=list(zip(*np.where(partMesh==True)))
      for j in range(0,len(ind)):
        for k in range(0,3):
          profileBcNodeNb.append(faces[ind[j][0]][k])
    profileBcNodeNb=np.unique(np.array(profileBcNodeNb))
    # print('profileBcNodeNb=',profileBcNodeNb)

  

  # case=1
  if (case==1):
    hybridFlow.writeHybridFlowIn(meshRefine_TETRA,meshRefine_triangle,topBcNodeNb,botBcNodeNb,addRegion,regionTag,Krock,poro_rock,poro_fracture)
  

  # case=1
  if (case==1):
    hybridFlow.runDualDomain_flowFortanProgram('DualDomain_flow')

  meger3dVtkF='meger3dTH.vtk'
  # case=1
  if (case==1):
    meger3dTH=hybridFlow.readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meshRefine_TETRA,meger3dVtkF)
    print('array_names=',meshRefine_TETRA.array_names)
  else:
    meger3dTH=pv.read('meger3dTH.vtk')
  print("read meger3dTH")

  '''
  # 讀取完全截切節點
  # 各節點差值獲得速度，最大速度位置釋放質點
  '''
  case=0
  if (case==1):
    # q123Folder=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
    noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123=hybridFlow.readConnSTL(q123Folder)
    Q1PT,Q2PT,Q3PT=hybridFlow.FindMaxFluxNode(q123Folder,noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123,meger3dTH,poro_fracture)
    Q1PT.save('Q1PT.vtk')
    Q2PT.save('Q2PT.vtk')
    Q3PT.save('Q3PT.vtk')
  else:
    Q1PT=pv.read('Q1PT.vtk')
    Q2PT=pv.read('Q2PT.vtk')
    Q3PT=pv.read('Q3PT.vtk')
  print('Q1PT=',Q1PT)

  '''
  # 測試PT 必定需要修改的程式
  '''
  case=0
  if (case==1):
    points=Q1PT.points
    q1EndPoint=np.zeros(3,dtype=np.float64)
    max_steps=5000
    Q1_streamlines =meger3dTH.streamlines_from_source(Q1PT, vectors='vectors', integrator_type=45, integration_direction='forward', surface_streamlines=False, initial_step_length=0.5, step_unit='cl', min_step_length=0.01, max_step_length=1.0, max_steps=max_steps, terminal_speed=1e-12, max_error=1e-06, max_time=None, compute_vorticity=True, rotation_scale=1.0, interpolator_type='point', progress_bar=False)
    Q1_streamlines.save('Q1_streamlines.vtk')
    print('streamlines Q1 OK')
    Q2_streamlines =meger3dTH.streamlines_from_source(Q2PT, vectors='vectors', integrator_type=45, integration_direction='forward', surface_streamlines=False, initial_step_length=0.5, step_unit='cl', min_step_length=0.01, max_step_length=1.0, max_steps=max_steps, terminal_speed=1e-12, max_error=1e-06, max_time=None, compute_vorticity=True, rotation_scale=1.0, interpolator_type='point', progress_bar=False)
    Q2_streamlines.save('Q2_streamlines.vtk')
    print('streamlines Q2 OK')
    Q3_streamlines =meger3dTH.streamlines_from_source(Q3PT, vectors='vectors', integrator_type=45, integration_direction='forward', surface_streamlines=False, initial_step_length=0.5, step_unit='cl', min_step_length=0.01, max_step_length=1.0, max_steps=max_steps, terminal_speed=1e-12, max_error=1e-06, max_time=None, compute_vorticity=True, rotation_scale=1.0, interpolator_type='point', progress_bar=False)
    Q3_streamlines.save('Q3_streamlines.vtk')
    print('streamlines Q3 OK')
    
    # for i in range(111,len(points)):
    #   print('len,i=',len(points),i)
    #   q1EndPoint[:]=points[i]
    #   totalTime=0.0
    #   q1EndPoint[:]=[-357.48123, 717.2025, -557.2381 ]
    #   totalTime=455.84603711814725
    #   q1EndPoint[:]=[472.23727,1189.9667,-561.3746 ]
    #   totalTime=6218.118711
    #   ptPoly=hybridFlow.PtOne(q1EndPoint,meger3dTH,meshRefine_TETRAPoly,poro_rock,totalTime)
    #   ptPoly.save('Q1res_PtNb'+str(i)+'.vtk')
  else:
    Q1_streamlines=pv.read('Q1_streamlines.vtk')
    Q2_streamlines=pv.read('Q2_streamlines.vtk')
    Q3_streamlines=pv.read('Q3_streamlines.vtk')
    
  # print('Q1_streamlines=',Q1_streamlines)
  # print(Q1_streamlines.points)
  # print(Q1_streamlines.lines)

  '''
  # PT streamlines資料處理
  # 判斷PT是否在innerMegered,獲得網格孔隙率
  # 計算真實流速與PT線段時間
  '''
  case=0
  '''Q1 PT'''
  if (case==1):
    # Q1
    vertices,lineNb=meshTool.streamlinesToList(Q1_streamlines)
    # cal=0
    # for ind in lineNb:
    #   if len(lineNb[ind])>max_steps: 
    #     cal=cal+1
    #     # print('ind=',ind)
    # print('max_steps,cal=',max_steps,cal)
    # quit()
    Q1ptTime,Q1ptDist=meshTool.ptTravelTime(Q1_streamlines,vertices,lineNb,innerMegered,poro_fracture,poro_rock[0],'Q1')

    # Q2
    vertices,lineNb=meshTool.streamlinesToList(Q2_streamlines)
    Q2ptTime,Q2ptDist=meshTool.ptTravelTime(Q2_streamlines,vertices,lineNb,innerMegered,poro_fracture,poro_rock[0],'Q2')

    # Q3
    vertices,lineNb=meshTool.streamlinesToList(Q3_streamlines)
    Q3ptTime,Q3ptDist=meshTool.ptTravelTime(Q3_streamlines,vertices,lineNb,innerMegered,poro_fracture,poro_rock[0],'Q3')

    '''
    # labels ==1 代表裂隙, containing_cell代表裂隙編號 
    # mask01 = innerMegered['containing_cell'] !=-1
    # mask02 = innerMegered['labels'] ==1 
    '''
  else:
    # Opening JSON file
    with open('Q1'+'ptTime.json') as json_file:
      Q1ptTime = json.load(json_file)
    with open('Q1'+'ptDist.json') as json_file:
      Q1ptDist = json.load(json_file)
    # Opening JSON file
    with open('Q2'+'ptTime.json') as json_file:
      Q2ptTime = json.load(json_file)
    with open('Q2'+'ptDist.json') as json_file:
      Q2ptDist = json.load(json_file)
    # Opening JSON file
    with open('Q3'+'ptTime.json') as json_file:
      Q3ptTime = json.load(json_file)
    with open('Q3'+'ptDist.json') as json_file:
      Q3ptDist = json.load(json_file)
    print('len(ptTime)=',len(Q1ptTime))
    print('len(ptDist)=',len(Q1ptDist))
  
  minxyz=np.min(meger3dTH.points,axis=0)
  maxxyz=np.max(meger3dTH.points,axis=0)
  print('minxyz=',minxyz)
  print('maxxyz=',maxxyz)
  outBound=[[minxyz[0],(minxyz[0]+maxxyz[0])/2.0],[(minxyz[1]+maxxyz[1])/2.0,maxxyz[1]],[minxyz[2]-25.0,minxyz[2]+60.0]]
  print('outBound=',outBound)

  case=1
  if (case==1):
    Q1numpyPT,Q1vertices,Q1lineNb,Q1ptTime,Q1ptDist,Q1deadEnd=meshTool.ptCDF(outBound,Q1_streamlines,Q1ptTime,Q1ptDist,'Q1')
    Q2numpyPT,Q2vertices,Q2lineNb,Q2ptTime,Q2ptDist,Q2deadEnd=meshTool.ptCDF(outBound,Q2_streamlines,Q2ptTime,Q2ptDist,'Q2')
    Q3numpyPT,Q3vertices,Q3lineNb,Q3ptTime,Q3ptDist,Q3deadEnd=meshTool.ptCDF(outBound,Q3_streamlines,Q3ptTime,Q3ptDist,'Q3')

    AF_CFD=1
    if (AF_CFD==1):
      meshTool.plotAllCDF(Q1numpyPT,Q2numpyPT,Q3numpyPT)

      parts=[0.56]
      meshTool.plotTecplotByCDF(Q1numpyPT,Q1vertices,Q1lineNb,Q1ptTime,Q1ptDist,Q1deadEnd,'Q1',parts)
      parts=[0.6]
      meshTool.plotTecplotByCDF(Q2numpyPT,Q2vertices,Q2lineNb,Q2ptTime,Q2ptDist,Q2deadEnd,'Q2',parts)
      parts=[0.5]
      meshTool.plotTecplotByCDF(Q3numpyPT,Q3vertices,Q3lineNb,Q3ptTime,Q3ptDist,Q3deadEnd,'Q3',parts)
    




  end = time.time()


  # 輸出結果
  print("執行時間：%f 秒" % (end - start))
  quit()

  

  def program():       
    case=0
    if (case==1):
        '''
        # 讀取500m DEM
        '''
        CsvFile=r'/root/ihsienlee/INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
        xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
        xyzCsv = xyzCsv.fillna('')
        xyzCsv.columns=["x", "y", "z"]   
        # print(xyzCsv) 
        '''
        # 讀取模擬範圍，並且依據dem計算表面高程
        '''
        nx=201
        ny=139
        domainFile=r'/root/ihsienlee/INER_DATA/Domain2D/domainboundary_large.xy'
        xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
        xyzDomain = xyzDomain.fillna('')
        xyzDomain.columns=["x", "y"]
        domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
        domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
    # quit()
    domain2d=pv.read('domain2d.vtk')
    demClip2d=pv.read('demClip2d.vtk')
    largeDem=pv.read('largeDem.vtk')
    # print(largeDem)
    # quit()

      

    '''# noTestFrac=10 代表局部裂隙，測試3D流場'''    
    noTestFrac=8000
    case=1
    if (case==1):
        dfnPoly=pyFabSTL.fabRead(noTestFrac)       
    else:
        dfnPoly=pv.read('dfnPoly.vtk')

    # testPLOTfilename=r'dfnPoly.dat'
    # z=np.zeros(len(dfnPoly.points))
    # pyToTecplot.tecplotPyvistaUSG_z(testPLOTfilename,dfnPoly.points,dfnPoly.faces.reshape(-1,4),z)



    case=1
    if (case==1):   
        '''
        # 讀取F1, F2 STL FIles，並利用top，建立網格
        '''
        StlFile=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
        fault_1=pv.read(StlFile)
        StlFile=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
        fault_2=pv.read(StlFile)
        demClip3dsurf,fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,EpsgIn,EpsgOut,fault_1,fault_2)
    else:
        demClip3dsurf=pv.read('demClip3dsurf.vtk')
        fault12=pv.read('fault12.vtk')
    # print("fault12")
    # print(fault12)
    # plotter = pv.Plotter(off_screen=True)
    # plotter.add_mesh(fault12, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)
    # plotter.export_gltf('./fault12.gltf', rotate_scene=False,save_normals=True)
    # gltf = GLTF.load('./fault12.gltf', load_file_resources=True)
    # gltf.export('fault12.glb') 
    # os.system("rm fault12.gltf")  
    # print(np.mean(fault12.points,axis=0))      
    # quit()


    '''水文參數設定'''
    Kfracture=5.0e-5
    poro_fracture=0.4
    aperture=5.0e-6
    alpha=0.008
    sa=3.2
    density=1.0*(1.0+alpha*sa)
    Krock=1.0e-10
    poro_rock=5.4e-3 
    matrix_alpha_w=4.8
    Ksurface=1.0e-5
    poro_surface=5.4e-3 
    surface_alpha_w=4.8
    F1F2K=5.0e-6
    poro_F1F2K=0.4
    F1F2K_alpha_w=1.0    

    meger3dVtkF='meger3dTH.vtk'
    meger3dNonDensityVtkF='meger3dTH_nonDenstiy.vtk'

    meshKey=1
    if (meshKey==1):
        '''
        # case=1 #會重新製作poly files
        '''
        case=2
        if (case==1 or case==2):
            '''
            # 設定表土層與底部表面，耦合f1f2
            '''
            meger3d,megerFace=plotPyvista.surToTetgen(case,depthSurfaceSoil,demClip3dsurf,fault12,dfnPoly)
            # quit()
        
        topPoly3d=pv.read('topPoly3d.vtk')
        profilePoly3d=pv.read('profilePoly3d.vtk')
        meger3d=pv.read('megerTetra3d.vtk')
        megerFace=pv.read('megerTri2d.vtk')

        case=1
        if (case==1):
            '''
            # 搜尋上邊界node編號
            '''
            meger3dClipTop=plotPyvista.findMegerTopBc(meger3d,topPoly3d)
        meger3dClipTop=pv.read('meger3dClipTop.vtk')
        # print("meger3dClipTop.array_names")
        # print(meger3dClipTop.array_names)
        # quit()
    

        case=1
        if (case==1):
            '''
            # 搜尋側邊界node編號
            '''
            radius=500.0
            profile2dBCPoly=plotPyvista.findMegerProfileBc(megerFace,largeDem,radius)
        bcProfileFace=pv.read('bcProfileFace.vtk')
        # print("bcProfileFace.array_names")
        # print(bcProfileFace.array_names)   

        case=1
        if (case==1):
            '''
            # 產生複合域水流輸入檔
            '''
            hybridFlow.writeInputDensity(density,meger3d,megerFace,meger3dClipTop,bcProfileFace,Kfracture,poro_fracture,Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K) 
        
        case=1
        if (case==1):
            hybridFlow.runDualDomain_flowFortanProgram('DualDomain_flow')


        # meger3dVtkF='meger3dTH.vtk'
        case=1
        if (case==1):
            meger3dTH=hybridFlow.readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meger3d,meger3dVtkF)
            # print(meger3d.array_names)
        
        # meger3dNonDensityVtkF='meger3dTH_nonDenstiy.vtk'
        case=1
        if (case==1):
            os.system('cp FEM_AGMG_BC1_inf_nonDensity.dat FEM_AGMG_BC1_inf.dat')
            hybridFlow.runDualDomain_flowFortanProgram('DualDomain_flow')   
            meger3dT_nonDenstiy=hybridFlow.readFlowTH(delta_x,delta_y,EpsgIn,EpsgOut,meger3d,meger3dNonDensityVtkF)       

        
    meger3dTH=pv.read(meger3dVtkF)
    meger3dTH_nonDensity=pv.read(meger3dNonDensityVtkF)
    case=1
    if (case==1):
        radius=500.0        
        meger3dTH=hybridFlow.Density2Con(meger3dVtkF,meger3dTH,meger3dTH_nonDensity,alpha,largeDem,radius)
    else:
        meger3dTH=pv.read(meger3dVtkF)
    print("con OK")
    quit()

    case=1
    if (case==1):
        q123Folder=r'/root/ihsienlee/CGAL/CGAL_LEE/HybridTW/001_stlToMesh'
        hybridFlow.particleTracking(noTestFrac,delta_x,delta_y,EpsgIn,EpsgOut,q123Folder,meger3dTH,dfnPoly,Kfracture,poro_fracture,aperture,Krock,poro_rock,matrix_alpha_w,Ksurface,poro_surface,surface_alpha_w,F1F2K,poro_F1F2K,F1F2K_alpha_w)




    print("Program END")

    '''
    # 測試PT 必定需要修改的程式
    '''
    case=0
    if (case==1):
        q1Stream=pv.read('Q1_stream.vtk')
        # print(q1Stream)
        # print(np.mean(q1Stream.points,axis=0))
        # plotter = pv.Plotter(off_screen=True)
        # plotter.add_mesh(q1Stream, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)
        # plotter.export_gltf('./q1Stream.gltf', rotate_scene=False,save_normals=True)
        # gltf = GLTF.load('./q1Stream.gltf', load_file_resources=True)
        # gltf.export('q1Stream.glb') 
        # os.system("rm q1Stream.gltf")
        # quit()

        meger3dTH=pv.read('meger3dTH.vtk')
        length=100.0

        
        nb=0
        q1EndPoint=q1Stream.points[nb]
        q1EndPoint=[554.97534,690.52313,-502.18243]
        cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
        q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
        q1EndCub.save('q1EndCub.vtk')
        quit()

        tryTime=0
        print("tryTime,q1EndPoint=",tryTime,q1EndPoint)
        while True:               
            if (tryTime>100):
                tryTime=tryTime+1         
                stream, src = meger3dTH.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                nb=len(stream.points)-1
                q1EndPoint=stream.points[nb]
                print("tryTime,q1EndPoint=",tryTime,q1EndPoint)
                if (tryTime==1):
                    total_stream=stream
                else:
                    total_stream=total_stream+stream
                    total_stream.save('total_stream.vtk')                
                if(q1EndPoint[2]>100.0):
                    break
                else:
                    oldtryTime=tryTime
                    while (tryTime-oldtryTime)<20:
                        tryTime=tryTime+1
                        cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
                        q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
                        stream, src = q1EndCub.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                        nb=len(stream.points)-1
                        q1EndPoint=stream.points[nb]
                        print("tryTime,Local q1EndPoint=",tryTime,q1EndPoint)
                        if (tryTime==1):
                            total_stream=stream
                        else:
                            total_stream=total_stream+stream
                            total_stream.save('total_stream.vtk')
                        if(q1EndPoint[2]>100.0):
                            break
            else:
                tryTime=tryTime+1
                cube=pv.Cube(center=(q1EndPoint), x_length=length, y_length=length, z_length=length, bounds=None)
                q1EndCub=meger3dTH.clip_surface(cube,invert=True,compute_distance = True)
                stream, src = q1EndCub.streamlines('vectors', return_source=True,terminal_speed=0.0, n_points=1,source_radius=0.0,source_center=(q1EndPoint),integration_direction ='forward',interpolator_type='point')
                nb=len(stream.points)-1
                q1EndPoint=stream.points[nb]
                print("tryTime,Local q1EndPoint=",tryTime,q1EndPoint)
                if (tryTime==1):
                    total_stream=stream
                else:
                    total_stream=total_stream+stream
                    total_stream.save('total_stream.vtk')
                if(q1EndPoint[2]>100.0):
                    break
        # plotter = pv.Plotter(off_screen=True)
        # plotter.add_mesh(stream, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
        # plotter.show(screenshot='stream.png')
        
        print("stream.points")
        print(stream.points)
        quit()



  def print2016Dfn():
    '''
    print('10610 '+str(innerOBJ.points[8087][0])+' '+str(innerOBJ.points[8087][1])+' '+str(innerOBJ.points[8087][2]))
    print('10611 '+str(innerOBJ.points[8088][0])+' '+str(innerOBJ.points[8088][1])+' '+str(innerOBJ.points[8088][2]))
    print('10612 '+str(innerOBJ.points[8089][0])+' '+str(innerOBJ.points[8089][1])+' '+str(innerOBJ.points[8089][2]))
    print('10613 '+str(innerOBJ.points[8090][0])+' '+str(innerOBJ.points[8090][1])+' '+str(innerOBJ.points[8090][2]))
    print('10614 '+str(innerOBJ.points[10544][0])+' '+str(innerOBJ.points[10544][1])+' '+str(innerOBJ.points[10544][2]))
    print('10615 '+str(innerOBJ.points[10545][0])+' '+str(innerOBJ.points[10545][1])+' '+str(innerOBJ.points[10545][2]))
    print('10616 '+str(innerOBJ.points[10546][0])+' '+str(innerOBJ.points[10546][1])+' '+str(innerOBJ.points[10546][2]))
    poly=[
      [10610,10616,10615],
      [10610,10615,10614],
      [10616,10613,10615],
      [10613,10612,10615],
      [10615,10612,10614],
      [10612,10611,10614],
      [6133,6132,10616],
      [6133,10616,10615],
      [10616,6132,10615],
      [6133,10615,10614],
      [10615,6132,10614],
      [6133,10614,9540],
      [10614,6132,9540]]
    for i in range(0,len(poly)):
      print('1  0  0  # '+str(11301+1+i))
      print('3 '+str(poly[i][0])+' '+str(poly[i][1])+' '+str(poly[i][2]))
    # 1  0  0  # 11301
    # 4    10609  10597  10590  10598
    quit()
    '''
 

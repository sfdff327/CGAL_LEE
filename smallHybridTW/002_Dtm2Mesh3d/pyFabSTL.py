import numpy as np
import re
import pyvista as pv
import os
from pathlib import Path
import pickle
from scipy.io import FortranFile

import pyToTecplot
import util
import copy
from meshpy.tet import MeshInfo, build,Options
import vtk

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()


def createEDZ():
  # file=r'output.bin'
  # flowTHPath = Path(file)
  # if flowTHPath.is_file():            
  #   f = FortranFile(file, 'r')
  #   no_node = f.read_ints(dtype=np.int32)[0]
  #   no_ele = f.read_ints(dtype=np.int32)[0]
  #   xyz = f.read_reals(dtype=np.float64).reshape(-1,3)
  #   ele_ind = f.read_reals(dtype=np.int32).reshape(-1,3)-1
  #   labels = f.read_ints(dtype=np.int32)
  dz=10.  #0.3
  tP=np.array([-336.26751709, 1440.24938965, -500.])

  points=np.zeros((424*2,3),dtype=np.float64)
  points[0]=[263.86373901, 1058.2220459 , -500.]
  points[1]=[255.20161438, 1063.21875,    -500.]

  points[2]=[ 501.88040161,  870.46972656, -500.]
  points[3]=[ 242.08292955, 1020.46381642, -500.]
  points[4]=[ 233.38092773, 1025.39139381, -500.]
  points[5]=[ -26.38717842, 1175.45788574, -500.]
  points[6]=[ 499.78442383,  866.83728027, -500.]
  points[7]=[ 239.98741216, 1016.83112193, -500.]
  points[8]=[ 231.28308105, 1021.76000977, -500.]
  points[9]=[ -28.48593903, 1171.82702637, -500.]

  points[10]=[ 481.89370728,  835.82098389, -500.] 
  # points[11]=[ 222.09596188,  985.8153102 , -500.] 
  # points[12]=[ 213.39392885,  990.74283588, -500.] 
  # points[13]=[ -46.37385559, 1140.8092041 , -500.] 
  # points[14]=[ 479.79776001,  832.1885376 , -500.] 
  # points[15]=[ 220.00044068,  982.18260911, -500.] 
  # points[16]=[ 211.29640198,  987.11132812, -500.] 
  # points[17]=[ -48.47261429, 1137.17834473, -500.]
  
  mtV=points[3]-points[0]
  dtV=points[10]-points[2]
  tV=tP-points[0]
  point10=points[10]

  for i in range(1,26):
    nb=2+i*8
    for j in range(0,8):
      # print(nb+j,nb-8+j)
      points[nb+j]=points[nb-8+j]+dtV

  points[10]=point10
  nb=2+26*8
  points[nb+0]=points[2+25*8+5]+mtV
  points[nb+1]=points[2+25*8+6]+mtV
  
  '''
  # zone 2
  '''
  Pnb=4+26*8
  points[0+Pnb]=[-336.26751709, 1440.24938965, -500.]
  points[1+Pnb]=[-344.92965698, 1445.24621582, -500.] 
  points[2+Pnb]=[-159.12539673, 1246.88867188, -500.] 
  points[3+Pnb]=[-375.59960938, 1371.94458008, -500.] 
  points[4+Pnb]=[-384.35381589, 1376.90226348, -500.] 
  points[5+Pnb]=[-600.79968262, 1501.86730957, -500.] 
  points[6+Pnb]=[-161.22416687, 1243.2578125 , -500.] 
  points[7+Pnb]=[-377.69836426, 1368.3137207 , -500.] 
  points[8+Pnb]=[-386.4493331 , 1373.2695689 , -500.] 
  points[9+Pnb]=[-602.89562988, 1498.23486328, -500.] 
  points[10+Pnb]=[-179.12342834, 1212.24658203, -500.]
  
  mtV=points[3+Pnb]-points[0+Pnb]
  dtV=points[10+Pnb]-points[2+Pnb]
  point10=points[10+Pnb]

  for i in range(1,26):
    nb=2+i*8
    for j in range(0,8):
      # print(nb+j,nb-8+j)
      points[nb+j+Pnb]=points[nb-8+j+Pnb]+dtV

  points[10+Pnb]=point10
  nb=2+26*8
  points[nb+0+Pnb]=points[2+25*8+5+Pnb]+mtV
  points[nb+1+Pnb]=points[2+25*8+6+Pnb]+mtV

  points[Pnb*2:Pnb*4,0:2]=points[0:Pnb*2,0:2]
  points[Pnb*2:Pnb*4,2:3]=-500-dz
  
  # for i in range(0,2*(4+26*8)):
  #   print(points[i])

  faces=[]
  ''' zone 1 Right Top'''
  faces.append([4,0,1,4,3])
  for i in range(0,26):
    nb=8*i
    faces.append([8,2+nb,3+nb,4+nb,5+nb,9+nb,8+nb,7+nb,6+nb])
  for i in range(0,25):
    nb=8*i
    faces.append([4,7+nb,8+nb,12+nb,11+nb])
  faces.append([4, 207, 208, 211, 210])  
  
  ''' zone 1 Lift Top'''
  # Pnb=4+26*8
  faces.append([4,0+Pnb,1+Pnb,4+Pnb,3+Pnb])
  for i in range(0,26):
    nb=8*i+Pnb
    faces.append([8,2+nb,3+nb,4+nb,5+nb,9+nb,8+nb,7+nb,6+nb])
  for i in range(0,25):
    nb=8*i+Pnb
    faces.append([4,7+nb,8+nb,12+nb,11+nb])
  faces.append([4, 207+Pnb, 208+Pnb, 211+Pnb, 210+Pnb]) 

  ''' zone 1 Right down'''
  Pnb2=(4+26*8)*2
  faces.append([4,0+Pnb2,3+Pnb2,4+Pnb2,1+Pnb2])
  for i in range(0,26):
    nb=8*i+Pnb2
    faces.append([8,2+nb,6+nb,7+nb,8+nb,9+nb,5+nb,4+nb,3+nb])
  for i in range(0,25):
    nb=8*i+Pnb2
    faces.append([4,7+nb,11+nb,12+nb,8+nb])
  faces.append([4,207+Pnb2,210+Pnb2, 211+Pnb2, 208+Pnb2])  
 
  ''' zone 1 Lift down'''
  # Pnb=4+26*8
  faces.append([4,0+Pnb+Pnb2,3+Pnb+Pnb2,4+Pnb+Pnb2,1+Pnb+Pnb2])
  for i in range(0,26):
    nb=8*i+Pnb+Pnb2
    faces.append([8,2+nb,6+nb,7+nb,8+nb,9+nb,5+nb,4+nb,3+nb])
  for i in range(0,25):
    nb=8*i+Pnb+Pnb2
    faces.append([4,7+nb,11+nb,12+nb,8+nb])
  faces.append([4,207+Pnb+Pnb2, 210+Pnb+Pnb2, 211+Pnb+Pnb2, 208+Pnb+Pnb2])  

  '''
  # Right profile
  '''
  faces.append([4, 4, 1, 1+Pnb2, 4+Pnb2])
  faces.append([4, 1, 0, 0+Pnb2, 1+Pnb2])
  faces.append([4, 0, 3, 3+Pnb2, 0+Pnb2])
  for i in range(0,26):
    nb=i*8
    faces.append([4, 3+nb, 2+nb, 2+nb+Pnb2, 3+nb+Pnb2])
    faces.append([4, 2+nb, 6+nb, 6+nb+Pnb2, 2+nb+Pnb2])
    faces.append([4, 6+nb, 7+nb, 7+nb+Pnb2, 6+nb+Pnb2])
    faces.append([4, 8+nb, 9+nb, 9+nb+Pnb2, 8+nb+Pnb2])
    faces.append([4, 9+nb, 5+nb, 5+nb+Pnb2, 9+nb+Pnb2])
    faces.append([4, 5+nb, 4+nb, 4+nb+Pnb2, 5+nb+Pnb2])
  for i in range(0,25):
    nb=i*8
    faces.append([4, 7+nb, 11+nb, 11+nb+Pnb2, 7+nb+Pnb2])
    faces.append([4, 12+nb, 8+nb, 8+nb+Pnb2, 12+nb+Pnb2])
  faces.append([4, 211, 208, 208+Pnb2, 211+Pnb2])
  faces.append([4, 210, 211, 211+Pnb2, 210+Pnb2])
  faces.append([4, 207, 210, 210+Pnb2, 207+Pnb2])

  '''
  # Lift profile
  '''
  faces.append([4, 4+Pnb, 1+Pnb, 1+Pnb2+Pnb, 4+Pnb2+Pnb])
  faces.append([4, 1+Pnb, 0+Pnb, 0+Pnb2+Pnb, 1+Pnb2+Pnb])
  faces.append([4, 0+Pnb, 3+Pnb, 3+Pnb2+Pnb, 0+Pnb2+Pnb])
  for i in range(0,26):
    nb=i*8+Pnb
    faces.append([4, 3+nb, 2+nb, 2+nb+Pnb2, 3+nb+Pnb2])
    faces.append([4, 2+nb, 6+nb, 6+nb+Pnb2, 2+nb+Pnb2])
    faces.append([4, 6+nb, 7+nb, 7+nb+Pnb2, 6+nb+Pnb2])
    faces.append([4, 8+nb, 9+nb, 9+nb+Pnb2, 8+nb+Pnb2])
    faces.append([4, 9+nb, 5+nb, 5+nb+Pnb2, 9+nb+Pnb2])
    faces.append([4, 5+nb, 4+nb, 4+nb+Pnb2, 5+nb+Pnb2])
  for i in range(0,25):
    nb=i*8+Pnb
    faces.append([4, 7+nb, 11+nb, 11+nb+Pnb2, 7+nb+Pnb2])
    faces.append([4, 12+nb, 8+nb, 8+nb+Pnb2, 12+nb+Pnb2])
  faces.append([4, 211+Pnb, 208+Pnb, 208+Pnb2+Pnb, 211+Pnb2+Pnb])
  faces.append([4, 210+Pnb, 211+Pnb, 211+Pnb2+Pnb, 210+Pnb2+Pnb])
  faces.append([4, 207+Pnb, 210+Pnb, 210+Pnb2+Pnb, 207+Pnb2+Pnb])

  edzTop=pv.PolyData(points,np.hstack(faces))
  # edzTop=edzTop.scale([1.0, 1.0, 1000.0], inplace=False)

  
  edzTop.save('edzNew.vtk')
  edzTop.save('edzNew.stl')

  # quit()


def fabRead(noTestFrac,delFracNb,TecFile,boolCheck,printInd=False):
  folder='/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes'
  FABResFileCheck = Path(folder+r'/no_frac.out')
  if FABResFileCheck.is_file():
    with open(folder+'/frac_XYZ.out', 'rb') as file:
      frac_XYZ = pickle.load(file)        
      file.close() 
    with open(folder+'/fracID.out', 'rb') as file:
      fracID = pickle.load(file)   
      file.close()     
    with open(folder+'/frac_ele_ind.out', 'rb') as file:
      frac_ele_ind = pickle.load(file) 
      file.close()           
    with open(folder+'/no_frac.out', 'rb') as file:
      ORno_frac = pickle.load(file)
      file.close()
    file=folder+'/frac_K_ap.bin'
    flowResFileCheck = Path(file)
    if (flowResFileCheck.is_file):
      f = FortranFile(file, 'r')
      frac_K = f.read_reals(dtype=np.float64)
      frac_ap = f.read_reals(dtype=np.float64)
      f.close()
    # print(np.max(fracID))
    # quit()

  no_frac=noTestFrac
  cells=[]
  frac_ele_k=[]
  frac_ele_ap=[]
  frac_local_ID=[]
  nodeCheck=np.zeros(len(frac_XYZ),dtype=np.int32)
  if (no_frac==None or no_frac<=1):
    no_frac=ORno_frac

  for i in range(0,len(fracID)):
    # if (fracID[i]<=no_frac and fracID[i] not in delFracNb):
    if (fracID[i]<=no_frac and int(fracID[i]) not in delFracNb):
      if (len(boolCheck)>0):
        addbool=boolCheck[i]
      else:
        addbool=True
      if addbool:
          # print("fracID[i]=",fracID[i])
          cells.append([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]])
          for j in range(0,3):
            nodeCheck[frac_ele_ind[i][j]]=1
          frac_ele_k.append(np.float64(frac_K[fracID[i]]))
          frac_ele_ap.append(np.float64(frac_ap[fracID[i]]))
          frac_local_ID.append(np.int32(fracID[i]))

  no_points=np.sum(nodeCheck)
  points=np.zeros((no_points,3),dtype=np.float64)
  nb=-1
  for i in range(0,len(frac_XYZ)):
    if (nodeCheck[i]==1):
      nb=nb+1
      points[nb]=frac_XYZ[i]
      nodeCheck[i]=nb
  for i in range(0,len(cells)):
    for j in range(1,4):
      cells[i][j]=nodeCheck[cells[i][j]]

  no_cells=len(cells)
  cells=np.array(cells).ravel()             
  dfnOrUsg = pv.PolyData(points,cells) 
  
  dfnOrUsg=dfnOrUsg.compute_normals()
  frac_ele_k=np.array(frac_ele_k)
  frac_ele_ap=np.array(frac_ele_ap)
  dfnOrUsg['frac_ele_k']=frac_ele_k
  dfnOrUsg['frac_ele_ap']=frac_ele_ap    
  dfnOrUsg['fracID']=frac_local_ID  
 
  # plotter = pv.Plotter(off_screen=True)
  # plotter.add_mesh(dfnOrUsg, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=Truelighting=True,reset_camera=True,culling=False,metallic=1.0)   
  # plotter.show(screenshot='dfnOrUsg.png')
  # testPLOTfilename=r'dfnOrUsg.dat'
  # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,dfnOrUsg.points,dfnOrUsg.faces.reshape(-1,4),dfnOrUs['frac_ele_k']) 
  dfnOrUsg.save('dfnOrUsg.vtk')

  # '''
  if (printInd==True):
    dfn_ele_ind=copy.deepcopy(dfnOrUsg.faces.reshape(-1,4))
    with open(TecFile+'.dat','w',encoding = 'utf-8') as f:
      f.write('Title="XY2D_plot"\n')
      f.write('Variables="x(m)","y(m)","z(m)","label"\n')
      f.write('Zone N='+str(dfnOrUsg.n_points)+',E='+str(len(dfnOrUsg.faces.reshape(-1,4)))+',F=feblock,et=triangle\n')
      f.write('varlocation=([4]=cellcentered)\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][0])+'\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][1])+'\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][2])+'\n')
      for i in range(0,len(dfn_ele_ind)):
        f.write(str(dfnOrUsg['fracID'][i])+'\n')
      for i in range(0,len(dfn_ele_ind)):
        f.write(str(dfn_ele_ind[i][1]+1)+' '+str(dfn_ele_ind[i][2]+1)+' '+str(dfn_ele_ind[i][3]+1)+'\n')
      if len(delFracNb)>0:
        nb=0
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):  
            nb=nb+1
        # print("delFracNb=",delFracNb)
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","label"\n')
        f.write('Zone N='+str(len(frac_XYZ))+',E='+str(nb)+',F=feblock,et=triangle\n') 
        f.write('varlocation=([4]=cellcentered)\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][0])+'\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][1])+'\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][2])+'\n')
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):  
            f.write(str(fracID[i])+'\n')
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):
            f.write(str(frac_ele_ind[i][0]+1)+' '+str(frac_ele_ind[i][1]+1)+' '+str(frac_ele_ind[i][2]+1)+'\n')
      f.close() 
  # '''


  '''
  with open('dfnOrUsg.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)"\n')
    f.write('Zone N='+str(dfnOrUsg.n_points)+',E='+str(dfnOrUsg.n_cells)+',F=fepoint,et=triangle\n')
    for i in range(0,dfnOrUsg.n_points):
      f.write(str(dfnOrUsg.points[i][0])+' '+str(dfnOrUsg.points[i][1])+' '+str(dfnOrUsg.points[i][2])+'\n')
    for i in range(0,dfnOrUsg.n_cells):
      f.write(str(frac_ele_ind[i][0]+1)+' '+str(frac_ele_ind[i][1]+1)+' '+str(frac_ele_ind[i][2]+1)+'\n')
    f.close()
  '''


  '''
  dfnOrUsg.save('dfnOrUsg.stl',binary=True)
  outputFileCheck = Path("output.obj")
  if outputFileCheck.is_file():
      os.remove(outputFileCheck) 
  os.system('./mesh_arrangement dfnOrUsg.stl')

  dfnPoly=pv.read("output.obj")
  '''
  # 計算mesh_arrangement之後,屬於DFN triangle
  '''
  centers=dfnPoly.cell_centers()
  labels=dfnOrUsg.find_closest_cell(centers.points)
  dfnPoly['labels']=labels
  # testPLOTfilename=r'dfnPoly.dat'
  # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,dfnPoly['labels'])  
  # dfnPoly.save('dfnPoly.vtk')
  # quit()
  '''
  return dfnOrUsg

def mesh_arrangement(dfnOrUsg,dh,edz,dt,mt):

  case=1
  if (case==1):

    dfnOrUsg.save('dfnOrUsg.stl')
    dh.save('dh.stl')
    edz.save('edz.stl')
    dt.save('dt.stl')
    mt.save('mt.stl')
    outputFileCheck = Path("output.obj")
    if outputFileCheck.is_file():
      os.remove(outputFileCheck) 
    # os.system('./mesh_arrangement dfnOrUsg.stl dh.stl edz.stl dt.stl mt.stl')
    os.system('./mesh_arrangement dfnOrUsg.stl edz.stl')
    # os.system('./mesh_arrangement dfnOrUsg.stl')
    os.system('./OBJread')
    innerOBJ=pv.read("output.obj")
    print("mesh_arrangement OK")

  os.system('./OBJread')
  file=r'output.bin'
  flowTHPath = Path(file)
  if flowTHPath.is_file():            
    f = FortranFile(file, 'r')
    no_node = f.read_ints(dtype=np.int32)[0]
    no_ele = f.read_ints(dtype=np.int32)[0]
    xyz = f.read_reals(dtype=np.float64).reshape(-1,3)
    ele_ind = f.read_reals(dtype=np.int32).reshape(-1,3)-1
    labels = f.read_ints(dtype=np.int32)

  # print("no_node=",no_node)
  # print("no_ele=",no_ele)
  # print("xyz=",xyz)
  # print("type xyz=",type(xyz[0][0]))
  # print("ele_ind=",ele_ind)
  # print("labels=",labels)
  # quit()
  # with open('mesh_arrangement.dat','w',encoding = 'utf-8') as f:
  #   f.write('Title="XY2D_plot"\n')
  #   f.write('Variables="x(m)","y(m)","z(m)"\n')
  #   f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEpoint,ET=TRIANGLE\n') 
  #   for i in range(0,no_node):
  #     f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
  #   for i in range(0,no_ele):
  #     f.write(str(ele_ind[i][0]+1)+' '+str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+'\n')
  #   f.close()


  '''
  nb=-1
  duplicatePointNb=[]
  for i in range(0,innerOBJ.n_points):
    if (i not in duplicatePointNb):
      nb=nb+1
      pt=innerOBJ.points[i]
      index=innerOBJ.find_closest_point(pt, n=20)
      replaceNB=[]
      for j in range(0,len(index)):
        # print(pt)
        # print(innerOBJ.points[index[j]])
        # print(i,index[j],dis3d(pt,innerOBJ.points[index[j]]))
        if (dis3d(pt,innerOBJ.points[index[j]])<1.e-8 and index[j]!=i):
          replaceNB.append(index[j])
      if (len(replaceNB)>0):
        # print('i,len(replaceNB),replaceNB')
        # print(i,len(replaceNB),replaceNB)
        for j in range(0,len(replaceNB)):
          duplicatePointNb.append(replaceNB[j])
          ind=list(zip(*np.where(ele_ind==replaceNB[j])))
          # print("ind=",ind)
          for k in range(0,len(ind)):
            # print(i,ele_ind[ind[k]])
            ele_ind[ind[k]]=nb
            # print(i,ele_ind[ind[k]])
      if (i!=nb):
        ind=list(zip(*np.where(ele_ind==i)))
        for k in range(0,len(ind)):
          ele_ind[ind[k]]=nb
  
  if (len(duplicatePointNb)>0):
    duplicatePointNb=np.unique(duplicatePointNb)
    # print(duplicatePointNb)
    # print(xyz.shape)
    xyz=np.delete(xyz,duplicatePointNb,axis=0)
    # print(xyz.shape)
  '''

  # print("len(ele_ind)=",len(ele_ind))
  ele_ind_new=np.full((len(ele_ind),4),3,dtype=np.int32)
  ele_ind_new[:,1:4]=ele_ind[:,0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  innerOBJ=pv.PolyData(xyz,ele_ind_new)

  innerOBJ['labels']=labels

  innerOBJ.save('innerOBJ.vtk')
  # quit()
  return innerOBJ

def find_closestCell(innerOBJ,dfnOrUsg,dh,edz,dt,mt):
  # cell_normals=innerOBJ.cell_normals
  centers=innerOBJ.cell_centers()
  case=1
  if case==1:
    innerOBJ['closest_cell']=np.zeros(innerOBJ.n_cells,dtype=np.int32)
    index=dfnOrUsg.find_closest_cell(centers.points)
    for i in range(0,len(index)):
      innerOBJ['closest_cell'][i]=dfnOrUsg['fracID'][index[i]]
    print("find_closest_cell OK")

    innerOBJ['containing_cell']=np.zeros(innerOBJ.n_cells,dtype=np.int32)
    index=dfnOrUsg.find_containing_cell(centers.points)
    for i in range(0,len(index)):
      innerOBJ['containing_cell'][i]=dfnOrUsg['fracID'][index[i]]  
    print("find_containing_cell OK")
    # print("np.max(innerOBJ['containing_cell'])=",np.max(innerOBJ['containing_cell']))
    innerOBJ.save('innerOBJ_closest.vtk')
  else:
    innerOBJ=pv.read('innerOBJ_closest.vtk')
  # quit()

  innerMegered=innerOBJ
  innerMegered.save('innerMegered.vtk')
  # print(innerMegered['contain'][startnb])

  return innerMegered

def triangleNormal(tri):
  U=tri[1]-tri[0]
  V=tri[2]-tri[0]
  # print("U=",U)
  # print("V=",V)
  N = np.array([
    U[1]*V[2] - U[2]*V[1],
    U[2]*V[0] - U[0]*V[2],
    U[0]*V[1] - U[1]*V[0]])
  # N=N/np.linalg.norm(N)
  # print("N=",N)
  return N

def point_in_triangle(tri,pt):
  one=1.0
  zero=1.0e-7
  Pru_zero=1.0e-11
  inout=False
  print((tri[0][0]+tri[1][0]+tri[2][0])/3-pt[0])
  area =determinant_value_new_2d(tri[0],tri[1],tri[2])
  area1=determinant_value_new_2d(pt,tri[0],tri[1])
  area2=determinant_value_new_2d(pt,tri[1],tri[2])
  area3=determinant_value_new_2d(pt,tri[2],tri[0])
  print(area,area1,area2,area3)
  print(abs(abs(area)-abs(area1)-abs(area2)-abs(area3)))

  if (abs(abs(area)-abs(area1)-abs(area2)-abs(area3))<zero):
    if (abs(area)>zero): test_area=area
    if (abs(area1)>zero): test_area=test_area*area1
    if (abs(area2)>zero): test_area=test_area*area2
    if (abs(area3)>zero): test_area=test_area*area3
    if (test_area>zero):
      inout=True
    else:
      if (area*area1>=0.0e0 and area*area2>=0.0e0 and area*area3>=0.0e0):
        inout=True
      else:
        disab=dis3d(tri[0],tri[1])
        disad=dis3d(tri[0],pt)
        disbd=dis3d(pt,tri[1])
        if (abs(disab-disad-disbd)<zero):
          inout=True
        else:
          disbc=dis3d(tri[1],tri[2])
          discd=dis3d(pt,tri[2])
          if (abs(disbc-disbd-discd)<zero):
            inout=True
          else:
            disac=dis3d(tri[2],tri[0])
            if (abs(disbc-disbd-discd)<zero):
              inout=True

  return inout

def dis3d(p1,p2):
  dis=np.linalg.norm(p1-p2)
  # dis2=((p1[0]-p2[0])**2.0+(p1[1]-p1[1])**2.0+(p1[2]-p2[2])**2.0)**0.5
  return dis

def determinant_value_new_2d(a,b,c):
  if (abs(a[2])<1.0e-12 and abs(b[2])<1.0e-12 and abs(c[2])<1.0e-12):
    a0=1.0
    b0=1.0
    c0=1.0
    v=(a[0]*b[1]*c0+b[0]*c[1]*a0+c[0]*a[1]*b0)-(c[0]*b[1]*a0+b[0]*a[1]*c0+a[0]*c[1]*b0)
  else:
   v=(a[0]*b[1]*c[2]+b[0]*c[1]*a[2]+c[0]*a[1]*b[2])-(c[0]*b[1]*a[2]+b[0]*a[1]*c[2]+a[0]*c[1]*b[2])
  return v

def writeNode(points,delPointNb,path_to_file):
  with open(path_to_file,'w',encoding = 'utf-8') as f:
    f.write(str(len(points))+'  3  0  0\n')
    nb=-1
    for i in range(0,len(points)):
      if (i not in delPointNb):
        nb=nb+1
        f.write(str(nb)+'  '+str(points[i][0])+'  '+str(points[i][1])+'  '+str(points[i][2])+'\n')
    f.close()

def runTetgen_coarsening(innerMegered,smallDomain,sp,dfnsp,max_angle,min_area,addRegion):
  innerMegered_faces=innerMegered.faces.reshape(-1,4)
  smallDomain_points=smallDomain.points
  smallDomain_cells=smallDomain.faces.reshape(-1,5)

  innerMegeredmax_angle=innerMegered.compute_cell_quality(quality_measure='max_angle')
  isGoodAngle=innerMegeredmax_angle['CellQuality']<max_angle 
  innerMegeredarea=innerMegered.compute_cell_quality(quality_measure='area')
  # isGoodArea=innerMegeredarea['CellQuality']>30.
  isGoodArea=innerMegeredarea['CellQuality']>min_area 
  no_isGoodAngle=int(np.sum(isGoodAngle!=0))
  innerMegeredmax_angle.save('innerMegeredmax_angle.vtk')
  innerMegeredarea.save('innerMegeredarea.vtk')

  tetgenF='merge'

  # '''
  # write test node poly
  points=np.zeros((innerMegered.n_points+smallDomain.n_points,3),dtype=np.float64)
  points[0:innerMegered.n_points,:]=innerMegered.points[0:innerMegered.n_points,:]
  points[innerMegered.n_points:innerMegered.n_points+smallDomain.n_points,:]=smallDomain.points[0:smallDomain.n_points,:]

  with open(tetgenF+'.node','w',encoding = 'utf-8') as f:
    f.write(str(innerMegered.n_points+smallDomain.n_points)+'  3  0  0\n')
    nb=-1
    for i in range(0,innerMegered.n_points):
      nb=nb+1
      f.write(str(nb)+'  '+str(innerMegered.points[i][0])+'  '+str(innerMegered.points[i][1])+'  '+str(innerMegered.points[i][2])+'\n')
    for i in range(0,smallDomain.n_points):
      nb=nb+1
      f.write(str(nb)+'  '+str(smallDomain.points[i][0])+'  '+str(smallDomain.points[i][1])+'  '+str(smallDomain.points[i][2])+'\n')
    f.close()


  cells=[]
  cellsMark=[]
  mtr=np.full(innerMegered.n_points+smallDomain.n_points,sp,dtype=np.float64)  
  # mtr=np.zeros(innerMegered.n_points+smallDomain.n_points,dtype=np.float64)  
  # cells=np.zeros((no_isGoodAngle,3),dtype=np.int32)
  # print(">>>>>>>>>")

  for i in range(0,len(innerMegered['labels'])):
    if (innerMegered['labels'][i]==1):
      if (isGoodAngle[i]==True and isGoodArea[i]==True):
        cells.append(innerMegered_faces[i,1:4].tolist())
        cellsMark.append(innerMegered['containing_cell'][i]+addRegion)
    else:
      # print("innerMegered['labels'][i]=",innerMegered['labels'][i])
      cells.append(innerMegered_faces[i,1:4].tolist())
      cellsMark.append(1)
      # print(cellsMark)


  # dfnsp=sp/50.  #sp/250.0
  print("set dfn sp=",dfnsp)
  for i in range(0,innerMegered.n_cells):
    if (isGoodAngle[i]==True):
      if (innerMegered['labels'][i]==1): #搜尋dfn的points
        for j in range(1,4):
          mtr[innerMegered_faces[i][j]]=dfnsp
      # else:
      #   for j in range(1,4):
      #     mtr[innerMegered_faces[i][j]]=(sp+dfnsp)/2 #dfnsp #(sp+dfnsp)/2


  # with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
  #   f.write(str(len(mtr))+' 1\n')
  #   for i in range(0,len(mtr)):
  #     f.write(str(mtr[i])+'\n')
  #   f.close()        
  with open(tetgenF+'_cells.dat','w',encoding='utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label"\n')
    f.write('Zone N='+str(len(points))+',E='+str(len(cells))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(points)):
      f.write(str(points[i][0])+'\n')
    for i in range(0,len(points)):
      f.write(str(points[i][1])+'\n')
    for i in range(0,len(points)):
      f.write(str(points[i][2])+'\n')
    for i in range(0,len(cells)):
      f.write(str(cellsMark[i])+'\n')
    for i in range(0,len(cells)):
      f.write(str(cells[i][0]+1)+' '+str(cells[i][1]+1)+' '+str(cells[i][2]+1)+'\n')
    f.close()

  print("smallDomain.n_cells=",smallDomain.n_cells)
  for i in range(0,smallDomain.n_cells):
    ind=copy.deepcopy(smallDomain_cells[i])
    for j in range(1,5):
      ind[j]=ind[j]+innerMegered.n_points
    cells.append([ind[1],ind[2],ind[3],ind[4]])
    cellsMark.append(-(i+1))




  # with open(tetgenF+'.poly','w',encoding = 'utf-8') as f:
  #   f.write('0  3  0  0\n')
  #   f.write(str(len(cells))+'  0\n')
  #   nb=-1
  #   for i in range(0,len(cells)):
  #     nb=nb+1
  #     f.write('1  0  0  # '+str(nb)+'\n')
  #     if len(cells[i])==3:
  #       f.write(str(3)+'    '+str(cells[i][0])+'  '+str(cells[i][1])+'  '+str(cells[i][2])+'\n')
  #     elif len(cells[i])==4:
  #       f.write(str(4)+'    '+str(cells[i][0])+'  '+str(cells[i][1])+'  '+str(cells[i][2])+'  '+str(cells[i][3])+'\n')
  #   f.write('0\n')
  #   f.write('0\n')
  #   f.close()

  '''
  mesh_info = MeshInfo()
  # mesh_info.load_node(tetgenF)
  mesh_info.load_poly(tetgenF)
  print("tetgen start")
  mesh = build(mesh_info, options=Options(switches='pAXM'))
  print("points=",np.array(mesh.points))
  print("elements=",np.array(mesh.elements))
  print("element_attributes=",np.array(mesh.element_attributes))
  mesh.write_vtk('mergeDfnEdz.vtk')
  mergeDfnEdz=pv.read('mergeDfnEdz.vtk')
  mergeDfnEdz['element_attributes']=np.array(mesh.element_attributes)
  mergeDfnEdz.save('mergeDfnEdz.vtk')
  quit()
  '''

  runTetgenCheck=0
  while runTetgenCheck==0:
    print("renew mesh_info")
    mesh_info = MeshInfo()
    mesh_info.load_node(tetgenF)
    mesh_info.set_facets(cells,cellsMark)
    # mesh_info.load_mtr(mtr)
    mesh_info.save_poly(tetgenF)
    path_to_file=tetgenF+'Out'
    file_exists = os.path.exists(path_to_file)
    if file_exists:
      os.remove(path_to_file)
    print('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    os.system('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    print("path_to_file=",path_to_file)
    file_exists = os.path.exists(path_to_file)
    print("file_exists=",file_exists)
    if file_exists:
      f = open(path_to_file)
      delNb=[]
      for line in f.readlines():      
        if 'No faces are intersecting.' in line :
          runTetgenCheck=1
          print("Self-intersection OK")
        if (' intersects facet #' in line) or (' duplicates facet #' in line):
          linelist =[int(s) for s in re.findall(r'-?\d+\.?\d*', line)][1]-1
          nb=(linelist)
          if (nb not in delNb):
            delNb.append(nb)
      f.close()  
      print("delNb=",delNb)
      if (len(delNb)>0):
        delNb=np.unique(delNb)
        for i in range(len(delNb)-1,-1,-1):
          del cells[delNb[i]] 
          del cellsMark[delNb[i]] 
        print("len(cells) AF",len(cells))
        # checkPointexist=np.zeros(len(points),dtype=np.int32)
        # delPointNb=[]
        # cellsNumpyhstack= np.hstack(cells)
        # for i in range(0,len(points)):
        #   if (i not in cellsNumpyhstack):
        #     delPointNb.append(i)
        # print("delPointNb=",delPointNb)
        # quit()
      else:
        runTetgenCheck=1
  print("tetgen start")
  # os.system('tetgen -pA '+tetgenF+'.poly')
  # mesh = build(mesh_info, options=Options(switches='pqmAo/150'))
  mesh = build(mesh_info, options=Options(switches='pA'))
  print("MeshCoarsening point=",len(mesh.points))
  print("MeshCoarsening elements=",len(mesh.elements))
  faces=[]
  eachNoFace=[]
  # print("OR cells=",cells)
  for i in range(0,len(cells)):
    faces.append(cells[i])
    if (len(cells[i])==3):
      eachNoFace.append(3)
    elif (len(cells[i])==4):
      eachNoFace.append(4)
  # print(faces)
  cells=np.hstack(faces)
  # print("np cells=",cells[0:30])
  
  return mesh,points,cells,eachNoFace,cellsMark,mtr

def createPvUSG_TETRA(mesh):
  points=np.array(mesh.points)
  elements=np.array(mesh.elements)
  meshfaces=np.array(mesh.faces)
  faces=np.full((len(meshfaces),4),fill_value=3,dtype=np.int32)
  faces[:,1:]=meshfaces[:,:]
  faces=np.hstack(faces)
  # print(faces)
  # for i in range(0,5):
  #   print(faces[i])
  #   print(meshfaces[i])
  # quit()
                     
  cells=[]
  for i in range(0,len(elements)):
    cells.append([4,elements[i][0],elements[i][1],elements[i][2],elements[i][3]])
  
    
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_TETRA
  meshTETRA = pv.UnstructuredGrid(cells, celltypes, points)
  meshTETRAPoly=pv.PolyData(points,faces)
  return meshTETRA,meshTETRAPoly

def createPvUSG_TRIANGLE(mesh):
  points=np.array(mesh.points)
  faces=np.array(mesh.faces)
                     
  cells=np.full((len(faces),4),3,dtype=np.int32)
  cells[:,1:4]=faces[:,0:3]
    
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_TRIANGLE
  meshTRIANGLE = pv.UnstructuredGrid(cells, celltypes, points)
  return meshTRIANGLE
  
def createMeshRefines(meshUSG_coarsening,meshUSG_coarseningVTK,edz,regionTag,addRegion):
# def createMeshRefines(OR_mesh_info,meshUSG_coarsening,meshUSG_coarseningVTK,edz,regionTag,addRegion):
  # print('len(OR_mesh_info.points)=',len(OR_mesh_info.points))
  # print('len(OR_mesh_info.faces)=',len(OR_mesh_info.faces))
  # print("len(meshUSG_coarsening['ini_cells'])=",len(meshUSG_coarsening['ini_cells']))
  # print("len(meshUSG_coarsening['ini_mtr'])=",len(meshUSG_coarsening['ini_mtr']))

  co=meshUSG_coarseningVTK.cell_centers()
  meshUSG_coarsening_clippedEdz=co.clip_surface(edz)
  meshUSG_coarsening_clippedEdz.save('meshUSG_coarsening_clippedEdz.vtk')
  bcCener=co.points[(meshUSG_coarseningVTK.find_closest_cell(np.max(meshUSG_coarseningVTK.points,axis=0)))]
  # print("bcCener=",bcCener)

  points=meshUSG_coarsening['points']
  elements=meshUSG_coarsening['elements']
  faces=meshUSG_coarsening['faces']
  face_markers=meshUSG_coarsening['face_markers']
  ini_points=tuple(map(tuple, meshUSG_coarsening['ini_points']))   
  ini_cells=meshUSG_coarsening['ini_cells']
  ini_eachNoFace=meshUSG_coarsening['ini_eachNoFace']
  ini_cellsMark=meshUSG_coarsening['ini_cellsMark'].tolist()
  ini_mtr=meshUSG_coarsening['ini_mtr'].tolist()

  # print(ini_points[0:3])
  # print(ini_cells[0:10])
  # print(ini_cellsMark[0:3])
  # print(ini_mtr[0:3])
  tetgenF='merge'
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(len(ini_mtr))+' 1\n')
    for i in range(0,len(ini_mtr)):
      f.write(str(ini_mtr[i])+'\n')
    f.close()
  
  '''
  # set mesh regions
  '''
  # print("edz no_cells=",meshUSG_clipped.n_points)
  
  cells=get_cellbyEachNo(ini_cells,ini_eachNoFace)
  mesh_info = MeshInfo()
  # mesh_info.load_node(tetgenF)
  mesh_info.set_points(ini_points)
  mesh_info.set_facets(cells,ini_cellsMark)
  # mesh_info.load_poly(tetgenF)
  mesh_info.load_mtr(tetgenF)

  # regionTag,addRegion
  mesh_info.regions.resize(int(meshUSG_coarsening_clippedEdz.n_points+1))
  mesh_info.regions[0]=[bcCener[0],bcCener[1],bcCener[2],regionTag[0],1e+5]
  for i in range(0,meshUSG_coarsening_clippedEdz.n_points):
    mesh_info.regions[i+1] = [meshUSG_coarsening_clippedEdz.points[i][0],meshUSG_coarsening_clippedEdz.points[i][1],meshUSG_coarsening_clippedEdz.points[i][2],regionTag[1],1e+5,]
  print("set regions OK")

  
  mesh_info.save_poly(tetgenF)
  # quit()
  # mesh = build(mesh_info, options=Options(switches='pqmAO3o/150'))
  mesh = build(mesh_info, options=Options(switches='pqAmnf'))
  # mesh = build(mesh_info, options=Options(switches='pAa'))
  # mesh = build(mesh_info, options=Options(switches='pA'))
  mesh.save_neighbors(tetgenF)
  print("refine mesh OK")
  # quit()  ### refine mesh quitPoint
  
  print("len(mesh.points)=",len(mesh.points))
  print("len(mesh.elements)=",len(mesh.elements))
  with open('mergeRefinesTETRA.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label3D"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.element_attributes[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
    f.close()
  with open('mergeRefinesTriangle.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label2D"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.faces))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.face_markers[i])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.faces[i][0]+1)+' '+str(mesh.faces[i][1]+1)+' '+str(mesh.faces[i][2]+1)+'\n')
    f.close()
  return mesh

def get_cellbyEachNo(cells,eachNoFace):
  faces=[]
  loc=0
  for i in range(0,len(eachNoFace)):
    faces.append(cells[loc:loc+eachNoFace[i]].tolist())
    loc=loc+eachNoFace[i]
  # print("faces=",faces)
  return faces
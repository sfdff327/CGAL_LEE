import copy
import pyvista as pv
# from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt
from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()

def calculateQuad(j,i,nrow,ncol,cells):
    c0=(j  )*ncol+i
    c1=(j  )*ncol+i+1
    c2=(j+1)*ncol+i+1
    c3=(j+1)*ncol+i
    cells.append([4,c0,c1,c2,c3])
    return cells

def demToMesh(nx,ny,demXYZ,domain):

    Csvcoords=[]
    nb=-1
    for j in range(0,ny):
        for i in range(0, nx):
            nb=nb+1
            Csvcoords.append(np.array([demXYZ['x'].values[nb],demXYZ['y'].values[nb],0.0],dtype=np.float64))
    Csvcoords=np.array(Csvcoords)    
    cells=[]
    for j in range(0,ny-1):
        for i in range(0, nx-1):
            cells=calculateQuad(j,i,ny,nx,cells)
    no_cells=len(cells)
    cells=np.array(cells).ravel()
    celltypes = np.empty(no_cells, dtype=np.uint32)
    celltypes[:] = vtk.VTK_QUAD
    grid = pv.UnstructuredGrid(cells, celltypes, Csvcoords)  

    z=demXYZ['z'].values
    grid['z']=z
    # edges = grid.extract_feature_edges()
    # print(edges)
    print('dem OK')
    # grid.save('dem.vtk')    
    # quit()


    # '''
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(grid, cmap='jet',scalars='z', style="surface",color='#000000',show_scalar_bar=False)
    plotter.add_scalar_bar('z',color='#000000', interactive=True, vertical=True,
                           title_font_size=20,
                           label_font_size=20,
                           outline=False,width=0.2)    
    # plotter.add_scalar_bar(title='z(m)', color='#000000')
    # plotter.add_mesh(edges, color='#000000', style="wireframe")
    plotter.show_grid()
    plotter.show_bounds(color='#000000', xlabel='x(m)', ylabel='y(m)', zlabel='z(m)',grid=True,all_edges=True)
    # plotter.camera_position = 'xy'
    plotter.show(screenshot='dem.png')
    grid.save('dem.vtk')
    # '''


    Domaincoords=[]
    domainz=[]
    cells=[]
    cells.append(len(domain))
    domain[['x', 'y']].values
    for i in range(0, len(domain)):
        Domaincoords.append(np.array([domain['x'].values[i],domain['y'].values[i],0.0],dtype=np.float64))
        domainz.append(np.float64(0.0))
        cells.append(i)
    # cells.append(0)
    surf = pv.PolyData(Domaincoords,cells)
    surf['z']=domainz
    # '''
    poly3=copy.deepcopy(surf)    
    plotter = pv.Plotter(off_screen=True)
    # plotter.add_axes(x_color='#000000')
    plotter.add_mesh(surf, color='w', style='points',render_points_as_spheres=True,point_size=10.0)
    plotter.add_mesh(poly3, color='r', style="wireframe",line_width=3.0)
    plotter.add_mesh(grid, cmap='jet',scalars='z', style="surface",show_scalar_bar=True)
    # plotter.add_scalar_bar(title='z',color='#000000',label_font_size=2.0,title_font_size=2.0)    # plotter.add_mesh(edges, color='#000000', style="wireframe",render_lines_as_tubes=True,line_width=3.0)
    plotter.show_grid()
    plotter.show_bounds(color='#000000', xlabel='x(m)', ylabel='y(m)', zlabel='z(m)',grid=True,all_edges=True)
    # plotter.camera_position = 'xy'
    plotter.show(screenshot='domain.png')    
    # quit()
    surf.save('domain.vtk')
    # '''

    clipGrid=grid.clip_surface(surf,invert=False,compute_distance = True)

    # '''
    plotter = pv.Plotter(off_screen=True)
    # plotter.add_axes(x_color='#000000')
    # plotter.add_mesh(poly1, cmap='terrain', clim=[-100, 400], style='surface')
    # plotter.add_mesh(surf, color='b', style='points',render_points_as_spheres=True,point_size=10.0)
    plotter.add_mesh(surf, color='r', style="wireframe")
    plotter.add_mesh(clipGrid, cmap='jet',scalars='z', style="surface",show_scalar_bar=False)
    # plotter.add_mesh(edges, color='#000000', style="wireframe",render_lines_as_tubes=True,line_width=3.0,show_scalar_bar=False)
    plotter.add_scalar_bar('z(m)',color='#000000', interactive=True, vertical=True,
                           title_font_size=20,
                           label_font_size=20,
                           outline=False,width=0.2)      
    plotter.show_grid()
    plotter.show_bounds(color='#000000', xlabel='x(m)', ylabel='y(m)', zlabel='z(m)',grid=True,all_edges=True)
    # plotter.camera_position = 'xy'
    plotter.show(screenshot='clipDtm.png')
    clipGrid.save('clipDtm.vtk')
    print("clipGrid")
    print(clipGrid)
    print(clipGrid.cells)
    # '''
    return surf,clipGrid,grid

# if __name__ == "__main__":
#     # ReadMT3D()
#     # CsvFile=r'xyz_591x_941y.dat'
#     CsvFile=r'/data/INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
#     xyzCsv=readCSV.readCSV(CsvFile)
#     xyzCsv.columns=["x", "y", "z"]
#     # print(xyzCsv)
#     # quit()
#     nx=201
#     ny=139
#     domainFile=r'/data/INER_DATA/Domain2D/domainboundary_large.xy'
#     xyzDomain=readCSV.readCSV(domainFile)
#     xyzDomain.columns=["x", "y"]
#     demToMesh(nx,ny,xyzCsv,xyzDomain)

    

